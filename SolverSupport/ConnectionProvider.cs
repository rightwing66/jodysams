﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using I2ES.Model;

namespace SolverSupport {
    public class ConnectionProvider : IConnectionProvider{
        public ISqlWrapper CreateWrapper() {
            if (ConfigurationManager.ConnectionStrings["ESRDConnectionString"] == null){
                throw new Exception("Conenction String was null.  Did you forget to put a web/app config in the right project?");
            }
            
            var connectionString = ConfigurationManager.ConnectionStrings["ESRDConnectionString"].ConnectionString;
            Debug.Assert(connectionString != null, "ConnectionString named 'ESRDConnectionString' was null");
            SqlConnection connection = new SqlConnection { ConnectionString = connectionString };
            connection.Open();
            var sqlWrapper = new SqlWrapper(connection);
            return sqlWrapper;
        }
    }
}
