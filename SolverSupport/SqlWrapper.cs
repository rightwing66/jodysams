using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using I2ES.Model;

namespace SolverSupport {
    public class SqlWrapper : ISqlWrapper {
        private readonly SqlConnection connection;

   

        public SqlWrapper(SqlConnection connection) {
            this.connection = connection;
        }

        public SqlTransaction Transaction { get; set; }

        public int RunSqlReturnScalar(string sql) {
            try {
                SqlCommand insertCommand = new SqlCommand(sql, connection){Transaction = Transaction};
                var result = insertCommand.ExecuteScalar();
                if (result == null) {
                    throw new Exception("No return from \r\n" + sql);
                }
                int returnScalar = int.Parse(result.ToString());
                insertCommand.Dispose();
                return returnScalar;
            }
            catch (SqlException sEx) {
                throw new Exception(sEx.Message + "\r\n" + sql, sEx);
            }
        }

        public List<IDataRecord> RunReturnIDataRecords(string sql) {
            try {
                using (SqlCommand command = new SqlCommand(sql, connection){Transaction = Transaction}) {
                    var reader = command.ExecuteReader();
                    var rawRecords = reader.Cast<IDataRecord>().ToList();
                    reader.Dispose();
                    return rawRecords;
                }
            }
            catch (SqlException sEx) {
                throw new Exception(sEx.Message + "\r\n" + sql, sEx);
            }
        }

        public SqlDataReader RunReturnReader(string sql) {
            try {
                using (SqlCommand command = new SqlCommand(sql, connection){Transaction = Transaction}) {
                    return command.ExecuteReader();
                }
            }
            catch (SqlException sEx) {
                throw new Exception(sEx.Message + "\r\n" + sql, sEx);
            }
        }


        public void RunSql(string sql) {
            try {
                SqlCommand insertCommand = new SqlCommand(sql, connection){Transaction = Transaction};
                insertCommand.ExecuteNonQuery();
                insertCommand.Dispose();
            }
            catch (SqlException sEx) {
                throw new Exception(sEx.Message + "\r\n" + sql, sEx);
            }
        }

        public void Dispose() {
            connection.Dispose();
        }


        public SqlTransaction BeginTransaction(){
            return connection.BeginTransaction();
        }
    }
}