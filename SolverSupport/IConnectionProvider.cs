using I2ES.Model;

namespace SolverSupport{
    public interface IConnectionProvider{
         ISqlWrapper CreateWrapper();
    }
}