﻿using System;
using System.Configuration;
using System.IO;

namespace SolverSupport {
    public class FileSupport {
        /// <summary>
        /// Looks up 'configKey' in the applicaiton configuration file.  Verifies
        /// that this path exists, and returns, otherwise throws an exception
        /// </summary>
        /// <param name="configKey"></param>
        /// <returns></returns>
        public string VerifyPathFromConfig(string configKey) {
            string path = ConfigurationManager.AppSettings[configKey];
            if (path == null) {
                throw new Exception("The key '" + configKey + "' was not found in configuration.");
            }
            if (!Directory.Exists(path)) {
                throw new Exception("The path '" + path + "' does not exist");
            }
            return path;
        }
    }
}