﻿using System.Linq;

namespace SolverSupport {
    static class CompileExtensions {
        public static string GetConsoleList(this string[] names){
            return names.Aggregate("\r\n", (current, name) => current + (name + "\r\n"));
        }
    }
}
