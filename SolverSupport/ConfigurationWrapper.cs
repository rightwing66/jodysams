﻿using System;
using System.Configuration;

namespace SolverSupport {
    public class ConfigurationWrapper : IConfigurationWrapper {
        public string NodeName { get { return getConfigValue("NodeName"); } }
        public string TrafficManagerUrl { get { return getConfigValue("TrafficManagerURL"); } }
        public string SolverRootDirectory { get { return getConfigValue("SolverRootDirectory"); } }
        public string SolverOutputRootDirectory { get { return getConfigValue("SolverOutputRootDirectory"); } }

        public string I2ESImagePath {
            get { return getConfigValue("I2ESImagePath"); }
        }

        private static string getConfigValue(string keyName) {
            if (ConfigurationManager.AppSettings[keyName] == null) {
                throw new Exception("Could not find configuration entry for '" + keyName + "'");
            }
            return ConfigurationManager.AppSettings[keyName];
        }
    }
}