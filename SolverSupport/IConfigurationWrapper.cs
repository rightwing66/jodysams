﻿namespace SolverSupport{
    public interface IConfigurationWrapper{
        string NodeName { get; }
        string SolverRootDirectory { get; }
        string SolverOutputRootDirectory { get;  }
        string I2ESImagePath { get; }
    }
}