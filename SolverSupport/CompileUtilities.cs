﻿using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace SolverSupport {
    public class CompileUtilities {
        private string callingNameSpace;
        public  string Namespace{
            get { return callingNameSpace; }
            set{
                if (!value.EndsWith(".")){
                   callingNameSpace = value + ".";
                }
                else{
                    callingNameSpace = value;
                }
            }
        }

        public  string GetResourceValue(string resourceName) {
            var names = Assembly.GetCallingAssembly().GetManifestResourceNames();
            var stream = Assembly.GetCallingAssembly().GetManifestResourceStream(Namespace +  resourceName);
            Debug.Assert(stream != null, "Couldn't find manifest assembly named " +Namespace +  resourceName + "\r\nAvailable names are:" + names.GetConsoleList());
            string xmlValue = new StreamReader(stream).ReadToEnd();
            return xmlValue;
        }

    }
}