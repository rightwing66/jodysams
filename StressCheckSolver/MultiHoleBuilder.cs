﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StressCheck;

namespace StressCheckSolver {
    public class MultiHoleBuilder {
        private static readonly log4net.ILog log =
                log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        WorkingData Working;
        StressCheck.Model SCModel;
        StressCheck.Document SCDoc;
        InputData Input;
        bool repairAnalysis;
        bool repairAnalysisExists;
        bool fatigueAnalysis;
        Hashtable typeOfParams;

        public MultiHoleBuilder(StressCheck.Model _SCModel, StressCheck.Document _SCDoc, InputData InputData, string setNameBase, bool isRepairAnalysis, bool repairExists, bool isFatigueAnalysis) {
            SCModel = _SCModel;
            SCDoc = _SCDoc;
            Input = InputData;
            repairAnalysis = isRepairAnalysis;
            repairAnalysisExists = repairExists;
            fatigueAnalysis = isFatigueAnalysis;

            Working = new WorkingData();
            Working.setNameBase = setNameBase;
            Working.bl_multiplier = 1.3;

            if (repairAnalysis) {
                typeOfParams = Input.varsRepair;
            }
            else {
                typeOfParams = Input.varsBP;
            }
        }

        public void CreateRectGeom(double x, double y, double z) {
            Working.X = x;
            Working.Y = y;
            Working.Z = z;

            int[] pointList = new int[3];

            //Create three points and store their ID Numbers
            pointList[0] = SCModel.Points.AddAutoGlobal(Working.X, Working.Y, Working.Z).Number;
            pointList[1] = SCModel.Points.AddAutoGlobal(Working.X + double.Parse(typeOfParams["L"].ToString()), Working.Y, Working.Z).Number;
            pointList[2] = SCModel.Points.AddAutoGlobal(Working.X, Working.Y + double.Parse(typeOfParams["W"].ToString()), Working.Z).Number;

            Working.SCBodyNum = SCModel.Planes.AddAuto3Pt(pointList).Number;
        }

        public List<HoleInformation> CreateFastenerHoles(int numHoles) {

            List<HoleInformation> LoadHoleLocation = new List<HoleInformation>();
            int[] systemStorage = new int[numHoles];

            int[] holeCircleSurfaces = new int[numHoles];
            int[] holeBoundaryLayerCurves = new int[(numHoles) * 9];
            int currBLCurve = 0;

            double z = Working.Z;

            for (int i = 1; i <= numHoles; i++) {

                double x = double.Parse(typeOfParams["dx" + i.ToString()].ToString());
                double y = double.Parse(typeOfParams["dy" + i.ToString()].ToString());
                double r = double.Parse(typeOfParams["do" + i.ToString()].ToString()) / 2;

                //Add a system at the hole center
                int sysNum = SCModel.Systems.AddAutoGlobal(SystemType.stCylindrical, x, y, z).Number;

                //Store system number for loaded holes
                systemStorage[i - 1] = sysNum;

                //Add circle surface for hole
                holeCircleSurfaces[i - 1] = SCModel.CircleSurfaces.AddAutoLocal(sysNum, r).Number;

                //Add circle curve for boundary layer
                int blCurveNum = SCModel.CircleCurves.AddAutoLocal(sysNum, r * Working.bl_multiplier).Number;

                holeBoundaryLayerCurves[currBLCurve] = blCurveNum;
                currBLCurve++;

                //Add 8 radial lines to force the automesh to maintain at least 8 elements around each hole
                int[] ptNums = new int[2];
                ptNums[0] = SCModel.Points.AddAutoGlobal(x, y, z).Number;
                for (int j = 0; j < 8; j++) {
                    ptNums[1] = SCModel.Points.AddAutoOffset(blCurveNum, j * 45 + 22.5, 0).Number;
                    holeBoundaryLayerCurves[currBLCurve] = SCModel.Lines.AddAutoTwoPoint(ptNums).Number;
                    currBLCurve++;
                }

            }

            //Perform Boolean operations to make holes
            Working.SCBodyNum = SCModel.Bodies.AddAutoBooleanSubtract(Working.SCBodyNum, holeCircleSurfaces).Number;

            //Imprint circles for boundary layers
            Working.SCBodyNum = DEFECTcreateImprintBody(holeBoundaryLayerCurves);

            //Pick all the new surfaces and add to a surface set
            int[] newSurfaceNums = new int[numHoles * 8 + 1];

            //To pick the big non-boundary-layer plate surface, pick a location in space just next to
            //the left-most edge of the plate.  There should be no fastener here.
            int obj_num = 0;
            int entity_num = 0;
            double xMidLeft = Working.X + 0.0001;
            double yMidLeft = Working.Y + double.Parse(typeOfParams["W"].ToString()) / 2;
            SCModel.EnableSelection(ObjectPick.opSurface);
            SCModel.PickObject(ObjectPick.opSurface, xMidLeft, yMidLeft, z, ref obj_num, ref entity_num, 0.000001);
            newSurfaceNums[0] = obj_num;

            int currSurfaceNum = 1;
            int lastNumber = -1;
            for (int p = 0; p < numHoles; p++) {
                int i = p + 1;
                double x = double.Parse(typeOfParams["dx" + i.ToString()].ToString());
                double y = double.Parse(typeOfParams["dy" + i.ToString()].ToString());
                double r = double.Parse(typeOfParams["do" + i.ToString()].ToString()) / 2;

                for (int q = 0; q < 8; q++) {
                    double theta = Math.PI / 16 + q * Math.PI / 4;
                    double radius = r * ((Working.bl_multiplier - 1) / 2 + 1);

                    try {
                        SCModel.PickObject(ObjectPick.opSurface, x + radius * Math.Cos(theta), y + radius * Math.Sin(theta), z, ref obj_num, ref entity_num, 0.00001);
                    }
                    catch {
                        //if pick fails rely on the fact that these are created in reverse order.
                        obj_num = lastNumber - 1;
                    }

                    lastNumber = obj_num;
                    newSurfaceNums[currSurfaceNum] = obj_num;
                    currSurfaceNum = currSurfaceNum + 1;
                }
            }

            //Make a set containing all plate surfaces
            SCModel.Sets.Add(Working.setNameBase + "_SURF", SetObjectType.stSurface, SetOptionType.sotList, newSurfaceNums);

            //Make a set containing the plate body
            int[] newBodyNum = new int[1];
            newBodyNum[0] = Working.SCBodyNum;
            SCModel.Sets.Add(Working.setNameBase + "_BODY", SetObjectType.stBody, SetOptionType.sotList, newBodyNum);

            //Pick curve around hole for load application
            //TODO If this becomes a problem it may be worth creating region sets using 

            //Dim scapp As StressCheck.Application
            //Dim locs(2) As Double, box(2) As Double
            //locs(0) = 1
            //locs(1) = 2
            //locs(2) = 3
            //box(0) = box(1) = box(2) = 0.05
            //scapp.Document.Model.Sets.AddRegionSet "TEST", stCurve, srtBox, locs, box

            SCModel.EnableSelection(ObjectPick.opPSurfCurve);
            obj_num = 0;
            entity_num = 0;
            for (int p = 0; p < numHoles; p++) {
                int i = p + 1;
                double x = double.Parse(typeOfParams["dx" + i.ToString()].ToString());
                double y = double.Parse(typeOfParams["dy" + i.ToString()].ToString());
                double radius = double.Parse(typeOfParams["do" + i.ToString()].ToString()) / 2;

                try {
                    SCModel.PickObject(ObjectPick.opPSurfCurve, x + radius, y, z, ref obj_num, ref entity_num, 0.001, PickType.ptCenter);
                }
                catch {
                    //if pick fails rely on the fact that these are created in reverse order.
                    obj_num = obj_num - 8;
                    if (obj_num <= 0)  //First pick failed
                    {
                        //viewed model to find pattern of how items are numbered.  Will work so long as we don't change SC :(
                        obj_num = 17 * numHoles + 1 + 8 * numHoles + 8 * numHoles + 8;
                    }
                }

                HoleInformation tempHoleStorage = new HoleInformation();

                tempHoleStorage.sysNumber = systemStorage[p];
                tempHoleStorage.holeNumber = obj_num;
                tempHoleStorage.holeLoc = "Hole #" + i.ToString();

                LoadHoleLocation.Add(tempHoleStorage);
            }

            return LoadHoleLocation;

        }

        public void CreateMesh() {
            double ratio = 0.3;
            double dh_ratio = 0.114;
            double min_len = 0.001;
            double rate_of_trans = 0.05;

            SCModel.Automeshes.AddAutoMeshSimGlobal(Working.setNameBase + "_BODY", AutomeshType.meshTri, true, true, true, true, true, true, false, ratio, dh_ratio, min_len, rate_of_trans, 0);

            SCModel.Automesh(AutomeshExecute.aeAutomesh);

        }

        public void AssignThickness() {
            Hashtable plateParams = typeOfParams;

            SCModel.ThicknessAssignments.Add(double.Parse(plateParams["th"].ToString()), Working.setNameBase + "_SURF", Assignment.atSet, 0, true);
        }

        public void AssignIsoMaterial(string ColorString) {
            Hashtable materialParams = typeOfParams;

            StressCheck.MATERIALDEF mat = new StressCheck.MATERIALDEF();
            object[] matData = new object[22];

            matData[0] = double.Parse(materialParams["Em"].ToString());
            matData[3] = double.Parse(materialParams["Nu"].ToString());

            mat.Type = MaterialType.matIsotropic;
            mat.Name = Working.setNameBase + "_MAT";
            mat.Case = CaseType.ptPlaneStress;
            mat.NLType = NLMaterialType.nlmatNotApplicable;
            mat.Units = UnitType.utUS;
            mat.Description = Working.setNameBase + " material";
            mat.Data = matData;

            SCModel.Materials.AddRecord(mat);

            SCModel.MaterialAssignments.Add(mat.Name, Working.setNameBase + "_SURF", Assignment.atSet, 0, MaterialAssignType.matHomogeneous, ColorString);

        }

        public void ConvertMapping() {
            int oldres = (int)SCModel.Display.Controls[ControlsType.ctEdgeResolution];
            SCModel.Display.Controls[ControlsType.ctEdgeResolution] = 3;

            SCModel.Update();
            SCModel.ConvertElementMapping(ConvertMappingType.cemGeometric);
            SCModel.Update();

            //Add elemnts to new sets
            MakeElementSet(Working.setNameBase + "_ELEMSET");

        }

        public void AssignRigidBodyConstraints() {
            double x = Working.X;
            double y = Working.Y;
            double z = Working.Z;

            int obj_num = 0;
            int entity_num = 0;
            int[] obj_nums = new int[2];
            SCModel.EnableSelection(ObjectPick.opPoint);
            SCModel.PickObject(ObjectPick.opPoint, x, y, z, ref obj_num, ref entity_num, 0.00001, PickType.ptCenter);
            obj_nums[0] = obj_num;

            SCModel.EnableSelection(ObjectPick.opPoint);
            SCModel.PickObject(ObjectPick.opPoint, x + double.Parse(typeOfParams["L"].ToString()), y, z, ref obj_num, ref entity_num, 0.00001, PickType.ptCenter);
            obj_nums[1] = obj_num;

            SCModel.Constraints.AddRigidBodyConstraint("CONST", ConstraintObject.coPoint, obj_nums);
        }

        public void SetupSolutionID() {
            SCModel.SolutionIDs.Add("COMB", "CONST", "POSITIVECOMB", SolutionStatus.ssActive);
            SCModel.SolutionIDs.Add("N_COMB", "CONST", "NEGATIVECOMB", SolutionStatus.ssActive);
            //SCModel.SolutionIDs.Add("BYP", "CONST", "POSITIVEBYP", SolutionStatus.ssActive);
            //SCModel.SolutionIDs.Add("N_BYP", "CONST", "NEGATIVEBPY", SolutionStatus.ssActive);
            //SCModel.SolutionIDs.Add("BRG", "CONST", "POSITIVEBRG", SolutionStatus.ssActive);
            //SCModel.SolutionIDs.Add("N_BRG", "CONST", "NEGATIVEBRG", SolutionStatus.ssActive);

            SCDoc.Solutions.Clear();

            StressCheck.LINEARSOLUTION LinearSol = new StressCheck.LINEARSOLUTION();

            LinearSol.Name = "1-LINSOL";
            LinearSol.Convergence = ExecConvergence.execUpward;
            LinearSol.Method = ExecMethod.execDirect;
            LinearSol.PLevel = 6;
            LinearSol.PLimit = 8;

            SCDoc.Solutions.AddLinear(LinearSol);
        }

        public void MakeElementSet(string setName) {
            Hashtable plateParams = typeOfParams;

            int obj_num = 0;
            int entity_num = 0;
            double xMidLeft = Working.X + 0.0001;
            double yMidLeft = Working.Y + double.Parse(plateParams["W"].ToString()) / 2;

            SCModel.Display.Elements = true;
            SCModel.EnableSelection(ObjectPick.opElement);
            try {
                SCModel.PickObject(ObjectPick.opElement, xMidLeft, yMidLeft, Working.Z, ref obj_num, ref entity_num, 0.000001);
            }
            catch (Exception e) {
                log.Debug(e.Message);

                const double tol = 0.0001;
                xMidLeft = Working.X + 2 * tol;
                yMidLeft = Working.Y + 2 * tol;
                SCModel.PickObject(ObjectPick.opElement, xMidLeft, yMidLeft, Working.Z, ref obj_num, ref entity_num, tol);
            }

            ushort temp = 0;
            SCModel.SelectSetByObject(SetObjectType.stElement, obj_num, entity_num, ref temp);
            SCModel.Sets.AddSelected(setName, SetObjectType.stElement);
            SCModel.ObjectOperation(ObjectPick.opElement, ActionType.atCancel);
        }

        public void ApplyBoundaryLoad(plateEdgeLabel edge) {
            //Apply Boundary Loads
            int sysNum;
            int curveNum = pickPlateEdge(edge, out sysNum);

            string setName = "CURVE" + curveNum.ToString() + "LOAD";

            SCModel.Sets.Add(setName, SetObjectType.stBoundary, SetOptionType.sotList, curveNum);

            object[] posLoadData = new object[2];
            object[] negLoadData = new object[2];
            posLoadData = edgeLoadData(edge, fatigueAnalysis);
            for (int i = 0; i < 2; i++) {
                negLoadData[i] = (object)("Neg_" + posLoadData[i]);
            }

            LoadValueType[] loadTypes = new LoadValueType[2];
            loadTypes[0] = LoadValueType.lvtFormula;
            loadTypes[1] = LoadValueType.lvtFormula;

            SCModel.Loads.AddBoundaryLoad("POSITIVECOMB", setName, LoadMethod.lmTraction, Direction.dtXYLocal, Assignment.atSet, sysNum, posLoadData, loadTypes);

            SCModel.Loads.AddBoundaryLoad("NEGATIVECOMB", setName, LoadMethod.lmTraction, Direction.dtXYLocal, Assignment.atSet, sysNum, negLoadData, loadTypes);
        }

        private object[] edgeLoadData(plateEdgeLabel edge, bool fatigueAnalysis) {
            object[] returnData = new object[2];
            string form1 = "";
            string form2 = "";

            if (fatigueAnalysis) {
                if (edge == plateEdgeLabel.xPositiveEdge) {
                    returnData[0] = "Fx1";
                    returnData[1] = "Fx1y1";

                    form1 = (double.Parse(Input.fatigueLoad["Fx1"].ToString()) / (double.Parse(typeOfParams["W"].ToString()) * double.Parse(typeOfParams["th"].ToString()))).ToString() + "-" + (double.Parse(Input.fatigueLoad["M1"].ToString()) / ((double)1 / 12 * double.Parse(typeOfParams["th"].ToString()) * Math.Pow(double.Parse(typeOfParams["W"].ToString()), 3))).ToString() + "*Y";

                    form2 = (double.Parse(Input.fatigueLoad["Fx1y1"].ToString()) / (double.Parse(typeOfParams["W"].ToString()) * double.Parse(typeOfParams["th"].ToString()))).ToString();
                }
                else if (edge == plateEdgeLabel.xNegativeEdge) {
                    returnData[0] = "Fx2";
                    returnData[1] = "Fx2y2";

                    form1 = (double.Parse(Input.fatigueLoad["Fx2"].ToString()) / (double.Parse(typeOfParams["W"].ToString()) * double.Parse(typeOfParams["th"].ToString()))).ToString() + "-" + (double.Parse(Input.fatigueLoad["M2"].ToString()) / ((double)1 / 12 * double.Parse(typeOfParams["th"].ToString()) * Math.Pow(double.Parse(typeOfParams["W"].ToString()), 3))).ToString() + "*Y";

                    form2 = "-1.0 * (" + (double.Parse(Input.fatigueLoad["Fx2y2"].ToString()) / (double.Parse(typeOfParams["W"].ToString()) * double.Parse(typeOfParams["th"].ToString()))).ToString() + ")";
                }
                else if (edge == plateEdgeLabel.yNegativeEdge) {
                    returnData[0] = "Fy2";
                    returnData[1] = "Fy2x2";

                    form1 = (double.Parse(Input.fatigueLoad["Fy2"].ToString()) / (double.Parse(typeOfParams["L"].ToString()) * double.Parse(typeOfParams["th"].ToString()))).ToString() + "-" + (double.Parse(Input.fatigueLoad["M4"].ToString()) / ((double)1 / 12 * double.Parse(typeOfParams["th"].ToString()) * Math.Pow(double.Parse(typeOfParams["L"].ToString()), 3))).ToString() + "*Y";

                    form2 = (double.Parse(Input.fatigueLoad["Fy2x2"].ToString()) / (double.Parse(typeOfParams["L"].ToString()) * double.Parse(typeOfParams["th"].ToString()))).ToString();

                }
                else if (edge == plateEdgeLabel.yPositiveEdge) {
                    returnData[0] = "Fy1";
                    returnData[1] = "Fy1x1";

                    form1 = (double.Parse(Input.fatigueLoad["Fy1"].ToString()) / (double.Parse(typeOfParams["L"].ToString()) * double.Parse(typeOfParams["th"].ToString()))).ToString() + "-" + (double.Parse(Input.fatigueLoad["M3"].ToString()) / ((double)1 / 12 * double.Parse(typeOfParams["th"].ToString()) * Math.Pow(double.Parse(typeOfParams["W"].ToString()), 3))).ToString() + "*Y";

                    form2 = "-1.0 * (" + (double.Parse(Input.fatigueLoad["Fy1x1"].ToString()) / (double.Parse(typeOfParams["L"].ToString()) * double.Parse(typeOfParams["th"].ToString()))).ToString() + ")";

                }
            }
            else {
                if (edge == plateEdgeLabel.xPositiveEdge) {
                    returnData[0] = "Fx1";
                    returnData[1] = "Fx1y1";

                    form1 = (double.Parse(Input.staticLoad["Fx1"].ToString()) / (double.Parse(typeOfParams["W"].ToString()) * double.Parse(typeOfParams["th"].ToString()))).ToString() + "-" + (double.Parse(Input.staticLoad["M1"].ToString()) / ((double)1 / 12 * double.Parse(typeOfParams["th"].ToString()) * Math.Pow(double.Parse(typeOfParams["W"].ToString()), 3))).ToString() + "*Y";

                    form2 = (double.Parse(Input.staticLoad["Fx1y1"].ToString()) / (double.Parse(typeOfParams["W"].ToString()) * double.Parse(typeOfParams["th"].ToString()))).ToString();
                }
                else if (edge == plateEdgeLabel.xNegativeEdge) {
                    returnData[0] = "Fx2";
                    returnData[1] = "Fx2y2";

                    form1 = (double.Parse(Input.staticLoad["Fx2"].ToString()) / (double.Parse(typeOfParams["W"].ToString()) * double.Parse(typeOfParams["th"].ToString()))).ToString() + "-" + (double.Parse(Input.staticLoad["M2"].ToString()) / ((double)1 / 12 * double.Parse(typeOfParams["th"].ToString()) * Math.Pow(double.Parse(typeOfParams["W"].ToString()), 3))).ToString() + "*Y";

                    form2 = "-1.0 * (" + (double.Parse(Input.staticLoad["Fx2y2"].ToString()) / (double.Parse(typeOfParams["W"].ToString()) * double.Parse(typeOfParams["th"].ToString()))).ToString() + ")";
                }
                else if (edge == plateEdgeLabel.yNegativeEdge) {
                    returnData[0] = "Fy2";
                    returnData[1] = "Fy2x2";

                    form1 = (double.Parse(Input.staticLoad["Fy2"].ToString()) / (double.Parse(typeOfParams["L"].ToString()) * double.Parse(typeOfParams["th"].ToString()))).ToString() + "-" + (double.Parse(Input.staticLoad["M4"].ToString()) / ((double)1 / 12 * double.Parse(typeOfParams["th"].ToString()) * Math.Pow(double.Parse(typeOfParams["L"].ToString()), 3))).ToString() + "*Y";

                    form2 = (double.Parse(Input.staticLoad["Fy2x2"].ToString()) / (double.Parse(typeOfParams["L"].ToString()) * double.Parse(typeOfParams["th"].ToString()))).ToString();
                }
                else if (edge == plateEdgeLabel.yPositiveEdge) {
                    returnData[0] = "Fy1";
                    returnData[1] = "Fy1x1";

                    form1 = (double.Parse(Input.staticLoad["Fy1"].ToString()) / (double.Parse(typeOfParams["L"].ToString()) * double.Parse(typeOfParams["th"].ToString()))).ToString() + "-" + (double.Parse(Input.staticLoad["M3"].ToString()) / ((double)1 / 12 * double.Parse(typeOfParams["th"].ToString()) * Math.Pow(double.Parse(typeOfParams["W"].ToString()), 3))).ToString() + "*Y";

                    form2 = "-1.0 * (" + (double.Parse(Input.staticLoad["Fy1x1"].ToString()) / (double.Parse(typeOfParams["L"].ToString()) * double.Parse(typeOfParams["th"].ToString()))).ToString() + ")";
                }
            }

            AddFormula((string)returnData[0], form1);
            AddFormula((string)returnData[1], form2);

            AddFormula("Neg_" + returnData[0], "-1.0 * (" + form1 + ")");
            AddFormula("Neg_" + returnData[1], "-1.0 * (" + form2 + ")");

            return returnData;
        }

        private void AddFormula(string name, string fmla) {
            StressCheck.FORMULADEF fstruct = new StressCheck.FORMULADEF();
            fstruct.Name = name;
            fstruct.FormulaExpression = fmla;
            SCModel.Formulae.Add(fstruct);
        }

        public void ApplyHoleLoad(List<HoleInformation> storedHoles, int i) {
            int holeNum = i + 1;

            int curveNum = storedHoles[i].holeNumber;
            string setName = "CURVE" + curveNum.ToString() + "LOAD";
            SCModel.Sets.Add(setName, SetObjectType.stBoundary, SetOptionType.sotList, curveNum);

            object[] posLoadData = new object[2];
            object[] negLoadData = new object[2];
            posLoadData = fastenerLoadData(fatigueAnalysis, holeNum);

            for (int j = 0; j < 2; j++) {
                negLoadData[j] = (object)(-1.0 * (double)posLoadData[j]);
            }

            LoadValueType[] loadTypes = new LoadValueType[2];
            loadTypes[0] = LoadValueType.lvtConstant;
            loadTypes[1] = LoadValueType.lvtConstant;

            SCModel.Loads.AddBoundaryLoad("POSITIVECOMB", setName, LoadMethod.lmBearing, Direction.dtVector, Assignment.atSet, storedHoles[i].sysNumber, posLoadData, loadTypes);

            SCModel.Loads.AddBoundaryLoad("NEGATIVECOMB", setName, LoadMethod.lmBearing, Direction.dtVector, Assignment.atSet, storedHoles[i].sysNumber, negLoadData, loadTypes);
        }

        private object[] fastenerLoadData(bool fatigueAnalysis, int holeNum) {
            object[] returnData = new object[2];

            if (fatigueAnalysis) {
                returnData[0] = double.Parse(Input.fatigueLoad["Px" + holeNum].ToString());
                returnData[1] = double.Parse(Input.fatigueLoad["Py" + holeNum].ToString());
            }
            else {
                returnData[0] = double.Parse(Input.staticLoad["Px" + holeNum].ToString());
                returnData[1] = double.Parse(Input.staticLoad["Py" + holeNum].ToString());
            }

            return returnData;
        }

        public List<List<HoleInformation>> HoleExtractionCurves(List<HoleInformation> loadHoleInfo) {

            List<HoleInformation> BPHoles = new List<HoleInformation>();
            List<HoleInformation> BPHoles01 = new List<HoleInformation>();
            List<HoleInformation> RepairHoles = new List<HoleInformation>();
            List<HoleInformation> RepairHoles01 = new List<HoleInformation>();

            for (int j = 0; j < loadHoleInfo.Count(); j++) {
                int i = j + 1;
                int sysNum = loadHoleInfo[j].sysNumber;

                if (repairAnalysisExists) {
                    if (repairAnalysis) {
                        double r = double.Parse(Input.varsRepair["do" + i.ToString()].ToString()) / 2;

                        HoleInformation storedNominalInfo = new HoleInformation();
                        HoleInformation storedNominalPlus01Info = new HoleInformation();

                        storedNominalInfo.holeLoc = "Repair Hole #" + i;
                        storedNominalPlus01Info.holeLoc = "Repair Hole 0.01 #" + i;

                        storedNominalInfo.holeNumber = SCModel.CircleCurves.AddAutoLocal(sysNum, r).Number;
                        storedNominalPlus01Info.holeNumber = SCModel.CircleCurves.AddAutoLocal(sysNum, r + 0.01).Number;

                        storedNominalInfo.sysNumber = sysNum;
                        storedNominalPlus01Info.sysNumber = sysNum;

                        RepairHoles.Add(storedNominalInfo);
                        RepairHoles01.Add(storedNominalPlus01Info);
                    }
                    else {
                        double r = double.Parse(Input.varsBP["do" + i.ToString()].ToString()) / 2;
                        double repairRad = double.Parse(Input.varsRepair["do" + i.ToString()].ToString()) / 2;

                        HoleInformation storedNominalInfo = new HoleInformation();
                        HoleInformation storedNominalPlus01Info = new HoleInformation();
                        HoleInformation storedNominalInfoRepair = new HoleInformation();
                        HoleInformation storedNominalPlus01InfoRepair = new HoleInformation();

                        storedNominalInfo.holeLoc = "Blue Print Hole #" + i;
                        storedNominalPlus01Info.holeLoc = "Blue Print Hole 0.01 #" + i;
                        storedNominalInfoRepair.holeLoc = "Repair Hole #" + i;
                        storedNominalPlus01InfoRepair.holeLoc = "Repair Hole 0.01 #" + i;

                        storedNominalInfo.holeNumber = SCModel.CircleCurves.AddAutoLocal(sysNum, r).Number;
                        storedNominalPlus01Info.holeNumber = SCModel.CircleCurves.AddAutoLocal(sysNum, r + 0.01).Number;
                        storedNominalInfoRepair.holeNumber = SCModel.CircleCurves.AddAutoLocal(sysNum, repairRad).Number;
                        storedNominalPlus01InfoRepair.holeNumber = SCModel.CircleCurves.AddAutoLocal(sysNum, repairRad + 0.01).Number;

                        storedNominalInfo.sysNumber = sysNum;
                        storedNominalPlus01Info.sysNumber = sysNum;
                        storedNominalInfoRepair.sysNumber = sysNum;
                        storedNominalPlus01InfoRepair.sysNumber = sysNum;

                        BPHoles.Add(storedNominalInfo);
                        BPHoles01.Add(storedNominalPlus01Info);
                        RepairHoles.Add(storedNominalInfoRepair);
                        RepairHoles01.Add(storedNominalPlus01InfoRepair);

                    }
                }
                else {
                    if (repairAnalysis) {
                        //Oops
                    }
                    else {
                        double r = double.Parse(Input.varsBP["do" + i.ToString()].ToString()) / 2;

                        HoleInformation storedNominalInfo = new HoleInformation();
                        HoleInformation storedNominalPlus01Info = new HoleInformation();

                        storedNominalInfo.holeLoc = "Blue Print Hole #" + i;
                        storedNominalPlus01Info.holeLoc = "Blue Print Hole 0.01 #" + i;

                        storedNominalInfo.holeNumber = SCModel.CircleCurves.AddAutoLocal(sysNum, r).Number;
                        storedNominalPlus01Info.holeNumber = SCModel.CircleCurves.AddAutoLocal(sysNum, r + 0.01).Number;

                        storedNominalInfo.sysNumber = sysNum;
                        storedNominalPlus01Info.sysNumber = sysNum;

                        RepairHoles.Add(storedNominalInfo);
                        RepairHoles01.Add(storedNominalPlus01Info);
                    }
                }
            }

            //Complie a complete list of holes
            List<List<HoleInformation>> completeList = new List<List<HoleInformation>>();

            completeList.Add(RepairHoles);
            completeList.Add(RepairHoles01);
            completeList.Add(BPHoles);
            completeList.Add(BPHoles01);

            completeList.RemoveAll(item => item.Count == 0);

            return completeList;
        }

        public void CreateExtractions(List<List<HoleInformation>> extractionCurves) {
            //Create Global Error Extraction
            StressCheck.ERROREXTRACTION globalError = new StressCheck.ERROREXTRACTION();
            globalError.Name = "Error";
            globalError.RunMax = 3;
            globalError.RunMin = 1;
            globalError.SolutionID = "COMB";
            SCDoc.Extractions.AddErrorExtraction(globalError);

            //Create Fringe Plot Extraction
            StressCheck.PLOTEXTRACTION fringeS1 = new StressCheck.PLOTEXTRACTION();
            fringeS1.Name = "Max S1";
            fringeS1.Midsides = 10;
            fringeS1.Object = PlotObject.plotAll;
            fringeS1.EFunction = ElasticityFunctions.efS1;
            fringeS1.IsFringePlot = 1; //Guess since true and false don't work
            fringeS1.Run = 3;
            fringeS1.IsAutoscaleContour = 1; //Guess since true and false don't work
            fringeS1.SolutionID = "COMB";
            SCDoc.Plots.Add(fringeS1);

            //Create Min/Max search
            StressCheck.MINMAXEXTRACTION maxS1 = new StressCheck.MINMAXEXTRACTION();
            maxS1.Name = "LocalError";
            maxS1.EFunction = ElasticityFunctions.efS1;
            maxS1.Maximum = 1;
            maxS1.Midsides = 10;
            maxS1.Object = MinMaxObject.mmoAllElements;
            maxS1.RunMin = 1;
            maxS1.RunMax = 3;
            maxS1.SolutionID = "COMB";
            SCDoc.Extractions.AddMinMaxExtraction(maxS1);

            //Create Records for all hole extractions
            for (int j = 0; j < extractionCurves.Count(); j++) {
                List<HoleInformation> tempInfo = new List<HoleInformation>(extractionCurves[j]);
                for (int i = 0; i < tempInfo.Count(); i++) {
                    int holeNum = i + 1;
                    string start = "";
                    string end = "";

                    if (tempInfo[i].holeLoc.Contains("Blue")) {
                        start = "Hole #" + holeNum;
                    }
                    else {
                        start = "Hole #" + holeNum + " R";
                    }
                    if (tempInfo[i].holeLoc.Contains("0.01")) {
                        end = " 0.01";
                    }

                    string setName = "CURVE" + tempInfo[i].holeNumber.ToString() + "EXTR";
                    SCModel.Sets.Add(setName, SetObjectType.stBoundary, SetOptionType.sotList, tempInfo[i].holeNumber);

                    NewPointExtract(tempInfo, i, true, true, "CombS1", setName, start, end);
                    NewPointExtract(tempInfo, i, false, true, "Brg Et", setName, start, end);
                    NewPointExtract(tempInfo, i, false, false, "Brg Et", setName, start, end);
                    NewPointExtract(tempInfo, i, false, true, "Byp Et", setName, start, end);
                    NewPointExtract(tempInfo, i, false, false, "Byp Et", setName, start, end);

                }
            }
        }

        private void NewPointExtract(List<HoleInformation> tempInfo, int i, bool principalOnly, bool positiveLoad, string name, string setName, string start, string end) {
            //Need to make positive and negative bypass, bearing, combined, and for bp and repairs 20 total per hole

            //May want to rethink this later.  Single hole takes "NBrg_Et" and such for repair.  May need to rename those curves from "Repair" to print later.

            int funcs;
            StressCheck.ElasticityFunctions[] efunc = new StressCheck.ElasticityFunctions[0];
            if (principalOnly) {
                funcs = 1;
                Array.Resize(ref efunc, funcs);
                efunc[0] = ElasticityFunctions.efS1;
            }
            else {
                funcs = 3;
                Array.Resize(ref efunc, funcs);
                efunc[0] = ElasticityFunctions.efS1;
                efunc[1] = ElasticityFunctions.efSy;
                efunc[2] = ElasticityFunctions.efEy;
            }

            StressCheck.POINTEXTRACTION pntExtr = new StressCheck.POINTEXTRACTION();

            pntExtr.NumberOfPoints = 359;
            pntExtr.RunMin = 3;
            pntExtr.RunMax = 3;
            pntExtr.Object = PointsObject.poBoundary;
            pntExtr.SysFlag = SystemFlag.sysLocal;
            pntExtr.SysNumber = tempInfo[i].sysNumber;
            pntExtr.EFunctionsArray = efunc;
            pntExtr.SetName = setName;

            string tempName;
            if (positiveLoad && principalOnly) {
                tempName = "_";
            }
            else if (positiveLoad) {
                tempName = "_P";
            }
            else {
                tempName = "_N";
            }

            string solutionName = "COMB";
            if (name == "CombS1") {
                solutionName = "COMB";
            }
            else if (name == "Brg Et") {//TODO
                //solutionName = "BRG";
            }
            else if (name == "Byp Et") {//TODO
                //solutionName = "BYP";
            }
            else {
                //ooops
            }

            pntExtr.SolutionID = solutionName;
            pntExtr.Name = start + tempName + name + end;

            SCDoc.Extractions.AddPointExtraction(pntExtr);

        }

        public void SetInitialDisplay() {
            SCModel.Display.Elements = true;
            SCModel.Display.Curves = true;
            SCModel.Display.Points = true;
            SCModel.Display.Systems = true;
            SCModel.Display.Surfaces = true;
            SCModel.Display.Nodes = true;
            SCModel.Display.Edges = true;
            SCModel.Display.Orientation = ViewType.vtCenter;

            SCModel.Update();
        }

        public void SetDisplay() {
            SCModel.Display.Elements = true;
            SCModel.Display.Curves = false;
            SCModel.Display.Points = false;
            SCModel.Display.Systems = false;
            SCModel.Display.Surfaces = false;
            SCModel.Display.Nodes = false;
            SCModel.Display.Edges = false;
            SCModel.Display.Orientation = ViewType.vtCenter;

            SCModel.Update();
        }

        public enum plateEdgeLabel {
            xPositiveEdge, xNegativeEdge, yPositiveEdge, yNegativeEdge
        }

        private int pickPlateEdge(plateEdgeLabel edge, out int sysNum) {

            int obj_num = 0;
            int entity_num = 0;
            double x = double.NaN;
            double y = double.NaN;
            double z = double.NaN;
            object[] rotation = new object[3];

            if (edge == plateEdgeLabel.xPositiveEdge) {
                x = Working.X + double.Parse(typeOfParams["L"].ToString());
                y = Working.Y + double.Parse(typeOfParams["W"].ToString()) / 2;
                rotation[0] = 0;
                rotation[1] = 0;
                rotation[2] = 0;
            }
            else if (edge == plateEdgeLabel.xNegativeEdge) {
                x = Working.X;
                y = Working.Y + double.Parse(typeOfParams["W"].ToString()) / 2;
                rotation[0] = 180;
                rotation[1] = 0;
                rotation[2] = 180;
            }
            else if (edge == plateEdgeLabel.yNegativeEdge) {
                x = Working.X + double.Parse(typeOfParams["L"].ToString()) / 2;
                y = Working.Y;
                rotation[0] = 180;
                rotation[1] = 0;
                rotation[2] = 90;
            }
            else if (edge == plateEdgeLabel.yPositiveEdge) {
                x = Working.X + double.Parse(typeOfParams["L"].ToString()) / 2;
                y = Working.Y + double.Parse(typeOfParams["W"].ToString());
                rotation[0] = 0;
                rotation[1] = 0;
                rotation[2] = 90;
            }
            z = Working.Z;
            SCModel.EnableSelection(ObjectPick.opPSurfCurve);
            SCModel.PickObject(ObjectPick.opPSurfCurve, x, y, z, ref obj_num, ref entity_num, 0.00001, PickType.ptCenter);

            sysNum = SCModel.Systems.AddAutoGlobal(SystemType.stCartesian, x, y, z, rotation).Number;

            return obj_num;
        }

        private class WorkingData {
            public double X, Y, Z, bl_multiplier;
            public int SCBodyNum;
            public string setNameBase;
        }

        public class HoleInformation {
            public string holeLoc;
            public int holeNumber, sysNumber;
        }

        private int DEFECTcreateImprintBody(int[] bndry_nums) {
            SCModel.Bodies.AddAutoImprintCurveNormal(Working.SCBodyNum, bndry_nums, 0.00001);
            return SCModel.LastBodyNumber;
        }
    }
}
