using System;
using System.IO;

namespace StressCheckSolver {
    /// <summary>
    /// Define an output directory for results that the user wants to save.
    /// Does NOT create the directory until CreateFolder() is called.
    /// </summary>
    public class UserOutputFolder : OutputFolder {
        public UserOutputFolder(string inWhatDirectory, bool isFullPath = false) {
            if (isFullPath) {
                fullPath = inWhatDirectory;
                return;
            }
            DateTime dt = DateTime.Now;
            string timestamp = dt.Year + "." + dt.Month + "." + dt.Day +
                               "." + dt.Hour + "." + dt.Minute + "." + dt.Second;
            string folderName = "Solve Started at " + timestamp;
            fullPath = Path.Combine(inWhatDirectory, folderName);
        }
    }
}