﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StressCheckSolver
{
    public class KtFactors
    {
        /// <summary>
        /// Determine pin bending (peaking) factor
        /// </summary>
        /// <param name="E_Ratio"> Eplate/Epin </param>
        /// <param name="dt_Ratio"> diameter/thickness </param>
        /// <param name="errString">output error string</param>
        /// <returns></returns>
        public double Kt_Peaking(double E_Ratio, double dt_Ratio, out string errString) //Inputs are Eplate/Epin and diameter/thickness
        {
            //below equations from MAKS
            errString = "";

            if (dt_Ratio > 3)
            {
                dt_Ratio = 3;  //TODO add error string that can be added to other string below
            }

            double kt36 = 0.067502 * Math.Pow(dt_Ratio, 6) - 0.81099 * Math.Pow(dt_Ratio, 5) + 3.9735 * Math.Pow(dt_Ratio, 4) - 10.181 * Math.Pow(dt_Ratio, 3) + 14.491 * Math.Pow(dt_Ratio, 2) - 11.079 * dt_Ratio + 4.7611;
            double kt55 = 0.10972 * Math.Pow(dt_Ratio, 6) - 1.2488 * Math.Pow(dt_Ratio, 5) + 5.7682 * Math.Pow(dt_Ratio, 4) - 13.915 * Math.Pow(dt_Ratio, 3) + 18.764 * Math.Pow(dt_Ratio, 2) - 13.881 * dt_Ratio + 5.7675;
            double kt1 = 0.10444 * Math.Pow(dt_Ratio, 6) - 1.2464 * Math.Pow(dt_Ratio, 5) + 6.0871 * Math.Pow(dt_Ratio, 4) - 15.686 * Math.Pow(dt_Ratio, 3) + 22.844 * Math.Pow(dt_Ratio, 2) - 18.403 * dt_Ratio + 7.8867;
            double kt1875 = 0.1054 * Math.Pow(dt_Ratio, 6) - 1.3198 * Math.Pow(dt_Ratio, 5) + 6.7887 * Math.Pow(dt_Ratio, 4) - 18.474 * Math.Pow(dt_Ratio, 3) + 28.458 * Math.Pow(dt_Ratio, 2) - 24.303 * dt_Ratio + 10.708;

            if (E_Ratio < 0.36 & E_Ratio >= 0.300)  //MAKS uses 0.354 but this is a problem for alum/steel (10/30 = 0.3333). 
            {
                E_Ratio = 0.36;
            }

            if (dt_Ratio < 0.5)
            {
                errString = "Unable to determine Ktp (d/t < 0.5)";
                return 1;
            }
            if (E_Ratio < 0.36 || E_Ratio > 1.875)
            {
                errString = "Unable to determine Ktp (Eplate/Epin < 0.36 or Eplate/Epin > 1.875)";
                return 1;
            }
            if (E_Ratio < 0.55)
            {
                return (kt55 - kt36) * (E_Ratio - 0.36) / (0.55 - 0.36) + kt36;
            }
            else if (E_Ratio < 1)
            {
                return (kt1 - kt55) * (E_Ratio - 0.55) / (1 - 0.55) + kt55;
            }
            else if (E_Ratio <= 1.875)
            {
                return (kt1875 - kt1) * (E_Ratio - 1) / (1.875 - 1) + kt1;
            }
            else
            {
                errString = "Unknown error determining Ktp.  Ktp set to 1.0";
                return 1;
            }
        }
        /// <summary>
        /// Calculate KtCSK factor
        /// </summary>
        /// <param name="depth_Ratio"> csk_depth/thickness </param>
        /// <param name="td_Ratio"> thickness/diameter </param>
        /// <param name="errString"> output error string </param>
        /// <returns></returns>
        public double Ktcsk(double depth_Ratio, double td_Ratio, out string errString) //Inputs are csk_depth/thickness and diameter/thickness
        {
            //From MAKS
            errString = "";
            double a = 0.99380435;
            double b = 0.065667339;
            double c = 0.016734544;
            double d = 0.023160017;
            double E = 0.018288624;
            double f = 1.1536899;
            double g = -0.031085242;
            double H = -0.052134246;
            double i = 0.043994816;
            double j = -0.45073982;

            if (td_Ratio > 1.2)
                td_Ratio = 1.2; //TOOD add error string that can be added to strings below
            if (depth_Ratio > 0.7)
            {
                errString = "Unable to determine Ktcsk (depth/t > 0.7)";
                return 1;
            }
            else if (depth_Ratio > 0)
            {
                return a + (b * td_Ratio) + (c * depth_Ratio) + (d * Math.Pow(td_Ratio, 2)) + (E * Math.Pow(depth_Ratio, 2)) + (f * td_Ratio * depth_Ratio) + (g * Math.Pow(td_Ratio, 3)) + (H * Math.Pow(depth_Ratio, 3)) + (i * td_Ratio * Math.Pow(depth_Ratio, 2)) + (j * Math.Pow(td_Ratio, 2) * depth_Ratio);
            }
            else
            {
                errString = "Unknown error determining Ktcsk.  Ktcsk set to 1.0";
                return 1;
            }
        }
        /// <summary>
        /// Thickness over diameter correction
        /// </summary>
        /// <param name="td_Ratio">thickness/diameter</param>
        /// <param name="errString">output error string</param>
        /// <returns></returns>
        public double Kt_thick(double td_Ratio, out string errString) //inputs are diameter/thickness
        {
            errString = "";
            if (td_Ratio < 1.3)
                return 0.0035475 * Math.Pow(td_Ratio, 5) - 0.042991 * Math.Pow(td_Ratio, 4) + 0.068296 * Math.Pow(td_Ratio, 3) - 0.035295 * Math.Pow(td_Ratio, 2) + 0.053142 * td_Ratio + 0.9999;

            else if (td_Ratio <= 10)
                return -0.0000031332 * Math.Pow(td_Ratio, 5) + 0.00011337 * Math.Pow(td_Ratio, 4) - 0.0016315 * Math.Pow(td_Ratio, 3) + 0.011815 * Math.Pow(td_Ratio, 2) - 0.045194 * td_Ratio + 1.0921;

            else if (td_Ratio > 10)
            {
                errString = "Unable to determine Kt_Thickness (t/d > 10)";
                return 1;
            }
            else
            {
                errString = "Unknown error determining Kt_Thickness.  Kt_Thickness set to 1.0";
                return 1;
            }
        }
        /// <summary>
        /// Neat fit factor
        /// </summary>
        /// <param name="E_Ratio">Epin/Eplate</param>
        /// <param name="tension">Tension or compression case (true is tension)</param>
        /// <param name="angle">angle from max location (+/-30)</param>
        /// <param name="errString">output error string</param>
        /// <returns></returns>
        public double KtNF(double E_Ratio, bool tension, double angle, out string errString) //Inputs are Eplate/Epin and whether this is a tension or compression case
        {
            errString = "";

            if (E_Ratio > 3)
            {
                errString = "Epin/Eplate is outside bounds.  KtNF set to 1.0 (Epin/Eplate > 3)";
                return 1;
            }
            else if (E_Ratio < 0.1)
            {
                errString = "Epin/Eplate is outside bounds.  KtNF set to 1.0 (Epin/Eplate < 0.1)";
                return 1;
            }
            else
            {
                if (tension == true)
                    return (-0.0086485 * Math.Pow(E_Ratio, 3) + 0.053983 * Math.Pow(E_Ratio, 2) - 0.12026 * E_Ratio + 0.99961 - 1) * (1 - Math.Abs(angle) / 30.0) + 1;  //The -1 and + 1 are to scale based on angle

                else if (tension == false)
                    return (-0.014702 * Math.Pow(E_Ratio, 6) + 0.13806 * Math.Pow(E_Ratio, 5) - 0.48844 * Math.Pow(E_Ratio, 4) + 0.75765 * Math.Pow(E_Ratio, 3) - 0.32209 * Math.Pow(E_Ratio, 2) - 0.43548 * E_Ratio + 1 - 1) * (1 - Math.Abs(angle) / 30.0) + 1;  //The -1 and + 1 are to scale based on angle

                else
                {
                    errString = "Unknown error determining Kt Neat-Fit.  Kt Neat-Fit set to 1.0";
                    return 1;
                }
            }
        }
    }
}
