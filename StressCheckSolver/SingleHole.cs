﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StressCheckSolver
{
    class SingleHole
    {
        SingleHoleParameterData param;

        public void SetInputs(object inputs, bool repairAnalysis, bool repairAnalysisExists, int holeNumber = 1)
        {
            param = XMLtoData((InputData)inputs, repairAnalysis, repairAnalysisExists, holeNumber);
        }

        public ResultsFormatted HoleResults(ResultsFormatted results ,List<ResultData.ExtractedSCData> storedExtractions, bool isLugAnalysis)
        {

            string errStringBP = "";
            string errStringRepair = "";

            if (param.repairAnalysis)
            {
                #region Repair Extractions

                results.GlobalErrorRepair = ExtractGlobalError("Error", storedExtractions);
                results.PrincipalConvergeRepair = ExtractLocalError("LocalError", storedExtractions);

                CreateBearingLists brgLists = new CreateBearingLists();
                CreateBypassLists bypLists = new CreateBypassLists();
                CreatePrincipalLists princLists = new CreatePrincipalLists();

                if (!isLugAnalysis)
                {
                    ExtractToList("NByp Et 0.01", "PByp Et 0.01", storedExtractions, param.currentHoleNumber, bypLists.unfactoredBypassRepairConfig01);
                    ExtractToList("NByp Et", "PByp Et", storedExtractions, param.currentHoleNumber, bypLists.unfactoredBypassRepairConfig, bypLists.factoredBypassFaySurfaceRepair, bypLists.factoredBypassMidSurfaceRepair);
                }

                ExtractToList("NBrg Et", "PBrg Et", storedExtractions, param.currentHoleNumber, brgLists.unfactoredBearingRepairConfig, brgLists.factoredBearingFaySurfaceRepair, brgLists.factoredBearingMidSurfaceRepair);
                ExtractToList("NBrg Et 0.01", "PBrg Et 0.01", storedExtractions, param.currentHoleNumber, brgLists.unfactoredBearingRepairConfig01);
                ExtractToList("CombS1", "CombS1", storedExtractions, param.currentHoleNumber, princLists.principalRepair);
                ExtractToList("CombS1 0.01", "CombS1 0.01", storedExtractions, param.currentHoleNumber, princLists.principalRepair01);

                if (!isLugAnalysis)
                {
                    ApplyFactors(bypLists.factoredBypassMidSurfaceRepair, bypLists.factoredBypassFaySurfaceRepair, brgLists.factoredBearingMidSurfaceRepair, brgLists.factoredBearingFaySurfaceRepair, true,
                        out errStringRepair, out results.ktCSKRepair, out results.KttdRepair, out results.KtPeakRepair, out results.neatFitFactsRepair);

                    ResultFormatting(results, true, brgLists.factoredBearingMidSurfaceRepair, bypLists.factoredBypassMidSurfaceRepair,
                        brgLists.factoredBearingFaySurfaceRepair, bypLists.factoredBypassFaySurfaceRepair, brgLists.unfactoredBearingRepairConfig, bypLists.unfactoredBypassRepairConfig,
                        brgLists.unfactoredBearingRepairConfig01, bypLists.unfactoredBypassRepairConfig01, princLists.principalRepair, princLists.principalRepair01);
                }
                else
                {
                    ApplyLugFactors(brgLists.factoredBearingMidSurfaceRepair, brgLists.factoredBearingFaySurfaceRepair, true, out errStringRepair, out results.ktCSKRepair, out results.KtPeakRepair);

                    ResultLugFormatting(results, true, brgLists.factoredBearingMidSurfaceRepair, brgLists.factoredBearingFaySurfaceRepair,
                        brgLists.unfactoredBearingRepairConfig, brgLists.unfactoredBearingRepairConfig01, princLists.principalRepair, princLists.principalRepair01);
                }

                #endregion
            }
            else
            {
                #region Blue Print Extractions


                results.GlobalErrorBP = ExtractGlobalError("Error", storedExtractions);
                results.PrincipalConvergeBP = ExtractLocalError("LocalError", storedExtractions);

                CreateBearingLists brgLists = new CreateBearingLists();
                CreateBypassLists bypLists = new CreateBypassLists();
                CreatePrincipalLists princLists = new CreatePrincipalLists();

                if (!isLugAnalysis)
                {
                    ExtractToList("NByp Et", "PByp Et", storedExtractions, param.currentHoleNumber, bypLists.unfactoredBypassBP, bypLists.factoredBypassFaySurfaceBP, bypLists.factoredBypassMidSurfaceBP);
                    ExtractToList("NByp Et 0.01", "PByp Et 0.01", storedExtractions, param.currentHoleNumber, bypLists.unfactoredBypassBP01);
                    ExtractToList("R_NByp Et", "R_PByp Et", storedExtractions, param.currentHoleNumber, bypLists.unfactoredBypassBPRepairLoc);
                    ExtractToList("R_NByp Et 0.01", "R_PByp Et 0.01", storedExtractions, param.currentHoleNumber, bypLists.unfactoredBypassBPRepairLoc01);
                }

                ExtractToList("NBrg Et", "PBrg Et", storedExtractions, param.currentHoleNumber, brgLists.unfactoredBearingBP, brgLists.factoredBearingFaySurfaceBP, brgLists.factoredBearingMidSurfaceBP);
                ExtractToList("NBrg Et 0.01", "PBrg Et 0.01", storedExtractions, param.currentHoleNumber, brgLists.unfactoredBearingBP01);
                ExtractToList("R_NBrg Et", "R_PBrg Et", storedExtractions, param.currentHoleNumber, brgLists.unfactoredBearingBPRepairLoc);
                ExtractToList("R_NBrg Et 0.01", "R_PBrg Et 0.01", storedExtractions, param.currentHoleNumber, brgLists.unfactoredBearingBPRepairLoc01);
                ExtractToList("CombS1", "CombS1", storedExtractions, param.currentHoleNumber, princLists.principalBP);
                ExtractToList("CombS1 0.01", "CombS1 0.01", storedExtractions, param.currentHoleNumber, princLists.principalBP01);
                ExtractToList("R_CombS1", "R_CombS1", storedExtractions, param.currentHoleNumber, princLists.principalBPRepairLoc);
                ExtractToList("R_CombS1 0.01", "R_CombS1 0.01", storedExtractions, param.currentHoleNumber, princLists.principalBPRepairLoc01);

                if (!isLugAnalysis)
                {
                    ApplyFactors(bypLists.factoredBypassMidSurfaceBP, bypLists.factoredBypassFaySurfaceBP, brgLists.factoredBearingMidSurfaceBP, brgLists.factoredBearingFaySurfaceBP, false,
                        out errStringBP, out results.ktCSKBP, out results.KttdBP, out results.KtPeakBP, out results.neatFitFactsBP);

                    //Set BP factors equal to each other prior to final result formatting.
                    results.ktCSKBP2 = results.ktCSKBP;
                    results.KttdBP2 = results.KttdBP;
                    results.KtPeakBP2 = results.KtPeakBP;
                    results.neatFitFactsBP2 = results.neatFitFactsBP;

                    if (param.repairAnalysisExists) //use fay/mid surface from repair and match angle
                    {
                        ResultFormatting(results, false, results.faySurfaceRepairConfig, brgLists.factoredBearingMidSurfaceBP, bypLists.factoredBypassMidSurfaceBP,
                        brgLists.factoredBearingFaySurfaceBP, bypLists.factoredBypassFaySurfaceBP, brgLists.unfactoredBearingBP, bypLists.unfactoredBypassBP,
                        brgLists.unfactoredBearingBP01, bypLists.unfactoredBypassBP01, brgLists.unfactoredBearingBPRepairLoc, bypLists.unfactoredBypassBPRepairLoc,
                        brgLists.unfactoredBearingBPRepairLoc01, bypLists.unfactoredBypassBPRepairLoc01,
                        princLists.principalBP, princLists.principalBP01, princLists.principalBPRepairLoc, princLists.principalBPRepairLoc01);
                    }
                    else
                    {
                        ResultFormatting(results, false, brgLists.factoredBearingMidSurfaceBP, bypLists.factoredBypassMidSurfaceBP,
                            brgLists.factoredBearingFaySurfaceBP, bypLists.factoredBypassFaySurfaceBP, brgLists.unfactoredBearingBP, bypLists.unfactoredBypassBP,
                            brgLists.unfactoredBearingBP01, bypLists.unfactoredBypassBP01, brgLists.unfactoredBearingBPRepairLoc, bypLists.unfactoredBypassBPRepairLoc,
                            brgLists.unfactoredBearingBPRepairLoc01, bypLists.unfactoredBypassBPRepairLoc01,
                            princLists.principalBP, princLists.principalBP01, princLists.principalBPRepairLoc, princLists.principalBPRepairLoc01);
                    }
                }
                else
                {
                    ApplyLugFactors(brgLists.factoredBearingMidSurfaceBP, brgLists.factoredBearingFaySurfaceBP, false, out errStringBP, out results.ktCSKBP, out results.KtPeakBP);

                    //Set BP factors equal to each other prior to final result formatting.
                    results.ktCSKBP2 = results.ktCSKBP;
                    results.KtPeakBP2 = results.KtPeakBP;

                    if (param.repairAnalysisExists) //use fay/mid surface from repair and match angle
                    {
                        ResultLugFormatting(results, false, results.faySurfaceRepairConfig, brgLists.factoredBearingMidSurfaceBP, brgLists.factoredBearingFaySurfaceBP, brgLists.unfactoredBearingBP,
        brgLists.unfactoredBearingBP01, brgLists.unfactoredBearingBPRepairLoc, brgLists.unfactoredBearingBPRepairLoc01,
        princLists.principalBP, princLists.principalBP01, princLists.principalBPRepairLoc, princLists.principalBPRepairLoc01);
                    }
                    else
                    {
                        ResultLugFormatting(results, false, brgLists.factoredBearingMidSurfaceBP, brgLists.factoredBearingFaySurfaceBP, brgLists.unfactoredBearingBP,
                            brgLists.unfactoredBearingBP01, brgLists.unfactoredBearingBPRepairLoc, brgLists.unfactoredBearingBPRepairLoc01,
                            princLists.principalBP, princLists.principalBP01, princLists.principalBPRepairLoc, princLists.principalBPRepairLoc01);
                    }
                }

                #endregion
            }

            //results.errorFactors = errStringBP + errStringRepair; //TODO make this presentable and probably pull out of this if statement

            return results;

        }

        /// <summary>
        /// Combines the bearing and bypass components.  Expects a positive and negative loading case for each
        /// </summary>
        /// <param name="Brg">Bearing Extracted Strain Group List</ExtractedStrainGroup></param>
        /// <param name="Byp">Bypass Extracted Strain Group List</param>
        /// <param name="name">Input name</param>
        /// <returns></returns>
        public List<ExtractedStrainGroup> CombineBrgByp(List<ExtractedStrainGroup> Brg, List<ExtractedStrainGroup> Byp, string name)
        {
            List<ExtractedStrainGroup> combined = new List<ExtractedStrainGroup>();
            for (int i = 0; i < Brg.Count(); i++)
            {
                ExtractedStrainGroup tempGroup = new ExtractedStrainGroup();
                for (int j = 0; j < Brg[i].StrainPoints.Count(); j++)
                {
                    ExtractedStrains tempStrains = new ExtractedStrains();
                    tempStrains.TangentStrain = Brg[i].StrainPoints[j].TangentStrain + Byp[i].StrainPoints[j].TangentStrain;
                    tempStrains.TangentStress = Brg[i].StrainPoints[j].TangentStress + Byp[i].StrainPoints[j].TangentStress;
                    //tempStrains.PrincStress = Brg[i].StrainPoints[j].PrincStress + Byp[i].StrainPoints[j].PrincStress; //Bad idea but request by Navy.  Not always same orientation :-/
                    tempStrains.Angle = Brg[i].StrainPoints[j].Angle;
                    tempStrains.X = Brg[i].StrainPoints[j].X;
                    tempStrains.Y = Brg[i].StrainPoints[j].Y;
                    string direction = "";
                    if (i == 1)
                        direction = " Positive";
                    else
                        direction = " Negative";
                    tempGroup.extractSetName = name + direction;
                    tempGroup.StrainPoints.Add(tempStrains);
                }
                combined.Add(tempGroup);
            }
            return combined;
        }

        /// <summary>
        /// Apply all Kt Factors based on FAPM.  Expecting list with both positive and negative load cases
        /// </summary>
        /// <param name="FBYPMS">Bypass mid-surface extracted strain group list</param>
        /// <param name="FBYPFAY">Bypass fay-surface extracted strain group list</param>
        /// <param name="FBRGMS">Bearing mid-surface extracted strain group list</param>
        /// <param name="FBRGFAY">Bearing fay-surface extracted strain group list</param>
        /// <param name="param">model parameters</param>
        /// <param name="repair">boolean whether this is a repair location or not</param>
        /// <param name="errString">output error string</param>
        /// <param name="KtCSK">output KtCSK Factor</param>
        /// <param name="Kttd">output t/d factor</param>
        /// <param name="KtPeak">output pin bending factor</param>
        /// <param name="neatFitFacts">output NF factor (all angles both tension and compression case)</param>
        public void ApplyFactors(List<ExtractedStrainGroup> FBYPMS, List<ExtractedStrainGroup> FBYPFAY, List<ExtractedStrainGroup> FBRGMS, List<ExtractedStrainGroup> FBRGFAY, bool repair, out string errString, out double KtCSK, out double Kttd, out double KtPeak, out double[,] neatFitFacts)
        {
            string errStringCSKBYP = "";
            string errStringCSKBRG = "";
            string errStringTD = "";
            string errStringNFBYP = "";
            string errStringNFBRG = "";
            string errStringPeak = "";

            double[,] NFTEMP = new double[FBYPFAY.Count(), FBYPFAY[0].StrainPoints.Count()];  //Temp neat fit factor double

            for (int i = 0; i < FBYPFAY.Count(); i++)
            {
                for (int j = 0; j < FBYPFAY[0].StrainPoints.Count(); j++)
                {
                    NFTEMP[i, j] = 1;
                }
            }

            neatFitFacts = NFTEMP;

            bool csk, neatFit;
            if (repair == false)
            {
                csk = param.csk;
                neatFit = param.neatFit;
            }
            else
            {
                csk = param.cskRepair;
                neatFit = param.neatFitRepair;
            }
            //Thru Stress
            if (csk) //Apply either CSK or t/d Factor for mid surface
            {
                FBYPMS = CskApplication(FBYPMS, repair, out errStringCSKBYP, out KtCSK);
                Kttd = 1;
            }
            else
            {
                FBYPMS = thickApplication(FBYPMS, repair, out errStringTD, out Kttd);
                KtCSK = 1;
            }

            //Apply neat fit factor if required for mid-surface
            if (neatFit)  //Needs to only be done at +/- 30 degrees around max location and needs to linearlly ramp down
            {
                FBYPMS = neatFitApplication(FBYPMS, repair, out errStringNFBYP, out neatFitFacts);
            }

            //BRG stress

            if (csk)
            {
                FBRGMS = CskApplication(FBRGMS, repair, out errStringCSKBRG, out KtCSK);
            }

            //Fay surface analysis

            //Thru Stress

            if (neatFit)  //Needs to only be done at +/- 30 degrees around max location and needs to linearlly ramp down
            {
                FBYPFAY = neatFitApplication(FBYPFAY, repair, out errStringNFBRG, out neatFitFacts);
            }

            //BRG Stress

            FBRGFAY = peakingApplication(FBRGFAY, repair, out errStringPeak, out KtPeak);

            errString = errStringCSKBRG + " (CSK Bearing) " + errStringCSKBYP + " (CSK Bypass) " + errStringTD + " (t/d) " + errStringNFBRG + " (Neat-Fit Bearing) " + errStringNFBYP + " (Neat-Fit Bypass) " + errStringPeak + " (Peaking) ";
        }

        public void ApplyLugFactors(List<ExtractedStrainGroup> FBRGMS, List<ExtractedStrainGroup> FBRGFAY, bool repair, out string errString, out double KtCSK, out double KtPeak)
        {
            string errStringCSKBYP = "";
            string errStringCSKBRG = "";
            string errStringTD = "";
            string errStringNFBYP = "";
            string errStringNFBRG = "";
            string errStringPeak = "";

            bool csk;
            if (repair)
            {
                csk = param.cskRepair;
            }
            else
            {
                csk = param.csk;
            }

            //BRG stress

            if (csk == true)
            {
                FBRGMS = CskApplication(FBRGMS, repair, out errStringCSKBRG, out KtCSK);
            }
            else
            {
                KtCSK = 1;
            }

            //Fay surface analysis

            //BRG Stress

            FBRGFAY = peakingApplication(FBRGFAY, repair, out errStringPeak, out KtPeak);

            errString = errStringCSKBRG + " (CSK Bearing) " + errStringCSKBYP + " (CSK Bypass) " + errStringTD + " (t/d) " + errStringNFBRG + " (Neat-Fit Bearing) " + errStringNFBYP + " (Neat-Fit Bypass) " + errStringPeak + " (Peaking) ";
        }

        public List<ExtractedStrainGroup> CskApplication(List<ExtractedStrainGroup> input, bool repair, out string errString, out double KtCsk)
        {
            List<ExtractedStrainGroup> output = new List<ExtractedStrainGroup>(input);
            KtFactors factors = new KtFactors();
            string errorString = "";
            if (repair == false)
            {
                KtCsk = factors.Ktcsk(param.cskDepth / param.th, param.th / param.holeDia, out errorString);
            }
            else
            {
                KtCsk = factors.Ktcsk(param.cskDepthRepair / param.thRepair, param.thRepair / param.holeDiaRepair, out errorString);
            }
            for (int i = 0; i < input.Count(); i++)
            {
                for (int j = 0; j < input[i].StrainPoints.Count(); j++)
                {
                    output[i].StrainPoints[j].TangentStrain = input[i].StrainPoints[j].TangentStrain * KtCsk;
                }
            }

            errString = errorString;
            return output;
        }

        public List<ExtractedStrainGroup> thickApplication(List<ExtractedStrainGroup> input, bool repair, out string errString, out double Kttd)
        {
            List<ExtractedStrainGroup> output = new List<ExtractedStrainGroup>(input);
            KtFactors factors = new KtFactors();
            string errorString = "";
            if (repair == false)
            {
                Kttd = factors.Kt_thick(param.th / param.holeDia, out errorString);
            }
            else
            {
                Kttd = factors.Kt_thick(param.thRepair / param.holeDiaRepair, out errorString);
            }
            for (int i = 0; i < input.Count(); i++)
            {
                for (int j = 0; j < input[i].StrainPoints.Count(); j++)
                {
                    output[i].StrainPoints[j].TangentStrain = input[i].StrainPoints[j].TangentStrain * Kttd;
                }
            }
            errString = errorString;
            return output;
        }

        public List<ExtractedStrainGroup> peakingApplication(List<ExtractedStrainGroup> input, bool repair, out string errString, out double KtPeaking)
        {
            List<ExtractedStrainGroup> output = new List<ExtractedStrainGroup>(input);
            KtFactors factors = new KtFactors();
            string errorString = "";
            if (repair == false)
            {
                KtPeaking = factors.Kt_Peaking(param.Em_Plate / param.Em_Fastener, param.holeDia / param.th, out errorString);
            }
            else
            {
                KtPeaking = factors.Kt_Peaking(param.Em_Plate / param.Em_FastenerRepair, param.holeDiaRepair / param.thRepair, out errorString);
            }
            for (int i = 0; i < input.Count(); i++)
            {
                for (int j = 0; j < input[i].StrainPoints.Count(); j++)
                {
                    output[i].StrainPoints[j].TangentStrain = input[i].StrainPoints[j].TangentStrain * KtPeaking;
                }
            }

            errString = errorString;
            return output;
        }

        public List<ExtractedStrainGroup> neatFitApplication(List<ExtractedStrainGroup> input, bool repair, out string errString, out double[,] neatFitFacts)
        {
            List<ExtractedStrainGroup> output = new List<ExtractedStrainGroup>(input);
            KtFactors factors = new KtFactors();
            string errorString = "";
            bool tension;
            neatFitFacts = new double[input.Count(), input[0].StrainPoints.Count()];

            double maxloc = input[1].StrainPoints.FindIndex(index => index.TangentStrain.Equals(input[1].StrainPoints.Max(max => max.TangentStrain)));

            if (maxloc > 180)
                maxloc = maxloc - 180;

            double maxloc2 = maxloc + 180;

            for (int i = 0; i < input.Count(); i++)
            {
                if (input[i].extractSetName.Contains("P") == true)  //Positive Case
                {
                    tension = true;
                }
                else
                {
                    tension = false;
                }
                double firstAngle;
                for (int j = 0; j < input[i].StrainPoints.Count(); j++)
                {
                    if (Math.Abs(maxloc - input[i].StrainPoints[j].Angle) <= 30.0)
                    {
                        firstAngle = Math.Abs(maxloc - input[i].StrainPoints[j].Angle);
                    }
                    else if (Math.Abs(maxloc - input[i].StrainPoints[j].Angle + 360.0) <= 30.0)
                    {
                        firstAngle = Math.Abs(maxloc - input[i].StrainPoints[j].Angle + 360.0);
                    }
                    else if (Math.Abs(maxloc - input[i].StrainPoints[j].Angle - 360.0) <= 30.0)
                    {
                        firstAngle = Math.Abs(maxloc - input[i].StrainPoints[j].Angle - 360.0);
                    }
                    else if (Math.Abs(maxloc2 - input[i].StrainPoints[j].Angle) <= 30.0)
                    {
                        firstAngle = Math.Abs(maxloc2 - input[i].StrainPoints[j].Angle);
                    }
                    else if (Math.Abs(maxloc2 - input[i].StrainPoints[j].Angle + 360.0) <= 30.0)
                    {
                        firstAngle = Math.Abs(maxloc2 - input[i].StrainPoints[j].Angle + 360.0);
                    }
                    else if (Math.Abs(maxloc2 - input[i].StrainPoints[j].Angle - 360.0) <= 30.0)
                    {
                        firstAngle = Math.Abs(maxloc2 - input[i].StrainPoints[j].Angle - 360.0);
                    }
                    else
                    {
                        firstAngle = 30; //30 will always return at factor of 1.000
                    }

                    if (repair == false)
                    {
                        neatFitFacts[i, j] = factors.KtNF(param.Em_Fastener / param.Em_Plate, tension, firstAngle, out errorString);
                    }
                    else
                    {
                        neatFitFacts[i, j] = factors.KtNF(param.Em_FastenerRepair / param.Em_Plate, tension, firstAngle, out errorString);
                    }
                    output[i].StrainPoints[j].TangentStrain = input[i].StrainPoints[j].TangentStrain * neatFitFacts[i, j];
                }
            }
            errString = errorString;
            return output;
        }

        public void ResultFormatting(ResultsFormatted results, bool repair, params List<ExtractedStrainGroup>[] Lists)
        {

            double[] minNF = new double[2];

            if (repair)
            {
                minNF[0] = results.neatFitFactsRepair[0, 0];
                minNF[1] = results.neatFitFactsRepair[1, 0];

                for (int i = 0; i < results.neatFitFactsRepair.Rank; i++)
                {
                    for (int j = 0; j < results.neatFitFactsRepair.Length / results.neatFitFactsRepair.Rank; j++)
                    {
                        if (minNF[i] > results.neatFitFactsRepair[i, j])
                            minNF[i] = results.neatFitFactsRepair[i, j];
                    }
                }

                results.KtNFTensRepair = minNF[1];
                results.KtNFCompRepair = minNF[0];

                results.combinedMidRepairConfig = CombineBrgByp(Lists[0], Lists[1], "Repair Config Combined Mid Surface");
                results.combinedFayRepairConfig = CombineBrgByp(Lists[2], Lists[3], "Repair Config Combined Fay Surface");
                results.combinedRepairConfig = CombineBrgByp(Lists[4], Lists[5], "Repair Config");
                results.combinedRepair01Config = CombineBrgByp(Lists[6], Lists[7], "Repair Config 0.01");

                List<ResultTable> resTableMSRepair = new List<ResultTable>();
                List<ResultTable> resTableFAYRepair = new List<ResultTable>();
                for (int i = 0; i < Lists[0][1].StrainPoints.Count(); i++)
                {
                    resTableMSRepair.Add(new ResultTable());
                    resTableFAYRepair.Add(new ResultTable());
                }
                for (int i = 0; i < Lists[0][1].StrainPoints.Count(); i++)
                {
                    resTableMSRepair[i].Angle = (Lists[4][1].StrainPoints[i].Angle);
                    resTableMSRepair[i].TangentStrainToStressBrg = (Lists[4][1].StrainPoints[i].TangentStrain) * param.Em_Plate * 10e6;
                    resTableMSRepair[i].TangentStrainToStressBrgNeg = (Lists[4][0].StrainPoints[i].TangentStrain) * param.Em_Plate * 10e6;
                    resTableMSRepair[i].TangentStrainToStressByp = (Lists[5][1].StrainPoints[i].TangentStrain) * param.Em_Plate * 10e6;
                    resTableMSRepair[i].TangentStrainToStressBypNeg = (Lists[5][0].StrainPoints[i].TangentStrain) * param.Em_Plate * 10e6;
                    resTableMSRepair[i].KtCSK = results.ktCSKRepair;
                    resTableMSRepair[i].KtNFTens = results.neatFitFactsRepair[1, i];
                    resTableMSRepair[i].KtNFComp = results.neatFitFactsRepair[0, i];
                    resTableMSRepair[i].Kttd = results.KttdRepair;
                    resTableMSRepair[i].TangentStrainToStressComb = results.combinedMidRepairConfig[1].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableMSRepair[i].TangentStrainToStressCombNeg = results.combinedMidRepairConfig[0].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableMSRepair[i].TangentStrainToStressBrgFactored = Lists[0][1].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableMSRepair[i].TangentStrainToStressBrgNegFactored = Lists[0][0].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableMSRepair[i].TangentStrainToStressBypFactored = Lists[1][1].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableMSRepair[i].TangentStrainToStressBypNegFactored = Lists[1][0].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableMSRepair[i].KcKt = -resTableMSRepair[i].TangentStrainToStressCombNeg / resTableMSRepair[i].TangentStrainToStressComb;


                    resTableFAYRepair[i].Angle = resTableMSRepair[i].Angle;
                    resTableFAYRepair[i].TangentStrainToStressBrg = resTableMSRepair[i].TangentStrainToStressBrg;
                    resTableFAYRepair[i].TangentStrainToStressBrgNeg = resTableMSRepair[i].TangentStrainToStressBrgNeg;
                    resTableFAYRepair[i].TangentStrainToStressByp = resTableMSRepair[i].TangentStrainToStressByp;
                    resTableFAYRepair[i].TangentStrainToStressBypNeg = resTableMSRepair[i].TangentStrainToStressBypNeg;
                    resTableFAYRepair[i].KtNFTens = resTableMSRepair[i].KtNFTens;
                    resTableFAYRepair[i].KtNFComp = resTableMSRepair[i].KtNFComp;
                    resTableFAYRepair[i].KtPeak = results.KtPeakRepair;
                    resTableFAYRepair[i].TangentStrainToStressComb = results.combinedFayRepairConfig[1].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableFAYRepair[i].TangentStrainToStressCombNeg = results.combinedFayRepairConfig[0].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableFAYRepair[i].TangentStrainToStressBrgFactored = Lists[2][1].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableFAYRepair[i].TangentStrainToStressBrgNegFactored = Lists[2][0].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableFAYRepair[i].TangentStrainToStressBypFactored = Lists[3][1].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableFAYRepair[i].TangentStrainToStressBypNegFactored = Lists[3][0].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableFAYRepair[i].KcKt = -resTableFAYRepair[i].TangentStrainToStressCombNeg / resTableFAYRepair[i].TangentStrainToStressComb;
                }

                resTableFAYRepair.RemoveAt(Lists[0][1].StrainPoints.Count() - 1);
                resTableMSRepair.RemoveAt(Lists[0][1].StrainPoints.Count() - 1);

                results.resultTableMSRepair = resTableMSRepair;
                results.resultTableFAYRepair = resTableFAYRepair;

                int maxStrainLocationFayRepairConfig = results.combinedFayRepairConfig[1].StrainPoints.FindIndex(index => index.TangentStrain.Equals(results.combinedFayRepairConfig[1].StrainPoints.Max(max => max.TangentStrain)));
                int maxStrainLocationMidRepairConfig = results.combinedMidRepairConfig[1].StrainPoints.FindIndex(index => index.TangentStrain.Equals(results.combinedMidRepairConfig[1].StrainPoints.Max(max => max.TangentStrain)));

                if (results.combinedFayRepairConfig[1].StrainPoints[maxStrainLocationFayRepairConfig].TangentStrain > results.combinedMidRepairConfig[1].StrainPoints[maxStrainLocationMidRepairConfig].TangentStrain)
                {
                    results.KttdRepair = 1; //set this to 1 if fay is winner
                    results.ktCSKRepair = 1; //same as above
                    results.RepairKtS = results.combinedFayRepairConfig[1].StrainPoints[maxStrainLocationFayRepairConfig].TangentStrain * param.Em_Plate * 10e6;
                    results.RepairKcKt = -results.combinedFayRepairConfig[0].StrainPoints[maxStrainLocationFayRepairConfig].TangentStrain / results.combinedFayRepairConfig[1].StrainPoints[maxStrainLocationFayRepairConfig].TangentStrain;
                    results.maxAngleRepair = results.combinedFayRepairConfig[1].StrainPoints[maxStrainLocationFayRepairConfig].Angle;
                    results.faySurfaceRepairConfig = true;
                    results.TangentRepairSG = results.combinedRepair01Config[1].StrainPoints[maxStrainLocationFayRepairConfig].TangentStress / results.combinedRepairConfig[1].StrainPoints[maxStrainLocationFayRepairConfig].TangentStress;
                    results.PrincipalRepairSG = Lists[9][1].StrainPoints[maxStrainLocationFayRepairConfig].PrincStress / Lists[8][1].StrainPoints[maxStrainLocationFayRepairConfig].PrincStress;
                }
                else
                {
                    results.KtPeakRepair = 1; //set this to 1 if MS is winner
                    results.RepairKtS = results.combinedMidRepairConfig[1].StrainPoints[maxStrainLocationMidRepairConfig].TangentStrain * param.Em_Plate * 10e6;
                    results.RepairKcKt = -results.combinedMidRepairConfig[0].StrainPoints[maxStrainLocationMidRepairConfig].TangentStrain / results.combinedMidRepairConfig[1].StrainPoints[maxStrainLocationMidRepairConfig].TangentStrain;
                    results.maxAngleRepair = results.combinedMidRepairConfig[1].StrainPoints[maxStrainLocationMidRepairConfig].Angle;
                    results.faySurfaceRepairConfig = false;
                    results.TangentRepairSG = results.combinedRepair01Config[1].StrainPoints[maxStrainLocationMidRepairConfig].TangentStress / results.combinedRepairConfig[1].StrainPoints[maxStrainLocationMidRepairConfig].TangentStress;
                    results.PrincipalRepairSG = Lists[9][1].StrainPoints[maxStrainLocationMidRepairConfig].PrincStress / Lists[8][1].StrainPoints[maxStrainLocationMidRepairConfig].PrincStress;
                }
            }
            else //Blue Print Case
            {
                minNF[0] = results.neatFitFactsBP[0, 0];
                minNF[1] = results.neatFitFactsBP[1, 0];

                for (int i = 0; i < results.neatFitFactsBP.Rank; i++)
                {
                    for (int j = 0; j < results.neatFitFactsBP.Length / results.neatFitFactsBP.Rank; j++)
                    {
                        if (minNF[i] > results.neatFitFactsBP[i, j])
                            minNF[i] = results.neatFitFactsBP[i, j];
                    }
                }

                results.KtNFTensBP = minNF[1];
                results.KtNFCompBP = minNF[0];

                results.combinedMidBPConfig = CombineBrgByp(Lists[0], Lists[1], "Combined Mid Surface BP");
                results.combinedFayBPConfig = CombineBrgByp(Lists[2], Lists[3], "Combined Fay Surface BP");
                results.combinedBPConfig = CombineBrgByp(Lists[4], Lists[5], "Blue Print");
                results.combinedBP01Config = CombineBrgByp(Lists[6], Lists[7], "Blue Print 0.01");
                results.combinedRepairBPConfig = CombineBrgByp(Lists[8], Lists[9], "BP Repair Location");
                results.combinedRepairBP01Config = CombineBrgByp(Lists[10], Lists[11], "BP Repair Location 0.01");

                List<ResultTable> resTableMSBP = new List<ResultTable>();
                List<ResultTable> resTableFAYBP = new List<ResultTable>();
                for (int i = 0; i < Lists[0][1].StrainPoints.Count(); i++)
                {
                    resTableMSBP.Add(new ResultTable());
                    resTableFAYBP.Add(new ResultTable());
                }
                for (int i = 0; i < Lists[0][1].StrainPoints.Count(); i++)
                {
                    resTableMSBP[i].Angle = (Lists[4][1].StrainPoints[i].Angle);
                    resTableMSBP[i].TangentStrainToStressBrg = (Lists[4][1].StrainPoints[i].TangentStrain) * param.Em_Plate * 10e6;
                    resTableMSBP[i].TangentStrainToStressBrgNeg = (Lists[4][0].StrainPoints[i].TangentStrain) * param.Em_Plate * 10e6;
                    resTableMSBP[i].TangentStrainToStressByp = (Lists[5][1].StrainPoints[i].TangentStrain) * param.Em_Plate * 10e6;
                    resTableMSBP[i].TangentStrainToStressBypNeg = (Lists[5][0].StrainPoints[i].TangentStrain) * param.Em_Plate * 10e6;
                    resTableMSBP[i].KtCSK = results.ktCSKBP;
                    resTableMSBP[i].KtNFTens = results.neatFitFactsBP[1, i];
                    resTableMSBP[i].KtNFComp = results.neatFitFactsBP[0, i];
                    resTableMSBP[i].Kttd = results.KttdBP;
                    resTableMSBP[i].TangentStrainToStressComb = results.combinedMidBPConfig[1].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableMSBP[i].TangentStrainToStressCombNeg = results.combinedMidBPConfig[0].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableMSBP[i].TangentStrainToStressBrgFactored = Lists[0][1].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableMSBP[i].TangentStrainToStressBrgNegFactored = Lists[0][0].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableMSBP[i].TangentStrainToStressBypFactored = Lists[1][1].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableMSBP[i].TangentStrainToStressBypNegFactored = Lists[1][0].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableMSBP[i].KcKt = -resTableMSBP[i].TangentStrainToStressCombNeg / resTableMSBP[i].TangentStrainToStressComb;

                    resTableFAYBP[i].Angle = resTableMSBP[i].Angle;
                    resTableFAYBP[i].TangentStrainToStressBrg = resTableMSBP[i].TangentStrainToStressBrg;
                    resTableFAYBP[i].TangentStrainToStressBrgNeg = resTableMSBP[i].TangentStrainToStressBrgNeg;
                    resTableFAYBP[i].TangentStrainToStressByp = resTableMSBP[i].TangentStrainToStressByp;
                    resTableFAYBP[i].TangentStrainToStressBypNeg = resTableMSBP[i].TangentStrainToStressBypNeg;
                    resTableFAYBP[i].KtNFTens = resTableMSBP[i].KtNFTens;
                    resTableFAYBP[i].KtNFComp = resTableMSBP[i].KtNFComp;
                    resTableFAYBP[i].KtPeak = results.KtPeakBP;
                    resTableFAYBP[i].TangentStrainToStressComb = results.combinedFayBPConfig[1].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableFAYBP[i].TangentStrainToStressCombNeg = results.combinedFayBPConfig[0].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableFAYBP[i].TangentStrainToStressBrgFactored = Lists[2][1].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableFAYBP[i].TangentStrainToStressBrgNegFactored = Lists[2][0].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableFAYBP[i].TangentStrainToStressBypFactored = Lists[3][1].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableFAYBP[i].TangentStrainToStressBypNegFactored = Lists[3][0].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableFAYBP[i].KcKt = -resTableFAYBP[i].TangentStrainToStressCombNeg / resTableFAYBP[i].TangentStrainToStressComb;
                }

                resTableFAYBP.RemoveAt(Lists[0][1].StrainPoints.Count() - 1);
                resTableMSBP.RemoveAt(Lists[0][1].StrainPoints.Count() - 1);

                results.resultTableMSBP = resTableMSBP;
                results.resultTableFAYBP = resTableFAYBP;

                int maxStrainLocationFay = results.combinedFayBPConfig[1].StrainPoints.FindIndex(index => index.TangentStrain.Equals(results.combinedFayBPConfig[1].StrainPoints.Max(max => max.TangentStrain)));
                int maxStrainLocationMid = results.combinedMidBPConfig[1].StrainPoints.FindIndex(index => index.TangentStrain.Equals(results.combinedMidBPConfig[1].StrainPoints.Max(max => max.TangentStrain)));

                if (results.combinedFayBPConfig[1].StrainPoints[maxStrainLocationFay].TangentStrain > results.combinedMidBPConfig[1].StrainPoints[maxStrainLocationMid].TangentStrain)
                {
                    results.KttdBP = 1;  //set this to 1 if fay is winner
                    results.ktCSKBP = 1; //same as above
                    results.BPKtS = results.combinedFayBPConfig[1].StrainPoints[maxStrainLocationFay].TangentStrain * param.Em_Plate * 10e6;
                    results.BPKcKt = -results.combinedFayBPConfig[0].StrainPoints[maxStrainLocationFay].TangentStrain / results.combinedFayBPConfig[1].StrainPoints[maxStrainLocationFay].TangentStrain;
                    results.maxAngle = results.combinedFayBPConfig[1].StrainPoints[maxStrainLocationFay].Angle;
                    results.faySurface = true;
                    results.TangentBPSG = results.combinedBP01Config[1].StrainPoints[maxStrainLocationFay].TangentStress / results.combinedBPConfig[1].StrainPoints[maxStrainLocationFay].TangentStress;
                    results.PrincipalBPSG = Lists[13][1].StrainPoints[maxStrainLocationFay].PrincStress / Lists[12][1].StrainPoints[maxStrainLocationFay].PrincStress;
                    results.RepairBndyStress = results.combinedRepairBPConfig[1].StrainPoints[maxStrainLocationFay].TangentStrain * param.Em_Plate * 10e6;
                    results.TangentRepairBndySG = results.combinedRepairBP01Config[1].StrainPoints[maxStrainLocationFay].TangentStress / results.combinedRepairBPConfig[1].StrainPoints[maxStrainLocationFay].TangentStress;
                    results.PrincipalRepairBndySG = Lists[15][1].StrainPoints[maxStrainLocationFay].PrincStress / Lists[14][1].StrainPoints[maxStrainLocationFay].PrincStress;
                }
                else
                {
                    results.KtPeakBP = 1; //set this to 1 if mid is winner
                    results.BPKtS = results.combinedMidBPConfig[1].StrainPoints[maxStrainLocationMid].TangentStrain * param.Em_Plate * 10e6;
                    results.BPKcKt = -results.combinedMidBPConfig[0].StrainPoints[maxStrainLocationMid].TangentStrain / results.combinedMidBPConfig[1].StrainPoints[maxStrainLocationMid].TangentStrain;
                    results.maxAngle = results.combinedMidBPConfig[1].StrainPoints[maxStrainLocationMid].Angle;
                    results.faySurface = false;
                    results.TangentBPSG = results.combinedBP01Config[1].StrainPoints[maxStrainLocationMid].TangentStress / results.combinedBPConfig[1].StrainPoints[maxStrainLocationMid].TangentStress;
                    results.PrincipalBPSG = Lists[13][1].StrainPoints[maxStrainLocationMid].PrincStress / Lists[12][1].StrainPoints[maxStrainLocationMid].PrincStress;
                    results.RepairBndyStress = results.combinedRepairBPConfig[1].StrainPoints[maxStrainLocationMid].TangentStrain * param.Em_Plate * 10e6;
                    results.TangentRepairBndySG = results.combinedRepairBP01Config[1].StrainPoints[maxStrainLocationMid].TangentStress / results.combinedRepairBPConfig[1].StrainPoints[maxStrainLocationMid].TangentStress;
                    results.PrincipalRepairBndySG = Lists[15][1].StrainPoints[maxStrainLocationMid].PrincStress / Lists[14][1].StrainPoints[maxStrainLocationMid].PrincStress;
                }
            }
        }

        public void ResultFormatting(ResultsFormatted results, bool repair, bool faySurface, params List<ExtractedStrainGroup>[] Lists)
        {
            //This version will use repair mid/fay location and search on the max angle based on repair config
            double[] minNF = new double[2];

            {
                minNF[0] = results.neatFitFactsBP[0, 0];
                minNF[1] = results.neatFitFactsBP[1, 0];

                for (int i = 0; i < results.neatFitFactsBP.Rank; i++)
                {
                    for (int j = 0; j < results.neatFitFactsBP.Length / results.neatFitFactsBP.Rank; j++)
                    {
                        if (minNF[i] > results.neatFitFactsBP[i, j])
                            minNF[i] = results.neatFitFactsBP[i, j];
                    }
                }

                results.KtNFTensBP = minNF[1];
                results.KtNFCompBP = minNF[0];
                //Set Max BP and BP at Max Repair
                results.KtNFTensBP2 = results.KtNFTensBP;
                results.KtNFCompBP2 = results.KtNFCompBP;

                results.combinedMidBPConfig = CombineBrgByp(Lists[0], Lists[1], "Combined Mid Surface BP");
                results.combinedFayBPConfig = CombineBrgByp(Lists[2], Lists[3], "Combined Fay Surface BP");
                results.combinedBPConfig = CombineBrgByp(Lists[4], Lists[5], "Blue Print");
                results.combinedBP01Config = CombineBrgByp(Lists[6], Lists[7], "Blue Print 0.01");
                results.combinedRepairBPConfig = CombineBrgByp(Lists[8], Lists[9], "BP Repair Location");
                results.combinedRepairBP01Config = CombineBrgByp(Lists[10], Lists[11], "BP Repair Location 0.01");

                List<ResultTable> resTableMSBP = new List<ResultTable>();
                List<ResultTable> resTableFAYBP = new List<ResultTable>();
                for (int i = 0; i < Lists[0][1].StrainPoints.Count(); i++)
                {
                    resTableMSBP.Add(new ResultTable());
                    resTableFAYBP.Add(new ResultTable());
                }
                for (int i = 0; i < Lists[0][1].StrainPoints.Count(); i++)
                {
                    resTableMSBP[i].Angle = (Lists[4][1].StrainPoints[i].Angle);
                    resTableMSBP[i].TangentStrainToStressBrg = (Lists[4][1].StrainPoints[i].TangentStrain) * param.Em_Plate * 10e6;
                    resTableMSBP[i].TangentStrainToStressBrgNeg = (Lists[4][0].StrainPoints[i].TangentStrain) * param.Em_Plate * 10e6;
                    resTableMSBP[i].TangentStrainToStressByp = (Lists[5][1].StrainPoints[i].TangentStrain) * param.Em_Plate * 10e6;
                    resTableMSBP[i].TangentStrainToStressBypNeg = (Lists[5][0].StrainPoints[i].TangentStrain) * param.Em_Plate * 10e6;
                    resTableMSBP[i].KtCSK = results.ktCSKBP;
                    resTableMSBP[i].KtNFTens = results.neatFitFactsBP[1, i];
                    resTableMSBP[i].KtNFComp = results.neatFitFactsBP[0, i];
                    resTableMSBP[i].Kttd = results.KttdBP;
                    resTableMSBP[i].TangentStrainToStressComb = results.combinedMidBPConfig[1].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableMSBP[i].TangentStrainToStressCombNeg = results.combinedMidBPConfig[0].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableMSBP[i].TangentStrainToStressBrgFactored = Lists[0][1].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableMSBP[i].TangentStrainToStressBrgNegFactored = Lists[0][0].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableMSBP[i].TangentStrainToStressBypFactored = Lists[1][1].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableMSBP[i].TangentStrainToStressBypNegFactored = Lists[1][0].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableMSBP[i].KcKt = -resTableMSBP[i].TangentStrainToStressCombNeg / resTableMSBP[i].TangentStrainToStressComb;

                    resTableFAYBP[i].Angle = resTableMSBP[i].Angle;
                    resTableFAYBP[i].TangentStrainToStressBrg = resTableMSBP[i].TangentStrainToStressBrg;
                    resTableFAYBP[i].TangentStrainToStressBrgNeg = resTableMSBP[i].TangentStrainToStressBrgNeg;
                    resTableFAYBP[i].TangentStrainToStressByp = resTableMSBP[i].TangentStrainToStressByp;
                    resTableFAYBP[i].TangentStrainToStressBypNeg = resTableMSBP[i].TangentStrainToStressBypNeg;
                    resTableFAYBP[i].KtNFTens = resTableMSBP[i].KtNFTens;
                    resTableFAYBP[i].KtNFComp = resTableMSBP[i].KtNFComp;
                    resTableFAYBP[i].KtPeak = results.KtPeakBP;
                    resTableFAYBP[i].TangentStrainToStressComb = results.combinedFayBPConfig[1].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableFAYBP[i].TangentStrainToStressCombNeg = results.combinedFayBPConfig[0].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableFAYBP[i].TangentStrainToStressBrgFactored = Lists[2][1].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableFAYBP[i].TangentStrainToStressBrgNegFactored = Lists[2][0].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableFAYBP[i].TangentStrainToStressBypFactored = Lists[3][1].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableFAYBP[i].TangentStrainToStressBypNegFactored = Lists[3][0].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableFAYBP[i].KcKt = -resTableFAYBP[i].TangentStrainToStressCombNeg / resTableFAYBP[i].TangentStrainToStressComb;
                }

                resTableFAYBP.RemoveAt(Lists[0][1].StrainPoints.Count() - 1);
                resTableMSBP.RemoveAt(Lists[0][1].StrainPoints.Count() - 1);

                results.resultTableMSBP = resTableMSBP;
                results.resultTableFAYBP = resTableFAYBP;

                int maxStrainLocationFayRepairConfig = results.combinedFayRepairConfig[1].StrainPoints.FindIndex(index => index.TangentStrain.Equals(results.combinedFayRepairConfig[1].StrainPoints.Max(max => max.TangentStrain)));
                int maxStrainLocationMidRepairConfig = results.combinedMidRepairConfig[1].StrainPoints.FindIndex(index => index.TangentStrain.Equals(results.combinedMidRepairConfig[1].StrainPoints.Max(max => max.TangentStrain)));

                if (faySurface)  //BP at max repair location
                {
                    results.KttdBP2 = 1;  //set this to 1 if fay is winner
                    results.ktCSKBP2 = 1; //same as above
                    results.BPKtS2 = results.combinedFayBPConfig[1].StrainPoints[maxStrainLocationFayRepairConfig].TangentStrain * param.Em_Plate * 10e6;
                    results.BPKcKt2 = -results.combinedFayBPConfig[0].StrainPoints[maxStrainLocationFayRepairConfig].TangentStrain / results.combinedFayBPConfig[1].StrainPoints[maxStrainLocationFayRepairConfig].TangentStrain;
                    results.maxAngle2 = results.combinedFayBPConfig[1].StrainPoints[maxStrainLocationFayRepairConfig].Angle;
                    results.faySurface2 = true;
                    results.TangentBPSG2 = results.combinedBP01Config[1].StrainPoints[maxStrainLocationFayRepairConfig].TangentStress / results.combinedBPConfig[1].StrainPoints[maxStrainLocationFayRepairConfig].TangentStress;
                    results.PrincipalBPSG2 = Lists[13][1].StrainPoints[maxStrainLocationFayRepairConfig].PrincStress / Lists[12][1].StrainPoints[maxStrainLocationFayRepairConfig].PrincStress;
                    results.RepairBndyStress2 = results.combinedRepairBPConfig[1].StrainPoints[maxStrainLocationFayRepairConfig].TangentStrain * param.Em_Plate * 10e6;
                    results.TangentRepairBndySG2 = results.combinedRepairBP01Config[1].StrainPoints[maxStrainLocationFayRepairConfig].TangentStress / results.combinedRepairBPConfig[1].StrainPoints[maxStrainLocationFayRepairConfig].TangentStress;
                    results.PrincipalRepairBndySG2 = Lists[15][1].StrainPoints[maxStrainLocationFayRepairConfig].PrincStress / Lists[14][1].StrainPoints[maxStrainLocationFayRepairConfig].PrincStress;
                }
                else
                {
                    results.KtPeakBP2 = 1; //set this to 1 if mid is winner
                    results.BPKtS2 = results.combinedMidBPConfig[1].StrainPoints[maxStrainLocationMidRepairConfig].TangentStrain * param.Em_Plate * 10e6;
                    results.BPKcKt2 = -results.combinedMidBPConfig[0].StrainPoints[maxStrainLocationMidRepairConfig].TangentStrain / results.combinedMidBPConfig[1].StrainPoints[maxStrainLocationMidRepairConfig].TangentStrain;
                    results.maxAngle2 = results.combinedMidBPConfig[1].StrainPoints[maxStrainLocationMidRepairConfig].Angle;
                    results.faySurface2 = false;
                    results.TangentBPSG2 = results.combinedBP01Config[1].StrainPoints[maxStrainLocationMidRepairConfig].TangentStress / results.combinedBPConfig[1].StrainPoints[maxStrainLocationMidRepairConfig].TangentStress;
                    results.PrincipalBPSG2 = Lists[13][1].StrainPoints[maxStrainLocationMidRepairConfig].PrincStress / Lists[12][1].StrainPoints[maxStrainLocationMidRepairConfig].PrincStress;
                    results.RepairBndyStress2 = results.combinedRepairBPConfig[1].StrainPoints[maxStrainLocationMidRepairConfig].TangentStrain * param.Em_Plate * 10e6;
                    results.TangentRepairBndySG2 = results.combinedRepairBP01Config[1].StrainPoints[maxStrainLocationMidRepairConfig].TangentStress / results.combinedRepairBPConfig[1].StrainPoints[maxStrainLocationMidRepairConfig].TangentStress;
                    results.PrincipalRepairBndySG2 = Lists[15][1].StrainPoints[maxStrainLocationMidRepairConfig].PrincStress / Lists[14][1].StrainPoints[maxStrainLocationMidRepairConfig].PrincStress;
                }

                int maxStrainLocationFay = results.combinedFayBPConfig[1].StrainPoints.FindIndex(index => index.TangentStrain.Equals(results.combinedFayBPConfig[1].StrainPoints.Max(max => max.TangentStrain)));
                int maxStrainLocationMid = results.combinedMidBPConfig[1].StrainPoints.FindIndex(index => index.TangentStrain.Equals(results.combinedMidBPConfig[1].StrainPoints.Max(max => max.TangentStrain)));

                if (results.combinedFayBPConfig[1].StrainPoints[maxStrainLocationFay].TangentStrain > results.combinedMidBPConfig[1].StrainPoints[maxStrainLocationMid].TangentStrain)  //Max BP location
                {
                    results.KttdBP = 1;  //set this to 1 if fay is winner
                    results.ktCSKBP = 1; //same as above
                    results.BPKtS = results.combinedFayBPConfig[1].StrainPoints[maxStrainLocationFay].TangentStrain * param.Em_Plate * 10e6;
                    results.BPKcKt = -results.combinedFayBPConfig[0].StrainPoints[maxStrainLocationFay].TangentStrain / results.combinedFayBPConfig[1].StrainPoints[maxStrainLocationFay].TangentStrain;
                    results.maxAngle = results.combinedFayBPConfig[1].StrainPoints[maxStrainLocationFay].Angle;
                    results.faySurface = true;
                    results.TangentBPSG = results.combinedBP01Config[1].StrainPoints[maxStrainLocationFay].TangentStress / results.combinedBPConfig[1].StrainPoints[maxStrainLocationFay].TangentStress;
                    results.PrincipalBPSG = Lists[13][1].StrainPoints[maxStrainLocationFay].PrincStress / Lists[12][1].StrainPoints[maxStrainLocationFay].PrincStress;
                    results.RepairBndyStress = results.combinedRepairBPConfig[1].StrainPoints[maxStrainLocationFay].TangentStrain * param.Em_Plate * 10e6;
                    results.TangentRepairBndySG = results.combinedRepairBP01Config[1].StrainPoints[maxStrainLocationFay].TangentStress / results.combinedRepairBPConfig[1].StrainPoints[maxStrainLocationFay].TangentStress;
                    results.PrincipalRepairBndySG = Lists[15][1].StrainPoints[maxStrainLocationFay].PrincStress / Lists[14][1].StrainPoints[maxStrainLocationFay].PrincStress;
                }
                else
                {
                    results.KtPeakBP = 1; //set this to 1 if mid is winner
                    results.BPKtS = results.combinedMidBPConfig[1].StrainPoints[maxStrainLocationMid].TangentStrain * param.Em_Plate * 10e6;
                    results.BPKcKt = -results.combinedMidBPConfig[0].StrainPoints[maxStrainLocationMid].TangentStrain / results.combinedMidBPConfig[1].StrainPoints[maxStrainLocationMid].TangentStrain;
                    results.maxAngle = results.combinedMidBPConfig[1].StrainPoints[maxStrainLocationMid].Angle;
                    results.faySurface = false;
                    results.TangentBPSG = results.combinedBP01Config[1].StrainPoints[maxStrainLocationMid].TangentStress / results.combinedBPConfig[1].StrainPoints[maxStrainLocationMid].TangentStress;
                    results.PrincipalBPSG = Lists[13][1].StrainPoints[maxStrainLocationMid].PrincStress / Lists[12][1].StrainPoints[maxStrainLocationMid].PrincStress;
                    results.RepairBndyStress = results.combinedRepairBPConfig[1].StrainPoints[maxStrainLocationMid].TangentStrain * param.Em_Plate * 10e6;
                    results.TangentRepairBndySG = results.combinedRepairBP01Config[1].StrainPoints[maxStrainLocationMid].TangentStress / results.combinedRepairBPConfig[1].StrainPoints[maxStrainLocationMid].TangentStress;
                    results.PrincipalRepairBndySG = Lists[15][1].StrainPoints[maxStrainLocationMid].PrincStress / Lists[14][1].StrainPoints[maxStrainLocationMid].PrincStress;
                }
            }
        }

        public void ResultLugFormatting(ResultsFormatted results, bool repair, bool faySurface, params List<ExtractedStrainGroup>[] Lists)
        {
            //This version will use repair mid/fay location and search on the max angle based on repair config

                results.combinedMidBPConfig = Lists[0];
                results.combinedFayBPConfig = Lists[1];
                results.combinedBPConfig = Lists[2];
                results.combinedBP01Config = Lists[3];
                results.combinedRepairBPConfig = Lists[4];
                results.combinedRepairBP01Config = Lists[5];

                List<ResultTable> resTableMSBP = new List<ResultTable>();
                List<ResultTable> resTableFAYBP = new List<ResultTable>();
                for (int i = 0; i < Lists[0][1].StrainPoints.Count(); i++)
                {
                    resTableMSBP.Add(new ResultTable());
                    resTableFAYBP.Add(new ResultTable());
                }
                for (int i = 0; i < Lists[0][1].StrainPoints.Count(); i++)
                {
                    resTableMSBP[i].Angle = (Lists[2][1].StrainPoints[i].Angle);
                    resTableMSBP[i].TangentStrainToStressBrg = (Lists[2][1].StrainPoints[i].TangentStrain) * param.Em_Plate * 10e6;
                    resTableMSBP[i].TangentStrainToStressBrgNeg = (Lists[2][0].StrainPoints[i].TangentStrain) * param.Em_Plate * 10e6;
                    resTableMSBP[i].KtCSK = results.ktCSKBP;
                    resTableMSBP[i].Kttd = results.KttdBP;
                    resTableMSBP[i].TangentStrainToStressBrgFactored = Lists[0][1].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableMSBP[i].TangentStrainToStressBrgNegFactored = Lists[0][0].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableMSBP[i].KcKt = -resTableMSBP[i].TangentStrainToStressBrgNegFactored / resTableMSBP[i].TangentStrainToStressBrgFactored;

                    resTableFAYBP[i].Angle = resTableMSBP[i].Angle;
                    resTableFAYBP[i].TangentStrainToStressBrg = resTableMSBP[i].TangentStrainToStressBrg;
                    resTableFAYBP[i].TangentStrainToStressBrgNeg = resTableMSBP[i].TangentStrainToStressBrgNeg;
                    resTableFAYBP[i].KtPeak = results.KtPeakBP;
                    resTableFAYBP[i].TangentStrainToStressBrgFactored = Lists[1][1].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableFAYBP[i].TangentStrainToStressBrgNegFactored = Lists[1][0].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableFAYBP[i].KcKt = -resTableFAYBP[i].TangentStrainToStressBrgNegFactored / resTableFAYBP[i].TangentStrainToStressBrgFactored;
                }

                resTableFAYBP.RemoveAt(Lists[0][1].StrainPoints.Count() - 1);
                resTableMSBP.RemoveAt(Lists[0][1].StrainPoints.Count() - 1);

                results.resultTableMSBP = resTableMSBP;
                results.resultTableFAYBP = resTableFAYBP;

                int maxStrainLocationFayRepairConfig = results.combinedFayRepairConfig[1].StrainPoints.FindIndex(index => index.TangentStrain.Equals(results.combinedFayRepairConfig[1].StrainPoints.Max(max => max.TangentStrain)));
                int maxStrainLocationMidRepairConfig = results.combinedMidRepairConfig[1].StrainPoints.FindIndex(index => index.TangentStrain.Equals(results.combinedMidRepairConfig[1].StrainPoints.Max(max => max.TangentStrain)));

                if (faySurface)
                {
                    results.KttdBP2 = 1;  //set this to 1 if fay is winner
                    results.ktCSKBP2 = 1; //same as above
                    results.BPKtS2 = results.combinedFayBPConfig[1].StrainPoints[maxStrainLocationFayRepairConfig].TangentStrain * param.Em_Plate * 10e6;
                    results.BPKcKt2 = -results.combinedFayBPConfig[0].StrainPoints[maxStrainLocationFayRepairConfig].TangentStrain / results.combinedFayBPConfig[1].StrainPoints[maxStrainLocationFayRepairConfig].TangentStrain;
                    results.maxAngle2 = results.combinedFayBPConfig[1].StrainPoints[maxStrainLocationFayRepairConfig].Angle;
                    results.faySurface2 = true;
                    results.TangentBPSG2 = results.combinedBP01Config[1].StrainPoints[maxStrainLocationFayRepairConfig].TangentStress / results.combinedBPConfig[1].StrainPoints[maxStrainLocationFayRepairConfig].TangentStress;
                    results.PrincipalBPSG2 = Lists[7][1].StrainPoints[maxStrainLocationFayRepairConfig].PrincStress / Lists[6][1].StrainPoints[maxStrainLocationFayRepairConfig].PrincStress;
                    results.RepairBndyStress2 = results.combinedRepairBPConfig[1].StrainPoints[maxStrainLocationFayRepairConfig].TangentStrain * param.Em_Plate * 10e6;
                    results.TangentRepairBndySG2 = results.combinedRepairBP01Config[1].StrainPoints[maxStrainLocationFayRepairConfig].TangentStress / results.combinedRepairBPConfig[1].StrainPoints[maxStrainLocationFayRepairConfig].TangentStress;
                    results.PrincipalRepairBndySG2 = Lists[9][1].StrainPoints[maxStrainLocationFayRepairConfig].PrincStress / Lists[8][1].StrainPoints[maxStrainLocationFayRepairConfig].PrincStress;
                }
                else
                {
                    results.KtPeakBP2 = 1; //set this to 1 if mid is winner
                    results.BPKtS2 = results.combinedMidBPConfig[1].StrainPoints[maxStrainLocationMidRepairConfig].TangentStrain * param.Em_Plate * 10e6;
                    results.BPKcKt2 = -results.combinedMidBPConfig[0].StrainPoints[maxStrainLocationMidRepairConfig].TangentStrain / results.combinedMidBPConfig[1].StrainPoints[maxStrainLocationMidRepairConfig].TangentStrain;
                    results.maxAngle2 = results.combinedMidBPConfig[1].StrainPoints[maxStrainLocationMidRepairConfig].Angle;
                    results.faySurface2 = false;
                    results.TangentBPSG2 = results.combinedBP01Config[1].StrainPoints[maxStrainLocationMidRepairConfig].TangentStress / results.combinedBPConfig[1].StrainPoints[maxStrainLocationMidRepairConfig].TangentStress;
                    results.PrincipalBPSG2 = Lists[7][1].StrainPoints[maxStrainLocationMidRepairConfig].PrincStress / Lists[6][1].StrainPoints[maxStrainLocationMidRepairConfig].PrincStress;
                    results.RepairBndyStress2 = results.combinedRepairBPConfig[1].StrainPoints[maxStrainLocationMidRepairConfig].TangentStrain * param.Em_Plate * 10e6;
                    results.TangentRepairBndySG2 = results.combinedRepairBP01Config[1].StrainPoints[maxStrainLocationMidRepairConfig].TangentStress / results.combinedRepairBPConfig[1].StrainPoints[maxStrainLocationMidRepairConfig].TangentStress;
                    results.PrincipalRepairBndySG2 = Lists[9][1].StrainPoints[maxStrainLocationMidRepairConfig].PrincStress / Lists[8][1].StrainPoints[maxStrainLocationMidRepairConfig].PrincStress;
                }

                int maxStrainLocationFay = results.combinedFayBPConfig[1].StrainPoints.FindIndex(index => index.TangentStrain.Equals(results.combinedFayBPConfig[1].StrainPoints.Max(max => max.TangentStrain)));
                int maxStrainLocationMid = results.combinedMidBPConfig[1].StrainPoints.FindIndex(index => index.TangentStrain.Equals(results.combinedMidBPConfig[1].StrainPoints.Max(max => max.TangentStrain)));

                if (results.combinedFayBPConfig[1].StrainPoints[maxStrainLocationFay].TangentStrain > results.combinedMidBPConfig[1].StrainPoints[maxStrainLocationMid].TangentStrain)
                {
                    results.KttdBP = 1;  //set this to 1 if fay is winner
                    results.ktCSKBP = 1; //same as above
                    results.BPKtS = results.combinedFayBPConfig[1].StrainPoints[maxStrainLocationFay].TangentStrain * param.Em_Plate * 10e6;
                    results.BPKcKt = -results.combinedFayBPConfig[0].StrainPoints[maxStrainLocationFay].TangentStrain / results.combinedFayBPConfig[1].StrainPoints[maxStrainLocationFay].TangentStrain;
                    results.maxAngle = results.combinedFayBPConfig[1].StrainPoints[maxStrainLocationFay].Angle;
                    results.faySurface = true;
                    results.TangentBPSG = results.combinedBP01Config[1].StrainPoints[maxStrainLocationFay].TangentStress / results.combinedBPConfig[1].StrainPoints[maxStrainLocationFay].TangentStress;
                    results.PrincipalBPSG = Lists[7][1].StrainPoints[maxStrainLocationFay].PrincStress / Lists[6][1].StrainPoints[maxStrainLocationFay].PrincStress;
                    results.RepairBndyStress = results.combinedRepairBPConfig[1].StrainPoints[maxStrainLocationFay].TangentStrain * param.Em_Plate * 10e6;
                    results.TangentRepairBndySG = results.combinedRepairBP01Config[1].StrainPoints[maxStrainLocationFay].TangentStress / results.combinedRepairBPConfig[1].StrainPoints[maxStrainLocationFay].TangentStress;
                    results.PrincipalRepairBndySG = Lists[9][1].StrainPoints[maxStrainLocationFay].PrincStress / Lists[8][1].StrainPoints[maxStrainLocationFay].PrincStress;
                }
                else
                {
                    results.KtPeakBP = 1; //set this to 1 if mid is winner
                    results.BPKtS = results.combinedMidBPConfig[1].StrainPoints[maxStrainLocationMid].TangentStrain * param.Em_Plate * 10e6;
                    results.BPKcKt = -results.combinedMidBPConfig[0].StrainPoints[maxStrainLocationMid].TangentStrain / results.combinedMidBPConfig[1].StrainPoints[maxStrainLocationMid].TangentStrain;
                    results.maxAngle = results.combinedMidBPConfig[1].StrainPoints[maxStrainLocationMid].Angle;
                    results.faySurface = false;
                    results.TangentBPSG = results.combinedBP01Config[1].StrainPoints[maxStrainLocationMid].TangentStress / results.combinedBPConfig[1].StrainPoints[maxStrainLocationMid].TangentStress;
                    results.PrincipalBPSG = Lists[7][1].StrainPoints[maxStrainLocationMid].PrincStress / Lists[6][1].StrainPoints[maxStrainLocationMid].PrincStress;
                    results.RepairBndyStress = results.combinedRepairBPConfig[1].StrainPoints[maxStrainLocationMid].TangentStrain * param.Em_Plate * 10e6;
                    results.TangentRepairBndySG = results.combinedRepairBP01Config[1].StrainPoints[maxStrainLocationMid].TangentStress / results.combinedRepairBPConfig[1].StrainPoints[maxStrainLocationMid].TangentStress;
                    results.PrincipalRepairBndySG = Lists[9][1].StrainPoints[maxStrainLocationMid].PrincStress / Lists[8][1].StrainPoints[maxStrainLocationMid].PrincStress;
                }
        }

        public void ResultLugFormatting(ResultsFormatted results, bool repair, params List<ExtractedStrainGroup>[] Lists)
        {

            if (repair)
            {
                results.combinedMidRepairConfig = Lists[0];
                results.combinedFayRepairConfig = Lists[1];
                results.combinedRepairConfig = Lists[2];
                results.combinedRepair01Config = Lists[3];

                List<ResultTable> resTableMSRepair = new List<ResultTable>();
                List<ResultTable> resTableFAYRepair = new List<ResultTable>();
                for (int i = 0; i < Lists[0][1].StrainPoints.Count(); i++)
                {
                    resTableMSRepair.Add(new ResultTable());
                    resTableFAYRepair.Add(new ResultTable());
                }
                for (int i = 0; i < Lists[0][1].StrainPoints.Count(); i++)
                {
                    resTableMSRepair[i].Angle = (Lists[2][1].StrainPoints[i].Angle);
                    resTableMSRepair[i].TangentStrainToStressBrg = (Lists[2][1].StrainPoints[i].TangentStrain) * param.Em_Plate * 10e6;
                    resTableMSRepair[i].TangentStrainToStressBrgNeg = (Lists[2][0].StrainPoints[i].TangentStrain) * param.Em_Plate * 10e6;
                    resTableMSRepair[i].KtCSK = results.ktCSKRepair;
                    resTableMSRepair[i].Kttd = results.KttdRepair;
                    resTableMSRepair[i].TangentStrainToStressBrgFactored = Lists[0][1].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableMSRepair[i].TangentStrainToStressBrgNegFactored = Lists[0][0].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableMSRepair[i].KcKt = -resTableMSRepair[i].TangentStrainToStressBrgNegFactored / resTableMSRepair[i].TangentStrainToStressBrgFactored;


                    resTableFAYRepair[i].Angle = resTableMSRepair[i].Angle;
                    resTableFAYRepair[i].TangentStrainToStressBrg = resTableMSRepair[i].TangentStrainToStressBrg;
                    resTableFAYRepair[i].TangentStrainToStressBrgNeg = resTableMSRepair[i].TangentStrainToStressBrgNeg;
                    resTableFAYRepair[i].KtPeak = results.KtPeakRepair;
                    resTableFAYRepair[i].TangentStrainToStressBrgFactored = Lists[1][1].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableFAYRepair[i].TangentStrainToStressBrgNegFactored = Lists[1][0].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableFAYRepair[i].KcKt = -resTableFAYRepair[i].TangentStrainToStressBrgNegFactored / resTableFAYRepair[i].TangentStrainToStressBrgFactored;
                }

                resTableFAYRepair.RemoveAt(Lists[0][1].StrainPoints.Count() - 1);
                resTableMSRepair.RemoveAt(Lists[0][1].StrainPoints.Count() - 1);

                results.resultTableMSRepair = resTableMSRepair;
                results.resultTableFAYRepair = resTableFAYRepair;

                int maxStrainLocationFayRepairConfig = results.combinedFayRepairConfig[1].StrainPoints.FindIndex(index => index.TangentStrain.Equals(results.combinedFayRepairConfig[1].StrainPoints.Max(max => max.TangentStrain)));
                int maxStrainLocationMidRepairConfig = results.combinedMidRepairConfig[1].StrainPoints.FindIndex(index => index.TangentStrain.Equals(results.combinedMidRepairConfig[1].StrainPoints.Max(max => max.TangentStrain)));

                if (results.combinedFayRepairConfig[1].StrainPoints[maxStrainLocationFayRepairConfig].TangentStrain > results.combinedMidRepairConfig[1].StrainPoints[maxStrainLocationMidRepairConfig].TangentStrain)
                {
                    results.KttdRepair = 1; //set this to 1 if fay is winner
                    results.ktCSKRepair = 1; //same as above
                    results.RepairKtS = results.combinedFayRepairConfig[1].StrainPoints[maxStrainLocationFayRepairConfig].TangentStrain * param.Em_Plate * 10e6;
                    results.RepairKcKt = -results.combinedFayRepairConfig[0].StrainPoints[maxStrainLocationFayRepairConfig].TangentStrain / results.combinedFayRepairConfig[1].StrainPoints[maxStrainLocationFayRepairConfig].TangentStrain;
                    results.maxAngleRepair = results.combinedFayRepairConfig[1].StrainPoints[maxStrainLocationFayRepairConfig].Angle;
                    results.faySurfaceRepairConfig = true;
                    results.TangentRepairSG = results.combinedRepair01Config[1].StrainPoints[maxStrainLocationFayRepairConfig].TangentStress / results.combinedRepairConfig[1].StrainPoints[maxStrainLocationFayRepairConfig].TangentStress;
                    results.PrincipalRepairSG = Lists[5][1].StrainPoints[maxStrainLocationFayRepairConfig].PrincStress / Lists[4][1].StrainPoints[maxStrainLocationFayRepairConfig].PrincStress;
                }
                else
                {
                    results.KtPeakRepair = 1; //set this to 1 if MS is winner
                    results.RepairKtS = results.combinedMidRepairConfig[1].StrainPoints[maxStrainLocationMidRepairConfig].TangentStrain * param.Em_Plate * 10e6;
                    results.RepairKcKt = -results.combinedMidRepairConfig[0].StrainPoints[maxStrainLocationMidRepairConfig].TangentStrain / results.combinedMidRepairConfig[1].StrainPoints[maxStrainLocationMidRepairConfig].TangentStrain;
                    results.maxAngleRepair = results.combinedMidRepairConfig[1].StrainPoints[maxStrainLocationMidRepairConfig].Angle;
                    results.faySurfaceRepairConfig = false;
                    results.TangentRepairSG = results.combinedRepair01Config[1].StrainPoints[maxStrainLocationMidRepairConfig].TangentStress / results.combinedRepairConfig[1].StrainPoints[maxStrainLocationMidRepairConfig].TangentStress;
                    results.PrincipalRepairSG = Lists[5][1].StrainPoints[maxStrainLocationMidRepairConfig].PrincStress / Lists[4][1].StrainPoints[maxStrainLocationMidRepairConfig].PrincStress;
                }
            }
            else //Blue Print Case
            {

                results.combinedMidBPConfig = Lists[0];
                results.combinedFayBPConfig = Lists[1];
                results.combinedBPConfig = Lists[2];
                results.combinedBP01Config = Lists[3];
                results.combinedRepairBPConfig = Lists[4];
                results.combinedRepairBP01Config = Lists[5];

                List<ResultTable> resTableMSBP = new List<ResultTable>();
                List<ResultTable> resTableFAYBP = new List<ResultTable>();
                for (int i = 0; i < Lists[0][1].StrainPoints.Count(); i++)
                {
                    resTableMSBP.Add(new ResultTable());
                    resTableFAYBP.Add(new ResultTable());
                }
                for (int i = 0; i < Lists[0][1].StrainPoints.Count(); i++)
                {
                    resTableMSBP[i].Angle = (Lists[2][1].StrainPoints[i].Angle);
                    resTableMSBP[i].TangentStrainToStressBrg = (Lists[2][1].StrainPoints[i].TangentStrain) * param.Em_Plate * 10e6;
                    resTableMSBP[i].TangentStrainToStressBrgNeg = (Lists[2][0].StrainPoints[i].TangentStrain) * param.Em_Plate * 10e6;
                    resTableMSBP[i].KtCSK = results.ktCSKBP;
                    resTableMSBP[i].Kttd = results.KttdBP;
                    resTableMSBP[i].TangentStrainToStressBrgFactored = Lists[0][1].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableMSBP[i].TangentStrainToStressBrgNegFactored = Lists[0][0].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableMSBP[i].KcKt = -resTableMSBP[i].TangentStrainToStressBrgNegFactored / resTableMSBP[i].TangentStrainToStressBrgFactored;

                    resTableFAYBP[i].Angle = resTableMSBP[i].Angle;
                    resTableFAYBP[i].TangentStrainToStressBrg = resTableMSBP[i].TangentStrainToStressBrg;
                    resTableFAYBP[i].TangentStrainToStressBrgNeg = resTableMSBP[i].TangentStrainToStressBrgNeg;
                    resTableFAYBP[i].KtPeak = results.KtPeakBP;
                    resTableFAYBP[i].TangentStrainToStressBrgFactored = Lists[1][1].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableFAYBP[i].TangentStrainToStressBrgNegFactored = Lists[1][0].StrainPoints[i].TangentStrain * param.Em_Plate * 10e6;
                    resTableFAYBP[i].KcKt = -resTableFAYBP[i].TangentStrainToStressBrgNegFactored / resTableFAYBP[i].TangentStrainToStressBrgFactored;
                }

                resTableFAYBP.RemoveAt(Lists[0][1].StrainPoints.Count() - 1);
                resTableMSBP.RemoveAt(Lists[0][1].StrainPoints.Count() - 1);

                results.resultTableMSBP = resTableMSBP;
                results.resultTableFAYBP = resTableFAYBP;

                int maxStrainLocationFay = results.combinedFayBPConfig[1].StrainPoints.FindIndex(index => index.TangentStrain.Equals(results.combinedFayBPConfig[1].StrainPoints.Max(max => max.TangentStrain)));
                int maxStrainLocationMid = results.combinedMidBPConfig[1].StrainPoints.FindIndex(index => index.TangentStrain.Equals(results.combinedMidBPConfig[1].StrainPoints.Max(max => max.TangentStrain)));

                if (results.combinedFayBPConfig[1].StrainPoints[maxStrainLocationFay].TangentStrain > results.combinedMidBPConfig[1].StrainPoints[maxStrainLocationMid].TangentStrain)
                {
                    results.KttdBP = 1;  //set this to 1 if fay is winner
                    results.ktCSKBP = 1; //same as above
                    results.BPKtS = results.combinedFayBPConfig[1].StrainPoints[maxStrainLocationFay].TangentStrain * param.Em_Plate * 10e6;
                    results.BPKcKt = -results.combinedFayBPConfig[0].StrainPoints[maxStrainLocationFay].TangentStrain / results.combinedFayBPConfig[1].StrainPoints[maxStrainLocationFay].TangentStrain;
                    results.maxAngle = results.combinedFayBPConfig[1].StrainPoints[maxStrainLocationFay].Angle;
                    results.faySurface = true;
                    results.TangentBPSG = results.combinedBP01Config[1].StrainPoints[maxStrainLocationFay].TangentStress / results.combinedBPConfig[1].StrainPoints[maxStrainLocationFay].TangentStress;
                    results.PrincipalBPSG = Lists[7][1].StrainPoints[maxStrainLocationFay].PrincStress / Lists[6][1].StrainPoints[maxStrainLocationFay].PrincStress;
                    results.RepairBndyStress = results.combinedRepairBPConfig[1].StrainPoints[maxStrainLocationFay].TangentStrain * param.Em_Plate * 10e6;
                    results.TangentRepairBndySG = results.combinedRepairBP01Config[1].StrainPoints[maxStrainLocationFay].TangentStress / results.combinedRepairBPConfig[1].StrainPoints[maxStrainLocationFay].TangentStress;
                    results.PrincipalRepairBndySG = Lists[9][1].StrainPoints[maxStrainLocationFay].PrincStress / Lists[8][1].StrainPoints[maxStrainLocationFay].PrincStress;
                }
                else
                {
                    results.KtPeakBP = 1; //set this to 1 if mid is winner
                    results.BPKtS = results.combinedMidBPConfig[1].StrainPoints[maxStrainLocationMid].TangentStrain * param.Em_Plate * 10e6;
                    results.BPKcKt = -results.combinedMidBPConfig[0].StrainPoints[maxStrainLocationMid].TangentStrain / results.combinedMidBPConfig[1].StrainPoints[maxStrainLocationMid].TangentStrain;
                    results.maxAngle = results.combinedMidBPConfig[1].StrainPoints[maxStrainLocationMid].Angle;
                    results.faySurface = false;
                    results.TangentBPSG = results.combinedBP01Config[1].StrainPoints[maxStrainLocationMid].TangentStress / results.combinedBPConfig[1].StrainPoints[maxStrainLocationMid].TangentStress;
                    results.PrincipalBPSG = Lists[7][1].StrainPoints[maxStrainLocationMid].PrincStress / Lists[6][1].StrainPoints[maxStrainLocationMid].PrincStress;
                    results.RepairBndyStress = results.combinedRepairBPConfig[1].StrainPoints[maxStrainLocationMid].TangentStrain * param.Em_Plate * 10e6;
                    results.TangentRepairBndySG = results.combinedRepairBP01Config[1].StrainPoints[maxStrainLocationMid].TangentStress / results.combinedRepairBPConfig[1].StrainPoints[maxStrainLocationMid].TangentStress;
                    results.PrincipalRepairBndySG = Lists[9][1].StrainPoints[maxStrainLocationMid].PrincStress / Lists[8][1].StrainPoints[maxStrainLocationMid].PrincStress;
                }
            }
        }
        /// <summary>
        /// Combines negative and positive load results at same location into a single list
        /// </summary>
        /// <param name="firstExtractionName">Negative extracted results name</param>
        /// <param name="secondExtractionName">positive extracted results name</param>
        /// <param name="storedExtractions">storedExtractions</param>
        /// <param name="holeNumber">Current hole number (0 if single hole or lug analysis) </param>
        /// <param name="Lists">Extracted strain group lists (example all bearing or all bypass)</param>
        public void ExtractToList(string firstExtractionName, string secondExtractionName, List<ResultData.ExtractedSCData> storedExtractions, int holeNumber, params List<ExtractedStrainGroup>[] Lists)
        {
            List<ResultData.ExtractedSCData> TempA = new List<ResultData.ExtractedSCData>(storedExtractions);
            List<ResultData.ExtractedSCData> TempB = new List<ResultData.ExtractedSCData>(storedExtractions);

            if (holeNumber == 0) //for single hole and lug
            {
                TempA.RemoveAll(item => item.extractSetName != firstExtractionName);
                TempB.RemoveAll(item => item.extractSetName != secondExtractionName);
            }
            else  //multi-hole
            {
                TempA.RemoveAll(item => !item.extractSetName.Contains("#" + holeNumber.ToString()));
                TempB.RemoveAll(item => !item.extractSetName.Contains("#" + holeNumber.ToString()));
                TempA.RemoveAll(item => !item.extractSetName.Contains(firstExtractionName));
                TempB.RemoveAll(item => !item.extractSetName.Contains(secondExtractionName));
                if (firstExtractionName.Contains("0.01"))
                {
                    TempA.RemoveAll(item => !item.extractSetName.Contains("0.01"));
                    TempB.RemoveAll(item => !item.extractSetName.Contains("0.01"));
                }
                else
                {
                    TempA.RemoveAll(item => item.extractSetName.Contains("0.01"));
                    TempB.RemoveAll(item => item.extractSetName.Contains("0.01"));
                }

                if (firstExtractionName.Contains("R_") && TempA.Count > 1)
                {
                    TempA.RemoveAll(item => !item.extractSetName.Contains("R_"));
                    TempB.RemoveAll(item => !item.extractSetName.Contains("R_"));
                }
                else if (TempA.Count > 1)
                {
                    TempA.RemoveAll(item => item.extractSetName.Contains("R_"));
                    TempB.RemoveAll(item => item.extractSetName.Contains("R_"));
                }
            }

            ExtractedStrainGroup Temp = reFormatData(TempA);
            ExtractedStrainGroup Temp2 = reFormatData(TempB);

            foreach (var list in Lists)
            {
                list.Add(Temp.DeepCopy(param));
                list.Add(Temp2.DeepCopy(param));
            }
        }

        public double ExtractGlobalError(string extractionName, List<ResultData.ExtractedSCData> storedExtractions)
        {
            List<ResultData.ExtractedSCData> TempA = new List<ResultData.ExtractedSCData>(storedExtractions);
            TempA.RemoveAll(item => item.extractSetName != extractionName);
            int index1 = TempA[0].columns.Count() - 1;
            int index2 = TempA[0].columns[index1].data.Count() - 1;
            return TempA[0].columns[index1].data[index2];
        }

        public double[] ExtractLocalError(string extractionName, List<ResultData.ExtractedSCData> storedExtractions)
        {
            List<ResultData.ExtractedSCData> TempA = new List<ResultData.ExtractedSCData>(storedExtractions);
            TempA.RemoveAll(item => item.extractSetName != extractionName);
            TempA[0].columns.RemoveAll(item => item.headerName != "Max. S1");

            double[] temp = new double[TempA[0].columns[0].data.Count()];
            for (int i = 0; i < TempA[0].columns[0].data.Count(); i++)
            {
                temp[i] = TempA[0].columns[0].data[i];
            }

            return temp;
        }

        public class CreateBypassLists
        {
            public List<ExtractedStrainGroup> factoredBypassMidSurfaceBP = new List<ExtractedStrainGroup>();  //mid surface
            public List<ExtractedStrainGroup> factoredBypassFaySurfaceBP = new List<ExtractedStrainGroup>();  //fay
            public List<ExtractedStrainGroup> unfactoredBypassBP = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> unfactoredBypassBP01 = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> unfactoredBypassBPRepairLoc = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> unfactoredBypassBPRepairLoc01 = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> factoredBypassMidSurfaceRepair = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> factoredBypassFaySurfaceRepair = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> unfactoredBypassRepairConfig = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> unfactoredBypassRepairConfig01 = new List<ExtractedStrainGroup>();
        }

        public class CreateBearingLists
        {
            public List<ExtractedStrainGroup> factoredBearingMidSurfaceBP = new List<ExtractedStrainGroup>();  //mid surface
            public List<ExtractedStrainGroup> factoredBearingFaySurfaceBP = new List<ExtractedStrainGroup>(); //fay
            public List<ExtractedStrainGroup> unfactoredBearingBP = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> unfactoredBearingBP01 = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> unfactoredBearingBPRepairLoc = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> unfactoredBearingBPRepairLoc01 = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> factoredBearingMidSurfaceRepair = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> factoredBearingFaySurfaceRepair = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> unfactoredBearingRepairConfig = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> unfactoredBearingRepairConfig01 = new List<ExtractedStrainGroup>();
            
        }

        public class CreatePrincipalLists
        {
            public List<ExtractedStrainGroup> principalBP = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> principalBP01 = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> principalBPRepairLoc = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> principalBPRepairLoc01 = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> principalRepair = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> principalRepair01 = new List<ExtractedStrainGroup>();
        }

        public class ResultsFormatted
        {
            public List<ExtractedStrainGroup> combinedMidBPConfig = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> combinedFayBPConfig = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> combinedBPConfig = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> combinedBP01Config = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> combinedRepairBPConfig = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> combinedRepairBP01Config = new List<ExtractedStrainGroup>();

            public List<ExtractedStrainGroup> combinedMidRepairConfig = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> combinedFayRepairConfig = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> combinedRepairConfig = new List<ExtractedStrainGroup>();
            public List<ExtractedStrainGroup> combinedRepair01Config = new List<ExtractedStrainGroup>();

            public List<ResultTable> resultTableMSBP = new List<ResultTable>();
            public List<ResultTable> resultTableFAYBP = new List<ResultTable>();

            public List<ResultTable> resultTableMSRepair = new List<ResultTable>();
            public List<ResultTable> resultTableFAYRepair = new List<ResultTable>();

            //Maximum Blue Print Location
            public double BPKtS, BPKcKt, TangentBPSG, PrincipalBPSG, RepairBndyStress, TangentRepairBndySG, PrincipalRepairBndySG, ktCSKBP, KttdBP, KtPeakBP, KtNFTensBP, KtNFCompBP, GlobalErrorBP, maxAngle;
            public double[,] neatFitFactsBP;
            public double[] PrincipalConvergeBP;
            public bool faySurface;

            //Maximum Repair Location
            public double RepairKtS, RepairKcKt, TangentRepairSG, PrincipalRepairSG, ktCSKRepair, KttdRepair, KtPeakRepair, KtNFTensRepair, KtNFCompRepair, GlobalErrorRepair, maxAngleRepair;
            public double[,] neatFitFactsRepair;
            public double[] PrincipalConvergeRepair;
            public bool faySurfaceRepairConfig;

            //Blue Print at Maximum Repair Location
            public double BPKtS2, BPKcKt2, TangentBPSG2, PrincipalBPSG2, RepairBndyStress2, TangentRepairBndySG2, PrincipalRepairBndySG2, ktCSKBP2, KttdBP2, KtPeakBP2, KtNFTensBP2, KtNFCompBP2, maxAngle2;
            public double[,] neatFitFactsBP2;
            public bool faySurface2;

            public string errorFactors = "";
        }

        public ExtractedStrainGroup reFormatData(List<ResultData.ExtractedSCData> List)
        {
            ExtractedStrainGroup Temp = new ExtractedStrainGroup();
            Temp.extractSetName = List[0].extractSetName;

            if (Temp.extractSetName.Contains("Comb"))
            {
                for (int i = 0; i < List[0].columns[0].data.Count; i++)
                {
                    ExtractedStrains tempStrains = new ExtractedStrains();
                    tempStrains.X = List[0].columns[1].data[i];
                    tempStrains.Y = List[0].columns[2].data[i];
                    tempStrains.PrincStress = List[0].columns[4].data[i];
                    Temp.StrainPoints.Add(tempStrains);
                }

                return Temp;
            }
            else
            {
                for (int i = 0; i < List[0].columns[0].data.Count; i++)
                {
                    ExtractedStrains tempStrains = new ExtractedStrains();
                    tempStrains.X = List[0].columns[1].data[i];
                    tempStrains.Y = List[0].columns[2].data[i];
                    tempStrains.TangentStrain = List[0].columns[4].data[i];
                    tempStrains.TangentStress = List[0].columns[5].data[i];
                    tempStrains.PrincStress = List[0].columns[6].data[i];
                    Temp.StrainPoints.Add(tempStrains);
                }

                return Temp;
            }
        }
        public class ExtractedStrains
        {
            public double Angle, X, Y, TangentStrain, TangentStress, PrincStress;

            public ExtractedStrains DeepCopy(SingleHoleParameterData param)
            {
                ExtractedStrains Temp = new ExtractedStrains();
                Temp.Angle = Math.Atan2(Y - param.holeY, X - param.holeX) * 180 / Math.PI;
                if (Math.Abs(Temp.Angle) < 1e-6)
                    Temp.Angle = 0;
                if (Temp.Angle < 0)
                    Temp.Angle = Temp.Angle + 360.0;
                Temp.X = X;
                Temp.Y = Y;
                Temp.TangentStrain = TangentStrain;
                Temp.TangentStress = TangentStress;
                Temp.PrincStress = PrincStress;
                return Temp;
            }
        }

        public class ExtractedStrainGroup
        {
            public string extractSetName;
            public List<ExtractedStrains> StrainPoints = new List<ExtractedStrains>();

            public ExtractedStrainGroup DeepCopy(SingleHoleParameterData param)
            {
                ExtractedStrainGroup Temp = new ExtractedStrainGroup();
                Temp.extractSetName = extractSetName;
                foreach (var s in StrainPoints)
                {
                    Temp.StrainPoints.Add(s.DeepCopy(param));
                }
                return Temp;
            }
        }

        public class ResultTable
        {
            public double Angle { get; set; }
            public double TangentStrainToStressBrg { get; set; }
            public double TangentStrainToStressBrgNeg { get; set; }
            public double TangentStrainToStressBrgFactored { get; set; }
            public double TangentStrainToStressBrgNegFactored { get; set; }
            public double TangentStrainToStressBypFactored { get; set; }
            public double TangentStrainToStressBypNegFactored { get; set; }
            public double TangentStrainToStressByp { get; set; }
            public double TangentStrainToStressBypNeg { get; set; }
            public double TangentStrainToStressComb { get; set; }
            public double TangentStrainToStressCombNeg { get; set; }
            public double KtCSK { get; set; }
            public double Kttd { get; set; }
            public double KtNFTens { get; set; }
            public double KtNFComp { get; set; }
            public double KcKt { get; set; }
            public double KtPeak { get; set; }
        }

        public SingleHoleParameterData XMLtoData(InputData parameters, bool repairAnalysis, bool repairAnalysisExists, int holeNumber)
        {
            SingleHoleParameterData paramAssign = new SingleHoleParameterData();

            if (parameters.fileName == InputData.handbook.SingleHole)
            {
                paramAssign.repairAnalysisExists = repairAnalysisExists;

                paramAssign.holeX = double.Parse(parameters.varsBP["dx"].ToString());
                paramAssign.holeY = double.Parse(parameters.varsBP["dy"].ToString());
                paramAssign.holeDia = double.Parse(parameters.varsBP["do"].ToString());
                paramAssign.th = double.Parse(parameters.varsBP["th"].ToString());
                paramAssign.Em_Plate = double.Parse(parameters.varsBP["Em"].ToString()) / 10e6;
                paramAssign.csk = bool.Parse(parameters.analysisVars["CSK"].ToString());
                paramAssign.cskDepth = double.Parse(parameters.analysisVars["cskDepth"].ToString());
                paramAssign.neatFit = bool.Parse(parameters.analysisVars["neatFit"].ToString());
                paramAssign.Em_Fastener = double.Parse(parameters.analysisVars["Em_Fastener"].ToString()) / 10e6;
                paramAssign.repairAnalysis = false;

                if (repairAnalysis)
                {
                    paramAssign.holeDiaRepair = double.Parse(parameters.varsRepair["do"].ToString());
                    paramAssign.thRepair = double.Parse(parameters.varsRepair["th"].ToString());
                    paramAssign.cskRepair = bool.Parse(parameters.analysisVarsRepair["CSK"].ToString());
                    paramAssign.cskDepthRepair = double.Parse(parameters.analysisVarsRepair["cskDepth"].ToString());
                    paramAssign.neatFitRepair = bool.Parse(parameters.analysisVarsRepair["neatFit"].ToString());
                    paramAssign.Em_FastenerRepair = double.Parse(parameters.analysisVarsRepair["Em_Fastener"].ToString()) / 10e6;
                    paramAssign.repairAnalysis = true;
                }

                return paramAssign;
            }

            else if (parameters.fileName == InputData.handbook.Lug)
            {
                paramAssign.repairAnalysisExists = repairAnalysisExists;

                paramAssign.csk = bool.Parse(parameters.analysisVars["CSK"].ToString());
                paramAssign.cskDepth = double.Parse(parameters.analysisVars["cskDepth"].ToString());
                paramAssign.Em_Fastener = double.Parse(parameters.analysisVars["Em_Fastener"].ToString()) / 10e6;

                paramAssign.holeDia = double.Parse(parameters.varsBP["Di"].ToString());
                paramAssign.th = double.Parse(parameters.varsBP["th"].ToString());
                paramAssign.Em_Plate = double.Parse(parameters.varsBP["Em"].ToString()) / 10e6;
                paramAssign.holeX = double.Parse(parameters.varsBP["W"].ToString()) + double.Parse(parameters.varsBP["W"].ToString()) / 2.0 - double.Parse(parameters.varsBP["ed"].ToString());
                paramAssign.holeY = 0.5 * double.Parse(parameters.varsBP["W"].ToString());

                paramAssign.repairAnalysis = false;

                if (repairAnalysis)
                {
                    paramAssign.holeDiaRepair = double.Parse(parameters.varsRepair["Di"].ToString());
                    paramAssign.thRepair = double.Parse(parameters.varsRepair["th"].ToString());
                    paramAssign.cskRepair = bool.Parse(parameters.analysisVarsRepair["CSK"].ToString());
                    paramAssign.cskDepthRepair = double.Parse(parameters.analysisVarsRepair["cskDepth"].ToString());
                    paramAssign.Em_FastenerRepair = double.Parse(parameters.analysisVarsRepair["Em_Fastener"].ToString()) / 10e6;
                    paramAssign.repairAnalysis = true;
                }

                return paramAssign;
            }
            else if (parameters.fileName == InputData.handbook.MultiHole)
            {
                paramAssign.repairAnalysisExists = repairAnalysisExists;

                paramAssign.currentHoleNumber = holeNumber;

                paramAssign.holeX = double.Parse(parameters.varsBP["dx" + holeNumber].ToString());
                paramAssign.holeY = double.Parse(parameters.varsBP["dy" + holeNumber].ToString());
                paramAssign.holeDia = double.Parse(parameters.varsBP["do" + holeNumber].ToString());
                paramAssign.th = double.Parse(parameters.varsBP["th"].ToString());
                paramAssign.Em_Plate = double.Parse(parameters.varsBP["Em"].ToString()) / 10e6;
                paramAssign.csk = bool.Parse(parameters.analysisVars["CSK" + holeNumber].ToString());
                paramAssign.cskDepth = double.Parse(parameters.analysisVars["cskDepth" + holeNumber].ToString());
                paramAssign.neatFit = bool.Parse(parameters.analysisVars["neatFit" + holeNumber].ToString());
                paramAssign.Em_Fastener = double.Parse(parameters.analysisVars["Em_Fastener" + holeNumber].ToString()) / 10e6;
                paramAssign.repairAnalysis = false;

                if (repairAnalysis)
                {
                    paramAssign.holeDiaRepair = double.Parse(parameters.varsRepair["do" + holeNumber].ToString());
                    paramAssign.thRepair = double.Parse(parameters.varsRepair["th"].ToString());
                    paramAssign.cskRepair = bool.Parse(parameters.analysisVarsRepair["CSK" + holeNumber].ToString());
                    paramAssign.cskDepthRepair = double.Parse(parameters.analysisVarsRepair["cskDepth" + holeNumber].ToString());
                    paramAssign.neatFitRepair = bool.Parse(parameters.analysisVarsRepair["neatFit" + holeNumber].ToString());
                    paramAssign.Em_FastenerRepair = double.Parse(parameters.analysisVarsRepair["Em_Fastener" + holeNumber].ToString()) / 10e6;
                    paramAssign.repairAnalysis = true;
                }

                return paramAssign;
            }
            else { return paramAssign; }
        }
    }
}
