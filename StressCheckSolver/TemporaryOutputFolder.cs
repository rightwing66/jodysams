using System;
using System.IO;

namespace StressCheckSolver
{
    /// <summary>
    /// Creates a temporary directory for storing temporary files. Deletes the files when no longer in use.
    /// </summary>
    public class TemporaryOutputFolder : OutputFolder
    {
        /// <summary>
        /// Create a new temporary folder.
        /// </summary>
        /// <param name="cleanupSelf">If true, temp folder gets deleted. If false, temp folder is not deleted.</param>
        public TemporaryOutputFolder()
        {
            Random rand = new Random();
            int r = rand.Next((int)1e9);
            DateTime dt = DateTime.Now;
            string timestamp = dt.Year + dt.Month.ToString() + dt.Day +
                               dt.Hour + dt.Minute + dt.Second + dt.Millisecond;
            string randFolderName = "Temp" + timestamp + "." + r;
            string systemtemp = Path.GetTempPath();
            fullPath = Path.Combine(systemtemp, "iSAMSTempFolder", randFolderName);

            CreateFolder();
        }

        ~TemporaryOutputFolder()
        {
            // This is probably not the best way to clean up, especially if any of the files are still in use
            try
            {
                if (Directory.Exists(fullPath))
                {
                    Directory.Delete(fullPath, true);
                }
            }
            catch (Exception) { }
        }
    }
}