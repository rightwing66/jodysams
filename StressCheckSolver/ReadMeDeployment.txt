﻿I have added changes StressCheckSolver so that all console output is now being written to a log (Exceptions still bubble to the console.)
The logs will by default appear in c:\ C:\Logs\ISAMS\I2ES.  This directory must exist and the executing identity must have write and delete privileges to it.
Logging is controlled through Logging.cs, and is configured in ISAMSLog4Net.xml.  The log output directory is configured in this xml file.

The publication list has changed.  At a minimum, you must now publish:

-StressCheckSolver.exe
-StressCheckSolver.exe.config
-log4net.dll
-ISAMSLog4Net.xml
