﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Linq;

namespace StressCheckSolver {
    /// <summary>
    ///   Load input data from xml stream and represent as dictionary.
    /// </summary>
    public class InputData {
        private static readonly log4net.ILog log =
                log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        /// <summary>
        ///   Generate new input data from an XML input file
        /// </summary>
        /// <param name="InputFile">xml parameter file</param>
        public InputData(string InputFile) {
            log.Debug("creating XmlDocument");
            XmlDocument infile = new XmlDocument();
            log.Debug("in directory: " + System.IO.Directory.GetCurrentDirectory());
            log.Debug("Opening file: " + InputFile);
            try {
                infile.Load(InputFile);
            }
            catch (IOException ioe) {
                log.Debug("Failed to open file: " + InputFile);
                log.Debug("Received exception: " + ioe.Message);
                log.Debug("io exception: " + ioe.StackTrace);
                throw;
                //Todo: do we really need to sleep here?

            }

            log.Debug("Loaded InputFile");
            XmlNode elInfo = infile.GetElementsByTagName("information")[0];
            XmlNode elFiles = infile.GetElementsByTagName("files")[0];
            if (elFiles == null) { throw new Exception("Input.xml is missing the 'files' element"); }
            XmlNode elMaterials = infile.GetElementsByTagName("materials")[0];

            List<NodeSplit> tempNodeList = new List<NodeSplit>();
            foreach (XmlNode singleNode in infile.GetElementsByTagName("parameters")[0].ChildNodes) {
                NodeSplit temp = new NodeSplit();
                temp.node = singleNode;
                temp.nodeName = singleNode.Attributes.GetNamedItem("name").InnerText;
                tempNodeList.Add(temp);
            }

            Func<NodeSplit, XmlNode> nodeNullCheck = new Func<NodeSplit, XmlNode>((test) => test == null ? null : test.node);

            XmlNode elParams = nodeNullCheck(tempNodeList.Find(name => name.nodeName.Equals("printParameters")));
            XmlNode elAnalysisParams = nodeNullCheck(tempNodeList.Find(name => name.nodeName.Equals("otherPrintParameters")));
            XmlNode elRepairParams = nodeNullCheck(tempNodeList.Find(name => name.nodeName.Equals("repairParameters")));
            XmlNode elRepairAnalysisParams = nodeNullCheck(tempNodeList.Find(name => name.nodeName.Equals("otherRepairParameters")));
            XmlNode elFatigueLoad = nodeNullCheck(tempNodeList.Find(name => name.nodeName.Equals("fatigueLoad")));
            XmlNode elStaticLoad = nodeNullCheck(tempNodeList.Find(name => name.nodeName.Equals("staticLoad")));

            log.Debug("Got Elements by tagname");

            if (elFiles.SelectSingleNode("file").InnerText == "Single_Hole.scw") {
                fileName = handbook.SingleHole;
            }
            else if (elFiles.SelectSingleNode("file").InnerText == "Lug2D.scw") {
                fileName = handbook.Lug;
            }
            else if (elFiles.SelectSingleNode("file").InnerText == "MultiHole.scw") {
                fileName = handbook.MultiHole;
            }

            log.Debug("got Handbook: " + fileName.ToString());
            jobid = elInfo.SelectSingleNode("jobid").InnerText;
            log.Debug("got jobid: " + jobid);

            varsBP = new Hashtable(); //SC BP variables
            foreach (XmlNode data_node in elParams.ChildNodes) {
                XmlAttribute data_name = (XmlAttribute)data_node.Attributes.GetNamedItem("name");
                string data_value = data_node.InnerText;

                log.Debug("Adding vars: " + data_name.Value + " = " + data_value);
                varsBP.Add(data_name.Value, data_value);
            }

            analysisVars = new Hashtable(); //Non SC variables
            foreach (XmlNode data_node in elAnalysisParams.ChildNodes) {
                XmlAttribute data_name = (XmlAttribute)data_node.Attributes.GetNamedItem("name");
                string data_value = data_node.InnerText;

                log.Debug("Adding additional vars: " + data_name.Value + " = " + data_value);
                analysisVars.Add(data_name.Value, data_value);
            }
            try {
                varsRepair = new Hashtable(); //SC Repair variables
                foreach (XmlNode data_node in elRepairParams.ChildNodes) {
                    XmlAttribute data_name = (XmlAttribute)data_node.Attributes.GetNamedItem("name");
                    string data_value = data_node.InnerText;

                    log.Debug("Adding repair vars: " + data_name.Value + " = " + data_value);
                    varsRepair.Add(data_name.Value, data_value);
                }
            }
            catch {
                if (varsRepair.Count <= 0) {
                    varsRepair = null;
                }
            }
            try {
                analysisVarsRepair = new Hashtable(); //Non SC Repair variables
                foreach (XmlNode data_node in elRepairAnalysisParams.ChildNodes) {
                    XmlAttribute data_name = (XmlAttribute)data_node.Attributes.GetNamedItem("name");
                    string data_value = data_node.InnerText;

                    log.Debug("Adding additional repair vars: " + data_name.Value + " = " + data_value);
                    analysisVarsRepair.Add(data_name.Value, data_value);
                }
            }
            catch {
                if (analysisVarsRepair.Count <= 0) {
                    analysisVarsRepair = null;
                }
            }
            try {
                fatigueLoad = new Hashtable(); //fatigue load case
                foreach (XmlNode data_node in elFatigueLoad.ChildNodes) {
                    XmlAttribute data_name = (XmlAttribute)data_node.Attributes.GetNamedItem("name");
                    string data_value = data_node.InnerText;

                    log.Debug("Adding fatigue load: " + data_name.Value + " = " + data_value);
                    fatigueLoad.Add(data_name.Value, data_value);
                }
            }
            catch {
                if (fatigueLoad.Count <= 0) {
                    fatigueLoad = null;
                }
            }
            try {
                staticLoad = new Hashtable(); //static loads
                foreach (XmlNode data_node in elStaticLoad.ChildNodes) {
                    XmlAttribute data_name = (XmlAttribute)data_node.Attributes.GetNamedItem("name");
                    string data_value = data_node.InnerText;

                    log.Debug("Adding static load: " + data_name.Value + " = " + data_value);
                    staticLoad.Add(data_name.Value, data_value);
                }
            }
            catch {
                if (staticLoad.Count <= 0) {
                    staticLoad = null;
                }
            }
            try {
                materials = new Hashtable();
                foreach (XmlNode materialNode in elMaterials.ChildNodes) {
                    XmlAttribute materialName = (XmlAttribute)materialNode.Attributes.GetNamedItem("name");
                    string materialValue = materialNode.InnerText;

                    log.Debug("Adding material: " + materialName.Value + " = " + materialValue);
                    materials.Add(materialName.Value, materialValue);
                }
            }
            catch {
                if (materials.Count <= 0) {
                    materials = null;
                }
            }
            log.Debug("Exiting InputData");
        }

        public double variable(Hashtable variable, string key) {
            return System.Convert.ToDouble(variable[key].ToString());
        }

        public class NodeSplit {
            public XmlNode node;
            public string nodeName;
        }

        // DATA
        public enum handbook { SingleHole, Lug, MultiHole }; // handbook model to be solved
        public handbook fileName { get; set; }
        public string jobid;    // name of job
        public Hashtable varsBP;  // variables from input stream (SC Specific)
        public Hashtable analysisVars; //variables for analysis not used in SC parameters.
        public Hashtable varsRepair;  // variables from input stream (SC Specific)
        public Hashtable analysisVarsRepair; //variables for analysis not used in SC parameters.
        public Hashtable fatigueLoad;  // variables from input stream (SC Specific)
        public Hashtable staticLoad; //variables for analysis not used in SC parameters.
        public Hashtable materials;
    }
}