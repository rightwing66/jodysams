﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using StressCheck;
using SolverSupport;

namespace StressCheckSolver {
    public class Solver : ISolver {
        static private ResultData results;
        private static readonly log4net.ILog log =
                log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string OutputDirectoryPath { get; set; }
        public string InputFilePath { get; set; }
        public bool showStressCheck { get; set; }

        public void Init() {
//            Logging logging = new Logging();
//            logging.WireUpLogging();
            log.Debug("StressCheckSolver Starting ***************************************************************************");
            validateStartupParameters();
        }


        public void Solve() { //need input.xml to be the argument, second argument is output directory, third is show/hide SC
            copyInputFilesToOutputDirectory();
            UserOutputFolder userFolder = new UserOutputFolder(OutputDirectoryPath, true);
            InputData InputData = new InputData(OutputDirectoryPath + @"\Input.xml");
            results = new ResultData();
            log.Debug("Preparing to perform solve");
            PerformSolve(InputData, userFolder, showStressCheck);
            results.WriteData(OutputDirectoryPath + @"\Output.xml");
            System.Threading.Thread.Sleep(0);
        }

        void PerformSolve(InputData InputData, UserOutputFolder userFolder, bool showSC) {
            int status = 0;
            log.Debug("set status - 0");
            Application scapp = null;
            log.Debug("set scapp - null");
            Document scdoc = null;
            log.Debug("Starting StressCheck");
            try {
                log.Debug("new Application");
                scapp = new Application();

                log.Debug("setting variables");

                string startfile = "";
                byte[] scwfile;

                scdoc = scapp.Document;

                FileSupport fileSupport = new FileSupport();

                scdoc.ScratchDirectory = fileSupport.VerifyPathFromConfig("StressCheckScratchDirectory");

                scapp.Show(showSC);

                switch (InputData.fileName) //TODO add cases here as sci files are generated
                {
                    case InputData.handbook.Lug:
                        scwfile = Properties.Resources.Lug2D;
                        startfile = userFolder.AddFile("Lug2D.scw", scwfile);
                        scdoc.Open(startfile);
                        SingleHoleSolve(InputData, userFolder, scdoc, scapp, status);
                        break;
                    case InputData.handbook.SingleHole:
                        scwfile = Properties.Resources.Single_Hole;
                        startfile = userFolder.AddFile("Single_Hole.scw", scwfile);
                        scdoc.Open(startfile);
                        SingleHoleSolve(InputData, userFolder, scdoc, scapp, status);
                        break;
                    case InputData.handbook.MultiHole:
                        MultiHoleSolve(InputData, userFolder, scdoc, scapp, status);
                        break;
                }
            }
            catch (System.Runtime.InteropServices.COMException e) {
                results.AddError(e.ErrorCode, e.Message);
                log.Debug("COM Exception: " + e.Message);
                throw;
            }
            catch (Exception e) {
                results.AddError(e.Message);
                log.Debug("Unexpected Exception: " + e.Message);
                status = 0;
                throw;
            }
        }

        private void copyInputFilesToOutputDirectory() {
            log.Debug("Copying input files to user directory.");
            try {
                File.Copy(InputFilePath, OutputDirectoryPath + @"\Input.xml");
            }
            catch {
                log.Debug("Files already exist. Deleting old files.");
                File.Delete(OutputDirectoryPath + @"\Input.xml");
                File.Delete(OutputDirectoryPath + @"\Output.xml");
                File.Copy(InputFilePath, OutputDirectoryPath + @"\Input.xml");
            }
        }

        private void validateStartupParameters() {
            if (string.IsNullOrEmpty(InputFilePath)) {
                throw new Exception("InputFilePath cannot be null or empty");
            }
            if (string.IsNullOrEmpty(Path.GetFileName(InputFilePath)) || string.IsNullOrEmpty(Path.GetExtension(InputFilePath))) {
                throw new Exception("InputFilePath must contain a file name");
            }
            if (string.IsNullOrEmpty(OutputDirectoryPath)) {
                throw new Exception("OutputDirectoryPath cannot be null or empty");
            }
        }


        static void SingleHoleSolve(InputData InputData, UserOutputFolder userFolder, StressCheck.Document scdoc, StressCheck.Application scapp, int status) {
            try {
                //ApplyMaterialProperties(InputData, scdoc);  //This currently does nothing

                if (InputData.fatigueLoad != null) {
                    PerformSingleHoleSolve(InputData, true, scdoc, userFolder);
                }

                if (InputData.staticLoad != null) {
                    PerformSingleHoleSolve(InputData, false, scdoc, userFolder);
                }

                log.Debug("Sleeping ...");
                System.Threading.Thread.Sleep(1000);

                status = 1;
            }
            catch (System.Runtime.InteropServices.COMException e) {
                results.AddError(e.ErrorCode, e.Message);
                log.Debug("COM Exception: " + e.Message);
                status = 0;
                throw;
            }
            catch (Exception e) {
                results.AddError(e.Message);
                log.Debug("Unexpected Exception: " + e.Message);
                status = 0;
                throw;
            }
            finally {
                log.Debug("Finishing solution");

                CloseStressCheck(scdoc, scapp);

                System.Threading.Thread.Sleep(3000);
                results.SetStatus(status);
            }
        }

        static void MultiHoleSolve(InputData InputData, UserOutputFolder userFolder, StressCheck.Document scdoc, StressCheck.Application scapp, int status) {
            try {
                if (InputData.fatigueLoad != null) {
                    PerformMultiHoleSolve(InputData, true, scdoc, userFolder);
                }

                if (InputData.staticLoad != null) {
                    PerformMultiHoleSolve(InputData, false, scdoc, userFolder);
                }

                log.Debug("Sleeping ...");
                System.Threading.Thread.Sleep(1000);

                status = 1;
            }
            catch (System.Runtime.InteropServices.COMException e) {
                results.AddError(e.ErrorCode, e.Message);
                log.Debug("COM Exception: " + e.Message);
                status = 0;

            }
            catch (Exception e) {
                results.AddError(e.Message);
                log.Debug("Unexpected Exception: " + e.Message);
                status = 0;

            }
            log.Debug("Finishing solution");

            CloseStressCheck(scdoc, scapp);

            System.Threading.Thread.Sleep(3000);
            results.SetStatus(status);
        }

        static void PerformSingleHoleSolve(InputData InputData, bool isFatigueAnalysis, StressCheck.Document scdoc, UserOutputFolder userFolder) {
            List<ResultData.ExtractedSCData> extractedSCData = new List<ResultData.ExtractedSCData>();
            SingleHole.ResultsFormatted resData = new SingleHole.ResultsFormatted();
            SingleHole sh = new SingleHole();
            Hashtable loadType = new Hashtable();
            bool repairExists = false;
            bool isRepairAnalysis;
            string analysisTypeStart;
            string analysisTypeEnd;

            if (isFatigueAnalysis) {
                analysisTypeStart = "Fatigue ";
                loadType = InputData.fatigueLoad;
            }
            else {
                analysisTypeStart = "Static ";
                loadType = InputData.staticLoad;
            }

            if (InputData.analysisVarsRepair != null) //Repair must be run first if it exists
            {
                analysisTypeEnd = "Repair";
                repairExists = true;
                isRepairAnalysis = true;

                UpdateParams(InputData.varsRepair, scdoc);
                UpdateParams(loadType, scdoc);
                extractedSCData = SolveAndExtract(scdoc, userFolder, analysisTypeStart + analysisTypeEnd);
                PostProcess(InputData.fileName, resData, sh, InputData, extractedSCData, isRepairAnalysis, repairExists);
            }
            analysisTypeEnd = "Blue Print";
            isRepairAnalysis = false;

            UpdateParams(InputData.varsBP, scdoc);
            UpdateParams(loadType, scdoc);
            extractedSCData = SolveAndExtract(scdoc, userFolder, analysisTypeStart + analysisTypeEnd);
            PostProcess(InputData.fileName, resData, sh, InputData, extractedSCData, isRepairAnalysis, repairExists);

            results.ToXML(resData, isFatigueAnalysis, repairExists, InputData.fileName, false);
        }

        static void PerformMultiHoleSolve(InputData InputData, bool isFatigueAnalysis, StressCheck.Document scdoc, UserOutputFolder userFolder) {
            int numHoles;
            string setNameBase = "Plate";
            List<ResultData.ExtractedSCData> extractedSCData = new List<ResultData.ExtractedSCData>();
            List<SingleHole.ResultsFormatted> resData = new List<SingleHole.ResultsFormatted>();
            SingleHole sh = new SingleHole();
            bool repairExists = false;
            bool isRepairAnalysis;
            string analysisTypeStart;
            string analysisTypeEnd;

            if (isFatigueAnalysis) {
                analysisTypeStart = "Fatigue ";
            }
            else {
                analysisTypeStart = "Static ";
            }

            if (InputData.analysisVarsRepair != null) //Repair must be run first if it exists
            {
                repairExists = true;
                isRepairAnalysis = true;
                analysisTypeEnd = "Repair";

                MultiHoleBuilder PlateBuildRepair = InitializeMultieHole(scdoc.Model, scdoc, InputData, setNameBase, isRepairAnalysis, repairExists, isFatigueAnalysis);

                BuildGeoMultiHolePlate(PlateBuildRepair, InputData, isRepairAnalysis, out numHoles);
                //ApplyMaterialProperties(InputData, scdoc);  //This currently does nothing

                //Solve and extract
                extractedSCData = SolveAndExtract(scdoc, userFolder, analysisTypeStart + analysisTypeEnd);

                for (int i = 0; i < numHoles; i++) {
                    SingleHole.ResultsFormatted tempData = new SingleHole.ResultsFormatted();
                    PostProcess(InputData.fileName, tempData, sh, InputData, extractedSCData, isRepairAnalysis, repairExists, i + 1);
                    resData.Add(tempData);
                }
            }
            //Create new document and make a plate
            isRepairAnalysis = false;
            analysisTypeEnd = "Blue Print";

            MultiHoleBuilder PlateBuildBluePrint = InitializeMultieHole(scdoc.Model, scdoc, InputData, setNameBase, isRepairAnalysis, repairExists, isFatigueAnalysis);

            BuildGeoMultiHolePlate(PlateBuildBluePrint, InputData, isRepairAnalysis, out numHoles);
            //ApplyMaterialProperties(InputData, scdoc);  //This currently does nothing

            //Solve and extract
            extractedSCData = SolveAndExtract(scdoc, userFolder, analysisTypeStart + analysisTypeEnd);

            for (int i = 0; i < numHoles; i++) {
                if (repairExists) {
                    PostProcess(InputData.fileName, resData[i], sh, InputData, extractedSCData, isRepairAnalysis, repairExists, i + 1);
                }
                else {
                    SingleHole.ResultsFormatted tempData = new SingleHole.ResultsFormatted();
                    PostProcess(InputData.fileName, tempData, sh, InputData, extractedSCData, isRepairAnalysis, repairExists, i + 1);
                    resData.Add(tempData);
                }
            }

            int currHole = 0;
            foreach (SingleHole.ResultsFormatted singleResData in resData) {
                currHole++;
                results.ToXML(singleResData, isFatigueAnalysis, repairExists, InputData.fileName, true, currHole);
            }
        }

        static MultiHoleBuilder InitializeMultieHole(StressCheck.Model SCModel, StressCheck.Document scdoc, InputData InputData, string setNameBase, bool isRepairAnalysis, bool repairExists, bool isFatigueAnalysis) {
            scdoc.New();
            scdoc.Model.Reference = ReferenceSystem.ref2D;

            MultiHoleBuilder tempHoleBuilder = new MultiHoleBuilder(scdoc.Model, scdoc, InputData, setNameBase, isRepairAnalysis, repairExists, isFatigueAnalysis);
            return tempHoleBuilder;
        }

        /// <summary>
        /// SC Pre-processing to build a multi-hole plate model
        /// </summary>
        /// <param name="PlateBuild">Input MulitHoleBuilder Constructor</param>
        /// <param name="InputData">XML InputData</param>
        /// <param name="repairAnalysis">bool for repair analysis</param>
        /// <param name="numHoles">return number of holes</param>
        static void BuildGeoMultiHolePlate(MultiHoleBuilder PlateBuild, InputData InputData, bool repairAnalysis, out int numHoles) {

            numHoles = CountHoles(InputData, repairAnalysis);

            PlateBuild.SetInitialDisplay();

            PlateBuild.CreateRectGeom(0, 0, 0);

            //Create the holes
            List<MultiHoleBuilder.HoleInformation> loadHoleInfo = PlateBuild.CreateFastenerHoles(numHoles);
            //Apply edge loading and loading to holes
            //Probably some additional work here based on navy requirements
            PlateBuild.ApplyBoundaryLoad(MultiHoleBuilder.plateEdgeLabel.xPositiveEdge);
            PlateBuild.ApplyBoundaryLoad(MultiHoleBuilder.plateEdgeLabel.xNegativeEdge);
            PlateBuild.ApplyBoundaryLoad(MultiHoleBuilder.plateEdgeLabel.yNegativeEdge);
            PlateBuild.ApplyBoundaryLoad(MultiHoleBuilder.plateEdgeLabel.yPositiveEdge);

            for (int i = 0; i < numHoles; i++) {
                PlateBuild.ApplyHoleLoad(loadHoleInfo, i);
            }
            //Constraints
            PlateBuild.AssignRigidBodyConstraints();

            //Create mesh and assign material and thickness
            PlateBuild.CreateMesh();
            PlateBuild.AssignThickness();
            PlateBuild.AssignIsoMaterial("Aluminum");
            PlateBuild.ConvertMapping();

            PlateBuild.SetupSolutionID();
            //Create Extraction Records
            List<List<MultiHoleBuilder.HoleInformation>> extractionCurves = PlateBuild.HoleExtractionCurves(loadHoleInfo);
            PlateBuild.CreateExtractions(extractionCurves);
            PlateBuild.SetDisplay();

        }

        static void CloseStressCheck(StressCheck.Document scdoc, StressCheck.Application scapp) {
            if (scdoc != null) {
                try {
                    scdoc.Close();
                }
                catch { }
            }

            if (scapp != null) {
                try {
                    scapp.Close(false);
                }
                catch { }
            }
        }

        static int CountHoles(InputData InputData, bool repairAnalysis) {
            //Count the number of holes
            int numHoles = 1;
            if (repairAnalysis) {
                while (true) {
                    if (InputData.varsRepair.ContainsKey("do" + numHoles.ToString())) {
                        numHoles++;
                    }
                    else {
                        numHoles--;
                        break;
                    }
                }
            }
            else {
                while (true) {
                    if (InputData.varsBP.ContainsKey("do" + numHoles.ToString())) {
                        numHoles++;
                    }
                    else {
                        numHoles--;
                        break;
                    }
                }
            }

            return numHoles;
        }

        static void UpdateParams(Hashtable variable, Document scdoc) {
            // set the parameters
            int count = 1;
            foreach (DictionaryEntry k in variable) {
                string varname = k.Key.ToString();
                double varval = variableConvert(variable, varname);
                bool update = false;

                if (count++ == variable.Count)
                    update = true;

                log.Debug("calling scdoc.ParameterAssign(\"" + varname + "\", \"" + varval + "\", " + update.ToString() + ");");

                scdoc.ParameterAssign(varname, varval, update);
            }
        }

        static double variableConvert(Hashtable variable, string key) {
            return System.Convert.ToDouble(variable[key].ToString());
        }

        static List<ResultData.ExtractedSCData> SolveAndExtract(StressCheck.Document scdoc, UserOutputFolder userFolder, string analysisName) {
            Solve(scdoc);
            AddPlot(scdoc, analysisName, userFolder);
            AddSCFile(scdoc, analysisName, userFolder);
            return ExtractRawData(scdoc);
        }

        static void Solve(Document scdoc) {
            log.Debug("Preparing to solve");

            // run each solution
            Solutions sols = scdoc.Solutions;
            foreach (Solution sol in sols) {
                log.Debug("Solving: " + sol.Name);
                sols.Solve(sol.Name);
            }
        }

        /// <summary>
        /// Apply material properties from Input XML if that section exists (currently not used)
        /// </summary>
        /// <param name="InputData">Input XML data</param>
        /// <param name="scdoc">StressCheck Document</param>
        static void ApplyMaterialProperties(InputData InputData, StressCheck.Document scdoc) {
            if (InputData.materials != null && InputData.materials.Count > 0 &&
                   scdoc.Model.Materials != null &&
                   scdoc.Model.MaterialAssignments != null) {
                log.Debug("Setting material properties");
                Materials materials = scdoc.Model.Materials;
                MaterialAssignments materialAssignments = scdoc.Model.MaterialAssignments;
                foreach (MaterialAssignment materialAssignment in materialAssignments) {
                    string setName = materialAssignment.SetName;
                    if (InputData.materials.ContainsKey(setName)) {
                        string materialName = (string)InputData.materials[setName];
                        foreach (Material material in materials) {
                            if (materialName == material.Name) {
                                log.Debug("Adding material: " + materialName);
                                materialAssignment.MaterialName = materialName;
                                materialAssignment.ColorName = setName;
                                materialAssignment.Update();
                                break;
                            }
                        }
                    }
                }
            }
        }

        static List<ResultData.ExtractedSCData> ExtractRawData(Document scdoc) {
            //Extract Data for additional post-processing in a common format
            List<ResultData.ExtractedSCData> storedExtractions = new List<ResultData.ExtractedSCData>();
            Extractions scExt = scdoc.Extractions;
            foreach (Extraction e in scExt) {
                ResultData.ExtractedSCData exdata = new ResultData.ExtractedSCData();
                DataTable dt = scExt.ExtractData(e.Name);
                object[,] dataTable = (object[,])dt.Data;
                string extname = e.Name;
                if (e.Name.IndexOf("-") > 0) {
                    extname = e.Name.Substring(e.Name.IndexOf("-") + 1);
                }
                exdata.extractSetName = extname;
                int cols = dt.Columns.Count;
                int rows = dt.rows.Count;
                for (int col = 0; col < cols; col++) {
                    ResultData.ExtractionColumn extValues = new ResultData.ExtractionColumn();
                    extValues.headerName = dt.Columns.Column[col].Label;
                    for (int row = 0; row < rows; row++) {
                        extValues.data.Add((double)dataTable[row, col]);
                    }
                    exdata.columns.Add(extValues);
                }
                storedExtractions.Add(exdata);
            }
            return storedExtractions;
        }

        /// <summary>
        /// Takes StressCheck extracted data for a single hole and performs FAPM factoring on the data
        /// </summary>
        /// <param name="fileName">Used to determine certain input parameters</param>
        /// <param name="resData">SingleHole.ResultsFormatted data to be returned</param>
        /// <param name="sh">SingleHole Constructor</param>
        /// <param name="InputData">XML Input Data</param>
        /// <param name="extractedSCData">Input SC Data as List of ResultData.ExtractedSCData</param>
        /// <param name="repairAnalysis">bool if this is repair analysis</param>
        /// <param name="repairAnalysisExists">bool if a repair analysis exists</param>
        /// <param name="holeNumber">optional current hole number (used only for multi-hole)</param>
        /// <returns></returns>
        static SingleHole.ResultsFormatted PostProcess(StressCheckSolver.InputData.handbook fileName, SingleHole.ResultsFormatted resData, SingleHole sh, InputData InputData, List<ResultData.ExtractedSCData> extractedSCData, bool repairAnalysis, bool repairAnalysisExists, int holeNumber = 1) {
            if (fileName == StressCheckSolver.InputData.handbook.Lug) //TODO all below needs to be in switch case instead probably
            {
                sh.SetInputs(InputData, repairAnalysis, repairAnalysisExists);
                sh.HoleResults(resData, extractedSCData, true);
            }
            else if (fileName == StressCheckSolver.InputData.handbook.SingleHole) {
                sh.SetInputs(InputData, repairAnalysis, repairAnalysisExists);
                sh.HoleResults(resData, extractedSCData, false);
            }
            else if (fileName == StressCheckSolver.InputData.handbook.MultiHole) {
                sh.SetInputs(InputData, repairAnalysis, repairAnalysisExists, holeNumber);
                sh.HoleResults(resData, extractedSCData, false);
            } //TODO other cases
            return resData;
        }

        static void AddPlot(Document scdoc, string outname, UserOutputFolder userFolder) {
            // generate each surface plot
            Plots plots = scdoc.Plots;
            Model model = scdoc.Model;
            foreach (Plot p in plots) {
                string plotname = p.Name;
                if (outname.IndexOf("-") > 0) {
                    outname = plotname.Substring(plotname.IndexOf("-") + 1);
                }
                outname += ".png";
                model.Display.ClearArtifacts();
                model.Display.Surfaces = false;
                model.Display.Curves = false;
                model.Display.Nodes = false;
                model.Display.Points = false;
                model.Display.Systems = false;
                model.Display.Update();
                log.Debug("Plotting: " + plotname);
                scdoc.xPlot(plotname, userFolder.fullPath + @"\" + outname, 10, 10);

                model.ObjectOperation(ObjectPick.opAll, ActionType.atUnblank);
                model.Display.Update();

                results.AddFile("SCImage", plotname, outname);
            }
        }
        static void AddSCFile(Document scdoc, string outname, UserOutputFolder userFolder) {
            outname += ".scp";
            log.Debug("Saving: " + outname);
            scdoc.SaveAs(userFolder.fullPath + @"\" + outname);
            results.AddFile("SCFile", "SCP File", outname);
        }
    }
}
