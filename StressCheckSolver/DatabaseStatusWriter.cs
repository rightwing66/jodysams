﻿using System;
using System.Reflection.Emit;
using I2ES.Model;
using SolverSupport;

namespace StressCheckSolver {
    public enum SolverState { Started = 1, Complete = 2, Error = 3 }
    public class DatabaseStatusWriter {
        private static readonly log4net.ILog log =
        log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void WriteState(SolverState newState, int jobId) {
            log.DebugFormat("WriteState newState:{0} jobId:{1}",newState,jobId);
            var wrapper = new ConfigurationWrapper();
            var connectionProvider = new ConnectionProvider();
            var sqlWrapper = connectionProvider.CreateWrapper();
            var compileUtilities = new CompileUtilities(){Namespace ="StressCheckSolver.sql" };
            try{
                string sql = compileUtilities.GetResourceValue("SetSolverStatus.sql")
                    .Replace("~OutcomeId", ((int)newState).ToString())
                    .Replace("~JobId", jobId.ToString());
                log.Debug(sql);
                sqlWrapper.RunSql(sql);
                sqlWrapper.Dispose();
            }
            catch (Exception e){
                log.Error(e);
                //otherwise eat the exception. We don't want to fail on database issues.
            }
        }
    }
}