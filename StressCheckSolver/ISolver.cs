﻿namespace StressCheckSolver
{
    public interface ISolver
    {
        string OutputDirectoryPath { get; set; }
        string InputFilePath { get; set; }
        bool showStressCheck { get; set; }
        void Init();
        void Solve();
    }
}