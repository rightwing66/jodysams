﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Linq;
using System.Linq.Expressions;

namespace StressCheckSolver {
    class ResultData {
        private static readonly log4net.ILog Log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

//        private readonly string _outfile;
        private XmlDocument xdoc;
        private XmlElement isams_output;
        private XmlElement isams_info;
        private XmlElement isams_files;
        private XmlElement isams_fatigue_results;
        private XmlElement isams_static_results;

        private Hashtable fatigueExtractions;
        private Hashtable staticExtractions;

        public static class MemberInfoGetting {
            public static string GetMemberName<T>(Expression<Func<T>> memberExpression) {
                MemberExpression expressionBody = (MemberExpression)memberExpression.Body;
                return expressionBody.Member.Name;
            }
        }

        public ResultData() {
            xdoc = new XmlDocument();
            isams_output = xdoc.CreateElement("isams_output");
            xdoc.AppendChild(isams_output);

            isams_info = xdoc.CreateElement("information");
            isams_output.AppendChild(isams_info);

            isams_files = xdoc.CreateElement("files");
            isams_output.AppendChild(isams_files);

            isams_fatigue_results = xdoc.CreateElement("fatigue_results");
            isams_output.AppendChild(isams_fatigue_results);

            isams_static_results = xdoc.CreateElement("static_results");
            isams_output.AppendChild(isams_static_results);

            fatigueExtractions = new Hashtable();
            staticExtractions = new Hashtable();
        }

        public void AddError(int code, string message) {
            XmlElement error = xdoc.CreateElement("error");
            error.SetAttribute("code", code.ToString());
            error.InnerText = message;

            isams_info.AppendChild(error);
        }

        public void AddError(string message) {
            XmlElement error = xdoc.CreateElement("error");
            error.InnerText = message;

            isams_info.AppendChild(error);
        }

        public void AddFile(string type, string label, string filename) {
            XmlElement file = xdoc.CreateElement("file");
            file.SetAttribute("type", type);
            file.SetAttribute("label", label);
            file.InnerText = filename;

            isams_files.AppendChild(file);
        }

        public string AddExtraction(string name, XmlElement resultType, Hashtable extractions) {
            XmlElement extraction = xdoc.CreateElement("extraction");
            extraction.SetAttribute("name", name);
            resultType.AppendChild(extraction);

            int count = 0;
            string hkey = name;

            while (extractions.ContainsKey(hkey)) {
                count++;
                hkey = name + "-" + count;
            }
            extractions.Add(hkey, extraction);
            return hkey;
        }

        public void AddToExtraction(string extraction, string name, Hashtable extractions) {
                XmlElement extract = (XmlElement)extractions[extraction];

                XmlElement field = xdoc.CreateElement(name);
                extract.AppendChild(field);
        }

        public void AddRowToExtraction(string extraction, ExtractionRowData data, Hashtable extractions) {
                XmlElement extract = (XmlElement)extractions[extraction];

                XmlElement row = xdoc.CreateElement("row");
                extract.AppendChild(row);

                int i = 0;
                foreach (KeyValuePair<string, string> kvp in data.pairs) {
                    XmlElement field = xdoc.CreateElement("field");
                    field.SetAttribute("units", data.units[i]);
                    field.SetAttribute("type", data.type[i]);
                    field.SetAttribute("name", kvp.Key);
                    field.InnerText = kvp.Value;

                    row.AppendChild(field);
                    i++;
                }
        }

        public void SetStatus(int status) {
            XmlElement stat = xdoc.CreateElement("status");
            stat.InnerText = status.ToString();

            isams_info.AppendChild(stat);
        }

        public void WriteData(string outputFile) {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                settings.IndentChars = "    ";
                settings.Encoding = new UTF8Encoding(false);
                XmlWriter xmlw = XmlWriter.Create(outputFile, settings);
                Log.InfoFormat("Writing output.xml  file:{0}", outputFile);
                xdoc.WriteTo(xmlw);
                xmlw.Flush();
                xmlw.Close();
        }

        public void ToXML(SingleHole.ResultsFormatted resData, bool fatigueLoad, bool repairAnalysisExists, StressCheckSolver.InputData.handbook fileName, bool isMultiHoleAnalysis, int currHole = 1) {
            string bpLoc = "";
            string bpLoc2 = "";
            string repairLoc = "";
            XmlElement loadCase;
            Hashtable extractions;

            string holeInfo;
            if (isMultiHoleAnalysis) {
                holeInfo = " at Hole #" + currHole.ToString();
            }
            else {
                holeInfo = "";
            }

            if (fatigueLoad) {
                loadCase = isams_fatigue_results;
                extractions = fatigueExtractions;

            }
            else {
                loadCase = isams_static_results;
                extractions = staticExtractions;
            }

            if (resData.faySurface) {
                bpLoc = "Fay Surface";
            }
            else {
                bpLoc = "Mid Surface";
            }

            if (repairAnalysisExists) {
                if (resData.faySurfaceRepairConfig) {
                    repairLoc = "Fay Surface";
                }
                else {
                    repairLoc = "Mid Surface";
                }

                if (resData.faySurface2) {
                    bpLoc2 = "Fay Surface";
                }
                else {
                    bpLoc2 = "Mid Surface";
                }
            }

            if (currHole == 1) {
                ExtractionRowData DictError = new ExtractionRowData();
                AddExtraction("Error Estimate", loadCase, extractions);
                DictError.pairs["Relative Error in Energy Norm B/P"] = resData.GlobalErrorBP.ToString("N2");
                DictError.type.Add("double");
                DictError.units.Add("percent");

                DictError.pairs["Max First Principal Stress Run #1 B/P"] = resData.PrincipalConvergeBP[0].ToString("N0");
                DictError.type.Add("double");
                DictError.units.Add("psi");

                DictError.pairs["Max First Principal Stress Run #2 B/P"] = resData.PrincipalConvergeBP[1].ToString("N0");
                DictError.type.Add("double");
                DictError.units.Add("psi");

                DictError.pairs["Max First Principal Stress Run #3 B/P"] = resData.PrincipalConvergeBP[2].ToString("N0");
                DictError.type.Add("double");
                DictError.units.Add("psi");

                if (repairAnalysisExists) {
                    DictError.pairs["Relative Error in Energy Norm Repair"] = resData.GlobalErrorRepair.ToString("N2");
                    DictError.type.Add("double");
                    DictError.units.Add("percent");

                    DictError.pairs["Max First Principal Stress Run #1 Repair"] = resData.PrincipalConvergeRepair[0].ToString("N0");
                    DictError.type.Add("double");
                    DictError.units.Add("psi");

                    DictError.pairs["Max First Principal Stress Run #2 Repair"] = resData.PrincipalConvergeRepair[1].ToString("N0");
                    DictError.type.Add("double");
                    DictError.units.Add("psi");

                    DictError.pairs["Max First Principal Stress Run #3 Repair"] = resData.PrincipalConvergeRepair[2].ToString("N0");
                    DictError.type.Add("double");
                    DictError.units.Add("psi");
                }

                AddRowToExtraction("Error Estimate", DictError, extractions);
            }

            ExtractionRowData DictKtS = new ExtractionRowData();
            AddExtraction("Peak Stress" + holeInfo, loadCase, extractions);
            DictKtS.pairs["KtS B/P"] = resData.BPKtS.ToString("N0");
            DictKtS.type.Add("double");
            DictKtS.units.Add("psi");

            DictKtS.pairs["Kc/Kt B/P"] = resData.BPKcKt.ToString("N3");
            DictKtS.type.Add("double");
            DictKtS.units.Add("");

            DictKtS.pairs["Angle B/P"] = resData.maxAngle.ToString("N0");
            DictKtS.type.Add("double");
            DictKtS.units.Add("degrees");

            DictKtS.pairs["Location B/P"] = bpLoc;
            DictKtS.type.Add("string");
            DictKtS.units.Add("");

            DictKtS.pairs["Tangent Shallow Gradient B/P"] = resData.TangentBPSG.ToString("N3");
            DictKtS.type.Add("double");
            DictKtS.units.Add("");

            DictKtS.pairs["Principal Shallow Gradient B/P"] = resData.PrincipalBPSG.ToString("N3");
            DictKtS.type.Add("double");
            DictKtS.units.Add("");

            if (repairAnalysisExists) {
                DictKtS.pairs["Repair Boundary Stress"] = resData.RepairBndyStress.ToString("N0");
                DictKtS.type.Add("double");
                DictKtS.units.Add("psi");

                DictKtS.pairs["Tangent Shallow Gradient at Repair Location"] = resData.TangentRepairBndySG.ToString("N3");
                DictKtS.type.Add("double");
                DictKtS.units.Add("");

                DictKtS.pairs["Principal Shallow Gradient at Repair Location"] = resData.PrincipalRepairBndySG.ToString("N3");
                DictKtS.type.Add("double");
                DictKtS.units.Add("");

                DictKtS.pairs["KtS Repair"] = resData.RepairKtS.ToString("N0");
                DictKtS.type.Add("double");
                DictKtS.units.Add("psi");

                DictKtS.pairs["Kc/Kt Repair"] = resData.RepairKcKt.ToString("N3");
                DictKtS.type.Add("double");
                DictKtS.units.Add("");

                DictKtS.pairs["Angle Repair"] = resData.maxAngleRepair.ToString("N0");
                DictKtS.type.Add("double");
                DictKtS.units.Add("degrees");

                DictKtS.pairs["Location Repair"] = repairLoc;
                DictKtS.type.Add("string");
                DictKtS.units.Add("");

                DictKtS.pairs["Tangent Shallow Gradient Repair"] = resData.TangentRepairSG.ToString("N3");
                DictKtS.type.Add("double");
                DictKtS.units.Add("");

                DictKtS.pairs["Principal Shallow Gradient Repair"] = resData.PrincipalRepairSG.ToString("N3");
                DictKtS.type.Add("double");
                DictKtS.units.Add("");

                DictKtS.pairs["KtS B/P at Max Repair Location"] = resData.BPKtS2.ToString("N0");
                DictKtS.type.Add("double");
                DictKtS.units.Add("psi");

                DictKtS.pairs["Kc/Kt B/P at Max Repair Location"] = resData.BPKcKt2.ToString("N3");
                DictKtS.type.Add("double");
                DictKtS.units.Add("");

                DictKtS.pairs["Angle B/P at Max Repair Location"] = resData.maxAngle2.ToString("N0");
                DictKtS.type.Add("double");
                DictKtS.units.Add("degrees");

                DictKtS.pairs["Location B/P at Max Repair Location"] = bpLoc2;
                DictKtS.type.Add("string");
                DictKtS.units.Add("");

                DictKtS.pairs["Tangent Shallow Gradient B/P at Max Repair Location"] = resData.TangentBPSG2.ToString("N3");
                DictKtS.type.Add("double");
                DictKtS.units.Add("");

                DictKtS.pairs["Principal Shallow Gradient B/P at Max Repair Location"] = resData.PrincipalBPSG2.ToString("N3");
                DictKtS.type.Add("double");
                DictKtS.units.Add("");

                DictKtS.pairs["Repair Boundary Stress at Max Repair Location"] = resData.RepairBndyStress2.ToString("N0");
                DictKtS.type.Add("double");
                DictKtS.units.Add("psi");

                DictKtS.pairs["Tangent Shallow Gradient at Repair Location at Max Repair Location"] = resData.TangentRepairBndySG2.ToString("N3");
                DictKtS.type.Add("double");
                DictKtS.units.Add("");

                DictKtS.pairs["Principal Shallow Gradient at Repair Location at Max Repair Location"] = resData.PrincipalRepairBndySG2.ToString("N3");
                DictKtS.type.Add("double");
                DictKtS.units.Add("");
            }

            AddRowToExtraction("Peak Stress" + holeInfo, DictKtS, extractions);

            ExtractionRowData DictFactors = new ExtractionRowData();
            AddExtraction("Factors" + holeInfo, loadCase, extractions);
            DictFactors.pairs["Kt CSK B/P"] = resData.ktCSKBP.ToString("N3");
            DictFactors.type.Add("double");
            DictFactors.units.Add("");

            DictFactors.pairs["Kt Peaking B/P"] = resData.KtPeakBP.ToString("N3");
            DictFactors.type.Add("double");
            DictFactors.units.Add("");

            if (fileName != StressCheckSolver.InputData.handbook.Lug) {
                DictFactors.pairs["Kt t/d B/P"] = resData.KttdBP.ToString("N3");
                DictFactors.type.Add("double");
                DictFactors.units.Add("");

                DictFactors.pairs["Kt Neat-Fit Tension B/P"] = resData.KtNFTensBP.ToString("N3");
                DictFactors.type.Add("double");
                DictFactors.units.Add("");

                DictFactors.pairs["Kt Neat-Fit Compression B/P"] = resData.KtNFCompBP.ToString("N3");
                DictFactors.type.Add("double");
                DictFactors.units.Add("");
            }

            if (repairAnalysisExists) {
                DictFactors.pairs["Kt CSK Repair"] = resData.ktCSKRepair.ToString("N3");
                DictFactors.type.Add("double");
                DictFactors.units.Add("");

                DictFactors.pairs["Kt Peaking Repair"] = resData.KtPeakRepair.ToString("N3");
                DictFactors.type.Add("double");
                DictFactors.units.Add("");

                if (fileName != StressCheckSolver.InputData.handbook.Lug) {
                    DictFactors.pairs["Kt t/d Repair"] = resData.KttdRepair.ToString("N3");
                    DictFactors.type.Add("double");
                    DictFactors.units.Add("");

                    DictFactors.pairs["Kt Neat-Fit Tension Repair"] = resData.KtNFTensRepair.ToString("N3");
                    DictFactors.type.Add("double");
                    DictFactors.units.Add("");

                    DictFactors.pairs["Kt Neat-Fit Compression Repair"] = resData.KtNFCompRepair.ToString("N3");
                    DictFactors.type.Add("double");
                    DictFactors.units.Add("");
                }

                DictFactors.pairs["Kt CSK B/P at Max Repair Location"] = resData.ktCSKBP2.ToString("N3");
                DictFactors.type.Add("double");
                DictFactors.units.Add("");

                DictFactors.pairs["Kt Peaking B/P at Max Repair Location"] = resData.KtPeakBP2.ToString("N3");
                DictFactors.type.Add("double");
                DictFactors.units.Add("");

                if (fileName != StressCheckSolver.InputData.handbook.Lug) {
                    DictFactors.pairs["Kt t/d B/P at Max Repair Location"] = resData.KttdBP2.ToString("N3");
                    DictFactors.type.Add("double");
                    DictFactors.units.Add("");

                    DictFactors.pairs["Kt Neat-Fit Tension B/P at Max Repair Location"] = resData.KtNFTensBP2.ToString("N3");
                    DictFactors.type.Add("double");
                    DictFactors.units.Add("");

                    DictFactors.pairs["Kt Neat-Fit Compression B/P at Max Repair Location"] = resData.KtNFCompBP2.ToString("N3");
                    DictFactors.type.Add("double");
                    DictFactors.units.Add("");
                }
            }

            AddRowToExtraction("Factors" + holeInfo, DictFactors, extractions);

            AddExtraction("Blue Print Results Mid-Surface" + holeInfo, loadCase, extractions);
            foreach (SingleHole.ResultTable tblItem in resData.resultTableMSBP) {
                ExtractionRowData DictTableMSBP = new ExtractionRowData();
                DictTableMSBP.pairs["Angle"] = tblItem.Angle.ToString("N1");
                DictTableMSBP.type.Add("double");
                DictTableMSBP.units.Add("degrees");

                DictTableMSBP.pairs["Blue Print Bearing (+ psi)"] = tblItem.TangentStrainToStressBrg.ToString("N0");
                DictTableMSBP.type.Add("double");
                DictTableMSBP.units.Add("psi");

                DictTableMSBP.pairs["Blue Print Bearing (- psi)"] = tblItem.TangentStrainToStressBrgNeg.ToString("N0");
                DictTableMSBP.type.Add("double");
                DictTableMSBP.units.Add("psi");

                DictTableMSBP.pairs["Kt CSK Bearing"] = tblItem.KtCSK.ToString("N3");
                DictTableMSBP.type.Add("double");
                DictTableMSBP.units.Add("");

                DictTableMSBP.pairs["Factored Blue Print Bearing (+ psi)"] = tblItem.TangentStrainToStressBrgFactored.ToString("N0");
                DictTableMSBP.type.Add("double");
                DictTableMSBP.units.Add("psi");

                DictTableMSBP.pairs["Factored Blue Print Bearing (- psi)"] = tblItem.TangentStrainToStressBrgNegFactored.ToString("N0");
                DictTableMSBP.type.Add("double");
                DictTableMSBP.units.Add("psi");

                if (fileName != StressCheckSolver.InputData.handbook.Lug) {
                    DictTableMSBP.pairs["Blue Print Bypass (+ psi)"] = tblItem.TangentStrainToStressByp.ToString("N0");
                    DictTableMSBP.type.Add("double");
                    DictTableMSBP.units.Add("psi");

                    DictTableMSBP.pairs["Blue Print Bypass (- psi)"] = tblItem.TangentStrainToStressBypNeg.ToString("N0");
                    DictTableMSBP.type.Add("double");
                    DictTableMSBP.units.Add("psi");

                    DictTableMSBP.pairs["Kt CSK Bypass"] = tblItem.KtCSK.ToString("N3");
                    DictTableMSBP.type.Add("double");
                    DictTableMSBP.units.Add("");

                    DictTableMSBP.pairs["Kt t/d"] = tblItem.Kttd.ToString("N3");
                    DictTableMSBP.type.Add("double");
                    DictTableMSBP.units.Add("");

                    DictTableMSBP.pairs["Kt Neat Fit (+)"] = tblItem.KtNFTens.ToString("N3");
                    DictTableMSBP.type.Add("double");
                    DictTableMSBP.units.Add("");

                    DictTableMSBP.pairs["Factored Blue Print Bypass (+ psi)"] = tblItem.TangentStrainToStressBypFactored.ToString("N0");
                    DictTableMSBP.type.Add("double");
                    DictTableMSBP.units.Add("psi");

                    DictTableMSBP.pairs["Kt Neat Fit (-)"] = tblItem.KtNFComp.ToString("N3");
                    DictTableMSBP.type.Add("double");
                    DictTableMSBP.units.Add("");

                    DictTableMSBP.pairs["Factored Blue Print Bypass (- psi)"] = tblItem.TangentStrainToStressBypNegFactored.ToString("N0");
                    DictTableMSBP.type.Add("double");
                    DictTableMSBP.units.Add("psi");

                    DictTableMSBP.pairs["Blue Print Combined (+ psi)"] = tblItem.TangentStrainToStressComb.ToString("N0");
                    DictTableMSBP.type.Add("double");
                    DictTableMSBP.units.Add("psi");

                    DictTableMSBP.pairs["Blue Print Combined (- psi)"] = tblItem.TangentStrainToStressCombNeg.ToString("N0");
                    DictTableMSBP.type.Add("double");
                    DictTableMSBP.units.Add("psi");

                }
                DictTableMSBP.pairs["Kc/Kt"] = tblItem.KcKt.ToString("N3");
                DictTableMSBP.type.Add("double");
                DictTableMSBP.units.Add("");

                AddRowToExtraction("Blue Print Results Mid-Surface" + holeInfo, DictTableMSBP, extractions);
            }

            AddExtraction("Blue Print Results Fay-Surface" + holeInfo, loadCase, extractions);
            foreach (SingleHole.ResultTable tblItem in resData.resultTableFAYBP) {
                ExtractionRowData DictTableFAYBP = new ExtractionRowData();
                DictTableFAYBP.pairs["Angle"] = tblItem.Angle.ToString("N1");
                DictTableFAYBP.type.Add("double");
                DictTableFAYBP.units.Add("degrees");

                DictTableFAYBP.pairs["Blue Print Bearing (+ psi)"] = tblItem.TangentStrainToStressBrg.ToString("N0");
                DictTableFAYBP.type.Add("double");
                DictTableFAYBP.units.Add("psi");

                DictTableFAYBP.pairs["Blue Print Bearing (- psi)"] = tblItem.TangentStrainToStressBrgNeg.ToString("N0");
                DictTableFAYBP.type.Add("double");
                DictTableFAYBP.units.Add("psi");

                DictTableFAYBP.pairs["Kt Peaking"] = tblItem.KtPeak.ToString("N3");
                DictTableFAYBP.type.Add("double");
                DictTableFAYBP.units.Add("");

                DictTableFAYBP.pairs["Factored Blue Print Bearing (+ psi)"] = tblItem.TangentStrainToStressBrgFactored.ToString("N0");
                DictTableFAYBP.type.Add("double");
                DictTableFAYBP.units.Add("psi");

                DictTableFAYBP.pairs["Factored Blue Print Bearing (- psi)"] = tblItem.TangentStrainToStressBrgNegFactored.ToString("N0");
                DictTableFAYBP.type.Add("double");
                DictTableFAYBP.units.Add("psi");

                if (fileName != StressCheckSolver.InputData.handbook.Lug) {
                    DictTableFAYBP.pairs["Blue Print Bypass (+ psi)"] = tblItem.TangentStrainToStressByp.ToString("N0");
                    DictTableFAYBP.type.Add("double");
                    DictTableFAYBP.units.Add("psi");

                    DictTableFAYBP.pairs["Blue Print Bypass (- psi)"] = tblItem.TangentStrainToStressBypNeg.ToString("N0");
                    DictTableFAYBP.type.Add("double");
                    DictTableFAYBP.units.Add("psi");

                    DictTableFAYBP.pairs["Kt Neat Fit (+)"] = tblItem.KtNFTens.ToString("N3");
                    DictTableFAYBP.type.Add("double");
                    DictTableFAYBP.units.Add("");

                    DictTableFAYBP.pairs["Factored Blue Print Bypass (+ psi)"] = tblItem.TangentStrainToStressBypFactored.ToString("N0");
                    DictTableFAYBP.type.Add("double");
                    DictTableFAYBP.units.Add("psi");

                    DictTableFAYBP.pairs["Kt Neat Fit (-)"] = tblItem.KtNFComp.ToString("N3");
                    DictTableFAYBP.type.Add("double");
                    DictTableFAYBP.units.Add("");

                    DictTableFAYBP.pairs["Factored Blue Print Bypass (- psi)"] = tblItem.TangentStrainToStressBypNegFactored.ToString("N0");
                    DictTableFAYBP.type.Add("double");
                    DictTableFAYBP.units.Add("psi");

                    DictTableFAYBP.pairs["Blue Print Combined (+ psi)"] = tblItem.TangentStrainToStressComb.ToString("N0");
                    DictTableFAYBP.type.Add("double");
                    DictTableFAYBP.units.Add("psi");

                    DictTableFAYBP.pairs["Blue Print Combined (- psi)"] = tblItem.TangentStrainToStressCombNeg.ToString("N0");
                    DictTableFAYBP.type.Add("double");
                    DictTableFAYBP.units.Add("psi");

                }
                DictTableFAYBP.pairs["Kc/Kt"] = tblItem.KcKt.ToString("N3");
                DictTableFAYBP.type.Add("double");
                DictTableFAYBP.units.Add("");

                AddRowToExtraction("Blue Print Results Fay-Surface" + holeInfo, DictTableFAYBP, extractions);
            }

            if (repairAnalysisExists) {
                AddExtraction("Repair Results Mid-Surface" + holeInfo, loadCase, extractions);
                foreach (SingleHole.ResultTable tblItem in resData.resultTableMSRepair) {
                    ExtractionRowData DictTableMSRepair = new ExtractionRowData();
                    DictTableMSRepair.pairs["Angle"] = tblItem.Angle.ToString("N1");
                    DictTableMSRepair.type.Add("double");
                    DictTableMSRepair.units.Add("degrees");

                    DictTableMSRepair.pairs["Repair Bearing (+ psi)"] = tblItem.TangentStrainToStressBrg.ToString("N0");
                    DictTableMSRepair.type.Add("double");
                    DictTableMSRepair.units.Add("psi");

                    DictTableMSRepair.pairs["Repair Bearing (- psi)"] = tblItem.TangentStrainToStressBrgNeg.ToString("N0");
                    DictTableMSRepair.type.Add("double");
                    DictTableMSRepair.units.Add("psi");

                    DictTableMSRepair.pairs["Kt CSK Bearing"] = tblItem.KtCSK.ToString("N3");
                    DictTableMSRepair.type.Add("double");
                    DictTableMSRepair.units.Add("");

                    DictTableMSRepair.pairs["Factored Repair Bearing (+ psi)"] = tblItem.TangentStrainToStressBrgFactored.ToString("N0");
                    DictTableMSRepair.type.Add("double");
                    DictTableMSRepair.units.Add("psi");

                    DictTableMSRepair.pairs["Factored Repair Bearing (- psi)"] = tblItem.TangentStrainToStressBrgNegFactored.ToString("N0");
                    DictTableMSRepair.type.Add("double");
                    DictTableMSRepair.units.Add("psi");

                    if (fileName != StressCheckSolver.InputData.handbook.Lug) {
                        DictTableMSRepair.pairs["Repair Bypass (+ psi)"] = tblItem.TangentStrainToStressByp.ToString("N0");
                        DictTableMSRepair.type.Add("double");
                        DictTableMSRepair.units.Add("psi");

                        DictTableMSRepair.pairs["Repair Bypass (- psi)"] = tblItem.TangentStrainToStressBypNeg.ToString("N0");
                        DictTableMSRepair.type.Add("double");
                        DictTableMSRepair.units.Add("psi");

                        DictTableMSRepair.pairs["Kt CSK Bypass"] = tblItem.KtCSK.ToString("N3");
                        DictTableMSRepair.type.Add("double");
                        DictTableMSRepair.units.Add("");

                        DictTableMSRepair.pairs["Kt t/d"] = tblItem.Kttd.ToString("N3");
                        DictTableMSRepair.type.Add("double");
                        DictTableMSRepair.units.Add("");

                        DictTableMSRepair.pairs["Kt Neat Fit (+)"] = tblItem.KtNFTens.ToString("N3");
                        DictTableMSRepair.type.Add("double");
                        DictTableMSRepair.units.Add("");

                        DictTableMSRepair.pairs["Factored Repair Bypass (+ psi)"] = tblItem.TangentStrainToStressBypFactored.ToString("N0");
                        DictTableMSRepair.type.Add("double");
                        DictTableMSRepair.units.Add("psi");

                        DictTableMSRepair.pairs["Kt Neat Fit (-)"] = tblItem.KtNFComp.ToString("N3");
                        DictTableMSRepair.type.Add("double");
                        DictTableMSRepair.units.Add("");

                        DictTableMSRepair.pairs["Factored Repair Bypass (- psi)"] = tblItem.TangentStrainToStressBypNegFactored.ToString("N0");
                        DictTableMSRepair.type.Add("double");
                        DictTableMSRepair.units.Add("psi");

                        DictTableMSRepair.pairs["Repair Combined (+ psi)"] = tblItem.TangentStrainToStressComb.ToString("N0");
                        DictTableMSRepair.type.Add("double");
                        DictTableMSRepair.units.Add("psi");

                        DictTableMSRepair.pairs["Repair Combined (- psi)"] = tblItem.TangentStrainToStressCombNeg.ToString("N0");
                        DictTableMSRepair.type.Add("double");
                        DictTableMSRepair.units.Add("psi");

                    }
                    DictTableMSRepair.pairs["Kc/Kt"] = tblItem.KcKt.ToString("N3");
                    DictTableMSRepair.type.Add("double");
                    DictTableMSRepair.units.Add("");

                    AddRowToExtraction("Repair Results Mid-Surface" + holeInfo, DictTableMSRepair, extractions);
                }

                AddExtraction("Repair Results Fay-Surface" + holeInfo, loadCase, extractions);
                foreach (SingleHole.ResultTable tblItem in resData.resultTableFAYRepair) {
                    ExtractionRowData DictTableFAYRepair = new ExtractionRowData();
                    DictTableFAYRepair.pairs["Angle"] = tblItem.Angle.ToString("N1");
                    DictTableFAYRepair.type.Add("double");
                    DictTableFAYRepair.units.Add("degrees");

                    DictTableFAYRepair.pairs["Repair Bearing (+ psi)"] = tblItem.TangentStrainToStressBrg.ToString("N0");
                    DictTableFAYRepair.type.Add("double");
                    DictTableFAYRepair.units.Add("psi");

                    DictTableFAYRepair.pairs["Repair Bearing (- psi)"] = tblItem.TangentStrainToStressBrgNeg.ToString("N0");
                    DictTableFAYRepair.type.Add("double");
                    DictTableFAYRepair.units.Add("psi");

                    DictTableFAYRepair.pairs["Kt Peaking"] = tblItem.KtPeak.ToString("N3");
                    DictTableFAYRepair.type.Add("double");
                    DictTableFAYRepair.units.Add("");

                    DictTableFAYRepair.pairs["Factored Repair Bearing (+ psi)"] = tblItem.TangentStrainToStressBrgFactored.ToString("N0");
                    DictTableFAYRepair.type.Add("double");
                    DictTableFAYRepair.units.Add("psi");

                    DictTableFAYRepair.pairs["Factored Repair Bearing (- psi)"] = tblItem.TangentStrainToStressBrgNegFactored.ToString("N0");
                    DictTableFAYRepair.type.Add("double");
                    DictTableFAYRepair.units.Add("psi");

                    if (fileName != StressCheckSolver.InputData.handbook.Lug) {
                        DictTableFAYRepair.pairs["Repair Bypass (+ psi)"] = tblItem.TangentStrainToStressByp.ToString("N0");
                        DictTableFAYRepair.type.Add("double");
                        DictTableFAYRepair.units.Add("psi");

                        DictTableFAYRepair.pairs["Repair Bypass (- psi)"] = tblItem.TangentStrainToStressBypNeg.ToString("N0");
                        DictTableFAYRepair.type.Add("double");
                        DictTableFAYRepair.units.Add("psi");

                        DictTableFAYRepair.pairs["Kt Neat Fit (+)"] = tblItem.KtNFTens.ToString("N3");
                        DictTableFAYRepair.type.Add("double");
                        DictTableFAYRepair.units.Add("");

                        DictTableFAYRepair.pairs["Factored Repair Bypass (+ psi)"] = tblItem.TangentStrainToStressBypFactored.ToString("N0");
                        DictTableFAYRepair.type.Add("double");
                        DictTableFAYRepair.units.Add("psi");

                        DictTableFAYRepair.pairs["Kt Neat Fit (-)"] = tblItem.KtNFComp.ToString("N3");
                        DictTableFAYRepair.type.Add("double");
                        DictTableFAYRepair.units.Add("");

                        DictTableFAYRepair.pairs["Factored Repair Bypass (- psi)"] = tblItem.TangentStrainToStressBypNegFactored.ToString("N0");
                        DictTableFAYRepair.type.Add("double");
                        DictTableFAYRepair.units.Add("psi");

                        DictTableFAYRepair.pairs["Repair Combined (+ psi)"] = tblItem.TangentStrainToStressComb.ToString("N0");
                        DictTableFAYRepair.type.Add("double");
                        DictTableFAYRepair.units.Add("psi");

                        DictTableFAYRepair.pairs["Repair Combined (- psi)"] = tblItem.TangentStrainToStressCombNeg.ToString("N0");
                        DictTableFAYRepair.type.Add("double");
                        DictTableFAYRepair.units.Add("psi");

                    }
                    DictTableFAYRepair.pairs["Kc/Kt"] = tblItem.KcKt.ToString("N3");
                    DictTableFAYRepair.type.Add("double");
                    DictTableFAYRepair.units.Add("");

                    AddRowToExtraction("Repair Results Fay-Surface" + holeInfo, DictTableFAYRepair, extractions);
                }
            }

        }

        public class ExtractedSCData {
            public string extractSetName;
            public List<ExtractionColumn> columns = new List<ExtractionColumn>();
        }

        public class ExtractionColumn {
            public string headerName;
            public List<double> data = new List<double>();
        }

        public class ExtractionRowData {
            public List<string> units = new List<string>();
            public List<string> type = new List<string>();
            public Dictionary<string, string> pairs = new Dictionary<string, string>();
        }
    }
}

