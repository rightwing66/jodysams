﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StressCheckSolver
{
    public class SingleHoleParameterData
    {
        public double holeX;
        public double holeY;
        public double holeDia;
        public double holeDiaRepair;
        public double th;
        public double thRepair;
        public double Em_Plate;
        public bool csk;
        public bool cskRepair;
        public double cskDepth;
        public double cskDepthRepair;
        public bool neatFit;
        public bool neatFitRepair;
        public double Em_Fastener;
        public double Em_FastenerRepair;
        public bool repairAnalysis; //Used to set whether current analysis is repair or not
        public bool repairAnalysisExists; //Used to set whether a repair was run at all or not
        public int currentHoleNumber = 0; //Used for multi-hole analysis, defaul to zero for lug or single hole analysis

        public double BYP1, BYP2, BYP3, AS1, AS2, AS3, AS4, AS5, AS6, AS7, AS8, AB1, AB2, AB3, AB4, SB1, SB2, SB3, SB4;
    }
}
