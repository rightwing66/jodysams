﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace StressCheckSolver
{
    public abstract class OutputFolder
    {
        public string parentFolder
        {
            get
            {
                return Directory.GetParent(fullPath).FullName;
            }
        }
        public string fullPath { get; protected set; }

        public void CreateFolder()
        {
            if (!Directory.Exists(fullPath))
            {
                Directory.CreateDirectory(fullPath);
            }
        }

        public static bool Exists(string parentFolder)
        {
            return Directory.Exists(parentFolder);
        }

        public int GetNumberFiles(string parentFolder)
        {
            return Directory.GetFiles(parentFolder).Length;
        }

        /// <summary>
        /// Unpacks the binary representation of a file into the file given by filename.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        public string AddFile(string filename, byte[] file)
        {
            string fname = Path.GetFullPath(Path.Combine(fullPath, filename));
            var fs = new FileStream(fname, FileMode.Create, FileAccess.Write);
            var s = new BinaryWriter(fs);
            s.Write(file);
            s.Close();
            fs.Close();

            return fname;
        }

        /// <summary>
        /// For a given filename, return the path to a temp directory where the file can be written.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public string GetFullPath(string filename)
        {
            return Path.GetFullPath(Path.Combine(fullPath, filename));
        }

        /// <summary>
        /// Get random string of 11 characters.
        /// </summary>
        /// <returns>Random string.</returns>
        public string GetRandomString()
        {
            string path = Path.GetRandomFileName();
            path = path.Replace(".", ""); // Remove period.
            return path;
        }

    }
}
