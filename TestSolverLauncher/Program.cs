﻿using System;
using StressCheckSolver;

namespace TestSolverLauncher {
    class Program {
        public static void Main() {
            string inputFilePath = @"C:\Solver\job570\input\input.xml";
            string outputDirectoryPath = @"C:\Solver\job570\output";
            Console.WriteLine(@"Starting Solver");
            Console.WriteLine("InputFilePath = " + inputFilePath);
            Console.WriteLine("OutputDirectoryPath = " + outputDirectoryPath);
            Console.WriteLine("ShowStressCheck = " + false);
            ISolver solver = new Solver {
                InputFilePath = inputFilePath,
                OutputDirectoryPath = outputDirectoryPath,
                showStressCheck = false
            };
            solver.Init();
            solver.Solve();

            Console.WriteLine(@"Solver Finished");
            Console.ReadKey(false);
            Environment.Exit(0);
        }
    }
}
