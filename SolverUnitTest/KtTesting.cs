﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest
{
    /// <summary>
    /// Summary description for KtTesting
    /// </summary>
    [TestClass]
    public class KtTesting
    {
        [TestMethod]
        public void TestKtFactors()
        {
            double[] EPlate = { 10e6, 10e6, 16e6, 16e6, 30e6, 30e6 };
            double[] EPin = { 10e6, 16e6, 30e6, 10e6, 16e6, 30e6 };
            double[] thk = { 0.1, 0.125, 0.4, 0.3, 0.25, 0.5 };
            double[] dia = { 0.125, 0.275, 0.35, 0.1, 0.19, 0.3 };
            double[] cskD = { 0.05, 0.08, 0.05, 0.15, 0.175, 0.1 };
            bool[] tension = {false, true, true, false, false, true};
            double[] angle = {0, 0, 30, 30, 15, 15};

            double[] KtP_Results = { 1.39576127929688, 1.0944820900266847, 1.4407759631876349, 1.0, 2.5159481514594244, 2.377404848640003 };
            double[] KtCSK_Results = { 1.3777058162260003, 1.3142613215643679, 1.1469853042985945, 1.439542644754, 1.5956565725340004, 1.2050859219039998 };
            double[] KtThick_Results = { 1.0383456832, 1.0214106941124241, 1.0500560226096269, 1.0272241024000002, 1.0497003976012269, 1.042877345679012 };
            double[] KtNF_Results = { 0.63499800000000006, 0.90996622399999993, 1.0, 1.0, 0.87858284800491626, 0.96234225000000007 };

            string errorString = "";
            StressCheckSolver.KtFactors KtFactors = new StressCheckSolver.KtFactors();
            for(int i = 0; i < 6; i++)
            {
                Assert.AreEqual(KtP_Results[i], KtFactors.Kt_Peaking(EPlate[i] / EPin[i], dia[i] / thk[i], out errorString), 1e-6);
                Assert.AreEqual(KtCSK_Results[i], KtFactors.Ktcsk(cskD[i] / thk[i], thk[i] / dia[i], out errorString), 1e-6);
                Assert.AreEqual(KtThick_Results[i], KtFactors.Kt_thick(thk[i] / dia[i], out errorString), 1e-6);
                Assert.AreEqual(KtNF_Results[i], KtFactors.KtNF(EPin[i] / EPlate[i], tension[i], angle[i], out errorString), 1e-6);
            }

        }
    }
}
