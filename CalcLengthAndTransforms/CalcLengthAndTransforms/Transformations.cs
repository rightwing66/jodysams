﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcLengthAndTransforms
{
    class Transformations
    {
        public int ElemId;
        public double TransformX;
        public double TransformY;
        public double TransformZ;

        public Transformations (int elemId, double transformX, double transformY, double transformZ)
        {
            ElemId = elemId;
            TransformX = transformX;
            TransformY = transformY;
            TransformZ = transformZ;
        }
    }
}
