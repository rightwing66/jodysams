﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CalcLengthAndTransforms
{
    class Program
    {
        static void Main(string[] args)
        {
            CalcLength();
        }

        private static double CalcTransform(List<double> lengthAndCoords, string coord)
        {
            double transform = 0;
            double length = lengthAndCoords[1];
            double x1 = lengthAndCoords[2];
            double x2 = lengthAndCoords[3];
            double y1 = lengthAndCoords[4];
            double y2 = lengthAndCoords[5];
            double z1 = lengthAndCoords[6];
            double z2 = lengthAndCoords[7];

            switch (coord)
            {
                case "x":
                    transform = (x2- x1)/length;
                    break;
                case "y":
                    transform = (y2- y1)/length;
                    break;
                case "z":
                    transform = (z2- z1)/length;
                    break;
            }
            return transform;
        }

        private static void CalcLength()
        {
            int eid;
            double x1, x2, y1, y2, z1, z2, difx, dify, difz, elemId;
            double transformx = 0, transformy = 0, transformz = 0;
            List<double> lengthAndCoords = new List<double>();
            Transformations transforms;
            List<Transformations> transformList = new List<Transformations>();

            x1 = x2 = y1 = y2 = z1 = z2 = difx = dify = difz = elemId = 0;

            string rodGeometryPath = "C:\\Users\\shoelzer\\desktop\\Rod Geometry.xml";
            XmlDocument rodGeometry = new XmlDocument();
            rodGeometry.Load(rodGeometryPath);
            XmlTextReader rodGeometry2 = new XmlTextReader(rodGeometryPath);
            XmlNodeList rodGeometryx = rodGeometry.SelectNodes("/dataroot/Rod_x0020_Geometry");
            foreach (XmlNode node in rodGeometryx)
            {
                lengthAndCoords.Clear();
                elemId = Convert.ToDouble(node["EID"].InnerText);
                eid = Convert.ToInt32(elemId);
                x1 = Convert.ToDouble(node["X1"].InnerText);
                y1 = Convert.ToDouble(node["Y1"].InnerText);
                z1 = Convert.ToDouble(node["Z1"].InnerText);
                x2 = Convert.ToDouble(node["X2"].InnerText);
                y2 = Convert.ToDouble(node["Y2"].InnerText);
                z2 = Convert.ToDouble(node["Z2"].InnerText);

                difx = x2 - x1;
                dify = y2 - y1;
                difz = z2 - z1;
                double length = Math.Pow((Math.Pow(difx, 2) + Math.Pow(dify, 2) + Math.Pow(difz, 2)), 0.5);

                lengthAndCoords.Add(elemId);
                lengthAndCoords.Add(length);
                lengthAndCoords.Add(x1);
                lengthAndCoords.Add(x2);
                lengthAndCoords.Add(y1);
                lengthAndCoords.Add(y2);
                lengthAndCoords.Add(z1);
                lengthAndCoords.Add(z2);

                transformx = CalcTransform(lengthAndCoords, "x");
                transformy = CalcTransform(lengthAndCoords, "y");
                transformz = CalcTransform(lengthAndCoords, "z");

                transforms = new Transformations(eid, transformx, transformy, transformz);
                transformList.Add(transforms);   
            }
            OutputXml(transformList);
            return;
        }

        private static void OutputXml(List<Transformations> transformList)
        {
            XmlDocument rodTransforms = new XmlDocument();
            XmlElement rods = rodTransforms.CreateElement("Rods");

            foreach (Transformations transform in transformList)
            {
                XmlElement transformations = rodTransforms.CreateElement("Transformations");
                XmlElement ElemId = rodTransforms.CreateElement("EID");
                ElemId.InnerText = transform.ElemId.ToString();
                XmlElement TransformX = rodTransforms.CreateElement("x_elem_x");
                TransformX.InnerText = transform.TransformX.ToString();
                XmlElement TransformY = rodTransforms.CreateElement("x_elem_y");
                TransformY.InnerText = transform.TransformY.ToString();
                XmlElement TransformZ = rodTransforms.CreateElement("x_elem_z");
                TransformZ.InnerText = transform.TransformZ.ToString();

                transformations.AppendChild(ElemId);
                transformations.AppendChild(TransformX);
                transformations.AppendChild(TransformY);
                transformations.AppendChild(TransformZ);
                rods.AppendChild(transformations);
                rodTransforms.AppendChild(rods);   
            }
            rodTransforms.Save("C:\\Users\\shoelzer\\Desktop\\RodTransforms.xml");
        }
    }
}
