﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using I2ES.Model;
using I2ES.Model.DomainModel;
using I2ES.Model.DomainModel.JobProcessModel;
using I2ES.Model.DomainModelContext;

namespace TrafficManagerWindowsService {
    public class TrafficManagerWatcher : IDisposable {
        private static readonly log4net.ILog Log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IJobProcessModel jobProcessModel;
        private readonly ISqlWrapper sqlWrapper;
        private readonly ConfigurationWrapper configuration;

        public TrafficManagerWatcher(IJobProcessModel jobProcessModel, ISqlWrapper sqlWrapper) {
            this.jobProcessModel = jobProcessModel;
            this.sqlWrapper = sqlWrapper;
            configuration = new ConfigurationWrapper();

        }

        public void ProcessJobs() {
            Log.Debug("Process Jobs");
            List<AppServer> appServers = getAppServers();
            foreach (var appServer in appServers) {
                int? jobLimit = configuration.JobLimit;
                int? slotsOpen = jobLimit - jobProcessModel.JobsCurrentlyInProcess(appServer);
                Log.Debug($"Slots Open:{slotsOpen} for server:{appServer.Name}");
                if (slotsOpen > 0) {
                   List<Job> jobsToSubmit =  jobProcessModel.LoadJobQueue(slotsOpen);
                    foreach (var job in jobsToSubmit) {
                         SubmitJob(job.JobId,appServer.Name,appServer.Url);
                    }
                }
            }
        }

        private List<AppServer> getAppServers() {
            ConfigurationWrapper config = new ConfigurationWrapper();
            List<string> names = new List<string> {config.SolverNode};
            return jobProcessModel.GetAppServers(names);
        }

 

        public   void SubmitJob(int jobId,  string solverNode, string solverUrl) {
            ISAMSModel model = new ISAMSModel(new ConnectionProvider(), new JobReader { SqlWrapper = sqlWrapper }, new JobTypeReader { SqlWrapper = sqlWrapper }, new JobWriter { SqlWrapper = sqlWrapper });
            Log.InfoFormat("jobId:{0} solverNode:{1}  solverUrl:{2}", jobId, solverNode, solverUrl);
            var job = model.LoadJob(jobId);
            job.SolverNode = solverNode;
            job.SolverState = BackEndProcessState.InProgress;
            job.JobState = JobState.SolverProcessing;
            job.SolverBeginTime = DateTime.Now;
            model.SaveChanges();
            try {
                signalSolver(job, solverNode, solverUrl);
                Log.InfoFormat("JobId: {0}  Call to solver returned", jobId);
            }
            catch (Exception ex) {
                Log.Error(ex);
                job.SolverState = BackEndProcessState.Failed;
                job.JobState = JobState.Failed;
                job.SolverMessage = ex.Message;
                model.SaveChanges();
            }
        }
        private void signalSolver(Job job, string solverNode, string solverUrl) {
            try {
                string url = "http://" + solverNode + "/" + solverUrl + "/Submit?id=" + job.JobId;
//                string url = "http://www.google.com";
                Log.Info("signalSolver "+ url);
                sendHTTPMessage(url);
            }
            catch (Exception ex) {
                Log.Info(ex);
            }
        }

        private async Task sendHTTPMessage(string url) {
            try {
                HttpClient httpClient = new HttpClient {MaxResponseContentBufferSize = 256000};
                httpClient.DefaultRequestHeaders.Add("user-agent",
                    "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)");
                var response =  httpClient.GetAsync(url);
            }
            catch (Exception e) {
                Log.Error(e);
            }
                        
        }

        public void Dispose() {
            jobProcessModel.Dispose();
            sqlWrapper.Dispose();
        }
    }
}