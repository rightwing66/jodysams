﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using I2ES.Model;
using I2ES.Model.DomainModel.JobProcessModel;

namespace TrafficManagerWindowsService {
    public partial class TrafficManagerWatcherService : ServiceBase {
        private static readonly log4net.ILog Log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private TrafficManagerWatcher watcher;
        private ConfigurationWrapper configuration;
        private Timer timer;

        public TrafficManagerWatcherService() {
            InitializeComponent();
            configuration = new ConfigurationWrapper();
        }

        protected override void OnStart(string[] args) {
            try {
                ConnectionProvider provider = new ConnectionProvider();
                watcher = new TrafficManagerWatcher(new JobProcessModel(provider.CreateWrapper()), provider.CreateWrapper());
                Logging logging = new Logging();
                logging.WireUpLogging();
                Log.Info($"OnStart timerInterval:{configuration.TimerInterval}");
            
                timer = new Timer(configuration.TimerInterval);
                timer.Elapsed += Timer_Elapsed;
                timer.Start();
            }
            catch (Exception e) {
                Trace.Write($"message:{e.Message}\r\nstack trace:{e.StackTrace}");
            }

        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e) {
            Log.Debug("Timer Elapsed");
            watcher.ProcessJobs();
        }

        protected override void OnStop() {
            watcher.Dispose();
            timer.Dispose();
        }
    }
}
