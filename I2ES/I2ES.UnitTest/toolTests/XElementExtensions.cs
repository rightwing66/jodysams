﻿using System.Xml.Linq;

namespace I2ES.UnitTest.toolTests {
    public static class XElementExtensions {
        public static string NullableValue(this XAttribute attribute) {
            if (attribute == null) {
                return null;
            }
            return attribute.Value;
        }

        public static string NullableValue(this XElement attribute) {
            if (attribute == null) {
                return null;
            }
            return attribute.Value;
        }
    }

}