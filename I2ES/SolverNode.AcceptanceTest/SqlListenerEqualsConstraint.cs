﻿using System.Runtime.Remoting.Messaging;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Xml.XPath;
using I2ES.Model.Extensions;
using I2ES.Model.SqlListener;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace SolverNode.AcceptanceTest {
    internal class SqlListenerEqualsConstraint : Constraint {
        private ISqlListener actualListener;
        private string expectedMessage;
        private string actualMessage;
        private readonly ISqlListener expectedListener;

        public SqlListenerEqualsConstraint(ISqlListener expected) {
            this.expectedListener = expected;
        }

        public override bool Matches(object actualValue) {
            actualListener = actualValue as SqlListener;
            if (actualListener == null) {
                setMessage("Valid SqlListener", "null");
                return false;
            }
            actual = actualValue;
            return compareListeners();
        }
        public override void WriteDescriptionTo(MessageWriter writer) {
            writer.WriteExpectedValue("Expected Shit");
        }

        public override void WriteMessageTo(MessageWriter writer) {
            writer.DisplayDifferences(expectedMessage, actualMessage);
        }
        private void setMessage(string expectedMessage, string actualMessage) {
            this.expectedMessage = expectedMessage;
            this.actualMessage = actualMessage;
        }



        private bool compareListeners() {
            var expectedEnumerator = expectedListener.SqlCommands.GetEnumerator();
            var actualEnumerator = actualListener.SqlCommands.GetEnumerator();
            while (expectedEnumerator.MoveNext()) {
                if (!actualEnumerator.MoveNext()) {
                    break;
                }
                string expected = expectedEnumerator.Current;
                string actual = actualEnumerator.Current;
                if (expected == null) {
                    if (actual == null) {
                        break;
                    }
                    Assert.That(actualEnumerator.Current, Is.EqualTo(expectedEnumerator.Current));
                }
                //remove any timestamps
                Regex regex = new Regex("[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]\\s[0-9][0-9]:[0-9][0-9]:[0-9][0-9]");
                expected =regex.Replace(expected, "xxx");
                actual =  regex.Replace(actual, "xxx");
                regex = new Regex("[vV]alues\\s*\\([0-9]*,");
                expected = regex.Replace(expected, "values(xxx,");
                actual = regex.Replace(actual, "values(xxx,");

                Assert.That(expected,Is.EqualTo(actual));
            }
   

            return true;
        }

    }
}