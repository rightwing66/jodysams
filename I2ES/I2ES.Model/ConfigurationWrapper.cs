﻿using System;
using System.Configuration;

namespace I2ES.Model {
    public class ConfigurationWrapper : IConfigurationWrapper {
        public string NodeName => GetConfigValue("NodeName");
        public string TrafficManagerUrl => GetConfigValue("TrafficManagerURL");
        public string SolverRootDirectory => GetConfigValue("SolverRootDirectory");
        public string SolverOutputRootDirectory => GetConfigValue("SolverOutputRootDirectory");

        public string SolverNode => GetConfigValue("SolverNode");
        public int? JobLimit => getIntValue("JobLimit");
        public double TimerInterval => getDoubleValue("TimerInterval");

        private double getDoubleValue(string keyName) {
            string limit = GetConfigValue(keyName);
            double jobLimit;
            if (double.TryParse(limit, out jobLimit)) {
                return jobLimit;
            }
            throw new Exception("Cannot parse configuration key '" + keyName + "' to a double");
        }

        private int? getIntValue(string keyName) {
            string limit = GetConfigValue(keyName);
            int jobLimit;
            if (int.TryParse(limit, out jobLimit)) {
                return jobLimit;
            }
            throw new Exception("Cannot parse configuration key '"+ keyName  + "' to an int");
        }


        public string GetConfigValue(string keyName) {
            if (ConfigurationManager.AppSettings[keyName] == null) {
                throw new Exception("Could not find configuration entry for '" + keyName + "'");
            }
            return ConfigurationManager.AppSettings[keyName];
        }
    }
}