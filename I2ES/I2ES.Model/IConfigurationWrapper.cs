﻿namespace I2ES.Model{
    public interface IConfigurationWrapper{
        string NodeName { get; }
        string SolverRootDirectory { get; }
        string SolverOutputRootDirectory { get;  }
    }
}