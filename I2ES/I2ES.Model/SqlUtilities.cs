﻿using System;
using System.Data.SqlClient;

namespace I2ES.Model{
    public class SqlUtilities{
        public static int RunReturnScalarInt(string field, string sql, SqlConnection connection){
            using (SqlCommand command = new SqlCommand(sql, connection)){
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read()){
                    return int.Parse(reader[field].ToString());
                }
            }
            throw new Exception("Got no results from \r\n"+sql);
        }

        public static SqlDataReader RunReturnReader(string sql, SqlConnection connection){
            try{
                using (SqlCommand command = new SqlCommand(sql,connection)){
                    return command.ExecuteReader();
                }
            }
            catch (SqlException sEx){
                throw new Exception(sEx.Message + "\r\n" + sql,sEx);
            }
        }

        public static void Run(string sql, SqlConnection connection){
            using (SqlCommand command = new SqlCommand(sql,connection)){
                command.ExecuteNonQuery();
            }
        }
    }
}