﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using I2ES.Model.SqlListener;

namespace I2ES.Model {
    public class ConnectionProvider : IConnectionProvider{

        public ConnectionProvider() {
        }


        public ISqlWrapper CreateWrapper( ISqlListener listener) {
            if (ConfigurationManager.ConnectionStrings["ESRDConnectionString"] == null) {
                throw new Exception("Conenction String was null.  Did you forget to put a web/app config in the right project?");
            }

            var connectionString = ConfigurationManager.ConnectionStrings["ESRDConnectionString"].ConnectionString;
            SqlConnection connection = new SqlConnection { ConnectionString = connectionString };
            connection.Open();
            var sqlWrapper = new SqlWrapper(connection);
            if (listener == null) {
                sqlWrapper.SqlListeners = new NullSqlListenerCollection();
            }
            else {
                sqlWrapper.SqlListeners = new SqlListenerCollection();
                sqlWrapper.SqlListeners.Add(listener);
            }
            return sqlWrapper;
        }

        public ISqlWrapper CreateWrapper() {
            return CreateWrapper(null);
        }
    }
}
