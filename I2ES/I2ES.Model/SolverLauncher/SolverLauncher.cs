﻿using System;
using System.Diagnostics;
using System.Threading;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using I2ES.Model.Xml;

namespace I2ES.Model.SolverLauncher {
    public class SolverLauncher {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly ISqlWrapper sqlWrapper;
        private readonly IConfigurationWrapper configurationWrapper;
        private DirectoryInfo inputDirectoryInfo;
        private DirectoryInfo outputDirectoryInfo;
        private string inputXmlFileName;
        private string solverRootPath;
        private string outputRootPath;
        private string solverOutputPath;
        private TimeSpan timeout = new TimeSpan(0, 0, 10, 0);

        public SolverLauncher(ISqlWrapper sqlWrapper, IConfigurationWrapper configurationWrapper) {
            this.sqlWrapper = sqlWrapper;
            this.configurationWrapper = configurationWrapper;
        }

        public void LaunchStressCheckSolver(int jobId, Action<int, string> completedCallback) {
            log.Info("JobId:" + jobId);
            var inputXmlDocument = createInputXmlDocument(jobId);
            createDirectories(jobId);
            writeInputXmlToFile(inputXmlDocument);
            launchStressCheckProcess();
            Thread solverThread = new Thread(waitForSolver) { Name = "WaitForSolver" };
            solverThread.Start(new ThreadParam { JobId = jobId, Callback = completedCallback });
        }

        private void waitForSolver(object param) {
            ThreadParam parameter = (ThreadParam)param;
            var startTime = DateTime.Now;
            log.Info("WaitForSolver " + parameter.JobId);
            while (true) {
                if (solverIsDone(parameter.JobId)) {
                    break;
                }
                if (DateTime.Now - startTime > timeout) {
                    break;
                }
                Thread.Sleep(20000);
            }
            log.Info("Done waiting for solver");
            if (DateTime.Now - startTime > timeout) {
                log.Info("Solver timed out at " + timeout.ToString());
            }
            else {
                int? outcome = solverOutcome(parameter.JobId);
                if (outcome == 3) {
                    log.Info("StressCheck reported an error on jobId:" + parameter.JobId);
                }
                else {
                    log.InfoFormat("Calling back with jobId:{0} and output Path:{1} ", parameter.JobId, solverOutputPath);
                    parameter.Callback.Invoke(parameter.JobId, solverOutputPath);
                }
            }
        }

        private bool solverIsDone(int jobId) {
            int? outcome = solverOutcome(jobId);
            return outcome == 2 || outcome == 3;
        }

        private int? solverOutcome(int jobId) {
            var compileUtilities = new CompileUtilities() { Namespace = "I2ES.Model.sql" };
            ConnectionProvider provider = new ConnectionProvider();
            ISqlWrapper localSqlWrapper = provider.CreateWrapper();
            log.Debug("sqlwrapper.connection is:" + localSqlWrapper.ConnectionState);
            string sql = compileUtilities.GetResourceValue("GetSolverOutcome.sql")
                .Replace("~JobId", jobId.ToString());
            log.Debug(sql);
            try {
                int? val = localSqlWrapper.RunSqlReturnScalar(sql);
                log.Info(val);
                return val;
            }
            catch (Exception e) {
                log.Info(e);
                throw;
            }
        }

        private void createDirectories(int jobId) {
            solverRootPath = configurationWrapper.SolverRootDirectory;
            outputRootPath = configurationWrapper.SolverOutputRootDirectory;
            string solverJobOutputPath = Path.Combine(outputRootPath, "job" + jobId);
            var solverInputPath = Path.Combine(solverJobOutputPath, "input");
            solverOutputPath = Path.Combine(solverJobOutputPath, "output");
            inputDirectoryInfo = new DirectoryInfo(solverInputPath);
            inputDirectoryInfo.Create();
            outputDirectoryInfo = new DirectoryInfo(solverOutputPath);
            outputDirectoryInfo.Create();
        }

        private void writeInputXmlToFile(XDocument inputXmlDocument) {
            inputXmlFileName = Path.Combine(inputDirectoryInfo.FullName, "input.xml");
            XmlWriter writer = XmlWriter.Create(inputXmlFileName);
            inputXmlDocument.WriteTo(writer);
            writer.Flush();
            writer.Close();
        }

        private XDocument createInputXmlDocument(int jobId) {
            var writer = new InputXmlDocumentWriter(sqlWrapper);
            var inpuXDocument = writer.CreateDocument(jobId);
            sqlWrapper.Dispose();
            return inpuXDocument;
        }

        private void launchStressCheckProcess() {
            log.Info("Launching Process");
            string args = "\"" + inputXmlFileName + "\" \"" + outputDirectoryInfo.FullName + "\"";
            log.Info("args:" + args);
            log.Info("solverRootPath:" + solverRootPath);
            //start process
            Process process = new Process();
            process.StartInfo.FileName = Path.Combine(solverRootPath, "StressCheckSolver.exe");
            process.StartInfo.Arguments = args;
            process.StartInfo.WorkingDirectory = solverRootPath;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.CreateNoWindow = true;
            process.Start();
        }

        private struct ThreadParam {
            public int JobId { get; set; }
            public Action<int, string> Callback { get; set; }
        }
    }
}