﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace I2ES.Model.Extensions {
    public static class XElementExtensions {
        public static string NullableValue(this XAttribute attribute) {
            if (attribute == null) {
                return null;
            }
            return attribute.Value;
        }

        public static string NullableValue(this XElement element) {
            if (element == null) {
                return null;
            }
            return element.Value;
        }

        public static int? NullableIntValue(this XAttribute attribute) {
            if (attribute == null) {
                return null;
            }
            return int.Parse(attribute.Value);
        }

        public static int? NullableIntValue(this XElement attribute) {
            if (attribute == null) {
                return null;
            }
            return int.Parse(attribute.Value);
        }

        public static string FirstNullableValue(this IEnumerable<XElement> elements) {
            return NullableValue(elements.FirstOrDefault());
        }
    }

}