﻿using System;

namespace I2ES.Model.Extensions {
    public static class DbReadExtensions {
        public static int? GetNullableint(this object field) {
            if (field == null) return null;
            return int.Parse(field.ToString());
        }

        public static float? GetNullablefloat(this object field) {
            if (field == DBNull.Value) return null;
            return float.Parse(field.ToString());
        }

        public static string GetNullableString(this object field) {
            if (field == DBNull.Value) return null;
            if (field == null) return null;
            return field.ToString();
        }

        public static DateTime? GetNullableDateTime(this object field){
            if (field == DBNull.Value) return null;
            if (field == null) return null;
            var d =DateTime.Parse(field.ToString());

            return d;
        }

        public static T EnumNullableParse<T>(this object value) where T:struct {

            if (value == null || string.IsNullOrEmpty(value.ToString())) return (T)Enum.Parse(typeof (T), "0");
            return (T)Enum.Parse(typeof (T), value.ToString());
        }

        public static String EnumGetName<T>(this object value) where T : struct{
            if (value == null || string.IsNullOrEmpty(value.ToString())) return "";
            return Enum.GetName(typeof (T), value);
        }
    }
}