﻿using System;
using System.Text;

namespace I2ES.Model.Extensions {
    public static class LocalExtensions {
        //Takes a string and parses for a double.  Null, emptystring and spacess all map to null. AlphaCharacters
        //will throw an exception, someday
        public static double? NullableParse(this string value) {
            if (string.IsNullOrEmpty(value)) return null;
            if (value.Strip(' ').Length <= 0) return null;
            try {
                return double.Parse(value);
            }
            catch (Exception) {
                return null;
            }
        }

        public static int? NullableParseInt(this string value) {
            if (string.IsNullOrEmpty(value)) return null;
            if (value.Strip(' ').Length <= 0) return null;
            try {
                return int.Parse(value);
            }
            catch (Exception) {
                return null;
            }
        }

        public static string Strip(this string value, char toStrip) {
            StringBuilder newValue = new StringBuilder();
            for (int i = 0; i < value.Length; i++) {
                if (value[i] != toStrip) {
                    newValue.Append(value[i]);
                }
            }
            return newValue.ToString();
        }

        public static string ToEnumInt(this Enum enumValue){
            return Convert.ToInt32(enumValue).ToString();
        }

        public static string ToSqlDate(this DateTime? dateTime) {
            return string.Format("{0:yyyyMMdd hh:mm:ss tt}", dateTime);
        }

        public static string ToSqlDate(this DateTime dateTime) {
            return string.Format("{0:yyyyMMdd hh:mm:ss tt}", dateTime);
        }

        public static string ToShortDateTimeString(this DateTime dateTime) {
            return string.Format("{0:dd-mm-yy hh:mm:ss}", dateTime);
        }


    }
}
