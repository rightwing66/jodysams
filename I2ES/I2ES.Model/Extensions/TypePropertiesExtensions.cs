﻿using System;

namespace I2ES.Model.Extensions {
    public static class TypePropertiesExtensions {
        public static object GetPropertyValue(this object data, string name) {
            Type type = data.GetType();
            var info = type.GetProperty(name);
            return info.GetValue(data, null);
        }
    }
}