﻿using System;

namespace I2ES.Model{
    public static class SqlTextExtensions{
        public static string  ToSql<T>(this T? value) where T:struct{
            if (value == null){
                return "null";
            }
            return "'" + value + "'";
        }
        public static string  ToSql(this string value) {
            if (value == null){
                return "null";
            }
            return "'" + value + "'";
        }
    }
}