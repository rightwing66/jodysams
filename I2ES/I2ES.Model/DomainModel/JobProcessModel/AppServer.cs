﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I2ES.Model.DomainModel.JobProcessModel {
    public class AppServer {
        public string Name { get; set; }
        public string Url { get; set; }
        public string ApplicationName { get; set; }
    }
}
