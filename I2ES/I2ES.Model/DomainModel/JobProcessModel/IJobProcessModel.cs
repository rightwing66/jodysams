﻿using System.Collections.Generic;

namespace I2ES.Model.DomainModel.JobProcessModel {
    public interface IJobProcessModel {
        List<Job> LoadJobQueue(int? slotsOpen);
        int? JobsCurrentlyInProcess(AppServer appServer);
        List<AppServer> GetAppServers(List<string> names);
        void Dispose();
    }
}