﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using I2ES.Model.DomainModelContext;
using I2ES.Model.Extensions;

namespace I2ES.Model.DomainModel.JobProcessModel {
    public class JobProcessModel : IJobProcessModel {
        private readonly ISqlWrapper sqlWrapper;
        private readonly CompileUtilities compileUtilities = new CompileUtilities { Namespace = "I2ES.Model.sql" };

        public JobProcessModel(ISqlWrapper sqlWrapper) {
            this.sqlWrapper = sqlWrapper;
        }

        public List<Job> LoadJobQueue(int? slotsOpen) {
            JobReader reader = new JobReader() { SqlWrapper = sqlWrapper };
            return reader.LoadSolverJobQueue(slotsOpen);
        }

        public int? JobsCurrentlyInProcess(AppServer appServer) {
            var sql = compileUtilities.GetResourceValue("GetSolverProcessingSlots.sql")
                .Replace("~AppServer", appServer.Name);
            int? count = sqlWrapper.RunSqlReturnScalar(sql);
            return count;
        }

        public List<AppServer> GetAppServers(List<string> names) {
            string servers = string.Join(",", names);
            var sql = compileUtilities.GetResourceValue("GetAppServers.sql").Replace("~ServerNames", servers);
            var records = sqlWrapper.RunReturnIDataRecords(sql);
            return records.Select(ReadFromDataRecord).ToList();
        }

        public void Dispose() {
            sqlWrapper.Dispose();
        }

        public AppServer ReadFromDataRecord(IDataRecord record) {
            var appServer = new AppServer();
            appServer.Name = record["ServerName"].GetNullableString();
            appServer.Url = record["Url"].GetNullableString();
            appServer.ApplicationName = record["ApplicationName"].GetNullableString();
            return appServer;
        }
    }
}