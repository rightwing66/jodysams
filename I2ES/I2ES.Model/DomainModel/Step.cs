﻿using System.Collections.Generic;

namespace I2ES.Model.DomainModel{
    public class Step{
        public string Name { get; set; }
        public string Caption { get; set; }
        public int StepId { get; set; }
        public List<ParameterGroup> ParameterGroups { get; set; }

        public Step(){
            ParameterGroups = new List<ParameterGroup>();
        }
    }
}