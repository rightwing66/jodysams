﻿namespace I2ES.Model.DomainModel {
    public class FatigueLoadData {
        public int? FatigueLoadDataId { get; set; }
        public int? JobId { get; set; }
        public int? EID { get; set; }
        public int? FatigueLc { get; set; }
        public int? FatigueNode { get; set; }
        public float? FatiguePx { get; set; }
        public float? FatiguePy { get; set; }
        public float? FatiguePz { get; set; }
        public float? FatigueMx { get; set; }
        public float? FatigueMy { get; set; }
        public float? FatigueMz { get; set; }
        public float? FatigueFx { get; set; }
        public float? FatigueFy { get; set; }
        public float? FatigueFxy { get; set; }
    }
}