﻿namespace I2ES.Model.DomainModel {
    public class StaticMaxMin {
        public int? StaticMaxMinId { get; set; }
        public int? JobId { get; set; }
        public int? ElementId { get; set; }
        public float? MaxPx { get; set; }
        public float? MaxPxLoadCase { get; set; }
        public float? MinPx { get; set; }
        public float? MinPxLoadCase { get; set; }
        public float? MaxPy { get; set; }
        public float? MaxPyLoadCase { get; set; }
        public float? MinFy { get; set; }
        public float? MinFyLoadCase { get; set; }
        public float? MaxFxy { get; set; }
        public float? MaxFxyLoadCase { get; set; }
        public float? MinFxy { get; set; }
        public float? MinFxyLoadCase { get; set; }
        public float? MaxFx { get; set; }
        public float? MaxFxLoadCase { get; set; }
        public float? MinFx { get; set; }
        public float? MinFxLoadCase { get; set; }
        public float? MaxFy { get; set; }
        public float? MaxFyLoadCase { get; set; }
        public float? MinMy { get; set; }
        public float? MinMyLoadCase { get; set; }
        public float? MaxMz { get; set; }
        public float? MaxMzLoadCase { get; set; }
        public float? MinMz { get; set; }
        public float? MinMzLoadCase { get; set; }
        public float? MaxMx { get; set; }
        public float? MaxMxLoadCase { get; set; }
        public float? MinMx { get; set; }
        public float? MinMxLoadCase { get; set; }
        public float? MaxMy { get; set; }
        public float? MaxMyLoadCase { get; set; }
        public float? MinPy { get; set; }
        public float? MinPyLoadCase { get; set; }
        public float? MaxPz { get; set; }
        public float? MaxPzLoadCase { get; set; }
        public float? MinPz { get; set; }
        public float? MinPzLoadCase { get; set; }
    }
}