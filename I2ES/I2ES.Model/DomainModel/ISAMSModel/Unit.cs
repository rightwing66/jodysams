﻿namespace I2ES.Model.DomainModel{
    public class Unit{
        public string Name { get; set; }
        public string Description { get; set; }
        public string ClrType { get; set; }
    }
}