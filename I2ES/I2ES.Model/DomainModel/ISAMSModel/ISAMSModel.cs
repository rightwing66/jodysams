﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using I2ES.Model.DomainModelContext;

namespace I2ES.Model.DomainModel {
    public class ISAMSModel : IDisposable, IISAMSModel {
        public static DateTime SqlZeroDateTime = new DateTime(1900,1,1);

        private readonly IJobReader jobReader;
        private readonly IJobTypeReader jobTypeReader;
        private readonly IJobWriter jobWriter;
        //        private readonly SqlConnection connection;
        private readonly ISqlWrapper sqlWrapper;

        public virtual Job Job { get; set; }
        public virtual JobType JobType { get; set; }
        public virtual bool IsTest { get; set; }
        public virtual List<Job> AllJobs { get; set; }
        public virtual List<JobType> AllJobTypes { get; set; }

        /// <summary>
        /// This constructor is for moq.  If you don't know what Moq is, don't use this constructor.
        /// </summary>
        public ISAMSModel() {
        }

        /// <summary>
        /// Primary aggregate root of the data model.  This model contains and manages its own connection.
        /// Dispose when you are done!
        /// </summary>
        /// <param name="connectionProvider"></param>
        /// <param name="jobReader"></param>
        /// <param name="jobTypeReader"></param>
        /// <param name="jobWriter"></param>
        public  ISAMSModel(IConnectionProvider connectionProvider, IJobReader jobReader, IJobTypeReader jobTypeReader, IJobWriter jobWriter) {
            this.jobReader = jobReader;
            this.jobTypeReader = jobTypeReader;
            this.jobWriter = jobWriter;
            sqlWrapper = connectionProvider.CreateWrapper();
            if (jobReader.SqlWrapper == null) jobReader.SqlWrapper = sqlWrapper;
            if(jobTypeReader.SqlWrapper==null) jobTypeReader.SqlWrapper = sqlWrapper;
            if (jobWriter.SqlWrapper == null) jobWriter.SqlWrapper = sqlWrapper;
        }


        /// <summary>
        /// This overload loads a JobType, but not a Job
        /// </summary>
        /// <param name="jobTypeId"></param>
        /// <param name="userId"></param>
        public virtual void Load(int? jobTypeId, string userId) {
            Load(jobTypeId, userId, null);
        }

        /// <summary>
        /// This overload will load a Job into the JobType when provided with a JobId
        /// </summary>
        /// <param name="jobTypeId"></param>
        /// <param name="userId"></param>
        /// <param name="jobId"></param>
        public virtual void Load(int? jobTypeId, string userId, int? jobId) {
            JobType = loadJobType(jobTypeId, jobId);
        }

        public virtual Job LoadJob(int? jobId) {
            Job = jobReader.ReadSingleJob(jobId);
            if (Job == null) {
                throw new Exception("Cannot Load Job with Id:" + jobId +
                                    "\r\n Did you change the connection string in ALL the web.configs?");
            }
            JobType = jobTypeReader.ReadJobType(Job.JobTypeId, Job);
            if (JobType == null) {
                throw new Exception("Cannot Load JobType with Id:" + Job.JobTypeId);
            }
            Job.JobType = JobType;
            Job.JobTypeName = JobType.Name;

            return Job;
        }

        public virtual Job CopyJobIntoNewJob(int? oldJobId, string userId) {
            Job oldJob = LoadJob(oldJobId);
            
            int? newJobId = InsertJobGetID(oldJob.JobType, userId);
            Job.JobId = newJobId.Value;
            Job.JobState = JobState.New;
            Job.JobStateId = 1;
            SaveChanges();
            return Job;
        }


        public void WriteParameterValue(int? jobId, string parameterName, double parameterValue) {
            ParameterWriter writer = new ParameterWriter(sqlWrapper);
            writer.SaveParameterValueByParameterName(parameterName, parameterValue,jobId);
        }

        public virtual Job CreateJob(string userId) {
            if (JobType == null) {
                throw new InvalidOperationException("You must call Load before calling CreateJob");
            }
            //todo:Clumsy injection, will remove
            JobWriter jobWriter = new JobWriter{SqlWrapper = sqlWrapper,IsTest = IsTest};
            Job = jobWriter.CreateJob(JobType, userId);
            return Job;
        }



        public virtual void SaveChanges() {
            JobWriter jobWriter = new JobWriter{SqlWrapper = sqlWrapper};
            jobWriter.SaveChanges(Job);
        }

        private  int? InsertJobGetID(JobType jobType, string userId) {
            return jobWriter.CreateJobId(jobType, userId);
        }

        private JobType loadJobType(int? jobTypeId, int? jobId) {
            if (jobId != null) {
                Job = jobReader.ReadSingleJob(jobId);
            }
            JobType = jobTypeReader.ReadJobType(jobTypeId, Job);
            if (Job == null) { return JobType;}

            Job.JobType = JobType;
            Job.JobTypeName = JobType.Name;
            return JobType;
        }

        public virtual void Dispose() {
            sqlWrapper.Dispose();
        }


        public virtual void LoadJobs(string sortIndex, string sortOrder) {
            AllJobs = new List<Job>();
            IJobReader reader = new JobReader {SqlWrapper = sqlWrapper};
            AllJobs = reader.ReadAllJobs(sortIndex, sortOrder);
        }

        public virtual void LoadJobsForUser(string userId) {
            AllJobs = new List<Job>();
            IJobReader reader = new JobReader { SqlWrapper = sqlWrapper };
            AllJobs = reader.LoadJobsForUser(userId);            
        }

        public virtual void LoadJobTypes() {
            AllJobTypes = jobTypeReader.ReadAllJobTypes();
        }

        public virtual void DeleteJob(int? jobId) {
            IJobReader reader = new JobReader { SqlWrapper = sqlWrapper };
            reader.DeleteJob(jobId);
        }


        public virtual JobState GetJobStatus(int jobId) {
            return jobReader.GetJobStatus(jobId);
        }

        public SqlTransaction BeginTransaction() {
            return sqlWrapper.BeginTransaction();
        }
    }
}