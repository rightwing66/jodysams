﻿using System;
using System.Collections.Generic;

namespace I2ES.Model.DomainModel{
    public enum JobState {
        New =1,
        WaitingOnSolver =2,
        WaitingOnStoreData = 3,
        WaitingOnTrueNumbers = 4,
        Complete=5,
        Failed=6,
        SolverProcessing=7
    };

    public enum BackEndProcessState{
        NotStarted=1,
        InProgress=2,
        Complete=3,
        Failed=4
    }

    public enum StructureType{
        Metal=1,
        Composite=2
    }

    public enum AircraftModel{
        // ReSharper disable InconsistentNaming
        FA18A=1,
        FA18B=2,
        FA18C=3,
        FA18D=4,
        FA18E=5,
        FA18F=6,
        FA18G=7
        // ReSharper restore InconsistentNaming
    }

    public class Job {
        public virtual string BUNO { get; set; }
        public virtual string REINumber { get; set; }
        public virtual string UserId { get; set; }
        public virtual int JobId{ get; set; }

        public virtual DateTime? CreateTime { get; set; }
        public virtual DateTime? EditTime { get; set; }
        public virtual DateTime? ProcessBeginTime { get; set; }
        public virtual DateTime? ProcessEndTime { get; set; }
        public virtual JobState JobState { get; set; }

        public virtual JobType JobType { get; set; }
        public virtual int? JobTypeId { get; set; }
        public virtual string JobTypeName { get; set; }
        public virtual string CompletionStage { get; set; }
        public virtual List<ParameterGroup> ParameterGroups { get; set; }
        public virtual List<Parameter> AllParameters { get; set; }
        public virtual int? JobStateId { get; set; }
        public virtual string SubmittingNode { get; set; }
        public virtual BackEndProcessState SolverState { get; set; }
        public virtual string SolverNode { get; set; }
        public virtual DateTime? SolverBeginTime { get; set; }
        public virtual DateTime? SolverEndTime { get; set; }
        public virtual string SolverMessage { get; set; }
        public virtual BackEndProcessState LoadDataRetrieverState { get; set; }
        public virtual DateTime? LoadDataRetrieverBeginTime { get; set; }
        public virtual DateTime? LoadDataRetrieverEndTime { get; set; }
        public virtual string LoadDataRetrieverMessage { get; set; }
        public virtual BackEndProcessState StoreResultsState { get; set; }
        public virtual DateTime? StoreResultsBeginTime { get; set; }
        public virtual DateTime? StoreResultsEndTime { get; set; }
        public virtual string StoreResultsMessage { get; set; }
        public virtual BackEndProcessState TrueNumberState { get; set; }
        public virtual DateTime? TrueNumberBeginTime { get; set; }
        public virtual DateTime? TrueNumberEndTime { get; set; }
        public virtual string TrueNumberMessage { get; set; }
        public virtual AircraftModel AircraftModel { get; set; }
        public virtual StructureType StructureType { get; set; }
        public virtual string PartNumber { get; set; }
        public virtual string Report { get; set; }
        


        public Job(){
            AllParameters = new List<Parameter>();
            ProcessEndTime = DateTime.MinValue;
        }

        public override string ToString() {
            string pattern = "JobId:{0} CreateTime:{1} JobState{2}";
            return string.Format(pattern, JobId, CreateTime, JobState);
        }
    }
}