﻿using System.Data;
using I2ES.Model.DomainModel;
using I2ES.Model.Extensions;
namespace I2ES.Model.DomainModel {
    public enum LoadDataType { Static = 0, Fatigue = 1 }
    public class LoadData {
        public LoadDataType LoadDataType { get; set; }
        public int? LoadDataId { get; set; }
        public int? JobId { get; set; }
        public int? EID { get; set; }
        public int? LoadCase { get; set; }
        public int? Node { get; set; }
        public float? Px { get; set; }
        public float? Py { get; set; }
        public float? Pz { get; set; }
        public float? Mx { get; set; }
        public float? My { get; set; }
        public float? Mz { get; set; }
        public float? Fx { get; set; }
        public float? Fy { get; set; }
        public float? Fxy { get; set; }


    }
}