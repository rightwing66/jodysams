﻿using System.Collections.Generic;

namespace I2ES.Model.DomainModel {
    public class JobType {
        public int JobTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string InputSchema { get; set; }
        public string OutputSchema { get; set; }
        public List<ParameterGroup> ParameterGroups { get; set; }
        public Dictionary<string,Parameter> AllParameters { get; set; }
             
        public string ImagePath { get; set; }

        public JobType() {
            ParameterGroups = new List<ParameterGroup>();
            AllParameters = new Dictionary<string, Parameter>();
        }
    }
}