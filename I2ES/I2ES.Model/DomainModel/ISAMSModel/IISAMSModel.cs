﻿using System.Collections.Generic;

namespace I2ES.Model.DomainModel{
    public interface IISAMSModel{
        Job Job { get; set; }
        JobType JobType { get; set; }
        bool IsTest { get; set; }
        List<Job> AllJobs { get; set; }
        List<JobType> AllJobTypes { get; set; }


        /// <summary>
        /// This overload loads a JobType, but not a Job
        /// </summary>
        /// <param name="jobTypeId"></param>
        /// <param name="userId"></param>
        void Load(int? jobTypeId, string userId);

        /// <summary>
        /// This overload will load a Job into the JobType when provided with a JobId
        /// </summary>
        /// <param name="jobTypeId"></param>
        /// <param name="userId"></param>
        /// <param name="jobId"></param>
        void Load(int? jobTypeId, string userId, int? jobId);

        Job CreateJob(string userId);
        void SaveChanges();
        void Dispose();
        void LoadJobs(string sortIndex, string sortOrder);
        void LoadJobsForUser(string userId);
        void LoadJobTypes();
        JobState GetJobStatus(int jobId);
        Job LoadJob(int? jobId);
        void WriteParameterValue(int? jobId, string parameterName, double parameterValue);
        Job CopyJobIntoNewJob(int? oldJobId, string userId);
    }
}