﻿using System.Collections.Generic;

namespace I2ES.Model.DomainModel {
    public class ParameterGroup {
        public string Name { get; set; }
        public string Description { get; set; }
        public JobType JobType { get; set; }
        public int ParameterGroupId { get; set; }
        public List<Parameter> Parameters { get; set; }

        public ParameterGroup() {
            Parameters = new List<Parameter>();
        }

        public override string ToString() {
            return ParameterGroupId + " " + Name;
        }
    }
    public class ParamaterGroupIdComparer : IEqualityComparer<ParameterGroup> {
        public bool Equals(ParameterGroup x, ParameterGroup y) {
            return x.ParameterGroupId == y.ParameterGroupId;
        }

        public int GetHashCode(ParameterGroup parameterGroup) {
            if (ReferenceEquals(parameterGroup, null)) return 0;
            int hashProductName = parameterGroup.Name == null ? 0 : parameterGroup.Name.GetHashCode();
            int hashDescriptionCode = parameterGroup.Description == null ? 0 : parameterGroup.Description.GetHashCode();
            int hashId = parameterGroup.ParameterGroupId.GetHashCode();
            return hashProductName ^ hashDescriptionCode ^ hashId;
        }
    }

}