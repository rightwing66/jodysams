namespace I2ES.Model.DomainModel {
    public class ParameterValue  {
        public  Job Job { get; set; }
        public  Parameter Parameter { get; set; }
        public  double? Value { get; set; }
        public int? ParameterId { get; set; }
        public int? JobId { get; set; }
    }



}