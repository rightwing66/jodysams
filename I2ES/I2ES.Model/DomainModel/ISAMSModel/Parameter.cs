﻿using System;
using System.Collections.Generic;

namespace I2ES.Model.DomainModel {
    public class Parameter {
        private ParameterValue parameterValue;
        public int? ParameterId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string DefaultValue { get; set; }
        public JobType JobType { get; set; }
        public Unit Unit { get; set; }
        public string Display { get; set; }


        public int? JobTypeId { get; set; }
        public int ParameterGroupId { get; set; }
        public ParameterGroup ParameterGroup { get; set; }
        public ParameterValue ParameterValue {
            get { return parameterValue ?? (parameterValue = new ParameterValue()); }
            set { parameterValue = value; }
        }

        public override string ToString() {
            return ParameterId.ToString() + " " + Name + " | " + Description;
        }
    }
    public class ParamaterIdComparer : IEqualityComparer<Parameter> {
        public bool Equals(Parameter x, Parameter y) {
            return x.ParameterId == y.ParameterId;
        }

        public int GetHashCode(Parameter parameter) {
            if (Object.ReferenceEquals(parameter, null)) return 0;
            int hashName = parameter.Name == null ? 0 : parameter.Name.GetHashCode();
            int hashDescription = parameter.Description == null ? 0 : parameter.Description.GetHashCode();
            int hashLabel = parameter.Type == null ? 0 : parameter.Type.GetHashCode();
            int hashDisplay = parameter.Display == null ? 0 : parameter.Display.GetHashCode();
            return hashName ^ hashDescription ^ hashLabel ^ hashDisplay;
        }
    }
}