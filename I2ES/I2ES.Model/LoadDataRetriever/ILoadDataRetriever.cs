﻿using System;
using I2ES.Model.DomainModel;

namespace I2ES.Model.LoadDataRetriever{
    public interface ILoadDataRetriever{
        int LoadData(int elementId, int jobId, string location, ISqlWrapper sqlWrapper, int staticLoadCase, int fatigueLoadCase);
        LoadDataRetriever.CompletionStateEnum CompletionState { get; set; }
    }
}