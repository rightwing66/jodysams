﻿using System;
using System.IO;
using System.Reflection;
using System.Xml;

namespace I2ES.Model.LoadDataRetriever {
    public class LoadDataRetriever : ILoadDataRetriever {
        public enum CompletionStateEnum {
            Failure,
            Success
        }

        public int LoadData(int elementId, int jobId, string location, ISqlWrapper sqlWrapper, int staticLoadCase, int fatigueLoadCase) {
            var utilities = new CompileUtilities { Namespace = "I2ES.Model.sql" };

            var rodBarOrShell = DetermineRodBarOrShell(elementId);
            if (rodBarOrShell != "RodBar" && rodBarOrShell != "Shell") {
                CompletionState = CompletionStateEnum.Failure;
                return 0;
            }

            var loadDataExists = CheckForExistingLoadData(jobId, staticLoadCase, fatigueLoadCase, sqlWrapper, utilities);
            if (loadDataExists) DeleteRowsForJobId(jobId, sqlWrapper, utilities);

            int? success = ExecuteGetLoadData(elementId, jobId, location, sqlWrapper, staticLoadCase, fatigueLoadCase, utilities, rodBarOrShell);
            CompletionState = success == 1 ? CompletionStateEnum.Success : CompletionStateEnum.Failure;
            if (staticLoadCase == 0) {
                success = ExecuteStaticMaxMinLoadData(jobId, elementId, rodBarOrShell, sqlWrapper, utilities);
                CompletionState = success == 1 ? CompletionStateEnum.Success : CompletionStateEnum.Failure;
            }
            if (fatigueLoadCase == 0) {
                success = ExecuteFatigueMaxMinLoadData(jobId, elementId, rodBarOrShell, sqlWrapper, utilities);
                CompletionState = success == 1 ? CompletionStateEnum.Success : CompletionStateEnum.Failure;
            }

            
            sqlWrapper.Dispose();
            return 0;
        }

        public void DeleteRowsForJobId(int jobId, ISqlWrapper sqlWrapper, CompileUtilities utilities) {
            var sql = utilities.GetResourceValue("DeleteLoadDataByJobId.sql")
               .Replace("~JobId", jobId.ToString());
            var success = sqlWrapper.RunSqlReturnScalar(sql);
            CompletionState = success == 1 ? CompletionStateEnum.Success : CompletionStateEnum.Failure;
        }

        private static bool CheckForExistingLoadData(int jobId, int staticLoadCase, int fatigueLoadCase, ISqlWrapper sqlWrapper, CompileUtilities utilities) {
            var loadDataExists = false;
            int? staticMaxMinExists = 0;
            int? fatigueMaxMinExists = 0;

            var sql = utilities.GetResourceValue("CheckStaticLoadDataForJobId.sql").Replace("~JobId", jobId.ToString());
            var staticLoadDataExists = sqlWrapper.RunSqlReturnScalar(sql);

            sql = utilities.GetResourceValue("CheckFatigueLoadDataForJobId.sql").Replace("~JobId", jobId.ToString());
            var fatigueLoadDataExists = sqlWrapper.RunSqlReturnScalar(sql);

            if (staticLoadCase == 0) {
                sql = utilities.GetResourceValue("CheckStaticMaxMinForJobId.sql").Replace("~JobId", jobId.ToString());
                staticMaxMinExists = sqlWrapper.RunSqlReturnScalar(sql);
            }
            if (fatigueLoadCase == 0) {
                sql = utilities.GetResourceValue("CheckFatigueMaxMinForJobId.sql").Replace("~JobId", jobId.ToString());
                fatigueMaxMinExists = sqlWrapper.RunSqlReturnScalar(sql);
            }

            if ((staticLoadDataExists > 0 && fatigueLoadDataExists > 0 && staticLoadCase > 0 && fatigueLoadCase > 0) ||
                (staticLoadDataExists > 0 && fatigueLoadDataExists > 0 && staticMaxMinExists > 0 &&
                 fatigueMaxMinExists > 0)) {
                loadDataExists = true;
            }

            return loadDataExists;
        }
        private static int? ExecuteFatigueMaxMinLoadData(int jobId, int elementId, string rodBarOrShell, ISqlWrapper sqlWrapper, CompileUtilities utilities) {
            var sql = utilities.GetResourceValue("GetFatigueMaxMinLoadData.sql")
               .Replace("~JobId", jobId.ToString())
               .Replace("~ElementId", elementId.ToString())
               .Replace("~RodBarOrShell", rodBarOrShell);
            var success = sqlWrapper.RunSqlReturnScalar(sql);
            return success;
        }

        private static int? ExecuteStaticMaxMinLoadData(int jobId, int elementId, string rodBarOrShell,
           ISqlWrapper sqlWrapper, CompileUtilities utilities) {
            var sql = utilities.GetResourceValue("GetStaticMaxMinLoadData.sql")
               .Replace("~JobId", jobId.ToString())
               .Replace("~ElementId", elementId.ToString())
               .Replace("~RodBarOrShell", rodBarOrShell);
            var success = sqlWrapper.RunSqlReturnScalar(sql);
            return success;
        }

        private static int? ExecuteGetLoadData(int elementId, int jobId, string location, ISqlWrapper sqlWrapper, int staticLoadCase,
           int fatigueLoadCase, CompileUtilities utilities, string rodBarOrShell) {
            var sql = utilities.GetResourceValue("GetLoadData.sql")
               .Replace("~ElementId", elementId.ToString())
               .Replace("~JobId", jobId.ToString())
               .Replace("~StaticLoadCase", staticLoadCase.ToString())
               .Replace("~FatigueLoadCase", fatigueLoadCase.ToString())
               .Replace("~Location", location)
               .Replace("~RodBarOrShell", rodBarOrShell);
            var success = sqlWrapper.RunSqlReturnScalar(sql);
            return success;
        }

        private static string DetermineRodBarOrShell(int elementId) {
            var rodBarOrShell = "";
            var rodListPath = Path.Combine(Path.GetDirectoryName(path: Assembly.GetExecutingAssembly().CodeBase), "LoadDataRetriever\\RodList.xml");
            var shellListPath = Path.Combine(Path.GetDirectoryName(path: Assembly.GetExecutingAssembly().CodeBase), "LoadDataRetriever\\ShellList.xml");

            var shellList2 = new XmlTextReader(shellListPath);
            var rodList2 = new XmlTextReader(rodListPath);
            rodBarOrShell = SearchRodList(elementId, rodList2, rodBarOrShell);
            if (rodBarOrShell != "RodBar") {
                rodBarOrShell = SearchShellList(elementId, shellList2, rodBarOrShell);
            }

            return rodBarOrShell;
        }

        private static string SearchShellList(int elementId, XmlReader shellList2, string rodBarOrShell) {
            while (shellList2.Read()) {
                shellList2.ReadToFollowing("EID");
                if (shellList2.ReadString() == elementId.ToString()) {
                    rodBarOrShell = "Shell";
                }
            }
            return rodBarOrShell;
        }

        private static string SearchRodList(int elementId, XmlReader rodList2, string rodBarOrShell) {
            while (rodList2.Read()) {
                rodList2.ReadToFollowing("EID");
                if (rodList2.ReadString() == elementId.ToString()) {
                    rodBarOrShell = "RodBar";
                }
            }
            return rodBarOrShell;
        }

        public CompletionStateEnum CompletionState { get; set; }

    }
}