﻿using System;
using System.Diagnostics;
using System.Xml.Linq;
using I2ES.Model.Extensions;

//using System.Data.SqlClient;

namespace I2ES.Model.Xml {

    public class ModelLoadXmlDatabaseWriter {
        private readonly ISqlWrapper sqlWrapper;
        private readonly CompileUtilities utilities = new CompileUtilities { Namespace = "I2ES.Model.Xml.XmlSql" };
        public ModelLoadXmlDatabaseWriter(ISqlWrapper wrapper) {
            sqlWrapper = wrapper;
        }



        public virtual int ParseInputXmlFile(string xmlstring) {
            var rootElement = XElement.Parse(xmlstring);
            var jobTypeName = rootElement.Element("information").Element("jobtype_name").NullableValue();
            Debug.Assert(jobTypeName != null, "Job name was null");
            var sql = utilities.GetResourceValue("GetJobTypeId.sql").Replace("~Name", jobTypeName);

            var jobTypeId = sqlWrapper.RunSqlReturnScalar(sql);

            var parameters = rootElement.Elements("parameters");

            var parameterGroups = parameters.Elements("parametergroup");
            foreach (var parameterGroupXElement in parameterGroups) {
                var parameterGroup = UpsertParameterGroup(parameterGroupXElement, jobTypeId);
                foreach (var parameterElement in parameterGroupXElement.Elements("parameter")) {
                    Console.WriteLine(parameterElement.Attribute("name"));
                    InsertParameters(parameterElement, jobTypeId, parameterGroup.ID,parameterGroup.Name);
                }
            }
            //InsertFiles(rootElement);
            return 0;
        }
        private void InsertParameters(XElement parameterElement, int? jobTypeId, int? parameterGroupId, string parameterGroupName) {
            var sql = utilities.GetResourceValue("InsertParameter.sql");
            var unitName = parameterElement.Attribute("units").NullableValue().ToSql();
            var parameterName =(parameterGroupName + "_"+ parameterElement.Attribute("name").NullableValue()).ToSql();
            var type = parameterElement.Attribute("type").NullableValue().ToSql();
            var label = parameterElement.Attribute("label").NullableValue().ToSql();
            var value = parameterElement.NullableValue().ToSql();

            sql = sql.Replace("~UnitName", unitName).Replace("~ParameterName", parameterName).Replace("~Label", label)
               .Replace("~Type", type).Replace("~JobTypeId", jobTypeId.ToString()).Replace("~Value", value)
               .Replace("~ParameterGroupId", parameterGroupId.ToString());

            //            Console.WriteLine(sql);
            //            Console.WriteLine("________________________________________");
            sqlWrapper.RunSqlReturnScalar(sql);

        }

        private NameID UpsertParameterGroup(XElement parameterGroupXElement, int? jobTypeId) {
            var sql = utilities.GetResourceValue("InsertJobParameters.sql");
            var name = parameterGroupXElement.Attribute("name").NullableValue();
            var description = parameterGroupXElement.Attribute("description").NullableValue() ?? "";


            sql = sql.Replace("~JobTypeId", jobTypeId.ToString()).Replace("~ParameterGroupName", name)
                .Replace("~ParameterGroupDescription", description);
            Console.WriteLine("Inserting ParameterGroup " + name);
            //            Console.WriteLine(sql);
            int? id =  sqlWrapper.RunSqlReturnScalar(sql);
            return new NameID {ID = id, Name = name};
        }

        private void InsertFiles(XElement rootElement, int solverOutputId) {
            var files = rootElement.Descendants("file");
            foreach (var fileElement in files) {
                var sql = utilities.GetResourceValue("InsertInputFile.sql");
                var fileType = fileElement.Attribute("type").NullableValue();
                var filePath = fileElement.NullableValue();
                sql = sql.Replace("~solverOutputId", solverOutputId.ToString())
                    .Replace("~FileType", fileType).Replace("~FilePath", filePath);
                sqlWrapper.RunSqlReturnScalar(sql);
            }
        }

        private class NameID {
            public string Name { get; set; }
            public int? ID { get; set; } 
        }

    }
}