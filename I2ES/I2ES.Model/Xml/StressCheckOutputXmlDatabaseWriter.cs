using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using I2ES.Model.Extensions;

namespace I2ES.Model.Xml {
    public enum ResultType { Fatigue = 1, Static = 2 }
    public class StressCheckOutputXmlDatabaseWriter {
        private static readonly log4net.ILog log =
    log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly ISqlWrapper sqlWrapper;
        private readonly CompileUtilities compileUtilities = new CompileUtilities { Namespace = "I2ES.Model.Xml.XmlSql" };

        public StressCheckOutputXmlDatabaseWriter(ISqlWrapper sqlWrapper) {
            this.sqlWrapper = sqlWrapper;
        }

        //todo: verify jobId parameter matches JobId in xml file
        public virtual int ParseOutputXmlFile(string solverOutputFolderPath,  string outputXmlFile, int jobId) {
            var rootElement = XElement.Parse(outputXmlFile);
            DeleteExistingOutputForJobId(jobId);
            var solverOutputId = InsertSolverOutput(rootElement, jobId);
            var solverOutput = Convert.ToInt32(solverOutputId);
            InsertFiles(solverOutputFolderPath, rootElement, solverOutputId);
            InsertFatigueResults(rootElement, solverOutputId, "fatigue_results");
            InsertStaticResults(rootElement, solverOutputId, "static_results");
            return solverOutput;
        }

        private void DeleteExistingOutputForJobId(int? jobId) {
            var sql = compileUtilities.GetResourceValue("DeleteJobFromResults.sql")
                .Replace("~JobId", jobId.ToString());
            sqlWrapper.RunSql(sql);
        }

        private void InsertStaticResults(XElement rootElement, int? solverOutputId, string resultType) {
            var staticResults = rootElement.Element(resultType).Elements("extraction");
            var sql = compileUtilities.GetResourceValue("InsertExtraction.sql");
            foreach (var extraction in staticResults) {
                var name = extraction.Attribute("name").NullableValue();
                var lineSql = sql.Replace("~SolverOutputId", solverOutputId.ToString())
                    .Replace("~Name", name).Replace("~ResultTypeName", ResultType.Static.ToString());
                var extractionId = sqlWrapper.RunSqlReturnScalar(lineSql);

                var fields = staticResults.FirstOrDefault(x => x.Attribute("name").Value==name).Descendants("field");
                var fieldSql = compileUtilities.GetResourceValue("InsertResultField.sql");
                foreach (var field in fields) {
                    var fieldName = field.Attribute("name").NullableValue();
                    var units = field.Attribute("units").NullableValue();
                    var dataType = field.Attribute("type").NullableValue();

                    var lineFieldSql = fieldSql.Replace("~Name", fieldName)
                        .Replace("~Units", units.ToSql())
                        .Replace("~DataType", dataType.ToSql())
                        .Replace("~ExtractionId", extractionId.ToString());
                    var valueString = field.NullableValue();
                    double result;
                    if (Double.TryParse(valueString, out result)) {
                        double? nullResult = result;
                        lineFieldSql = lineFieldSql.Replace("~NumericValue", nullResult.ToSql())
                            .Replace("~StringValue", "null")
                            .Replace("~IsNumber", "1");
                    }
                    else {
                        lineFieldSql = lineFieldSql.Replace("~NumericValue", "null")
                            .Replace("~StringValue", valueString.ToSql())
                            .Replace("~IsNumber", "0");
                    }
                    sqlWrapper.RunSql(lineFieldSql);
                }
            }
        }

        private void InsertFatigueResults(XElement rootElement, int? solverOutputId, string resultType) {
            var fatigueResults = rootElement.Element(resultType).Elements("extraction");
            var sql = compileUtilities.GetResourceValue("InsertExtraction.sql");
            foreach (var extraction in fatigueResults) {
                var name = extraction.Attribute("name").NullableValue();
                var lineSql = sql.Replace("~SolverOutputId", solverOutputId.ToString())
                    .Replace("~Name", name).Replace("~ResultTypeName", ResultType.Fatigue.ToString());
                var extractionId = sqlWrapper.RunSqlReturnScalar(lineSql);
                var extractionXml = fatigueResults.Where(x => x.Attribute("name").Value == name).FirstOrDefault();
                var fields = extractionXml.Descendants("field");
                var fieldSql = compileUtilities.GetResourceValue("InsertResultField.sql");
                foreach (var field in fields) {
                    var fieldName = field.Attribute("name").NullableValue();
                    var units = field.Attribute("units").NullableValue();
                    var dataType = field.Attribute("type").NullableValue();

                    var lineFieldSql = fieldSql.Replace("~Name", fieldName)
                        .Replace("~Units", units.ToSql())
                        .Replace("~DataType", dataType.ToSql())
                        .Replace("~ExtractionId", extractionId.ToString());
                    var valueString = field.NullableValue();
                    double result;
                    if (Double.TryParse(valueString, out result)) {
                        double? nullResult = result;
                        lineFieldSql = lineFieldSql.Replace("~NumericValue", nullResult.ToSql())
                            .Replace("~StringValue", "null")
                            .Replace("~IsNumber", "1");
                    }
                    else {
                        lineFieldSql = lineFieldSql.Replace("~NumericValue", "null")
                            .Replace("~StringValue", valueString.ToSql())
                            .Replace("~IsNumber", "0");
                    }
                    sqlWrapper.RunSql(lineFieldSql);
                }
            }
        }

        private void InsertFiles(string solverOutputFolderPath, XElement rootElement, int? solverOutputId) {
            var files = rootElement.Descendants("file");
            foreach (var fileElement in files) {
                var sql = compileUtilities.GetResourceValue("InsertResultFile.sql");
                var fileType = fileElement.Attribute("type").NullableValue();
                var filePath = Path.Combine(solverOutputFolderPath, fileElement.NullableValue());
                
                
                var imageBytes = File.ReadAllBytes(filePath);
                
                sql = sql.Replace("~SolverOutputId", solverOutputId.ToString())
                    .Replace("~FileType", fileType).Replace("~FilePath", filePath);
                sqlWrapper.RunCommandWithParameter(sql, "@File",imageBytes);
            }
        }

        private int? InsertSolverOutput(XElement rootElement, int? jobId) {
            var status = rootElement.Descendants("status").FirstOrDefault().NullableValue();
            var completionTime = DateTime.Now.ToSqlDate();
            var sql = compileUtilities.GetResourceValue("InsertSolverOutput.sql").Replace("~SolverId", "null")
                .Replace("~CompletionTime", completionTime).Replace("~Status", status)
                .Replace("~JobId", jobId.ToString());
            log.DebugFormat("JobID:{0} \r\n{1}",jobId,sql);
            return sqlWrapper.RunSqlReturnScalar(sql);
        }
    }
}