﻿namespace I2ES.Model.Xml {
    public static class XMLDocumentExtensions {
        public static string StripPrependedParameterGroupName(this string parameterName) {
            int underScore = parameterName.IndexOf("_");
            int total = parameterName.Length;
            string result = parameterName.Substring(underScore + 1, total - underScore - 1);
            return result;
        }
    }
}