﻿declare @unitId int
declare @parameterId int

if ~UnitName is not null
	if not exists(select UnitId from Unit where Name=~UnitName)
	Begin
		Insert Unit(Name) Values(~UnitName)
		set @unitId=@@Identity
	End
	else
		set @unitId = (select UnitId from Unit where Name=~UnitName)
else
	set @unitId = null

--if not exists(
--	select p.ParameterId 
--	from Parameter p join ParameterGroup_x_Parameter pxp on p.ParameterId = pxp.ParameterId
--	where Name = ~ParameterName and pxp.ParameterGroupId = ~ParameterGroupId
--)
--begin
	Insert Parameter(Name, [Type], JobTypeId, UnitId, Value, Label)
	Values (~ParameterName,~Type,~JobTypeId, @unitId, ~Value, ~Label)
	
	select @parameterId = @@Identity
	print '@parameterId'
	print @ParameterId
--end
--else
--begin
	--set @parameterId = (select parameterId from Parameter where Name = ~ParameterName and JobTypeId=~JobTypeId)
--end

Insert ParameterGroup_x_Parameter (ParameterId, ParameterGroupId)
values (@parameterId, ~ParameterGroupId)

select @parameterId

