﻿declare @JobTypeId int
set @JobTypeId= (select jobTypeId from jobtype where name = '~JobTypeName')
if @JobTypeId is not null
begin
	Delete ParameterGroup_x_Parameter where ParameterGroupId in 
		(
		select parameterGroupId from ParameterGroup
		where JobTypeId=@JobTypeId
		)

	delete ParameterGroup where jobtypeid = @JobTypeId
end	
select @JobTypeId