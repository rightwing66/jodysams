﻿declare @ResultTypeId int
Select @ResultTypeId = (Select ResultTypeId from Results.ResultType where Name='~ResultTypeName')
Insert Results.Extraction (SolverOutputId, ResultTypeId, Name)
Values (~SolverOutputId, @ResultTypeId, '~Name')
select @@Identity
