﻿declare @SolverOutputId int
declare @ExtractionId int
select @SolverOutputId = (Select SolverOutputId from Results.SolverOutput where JobId=~JobId)

if @SolverOutputId is not null
begin

	delete from Results.ResultField 
	from Results.ResultField as rf
	inner join Results.Extraction as e
	on rf.extractionId = e.Extractionid
	where e.SolverOutputId = @SolverOutputId

	delete Results.ResultFile
	where SolverOutputId = @SolverOutputId

	delete Results.Extraction
	where SolverOutputId = @SolverOutputId

	delete Results.SolverOutput
	where SolverOutputId = @SolverOutputId
end