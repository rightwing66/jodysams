﻿using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Xml.Linq;
using I2ES.Model.DomainModel;
using I2ES.Model.DomainModelContext;

namespace I2ES.Model.Xml {
    public class InputXmlDocumentWriter {
        private readonly ISqlWrapper sqlWrapper;

        public InputXmlDocumentWriter(ISqlWrapper sqlWrapper) {
            this.sqlWrapper = sqlWrapper;
        }

        public XDocument CreateDocument(int jobId) {
            IConnectionProvider connectionProvider = new ConnectionProvider();
            ISAMSModel model = new ISAMSModel(connectionProvider, new JobReader { SqlWrapper = sqlWrapper }, new JobTypeReader(){SqlWrapper = sqlWrapper}, new JobWriter());
            model.LoadJob(jobId);
            Job job = model.Job;
            var document = new XDocument(new XElement("rarat_input"));
            var root = document.Descendants().FirstOrDefault();
            AddInformationElementsToRoot(root, job);
            root.Add(new XElement("parameters"));
            var parameters = root.Descendants("parameters").FirstOrDefault();
            foreach (var parameterGroup in job.JobType.ParameterGroups) {
                AddParameterGroupsToRoot(parameters, parameterGroup);
            }
            AddFilesElement(root);
            return document;
        }

        private static void AddFilesElement(XElement root){
            root.Add(new XElement("files",
                new XElement("file",
                    new XAttribute("type", "handbook"),
                    "Single_Hole.scw")
                ));
        }

        private static void AddInformationElementsToRoot(XElement root, Job job){
            root.Add(new XElement("information",
                new XElement("jobid", job.JobId),
                new XElement("job_type", job.JobType.JobTypeId),
                new XElement("userId", job.UserId),
                new XElement("jobtype_name", job.JobType.Name)
                ));
        }

        private static void AddParameterGroupsToRoot(XElement parameters, ParameterGroup parameterGroup){
            parameters.Add(new XElement("parametergroup",
                new XAttribute("name", parameterGroup.Name), new XAttribute("description", parameterGroup.Description)));
            var parameterGroupElement =
                parameters.Descendants("parametergroup")
                    .FirstOrDefault(x => x.Attributes().Count(a => a.Name == "name" && a.Value == parameterGroup.Name) > 0);
            foreach (var parameter in parameterGroup.Parameters){
                addParametersToParameterGroup(parameter, parameterGroupElement);
            }
        }

        private static void addParametersToParameterGroup(Parameter parameter, XElement parameterGroupElement) {
            string parameterValue;
            if (parameter.ParameterValue == null || parameter.ParameterValue.Value == null) {
                parameterValue = parameter.DefaultValue;
            }
            else {
                parameterValue = parameter.ParameterValue.Value.ToString();
            }
//            var parameterValue = (parameter.ParameterValue == null || parameter.ParameterValue.Value == null)
//                ? parameter.DefaultValue
//                : parameter.ParameterValue.Value.ToString();
            
            parameterGroupElement.Add(
                new XElement("parameter",
                    new XAttribute("name", parameter.Name.StripPrependedParameterGroupName()),
                    parameterValue
                    )
                );
            var parameterElement =
                parameterGroupElement.Descendants("parameter").FirstOrDefault(p => p.Attribute("name").Value == parameter.Name.StripPrependedParameterGroupName());
            if (parameter.Type != null){
                parameterElement.Add(new XAttribute("type", parameter.Type));
            }
            if (parameter.Description != null){
                parameterElement.Add(new XAttribute("label", parameter.Description));
            }
            if (parameter.Unit != null && parameter.Unit.Name != null){
                parameterElement.Add(new XAttribute("units", parameter.Unit.Name));
            }
        }



    }
}