using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using I2ES.Model.SqlListener;

namespace I2ES.Model{
    public interface ISqlWrapper{
        int? RunSqlReturnScalar(string sql);
        List<IDataRecord> RunReturnIDataRecords(string sql);
        SqlDataReader RunReturnReader(string sql);
        void Dispose();
        void RunSql(string sql); 
        ConnectionState ConnectionState { get; }
        ISqlListenerCollection SqlListeners { get; set; }
        void RunCommandWithParameter(string sql, string name, object value);
        SqlTransaction BeginTransaction();
    }
}