using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using I2ES.Model.SqlListener;

namespace I2ES.Model {
    public class SqlWrapper : ISqlWrapper {
        private readonly SqlConnection connection;
        public ISqlListenerCollection SqlListeners { get; set; }

        public SqlTransaction Transaction { get; set; }

        public SqlTransaction BeginTransaction() {
            Transaction= connection.BeginTransaction();
            return Transaction;
        }

        public SqlWrapper(SqlConnection connection) {
            this.connection = connection;
            SqlListeners = new NullSqlListenerCollection();
        }

        public int? RunSqlReturnScalar(string sql) {
            try {
                SqlCommand insertCommand = new SqlCommand(sql, connection) { Transaction = Transaction };
                SqlListeners.AddSqlCommand(sql);
                var result = insertCommand.ExecuteScalar();
                if (result == null) {
                    throw new Exception("No return from \r\n" + sql);
                }
                if (result == DBNull.Value) return null;
                int returnScalar = int.Parse(result.ToString());
                insertCommand.Dispose();
                return returnScalar;
            }
            catch (SqlException sEx) {
                throw new Exception(sEx.Message + "\r\n" + sql, sEx);
            }
        }

        public List<IDataRecord> RunReturnIDataRecords(string sql) {
            try {
                using (SqlCommand command = new SqlCommand(sql, connection) { Transaction = Transaction }) {
                    SqlListeners.AddSqlCommand(sql);
                    var reader = command.ExecuteReader();
                    var rawRecords = reader.Cast<IDataRecord>().ToList();
                    reader.Dispose();
                    return rawRecords;
                }
            }
            catch (SqlException sEx) {
                throw new Exception(sEx.Message + "\r\n" + sql, sEx);
            }
        }

        public SqlDataReader RunReturnReader(string sql) {
            try {
                using (SqlCommand command = new SqlCommand(sql, connection) { Transaction = Transaction }) {
                    SqlListeners.AddSqlCommand(sql);
                    return command.ExecuteReader();
                }
            }
            catch (SqlException sEx) {
                throw new Exception(sEx.Message + "\r\n" + sql, sEx);
            }
        }


        public void RunSql(string sql) {
            try {
                SqlCommand insertCommand = new SqlCommand(sql, connection) { Transaction = Transaction };
                SqlListeners.AddSqlCommand(sql);
                insertCommand.ExecuteNonQuery();
                insertCommand.Dispose();
            }
            catch (SqlException sEx) {
                throw new Exception(sEx.Message + "\r\n" + sql, sEx);
            }
        }

        public ConnectionState ConnectionState => connection.State;

        public void RunCommandWithParameter(string sql, string name, object value) {
            try {
                SqlCommand command = new SqlCommand(sql, connection) { Transaction = Transaction };
                SqlListeners.AddSqlCommand(sql);
                command.Parameters.AddWithValue(name, value);
                command.ExecuteNonQuery();
                command.Dispose();
            }
            catch (SqlException sEx) {
                throw new Exception(sEx.Message + "\r\n" + sql, sEx);
            }
        }

        public void Dispose() {
            connection.Dispose();
        }
    }
}