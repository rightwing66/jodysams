﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using I2ES.Model.Extensions;

namespace I2ES.Model {
    public class CompileUtilities {
        private readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string callingNameSpace;
        public string Namespace {
            get { return callingNameSpace; }
            set {
                if (!value.EndsWith(".")) {
                    callingNameSpace = value + ".";
                }
                else {
                    callingNameSpace = value;
                }
            }
        }

        public string GetResourceValue(string resourceName) {
            var assemblyName = Assembly.GetCallingAssembly().FullName;
            var names = Assembly.GetCallingAssembly().GetManifestResourceNames();
            var stream = Assembly.GetCallingAssembly().GetManifestResourceStream(Namespace + resourceName);
            if (stream == null) {
                log.Error("Looking in "+assemblyName+"  Couldn't find manifest assembly named " + Namespace + resourceName +
                          "\r\nAvailable names are:" + names.GetConsoleList());
                throw new Exception("Couldn't find manifest assembly named " + Namespace + resourceName + "\r\nAvailable names are:" + names.GetConsoleList());
            }
            string xmlValue = new StreamReader(stream).ReadToEnd();
            return xmlValue;
        }
        public Stream GetResourceStream(string resourceName) {
            var names = Assembly.GetCallingAssembly().GetManifestResourceNames();
            var stream = Assembly.GetCallingAssembly().GetManifestResourceStream(Namespace + resourceName);
            if (stream == null) {
                throw new Exception("Couldn't find manifest assembly named " + Namespace + resourceName + "\r\nAvailable names are:" + names.GetConsoleList());
            }
            return stream;
        }

    }
}