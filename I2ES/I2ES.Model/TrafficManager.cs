﻿using I2ES.Model.DomainModel;
using I2ES.Model.LoadDataRetriever;

namespace I2ES.Model {
    public class TrafficManager : ITrafficManager {
        private readonly IConnectionProvider connectionProvider;
        private readonly IJobTypeModel model;
        private readonly ILoadDataRetriever loadDataRetriever;

        public TrafficManager(IConnectionProvider connectionProvider, IJobTypeModel model, ILoadDataRetriever loadDataRetriever) {
            this.connectionProvider = connectionProvider;
            this.model = model;
            this.loadDataRetriever = loadDataRetriever;
        }

        public JobState GetJobStatus(int jobId) {
            return model.GetJobStatus(jobId);
        }

        public void StartLoadDataRetriever(int elementId, int jobId, string location) {
            setJobStatus(jobId, JobState.InProgress);
            loadDataRetriever.LoadData(elementId, jobId, location, connectionProvider.CreateWrapper(), LoadDataRetreverComplete,0,0);
        }

        public void LoadDataRetreverComplete(int jobId) {
            setJobStatus(jobId, JobState.Complete);
        }

        private void setJobStatus(int jobId, JobState jobState) {
            model.SetJobStatus(jobId, jobState);
        }

        public void JobCreated(int jobId){
            model.SetJobStatus(jobId,JobState.Created);
        }
    }
}
