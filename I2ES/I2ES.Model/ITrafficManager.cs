﻿using I2ES.Model.DomainModel;
using I2ES.Model.Xml;

namespace I2ES.Model{
    public interface ITrafficManager{
        JobState GetJobStatus(int jobId);
        void StartLoadDataRetriever(int elementId, int jobId, string location);
        void LoadDataRetreverComplete(int jobId);
        void JobCreated(int jobId);
    }
}