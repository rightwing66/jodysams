﻿declare @ElementId int;
declare @JobId int;
declare @StaticLoadCase int;
declare @FatigueLoadCase int;
declare @Location nvarchar(30);
declare @RodBarOrShell nvarchar(10);
select @ElementId = '~ElementId';
select @JobId = '~JobId';
select @StaticLoadCase = '~StaticLoadCase';
select @FatigueLoadCase = '~FatigueLoadCase';
select @Location = '~Location';
select @RodBarOrShell = '~RodBarOrShell';

IF @RodBarOrShell = 'RodBar'

BEGIN

	IF @Location = 'Aft'

	BEGIN

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobId, AR.EID, AR.LC, AR.PX, AR.PY, AR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM F18GlobalLoads.dbo.AftStaticRods AS AR 
			INNER JOIN F18GlobalLoads.dbo.StaticMoments AS SM ON AR.EID = SM.EID AND AR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND AR.EID=@ElementId)
			OR (AR.EID=@ElementId AND AR.LC=@StaticLoadCase)
			
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM F18GlobalLoads.dbo.FullFatigueRods AS FFR 
			INNER JOIN F18GlobalLoads.dbo.FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)

	END

	ELSE IF @Location = 'Center'

	BEGIN

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobId, CR.EID, CR.LC, CR.PX, CR.PY, CR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM F18GlobalLoads.dbo.CenterStaticRods AS CR 
			INNER JOIN F18GlobalLoads.dbo.StaticMoments AS SM ON CR.EID = SM.EID AND CR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND CR.EID=@ElementId)
			OR (CR.EID=@ElementId AND CR.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM F18GlobalLoads.dbo.FullFatigueRods AS FFR 
			INNER JOIN F18GlobalLoads.dbo.FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'Forward'

	BEGIN

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobId, FR.EID, FR.LC, FR.PX, FR.PY, FR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM F18GlobalLoads.dbo.ForwardStaticRods AS FR 
			INNER JOIN F18GlobalLoads.dbo.StaticMoments AS SM ON FR.EID = SM.EID AND FR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND FR.EID=@ElementId)
			OR (FR.EID=@ElementId AND FR.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM F18GlobalLoads.dbo.FullFatigueRods AS FFR 
			INNER JOIN F18GlobalLoads.dbo.FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)

	END

	ELSE IF @Location = 'LEX'

	BEGIN

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobId, LR.EID, LR.LC, LR.PX, LR.PY, LR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM F18GlobalLoads.dbo.LEXStaticRods AS LR 
			INNER JOIN F18GlobalLoads.dbo.StaticMoments AS SM ON LR.EID = SM.EID AND LR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND LR.EID=@ElementId)
			OR (LR.EID=@ElementId AND LR.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM F18GlobalLoads.dbo.FullFatigueRods AS FFR 
			INNER JOIN F18GlobalLoads.dbo.FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)

	END

	ELSE IF @Location = 'NoseBarrel'

	BEGIN

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobId, NBR.EID, NBR.LC, NBR.PX, NBR.PY, NBR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM F18GlobalLoads.dbo.NoseBarrelStaticRods AS NBR 
			INNER JOIN F18GlobalLoads.dbo.StaticMoments AS SM ON NBR.EID = SM.EID AND NBR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND NBR.EID=@ElementId)
			OR (NBR.EID=@ElementId AND NBR.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM F18GlobalLoads.dbo.FullFatigueRods AS FFR 
			INNER JOIN F18GlobalLoads.dbo.FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)

	END

	ELSE IF @Location = 'Wing'

	BEGIN

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobId, WR.EID, WR.LC, WR.PX, WR.PY, WR.PZ, WSM.Node, WSM.Mx, WSM.My, WSM.Mz
		FROM F18GlobalLoads.dbo.WingStaticRods AS WR
			INNER JOIN F18GlobalLoads.dbo.WingStaticMoments AS WSM ON WR.EID = WSM.EID AND WR.LC = WSM.LC
		WHERE 
			(@StaticLoadCase=0 AND WR.EID=@ElementId)
			OR (WR.EID=@ElementId AND WR.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobId, WFR.EID, WFR.LC, WFR.PX, WFR.PY, WFR.PZ, WFM.Node, WFM.Mx, WFM.My, WFM.Mz
		FROM F18GlobalLoads.dbo.WingFatigueRodLoads AS WFR 
			INNER JOIN F18GlobalLoads.dbo.WingFatigueMoments AS WFM ON WFR.EID = WFM.EID AND WFR.LC = WFM.LC
		WHERE 
			(@FatigueLoadCase=0 AND WFR.EID=@ElementId)
			OR (WFR.EID=@ElementId AND WFR.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'ForwardCenter'

	BEGIN

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobId, FR.EID, FR.LC, FR.PX, FR.PY, FR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM F18GlobalLoads.dbo.ForwardStaticRods AS FR 
			INNER JOIN F18GlobalLoads.dbo.StaticMoments AS SM ON FR.EID = SM.EID AND FR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND FR.EID=@ElementId)
			OR (FR.EID=@ElementId AND FR.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM F18GlobalLoads.dbo.FullFatigueRods AS FFR 
			INNER JOIN F18GlobalLoads.dbo.FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)
	
		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobId, CR.EID, CR.LC, CR.PX, CR.PY, CR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM F18GlobalLoads.dbo.CenterStaticRods AS CR
			INNER JOIN F18GlobalLoads.dbo.StaticMoments AS SM ON CR.EID = SM.EID AND CR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND CR.EID=@ElementId)
			OR (CR.EID=@ElementId AND CR.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM F18GlobalLoads.dbo.FullFatigueRods AS FFR 
			INNER JOIN F18GlobalLoads.dbo.FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'ForwardNoseBarrel'

	BEGIN

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobId, FR.EID, FR.LC, FR.PX, FR.PY, FR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM F18GlobalLoads.dbo.ForwardStaticRods AS FR 
			INNER JOIN F18GlobalLoads.dbo.StaticMoments AS SM ON FR.EID = SM.EID AND FR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND FR.EID=@ElementId)
			OR (FR.EID=@ElementId AND FR.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM F18GlobalLoads.dbo.FullFatigueRods AS FFR 
			INNER JOIN F18GlobalLoads.dbo.FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobId, NBR.EID, NBR.LC, NBR.PX, NBR.PY, NBR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM F18GlobalLoads.dbo.NoseBarrelStaticRods AS NBR
			INNER JOIN F18GlobalLoads.dbo.StaticMoments AS SM ON NBR.EID = SM.EID AND NBR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND NBR.EID=@ElementId)
			OR (NBR.EID=@ElementId AND NBR.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM F18GlobalLoads.dbo.FullFatigueRods AS FFR 
			INNER JOIN F18GlobalLoads.dbo.FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'ForwardLEX'

	BEGIN

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobId, FR.EID, FR.LC, FR.PX, FR.PY, FR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM F18GlobalLoads.dbo.ForwardStaticRods AS FR 
			INNER JOIN F18GlobalLoads.dbo.StaticMoments AS SM ON FR.EID = SM.EID AND FR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND FR.EID=@ElementId)
			OR (FR.EID=@ElementId AND FR.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM F18GlobalLoads.dbo.FullFatigueRods AS FFR 
			INNER JOIN F18GlobalLoads.dbo.FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobId, LR.EID, LR.LC, LR.PX, LR.PY, LR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM F18GlobalLoads.dbo.LEXStaticRods AS LR
			INNER JOIN F18GlobalLoads.dbo.StaticMoments AS SM ON LR.EID = SM.EID AND LR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND LR.EID=@ElementId)
			OR (LR.EID=@ElementId AND LR.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM F18GlobalLoads.dbo.FullFatigueRods AS FFR 
			INNER JOIN F18GlobalLoads.dbo.FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'CenterLEX'

	BEGIN

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobId, CR.EID, CR.LC, CR.PX, CR.PY, CR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM F18GlobalLoads.dbo.CenterStaticRods AS CR
			INNER JOIN F18GlobalLoads.dbo.StaticMoments AS SM ON CR.EID = SM.EID AND CR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND CR.EID=@ElementId)
			OR (CR.EID=@ElementId AND CR.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM F18GlobalLoads.dbo.FullFatigueRods AS FFR 
			INNER JOIN F18GlobalLoads.dbo.FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobId, LR.EID, LR.LC, LR.PX, LR.PY, LR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM F18GlobalLoads.dbo.LEXStaticRods AS LR
			INNER JOIN F18GlobalLoads.dbo.StaticMoments AS SM ON LR.EID = SM.EID AND LR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND LR.EID=@ElementId)
			OR (LR.EID=@ElementId AND LR.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM F18GlobalLoads.dbo.FullFatigueRods AS FFR 
			INNER JOIN F18GlobalLoads.dbo.FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'CenterForwardNoseBarrel'

	BEGIN

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobId, CR.EID, CR.LC, CR.PX, CR.PY, CR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM F18GlobalLoads.dbo.CenterStaticRods AS CR
			INNER JOIN F18GlobalLoads.dbo.StaticMoments AS SM ON CR.EID = SM.EID AND CR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND CR.EID=@ElementId)
			OR (CR.EID=@ElementId AND CR.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM F18GlobalLoads.dbo.FullFatigueRods AS FFR 
			INNER JOIN F18GlobalLoads.dbo.FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)
	

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobId, FR.EID, FR.LC, FR.PX, FR.PY, FR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM F18GlobalLoads.dbo.ForwardStaticRods AS FR 
			INNER JOIN F18GlobalLoads.dbo.StaticMoments AS SM ON FR.EID = SM.EID AND FR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND FR.EID=@ElementId)
			OR (FR.EID=@ElementId AND FR.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM F18GlobalLoads.dbo.FullFatigueRods AS FFR 
			INNER JOIN F18GlobalLoads.dbo.FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobId, NBR.EID, NBR.LC, NBR.PX, NBR.PY, NBR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM F18GlobalLoads.dbo.NoseBarrelStaticRods AS NBR
			INNER JOIN F18GlobalLoads.dbo.StaticMoments AS SM ON NBR.EID = SM.EID AND NBR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND NBR.EID=@ElementId)
			OR (NBR.EID=@ElementId AND NBR.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM F18GlobalLoads.dbo.FullFatigueRods AS FFR 
			INNER JOIN F18GlobalLoads.dbo.FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'ForwardNoseBarrelLEX'

	BEGIN

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobId, FR.EID, FR.LC, FR.PX, FR.PY, FR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM F18GlobalLoads.dbo.ForwardStaticRods AS FR 
			INNER JOIN F18GlobalLoads.dbo.StaticMoments AS SM ON FR.EID = SM.EID AND FR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND FR.EID=@ElementId)
			OR (FR.EID=@ElementId AND FR.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM F18GlobalLoads.dbo.FullFatigueRods AS FFR 
			INNER JOIN F18GlobalLoads.dbo.FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobId, NBR.EID, NBR.LC, NBR.PX, NBR.PY, NBR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM F18GlobalLoads.dbo.NoseBarrelStaticRods AS NBR
			INNER JOIN F18GlobalLoads.dbo.StaticMoments AS SM ON NBR.EID = SM.EID AND NBR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND NBR.EID=@ElementId)
			OR (NBR.EID=@ElementId AND NBR.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM F18GlobalLoads.dbo.FullFatigueRods AS FFR 
			INNER JOIN F18GlobalLoads.dbo.FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobId, LR.EID, LR.LC, LR.PX, LR.PY, LR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM F18GlobalLoads.dbo.LEXStaticRods AS LR
			INNER JOIN F18GlobalLoads.dbo.StaticMoments AS SM ON LR.EID = SM.EID AND LR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND LR.EID=@ElementId)
			OR (LR.EID=@ElementId AND LR.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM F18GlobalLoads.dbo.FullFatigueRods AS FFR 
			INNER JOIN F18GlobalLoads.dbo.FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)

	END

	ELSE IF @Location = 'ForwardCenterLEX'

	BEGIN

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobId, FR.EID, FR.LC, FR.PX, FR.PY, FR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM F18GlobalLoads.dbo.ForwardStaticRods AS FR 
			INNER JOIN F18GlobalLoads.dbo.StaticMoments AS SM ON FR.EID = SM.EID AND FR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND FR.EID=@ElementId)
			OR (FR.EID=@ElementId AND FR.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM F18GlobalLoads.dbo.FullFatigueRods AS FFR 
			INNER JOIN F18GlobalLoads.dbo.FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobId, CR.EID, CR.LC, CR.PX, CR.PY, CR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM F18GlobalLoads.dbo.CenterStaticRods AS CR
			INNER JOIN F18GlobalLoads.dbo.StaticMoments AS SM ON CR.EID = SM.EID AND CR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND CR.EID=@ElementId)
			OR (CR.EID=@ElementId AND CR.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM F18GlobalLoads.dbo.FullFatigueRods AS FFR 
			INNER JOIN F18GlobalLoads.dbo.FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)
		
		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobId, LR.EID, LR.LC, LR.PX, LR.PY, LR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM F18GlobalLoads.dbo.LEXStaticRods AS LR
			INNER JOIN F18GlobalLoads.dbo.StaticMoments AS SM ON LR.EID = SM.EID AND LR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND LR.EID=@ElementId)
			OR (LR.EID=@ElementId AND LR.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM F18GlobalLoads.dbo.FullFatigueRods AS FFR 
			INNER JOIN F18GlobalLoads.dbo.FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)

	END

END

ELSE IF @RodBarOrShell = 'Shell'

BEGIN

	IF @Location = 'Aft'

	BEGIN

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobId, AFS.EID, AFS.LC, AFS.Fx, AFS.Fy, AFS.Fxy
		FROM F18GlobalLoads.dbo.AftStaticShells AS AFS 
		WHERE 
			(@StaticLoadCase=0 AND AFS.EID=@ElementId)
			OR (AFS.EID=@ElementId AND AFS.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM F18GlobalLoads.dbo.FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

	END

	ELSE IF @Location = 'Center'

	BEGIN

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobId, CS.EID, CS.LC, CS.Fx, CS.Fy, CS.Fxy
		FROM F18GlobalLoads.dbo.CenterStaticShells AS CS 
		WHERE 
			(@StaticLoadCase=0 AND CS.EID=@ElementId)
			OR (CS.EID=@ElementId AND CS.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM F18GlobalLoads.dbo.FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'Forward'

	BEGIN

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobId, FS.EID, FS.LC, FS.Fx, FS.Fy, FS.Fxy
		FROM F18GlobalLoads.dbo.ForwardStaticShells AS FS 
		WHERE 
			(@StaticLoadCase=0 AND FS.EID=@ElementId)
			OR (FS.EID=@ElementId AND FS.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM F18GlobalLoads.dbo.FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

	END

	ELSE IF @Location = 'LEX'

	BEGIN

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobId, LS.EID, LS.LC, LS.Fx, LS.Fy, LS.Fxy
		FROM F18GlobalLoads.dbo.LEXStaticShells AS LS 
		WHERE 
			(@StaticLoadCase=0 AND LS.EID=@ElementId)
			OR (LS.EID=@ElementId AND LS.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM F18GlobalLoads.dbo.FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

	END

	ELSE IF @Location = 'NoseBarrel'

	BEGIN

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobId, NBS.EID, NBS.LC, NBS.Fx, NBS.Fy, NBS.Fxy
		FROM F18GlobalLoads.dbo.NoseBarrelStaticShells AS NBS 
		WHERE 
			(@StaticLoadCase=0 AND NBS.EID=@ElementId)
			OR (NBS.EID=@ElementId AND NBS.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM F18GlobalLoads.dbo.FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

	END

	ELSE IF @Location = 'Wing'

	BEGIN

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobId, WS.EID, WS.LC, WS.Fx, WS.Fy, WS.Fxy
		FROM F18GlobalLoads.dbo.WingStaticShellLoads AS WS 
		WHERE 
			(@StaticLoadCase=0 AND WS.EID=@ElementId)
			OR (WS.EID=@ElementId AND WS.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobId, WFS.EID, WFS.LC, WFS.Fx, WFS.Fy, WFS.Fxy
		FROM F18GlobalLoads.dbo.WingFatigueShellLoads AS WFS 
		WHERE 
			(@FatigueLoadCase=0 AND WFS.EID=@ElementId)
			OR (WFS.EID=@ElementId AND WFS.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'ForwardCenter'

	BEGIN

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobId, FS.EID, FS.LC, FS.Fx, FS.Fy, FS.Fxy
		FROM F18GlobalLoads.dbo.ForwardStaticShells AS FS 
		WHERE 
			(@StaticLoadCase=0 AND FS.EID=@ElementId)
			OR (FS.EID=@ElementId AND FS.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM F18GlobalLoads.dbo.FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)
	
		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobId, CS.EID, CS.LC, CS.Fx, CS.Fy, CS.Fxy
		FROM F18GlobalLoads.dbo.CenterStaticShells AS CS 
		WHERE 
			(@StaticLoadCase=0 AND CS.EID=@ElementId)
			OR (CS.EID=@ElementId AND CS.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM F18GlobalLoads.dbo.FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'ForwardNoseBarrel'

	BEGIN

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobId, FS.EID, FS.LC, FS.Fx, FS.Fy, FS.Fxy
		FROM F18GlobalLoads.dbo.ForwardStaticShells AS FS 
		WHERE 
			(@StaticLoadCase=0 AND FS.EID=@ElementId)
			OR (FS.EID=@ElementId AND FS.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM F18GlobalLoads.dbo.FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobId, NBS.EID, NBS.LC, NBS.Fx, NBS.Fy, NBS.Fxy
		FROM F18GlobalLoads.dbo.NoseBarrelStaticShells AS NBS 
		WHERE 
			(@StaticLoadCase=0 AND NBS.EID=@ElementId)
			OR (NBS.EID=@ElementId AND NBS.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM F18GlobalLoads.dbo.FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'ForwardLEX'

	BEGIN

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobId, FS.EID, FS.LC, FS.Fx, FS.Fy, FS.Fxy
		FROM F18GlobalLoads.dbo.ForwardStaticShells AS FS 
		WHERE 
			(@StaticLoadCase=0 AND FS.EID=@ElementId)
			OR (FS.EID=@ElementId AND FS.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM F18GlobalLoads.dbo.FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobId, LS.EID, LS.LC, LS.Fx, LS.Fy, LS.Fxy
		FROM F18GlobalLoads.dbo.LEXStaticShells AS LS 
		WHERE 
			(@StaticLoadCase=0 AND LS.EID=@ElementId)
			OR (LS.EID=@ElementId AND LS.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM F18GlobalLoads.dbo.FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'CenterLEX'

	BEGIN

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobId, CS.EID, CS.LC, CS.Fx, CS.Fy, CS.Fxy
		FROM F18GlobalLoads.dbo.CenterStaticShells AS CS 
		WHERE 
			(@StaticLoadCase=0 AND CS.EID=@ElementId)
			OR (CS.EID=@ElementId AND CS.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM F18GlobalLoads.dbo.FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobId, LS.EID, LS.LC, LS.Fx, LS.Fy, LS.Fxy
		FROM F18GlobalLoads.dbo.LEXStaticShells AS LS 
		WHERE 
			(@StaticLoadCase=0 AND LS.EID=@ElementId)
			OR (LS.EID=@ElementId AND LS.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM F18GlobalLoads.dbo.FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'CenterForwardNoseBarrel'

	BEGIN

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobId, CS.EID, CS.LC, CS.Fx, CS.Fy, CS.Fxy
		FROM F18GlobalLoads.dbo.CenterStaticShells AS CS 
		WHERE 
			(@StaticLoadCase=0 AND CS.EID=@ElementId)
			OR (CS.EID=@ElementId AND CS.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM F18GlobalLoads.dbo.FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)
	

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobId, FS.EID, FS.LC, FS.Fx, FS.Fy, FS.Fxy
		FROM F18GlobalLoads.dbo.ForwardStaticShells AS FS 
		WHERE 
			(@StaticLoadCase=0 AND FS.EID=@ElementId)
			OR (FS.EID=@ElementId AND FS.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM F18GlobalLoads.dbo.FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobId, NBS.EID, NBS.LC, NBS.Fx, NBS.Fy, NBS.Fxy
		FROM F18GlobalLoads.dbo.NoseBarrelStaticShells AS NBS 
		WHERE 
			(@StaticLoadCase=0 AND NBS.EID=@ElementId)
			OR (NBS.EID=@ElementId AND NBS.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM F18GlobalLoads.dbo.FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'ForwardNoseBarrelLEX'

	BEGIN

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobId, FS.EID, FS.LC, FS.Fx, FS.Fy, FS.Fxy
		FROM F18GlobalLoads.dbo.ForwardStaticShells AS FS 
		WHERE 
			(@StaticLoadCase=0 AND FS.EID=@ElementId)
			OR (FS.EID=@ElementId AND FS.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM F18GlobalLoads.dbo.FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobId, NBS.EID, NBS.LC, NBS.Fx, NBS.Fy, NBS.Fxy
		FROM F18GlobalLoads.dbo.NoseBarrelStaticShells AS NBS 
		WHERE 
			(@StaticLoadCase=0 AND NBS.EID=@ElementId)
			OR (NBS.EID=@ElementId AND NBS.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM F18GlobalLoads.dbo.FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobId, LS.EID, LS.LC, LS.Fx, LS.Fy, LS.Fxy
		FROM F18GlobalLoads.dbo.LEXStaticShells AS LS 
		WHERE 
			(@StaticLoadCase=0 AND LS.EID=@ElementId)
			OR (LS.EID=@ElementId AND LS.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM F18GlobalLoads.dbo.FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

	END

	ELSE IF @Location = 'ForwardCenterLEX'

	BEGIN

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobId, FS.EID, FS.LC, FS.Fx, FS.Fy, FS.Fxy
		FROM F18GlobalLoads.dbo.ForwardStaticShells AS FS 
		WHERE 
			(@StaticLoadCase=0 AND FS.EID=@ElementId)
			OR (FS.EID=@ElementId AND FS.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM F18GlobalLoads.dbo.FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobId, CS.EID, CS.LC, CS.Fx, CS.Fy, CS.Fxy
		FROM F18GlobalLoads.dbo.CenterStaticShells AS CS 
		WHERE 
			(@StaticLoadCase=0 AND CS.EID=@ElementId)
			OR (CS.EID=@ElementId AND CS.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM F18GlobalLoads.dbo.FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

		INSERT INTO ISAMS.dbo.StaticLoadData(JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobId, LS.EID, LS.LC, LS.Fx, LS.Fy, LS.Fxy
		FROM F18GlobalLoads.dbo.LEXStaticShells AS LS 
		WHERE 
			(@StaticLoadCase=0 AND LS.EID=@ElementId)
			OR (LS.EID=@ElementId AND LS.LC=@StaticLoadCase)
		INSERT INTO ISAMS.dbo.FatigueLoadData(JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM F18GlobalLoads.dbo.FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

	END

END


select 1;
return;