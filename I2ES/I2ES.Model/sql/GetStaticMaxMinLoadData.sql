DECLARE @RodBarOrShell nvarchar(10);
DECLARE @ElementId int;
DECLARE @JobId int;
SELECT @RodBarOrShell = '~RodBarOrShell';
SELECT @ElementId = '~ElementId';
SELECT @JobId = '~JobId';

IF @RodBarOrShell = 'RodBar'

BEGIN

	INSERT INTO ISAMS.dbo.StaticMaxMin(JobId, ElementId, MaxPx, MinPx, MaxPy, MinPy, MaxPz, MinPz, MaxMx, MinMx, MaxMy, MinMy, MaxMz, MinMz)
	SELECT @JobId, @ElementId, MAX(StaticPx), MIN(StaticPx), MAX(StaticPy), MIN(StaticPy), MAX(StaticPz), MIN(StaticPz), MAX(StaticMx), MIN(StaticMx),
		MAX(StaticMy), MIN(StaticMy), MAX(StaticMz), MIN(StaticMz)
	FROM ISAMS.dbo.StaticLoadData
	WHERE JobId = @JobId 
		AND EID = @ElementId

	UPDATE ISAMS.dbo.StaticMaxMin 
	SET MaxPxLoadCase = SOLD.StaticLc 
	FROM ISAMS.dbo.StaticMaxMin AS SMM JOIN ISAMS.dbo.StaticLoadData AS SOLD ON SMM.JobId = SOLD.JobId 
	WHERE SOLD.JobId = @JobId 
		AND SOLD.EID = @ElementId 
		AND SOLD.StaticPx = SMM.MaxPx
	
	UPDATE ISAMS.dbo.StaticMaxMin 
	SET MinPxLoadCase = SOLD.StaticLc 
	FROM ISAMS.dbo.StaticMaxMin AS SMM JOIN ISAMS.dbo.StaticLoadData AS SOLD ON SMM.JobId = SOLD.JobId 
	WHERE SOLD.JobId = @JobId 
		AND SOLD.EID = @ElementId 
		AND SOLD.StaticPx = SMM.MinPx
	
	UPDATE ISAMS.dbo.StaticMaxMin 
	SET MaxPyLoadCase = 
	   (SELECT MIN(StaticLc) 
		FROM ISAMS.dbo.StaticLoadData AS SOLD
		WHERE SOLD.JobId = @JobId 
			AND SOLD.EID = @ElementId
			AND SOLD.StaticPy = SMM.MaxPy) 
	FROM ISAMS.dbo.StaticMaxMin AS SMM JOIN ISAMS.dbo.StaticLoadData AS SOLD ON SMM.JobId = SOLD.JobId 
	WHERE SOLD.JobId = @JobId 
		AND SOLD.EID = @ElementId 

	UPDATE ISAMS.dbo.StaticMaxMin 
	SET MinPyLoadCase = 
	   (SELECT MIN(StaticLc) 
		FROM ISAMS.dbo.StaticLoadData AS SOLD
		WHERE SOLD.JobId = @JobId 
			AND SOLD.EID = @ElementId
			AND SOLD.StaticPy = SMM.MinPy) 
	FROM ISAMS.dbo.StaticMaxMin AS SMM JOIN ISAMS.dbo.StaticLoadData AS SOLD ON SMM.JobId = SOLD.JobId 
	WHERE SOLD.JobId = @JobId 
		AND SOLD.EID = @ElementId 

	UPDATE ISAMS.dbo.StaticMaxMin 
	SET MaxPzLoadCase = 
	   (SELECT MIN(StaticLc) 
		FROM ISAMS.dbo.StaticLoadData AS SOLD
		WHERE SOLD.JobId = @JobId 
			AND SOLD.EID = @ElementId
			AND SOLD.StaticPz = SMM.MaxPz) 
	FROM ISAMS.dbo.StaticMaxMin AS SMM JOIN ISAMS.dbo.StaticLoadData AS SOLD ON SMM.JobId = SOLD.JobId 
	WHERE SOLD.JobId = @JobId 
		AND SOLD.EID = @ElementId 

	UPDATE ISAMS.dbo.StaticMaxMin 
	SET MinPzLoadCase = 
	   (SELECT MIN(StaticLc) 
		FROM ISAMS.dbo.StaticLoadData AS SOLD
		WHERE SOLD.JobId = @JobId 
			AND SOLD.EID = @ElementId
			AND SOLD.StaticPz = SMM.MinPz) 
	FROM ISAMS.dbo.StaticMaxMin AS SMM JOIN ISAMS.dbo.StaticLoadData AS SOLD ON SMM.JobId = SOLD.JobId 
	WHERE SOLD.JobId = @JobId 
		AND SOLD.EID = @ElementId 

	UPDATE ISAMS.dbo.StaticMaxMin 
	SET MaxMxLoadCase = 
	   (SELECT MIN(StaticLc) 
		FROM ISAMS.dbo.StaticLoadData AS SOLD 
		WHERE SOLD.JobId = @JobId 
			AND SOLD.EID = @ElementId
			AND SOLD.StaticMx = SMM.MaxMx) 
	FROM ISAMS.dbo.StaticMaxMin AS SMM JOIN ISAMS.dbo.StaticLoadData AS SOLD ON SMM.JobId = SOLD.JobId 
	WHERE SOLD.JobId = @JobId 
		AND SOLD.EID = @ElementId 

	UPDATE ISAMS.dbo.StaticMaxMin 
	SET MinMxLoadCase = 
	   (SELECT MIN(StaticLc) 
		FROM ISAMS.dbo.StaticLoadData AS SOLD
		WHERE SOLD.JobId = @JobId 
			AND SOLD.EID = @ElementId
			AND SOLD.StaticMx = SMM.MinMx) 
	FROM ISAMS.dbo.StaticMaxMin AS SMM JOIN ISAMS.dbo.StaticLoadData AS SOLD ON SMM.JobId = SOLD.JobId 
	WHERE SOLD.JobId = @JobId 
		AND SOLD.EID = @ElementId 

	UPDATE ISAMS.dbo.StaticMaxMin 
	SET MaxMyLoadCase = 
	   (SELECT MIN(StaticLc) 
		FROM ISAMS.dbo.StaticLoadData AS SOLD
		WHERE SOLD.JobId = @JobId 
			AND SOLD.EID = @ElementId
			AND SOLD.StaticMy = SMM.MaxMy) 
	FROM ISAMS.dbo.StaticMaxMin AS SMM JOIN ISAMS.dbo.StaticLoadData AS SOLD ON SMM.JobId = SOLD.JobId 
	WHERE SOLD.JobId = @JobId 
		AND SOLD.EID = @ElementId 

	UPDATE ISAMS.dbo.StaticMaxMin 
	SET MinMyLoadCase = 
	   (SELECT MIN(StaticLc) 
		FROM ISAMS.dbo.StaticLoadData AS SOLD
		WHERE SOLD.JobId = @JobId 
			AND SOLD.EID = @ElementId
			AND SOLD.StaticMy = SMM.MinMy) 
	FROM ISAMS.dbo.StaticMaxMin AS SMM JOIN ISAMS.dbo.StaticLoadData AS SOLD ON SMM.JobId = SOLD.JobId 
	WHERE SOLD.JobId = @JobId 
		AND SOLD.EID = @ElementId 

	UPDATE ISAMS.dbo.StaticMaxMin 
	SET MaxMzLoadCase = 
	   (SELECT MIN(StaticLc) 
		FROM ISAMS.dbo.StaticLoadData AS SOLD
		WHERE SOLD.JobId = @JobId 
			AND SOLD.EID = @ElementId
			AND SOLD.StaticMz = SMM.MaxMz) 
	FROM ISAMS.dbo.StaticMaxMin AS SMM JOIN ISAMS.dbo.StaticLoadData AS SOLD ON SMM.JobId = SOLD.JobId 
	WHERE SOLD.JobId = @JobId 
		AND SOLD.EID = @ElementId 

	UPDATE ISAMS.dbo.StaticMaxMin 
	SET MinMzLoadCase = 
	   (SELECT MIN(StaticLc) 
		FROM ISAMS.dbo.StaticLoadData AS SOLD
		WHERE SOLD.JobId = @JobId 
			AND SOLD.EID = @ElementId
			AND SOLD.StaticMz = SMM.MinMz) 
	FROM ISAMS.dbo.StaticMaxMin AS SMM JOIN ISAMS.dbo.StaticLoadData AS SOLD ON SMM.JobId = SOLD.JobId 
	WHERE SOLD.JobId = @JobId 
		AND SOLD.EID = @ElementId 

END

ELSE IF @RodBarOrShell = 'Shell'

BEGIN

	INSERT INTO ISAMS.dbo.StaticMaxMin(JobId, ElementId, MaxFx, MinFx, MaxFy, MinFy, MaxFxy, MinFxy)
	SELECT @JobId, @ElementId, MAX(StaticFx), MIN(StaticFx), MAX(StaticFy), MIN(StaticFy), MAX(StaticFxy), MIN(StaticFxy)
	FROM ISAMS.dbo.StaticLoadData
	WHERE JobId = @JobId 
		AND EID = @ElementId

	UPDATE ISAMS.dbo.StaticMaxMin 
	SET MaxFxLoadCase = SOLD.StaticLc 
	FROM ISAMS.dbo.StaticMaxMin AS SMM JOIN ISAMS.dbo.StaticLoadData AS SOLD ON SMM.JobId = SOLD.JobId 
	WHERE SOLD.JobId = @JobId 
		AND SOLD.EID = @ElementId 
		AND SOLD.StaticFx = SMM.MaxFx
	
	UPDATE ISAMS.dbo.StaticMaxMin 
	SET MinFxLoadCase = SOLD.StaticLc 
	FROM ISAMS.dbo.StaticMaxMin AS SMM JOIN ISAMS.dbo.StaticLoadData AS SOLD ON SMM.JobId = SOLD.JobId 
	WHERE SOLD.JobId = @JobId 
		AND SOLD.EID = @ElementId 
		AND SOLD.StaticFx = SMM.MinFx
	
	UPDATE ISAMS.dbo.StaticMaxMin 
	SET MaxFyLoadCase = 
	   (SELECT MIN(StaticLc) 
		FROM ISAMS.dbo.StaticLoadData AS SOLD 
		WHERE SOLD.JobId = @JobId 
			AND SOLD.EID = @ElementId
			AND SOLD.StaticFy = SMM.MaxFy) 
	FROM ISAMS.dbo.StaticMaxMin AS SMM JOIN ISAMS.dbo.StaticLoadData AS SOLD ON SMM.JobId = SOLD.JobId 
	WHERE SOLD.JobId = @JobId 
		AND SOLD.EID = @ElementId 

	UPDATE ISAMS.dbo.StaticMaxMin 
	SET MinFyLoadCase = 
	   (SELECT MIN(StaticLc) 
		FROM ISAMS.dbo.StaticLoadData AS SOLD
		WHERE SOLD.JobId = @JobId 
			AND SOLD.EID = @ElementId
			AND SOLD.StaticFy = SMM.MinFy) 
	FROM ISAMS.dbo.StaticMaxMin AS SMM JOIN ISAMS.dbo.StaticLoadData AS SOLD ON SMM.JobId = SOLD.JobId 
	WHERE SOLD.JobId = @JobId 
		AND SOLD.EID = @ElementId 

	UPDATE ISAMS.dbo.StaticMaxMin 
	SET MaxFxyLoadCase = 
	   (SELECT MIN(StaticLc) 
		FROM ISAMS.dbo.StaticLoadData AS SOLD
		WHERE SOLD.JobId = @JobId 
			AND SOLD.EID = @ElementId
			AND SOLD.StaticFxy = SMM.MaxFxy) 
	FROM ISAMS.dbo.StaticMaxMin AS SMM JOIN ISAMS.dbo.StaticLoadData AS SOLD ON SMM.JobId = SOLD.JobId 
	WHERE SOLD.JobId = @JobId 
		AND SOLD.EID = @ElementId 

	UPDATE ISAMS.dbo.StaticMaxMin 
	SET MinFxyLoadCase = 
	   (SELECT MIN(StaticLc) 
	    FROM ISAMS.dbo.StaticLoadData AS SOLD
		WHERE SOLD.JobId = @JobId 
			AND SOLD.EID = @ElementId
			AND SOLD.StaticFxy = SMM.MinFxy) 
	FROM ISAMS.dbo.StaticMaxMin AS SMM JOIN ISAMS.dbo.StaticLoadData AS SOLD ON SMM.JobId = SOLD.JobId 
	WHERE SOLD.JobId = @JobId 
		AND SOLD.EID = @ElementId

END
SELECT 1
RETURN