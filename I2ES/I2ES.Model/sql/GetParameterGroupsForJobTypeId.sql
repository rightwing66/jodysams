﻿select pg.ParameterGroupId, pg.JobTypeId, Pg.Name as ParameterGroupName, pg.Description as ParameterGroupDescription, 
		p.ParameterId, p.Name as ParameterName, p.Label as ParameterDescription, p.Type, p.Display, p.Value,  u.UnitId, u.Name as UnitName,
		u.Description as UnitDescription, u.CLRType
from parametergroup pg
	join parametergroup_x_parameter pxp on pg.ParameterGroupId = pxp.ParameterGroupId
	join parameter p on pxp.ParameterId = p.ParameterId
	left outer join unit u on p.UnitId = u.UnitId
where 
	pg.jobtypeid = ~JobTypeId
order by pg.ParameterGroupId, p.ParameterId
