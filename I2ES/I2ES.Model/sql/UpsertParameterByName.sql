﻿declare @parameterId int
declare @parameterValueId int

select @parameterId = p.ParameterId
from  Parameter p
where  p.Name = '~ParameterName'


if(@parameterId is null) THROW 51000, 'The parameter name ~ParameterName  does not exist' , 1;

if exists(select * from parameterValue pv where  pv.JobId = ~JobId and pv.parameterId = @parameterId)
begin
	delete ParameterValue where JobId = ~JobId and parameterId = @parameterId
end

Insert parameterValue(jobId, parameterId, value)
values (~JobId , @parameterId, ~Value)

select * from ParameterValue where ParameterId = @parameterId and JobId = ~JobId