﻿Select j.*, jt.Name JobTypeName
from Job j, JobType jt
where j.JobTypeId = jt.JobTypeId
order by ~sortIndex ~sortOrder
