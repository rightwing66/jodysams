﻿if exists(select * from ParameterValue where JobId = ~JobId and ParameterId = ~ParameterId)
begin
	update ParameterValue
	set value = ~Value
	where JobId = ~JobId 
		and ParameterId = ~ParameterId
end
else
begin
	insert ParameterValue(JobId, ParameterId, Value)
	values(~JobId,~ParameterId, ~Value)
end

select * from parameterValue