DECLARE @RodBarOrShell nvarchar(10);
DECLARE @ElementId int;
DECLARE @JobId int;
SELECT @RodBarOrShell = '~RodBarOrShell';
SELECT @ElementId = '~ElementId';
SELECT @JobId = '~JobId';

IF @RodBarOrShell = 'RodBar'

BEGIN

	INSERT INTO ISAMS.dbo.FatigueMaxMin(JobId, ElementId, MaxPx, MinPx, MaxPy, MinPy, MaxPz, MinPz, MaxMx, MinMx, MaxMy, MinMy, MaxMz, MinMz)
	SELECT @JobId, @ElementId, MAX(FatiguePx), MIN(FatiguePx), MAX(FatiguePy), MIN(FatiguePy), MAX(FatiguePz), MIN(FatiguePz), MAX(FatigueMx), MIN(FatigueMx),
		MAX(FatigueMy), MIN(FatigueMy), MAX(FatigueMz), MIN(FatigueMz)
	FROM ISAMS.dbo.FatigueLoadData
	WHERE JobId = @JobId 
		AND EID = @ElementId

	UPDATE ISAMS.dbo.FatigueMaxMin 
	SET MaxPxLoadCase = FOLD.FatigueLc 
	FROM ISAMS.dbo.FatigueMaxMin AS FMM JOIN ISAMS.dbo.FatigueLoadData AS FOLD ON FMM.JobId = FOLD.JobId 
	WHERE FOLD.JobId = @JobId 
		AND FOLD.EID = @ElementId 
		AND FOLD.FatiguePx = FMM.MaxPx
	
	UPDATE ISAMS.dbo.FatigueMaxMin 
	SET MinPxLoadCase = FOLD.FatigueLc 
	FROM ISAMS.dbo.FatigueMaxMin AS FMM JOIN ISAMS.dbo.FatigueLoadData AS FOLD ON FMM.JobId = FOLD.JobId 
	WHERE FOLD.JobId = @JobId 
		AND FOLD.EID = @ElementId 
		AND FOLD.FatiguePx = FMM.MinPx
	
	UPDATE ISAMS.dbo.FatigueMaxMin 
	SET MaxPyLoadCase = 
	   (SELECT MIN(FatigueLc) 
		FROM ISAMS.dbo.FatigueLoadData AS FOLD
		WHERE FOLD.JobId = @JobId 
			AND FOLD.EID = @ElementId
			AND FOLD.FatiguePy = FMM.MaxPy) 
	FROM ISAMS.dbo.FatigueMaxMin AS FMM JOIN ISAMS.dbo.FatigueLoadData AS FOLD ON FMM.JobId = FOLD.JobId 
	WHERE FOLD.JobId = @JobId 
		AND FOLD.EID = @ElementId 

	UPDATE ISAMS.dbo.FatigueMaxMin 
	SET MinPyLoadCase = 
	   (SELECT MIN(FatigueLc) 
		FROM ISAMS.dbo.FatigueLoadData AS FOLD
		WHERE FOLD.JobId = @JobId 
			AND FOLD.EID = @ElementId
			AND FOLD.FatiguePy = FMM.MinPy) 
	FROM ISAMS.dbo.FatigueMaxMin AS FMM JOIN ISAMS.dbo.FatigueLoadData AS FOLD ON FMM.JobId = FOLD.JobId 
	WHERE FOLD.JobId = @JobId 
		AND FOLD.EID = @ElementId 

	UPDATE ISAMS.dbo.FatigueMaxMin 
	SET MaxPzLoadCase = 
	   (SELECT MIN(FatigueLc) 
		FROM ISAMS.dbo.FatigueLoadData AS FOLD
		WHERE FOLD.JobId = @JobId 
			AND FOLD.EID = @ElementId
			AND FOLD.FatiguePz = FMM.MaxPz) 
	FROM ISAMS.dbo.FatigueMaxMin AS FMM JOIN ISAMS.dbo.FatigueLoadData AS FOLD ON FMM.JobId = FOLD.JobId 
	WHERE FOLD.JobId = @JobId 
		AND FOLD.EID = @ElementId 

	UPDATE ISAMS.dbo.FatigueMaxMin 
	SET MinPzLoadCase = 
	   (SELECT MIN(FatigueLc) 
		FROM ISAMS.dbo.FatigueLoadData AS FOLD
		WHERE FOLD.JobId = @JobId 
			AND FOLD.EID = @ElementId
			AND FOLD.FatiguePz = FMM.MinPz) 
	FROM ISAMS.dbo.FatigueMaxMin AS FMM JOIN ISAMS.dbo.FatigueLoadData AS FOLD ON FMM.JobId = FOLD.JobId 
	WHERE FOLD.JobId = @JobId 
		AND FOLD.EID = @ElementId 

	UPDATE ISAMS.dbo.FatigueMaxMin 
	SET MaxMxLoadCase = 
	   (SELECT MIN(FatigueLc) 
		FROM ISAMS.dbo.FatigueLoadData AS FOLD 
		WHERE FOLD.JobId = @JobId 
			AND FOLD.EID = @ElementId
			AND FOLD.FatigueMx = FMM.MaxMx) 
	FROM ISAMS.dbo.FatigueMaxMin AS FMM JOIN ISAMS.dbo.FatigueLoadData AS FOLD ON FMM.JobId = FOLD.JobId 
	WHERE FOLD.JobId = @JobId 
		AND FOLD.EID = @ElementId 

	UPDATE ISAMS.dbo.FatigueMaxMin 
	SET MinMxLoadCase = 
	   (SELECT MIN(FatigueLc) 
		FROM ISAMS.dbo.FatigueLoadData AS FOLD
		WHERE FOLD.JobId = @JobId 
			AND FOLD.EID = @ElementId
			AND FOLD.FatigueMx = FMM.MinMx) 
	FROM ISAMS.dbo.FatigueMaxMin AS FMM JOIN ISAMS.dbo.FatigueLoadData AS FOLD ON FMM.JobId = FOLD.JobId 
	WHERE FOLD.JobId = @JobId 
		AND FOLD.EID = @ElementId 

	UPDATE ISAMS.dbo.FatigueMaxMin 
	SET MaxMyLoadCase = 
	   (SELECT MIN(FatigueLc) 
		FROM ISAMS.dbo.FatigueLoadData AS FOLD
		WHERE FOLD.JobId = @JobId 
			AND FOLD.EID = @ElementId
			AND FOLD.FatigueMy = FMM.MaxMy) 
	FROM ISAMS.dbo.FatigueMaxMin AS FMM JOIN ISAMS.dbo.FatigueLoadData AS FOLD ON FMM.JobId = FOLD.JobId 
	WHERE FOLD.JobId = @JobId 
		AND FOLD.EID = @ElementId 

	UPDATE ISAMS.dbo.FatigueMaxMin 
	SET MinMyLoadCase = 
	   (SELECT MIN(FatigueLc) 
		FROM ISAMS.dbo.FatigueLoadData AS FOLD
		WHERE FOLD.JobId = @JobId 
			AND FOLD.EID = @ElementId
			AND FOLD.FatigueMy = FMM.MinMy) 
	FROM ISAMS.dbo.FatigueMaxMin AS FMM JOIN ISAMS.dbo.FatigueLoadData AS FOLD ON FMM.JobId = FOLD.JobId 
	WHERE FOLD.JobId = @JobId 
		AND FOLD.EID = @ElementId 

	UPDATE ISAMS.dbo.FatigueMaxMin 
	SET MaxMzLoadCase = 
	   (SELECT MIN(FatigueLc) 
		FROM ISAMS.dbo.FatigueLoadData AS FOLD
		WHERE FOLD.JobId = @JobId 
			AND FOLD.EID = @ElementId
			AND FOLD.FatigueMz = FMM.MaxMz) 
	FROM ISAMS.dbo.FatigueMaxMin AS FMM JOIN ISAMS.dbo.FatigueLoadData AS FOLD ON FMM.JobId = FOLD.JobId 
	WHERE FOLD.JobId = @JobId 
		AND FOLD.EID = @ElementId 

	UPDATE ISAMS.dbo.FatigueMaxMin 
	SET MinMzLoadCase = 
	   (SELECT MIN(FatigueLc) 
		FROM ISAMS.dbo.FatigueLoadData AS FOLD
		WHERE FOLD.JobId = @JobId 
			AND FOLD.EID = @ElementId
			AND FOLD.FatigueMz = FMM.MinMz) 
	FROM ISAMS.dbo.FatigueMaxMin AS FMM JOIN ISAMS.dbo.FatigueLoadData AS FOLD ON FMM.JobId = FOLD.JobId 
	WHERE FOLD.JobId = @JobId 
		AND FOLD.EID = @ElementId 

END

ELSE IF @RodBarOrShell = 'Shell'

BEGIN

	INSERT INTO ISAMS.dbo.FatigueMaxMin(JobId, ElementId, MaxFx, MinFx, MaxFy, MinFy, MaxFxy, MinFxy)
	SELECT @JobId, @ElementId, MAX(FatigueFx), MIN(FatigueFx), MAX(FatigueFy), MIN(FatigueFy), MAX(FatigueFxy), MIN(FatigueFxy)
	FROM ISAMS.dbo.FatigueLoadData
	WHERE JobId = @JobId 
		AND EID = @ElementId

	UPDATE ISAMS.dbo.FatigueMaxMin 
	SET MaxFxLoadCase = FOLD.FatigueLc 
	FROM ISAMS.dbo.FatigueMaxMin AS FMM JOIN ISAMS.dbo.FatigueLoadData AS FOLD ON FMM.JobId = FOLD.JobId 
	WHERE FOLD.JobId = @JobId 
		AND FOLD.EID = @ElementId 
		AND FOLD.FatigueFx = FMM.MaxFx
	
	UPDATE ISAMS.dbo.FatigueMaxMin 
	SET MinFxLoadCase = FOLD.FatigueLc 
	FROM ISAMS.dbo.FatigueMaxMin AS FMM JOIN ISAMS.dbo.FatigueLoadData AS FOLD ON FMM.JobId = FOLD.JobId 
	WHERE FOLD.JobId = @JobId 
		AND FOLD.EID = @ElementId 
		AND FOLD.FatigueFx = FMM.MinFx
	
	UPDATE ISAMS.dbo.FatigueMaxMin 
	SET MaxFyLoadCase = 
	   (SELECT MIN(FatigueLc) 
		FROM ISAMS.dbo.FatigueLoadData AS FOLD 
		WHERE FOLD.JobId = @JobId 
			AND FOLD.EID = @ElementId
			AND FOLD.FatigueFy = FMM.MaxFy) 
	FROM ISAMS.dbo.FatigueMaxMin AS FMM JOIN ISAMS.dbo.FatigueLoadData AS FOLD ON FMM.JobId = FOLD.JobId 
	WHERE FOLD.JobId = @JobId 
		AND FOLD.EID = @ElementId 

	UPDATE ISAMS.dbo.FatigueMaxMin 
	SET MinFyLoadCase = 
	   (SELECT MIN(FatigueLc) 
		FROM ISAMS.dbo.FatigueLoadData AS FOLD
		WHERE FOLD.JobId = @JobId 
			AND FOLD.EID = @ElementId
			AND FOLD.FatigueFy = FMM.MinFy) 
	FROM ISAMS.dbo.FatigueMaxMin AS FMM JOIN ISAMS.dbo.FatigueLoadData AS FOLD ON FMM.JobId = FOLD.JobId 
	WHERE FOLD.JobId = @JobId 
		AND FOLD.EID = @ElementId 

	UPDATE ISAMS.dbo.FatigueMaxMin 
	SET MaxFxyLoadCase = 
	   (SELECT MIN(FatigueLc) 
		FROM ISAMS.dbo.FatigueLoadData AS FOLD
		WHERE FOLD.JobId = @JobId 
			AND FOLD.EID = @ElementId
			AND FOLD.FatigueFxy = FMM.MaxFxy) 
	FROM ISAMS.dbo.FatigueMaxMin AS FMM JOIN ISAMS.dbo.FatigueLoadData AS FOLD ON FMM.JobId = FOLD.JobId 
	WHERE FOLD.JobId = @JobId 
		AND FOLD.EID = @ElementId 

	UPDATE ISAMS.dbo.FatigueMaxMin 
	SET MinFxyLoadCase = 
	   (SELECT MIN(FatigueLc) 
	    FROM ISAMS.dbo.FatigueLoadData AS FOLD
		WHERE FOLD.JobId = @JobId 
			AND FOLD.EID = @ElementId
			AND FOLD.FatigueFxy = FMM.MinFxy) 
	FROM ISAMS.dbo.FatigueMaxMin AS FMM JOIN ISAMS.dbo.FatigueLoadData AS FOLD ON FMM.JobId = FOLD.JobId 
	WHERE FOLD.JobId = @JobId 
		AND FOLD.EID = @ElementId

END
SELECT 1 
RETURN