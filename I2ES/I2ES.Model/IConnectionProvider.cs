namespace I2ES.Model{
    public interface IConnectionProvider{
         ISqlWrapper CreateWrapper();
    }
}