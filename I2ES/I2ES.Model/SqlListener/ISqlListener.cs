﻿using System.Collections.Generic;

namespace I2ES.Model.SqlListener {
    public interface ISqlListener {
        List<string> SqlCommands { get; set; }
    }
}