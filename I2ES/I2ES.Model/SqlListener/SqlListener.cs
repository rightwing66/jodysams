﻿using System;
using System.Collections.Generic;
using System.IO;

namespace I2ES.Model.SqlListener {
    public class SqlListener : ISqlListener {
        private const string commandTerminator = "~~~";
        public List<string> SqlCommands { get; set; }

        public SqlListener() {
            SqlCommands = new List<string>();
        }

        public void Save(StreamWriter writer) {
            foreach (var sqlCommand in SqlCommands) {
                writer.Write(sqlCommand);
                writer.Write(commandTerminator);
            }
            writer.Flush();
            writer.Close();
            writer.Dispose();
        }

        public void Load(StreamReader reader) {
            string bulk = reader.ReadToEnd();
            var commands = bulk.Split(new[] {commandTerminator}, StringSplitOptions.None);
            SqlCommands = new List<string>();
            foreach (string command in commands) {
                SqlCommands.Add(command);
            }
        }
    }
}