﻿namespace I2ES.Model.SqlListener {
    public interface ISqlListenerCollection {
        void Add(ISqlListener newListener);
        void AddSqlCommand(string message);
    }
}