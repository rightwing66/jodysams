﻿using System.Collections.Generic;

namespace I2ES.Model.SqlListener {
    public class SqlListenerCollection : ISqlListenerCollection {
        private readonly List<ISqlListener> listeners = new List<ISqlListener>(); 
        public void Add(ISqlListener newListener) {
            listeners.Add(newListener);
        }

        public void AddSqlCommand(string message) {
            foreach (var listener in listeners) {
                listener.SqlCommands.Add(message);
            }
        }


    }
}