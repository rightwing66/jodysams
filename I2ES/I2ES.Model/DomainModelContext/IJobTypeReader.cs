using System.Collections.Generic;
using I2ES.Model.DomainModel;

namespace I2ES.Model.DomainModelContext {
    public interface IJobTypeReader {
        JobType ReadJobType(int? jobTypeId, Job job);
        List<JobType> ReadAllJobTypes();
        ISqlWrapper SqlWrapper { get; set; }
    }
}