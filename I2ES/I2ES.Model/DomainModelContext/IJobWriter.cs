using I2ES.Model.DomainModel;

namespace I2ES.Model.DomainModelContext {
    public interface IJobWriter {
        bool IsTest { get; set; }
        ISqlWrapper SqlWrapper { get; set; }
        Job CreateJob(int jobTypeId, string userId);
        Job CreateJob(JobType jobType, string userId);
        void SaveChanges(Job job);
        int? CreateJobId(JobType jobType, string UserId);
    }
}