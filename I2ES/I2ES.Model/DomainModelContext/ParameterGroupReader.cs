﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using I2ES.Model.DomainModel;
using I2ES.Model.Extensions;
using I2ES.Model.Xml;

namespace I2ES.Model.DomainModelContext {
    public class ParameterGroupReader {
        private readonly ISqlWrapper sqlWrapper;
        private readonly IParameterValueReader parameterValueReader;
        private readonly CompileUtilities compileUtilities;

        public ParameterGroupReader(ISqlWrapper sqlWrapper, IParameterValueReader parameterValueReader) {
            this.sqlWrapper = sqlWrapper;
            this.parameterValueReader = parameterValueReader;
            compileUtilities = new CompileUtilities { Namespace = "I2ES.Model.sql." };
        }

        public void LoadParameterGroups(JobType jobType, Job job) {
            var rawRecords = loadDataRowsIntoLocalList(jobType);
            extractParameterGroups(jobType, rawRecords);
            var parameters = extractParameters(rawRecords);
            assembleParametersIntoParameterGroups(jobType, parameters);
            if (job != null) {
                List<ParameterValue> parameterValues = parameterValueReader.ReadAllParameterValues(job.JobId);
               LoadParameterValuesIntoParameters(jobType, job, parameterValues);
            }
        }

        public void LoadParameterValuesIntoParameters(JobType jobType, Job job, List<ParameterValue> parameterValues) {
            IEnumerable<Parameter> allParameters = new List<Parameter>();
            jobType.ParameterGroups.ForEach(g => allParameters = allParameters.Concat(g.Parameters));
            foreach (var parameter in allParameters) {
                parameter.ParameterValue = parameterValues.FirstOrDefault(p => p.ParameterId == parameter.ParameterId);
            }
            job.AllParameters = allParameters.ToList();
        }

        private List<IDataRecord> loadDataRowsIntoLocalList(JobType jobType){
            string sql = compileUtilities.GetResourceValue("GetParameterGroupsForJobTypeId.sql")
                .Replace("~JobTypeId", jobType.JobTypeId.ToString());
            SqlDataReader reader = sqlWrapper.RunReturnReader(sql);
            var rawRecords = reader.Cast<IDataRecord>().ToList();
            reader.Dispose();
            return rawRecords;
        }

        private static void assembleParametersIntoParameterGroups(JobType jobType, IEnumerable<Parameter> parameters){
            foreach (var parameterGroup in jobType.ParameterGroups){
                int parameterGroupId = parameterGroup.ParameterGroupId;
                // ReSharper disable once PossibleMultipleEnumeration
                var list = parameters.Where(p => p.ParameterGroupId == parameterGroupId);
                foreach (var parameter in list){
                    parameter.ParameterValue = new ParameterValue();
                    parameterGroup.Parameters.Add(parameter);
                    parameter.ParameterGroup = parameterGroup;
                }
            }
        }

        private static IEnumerable<Parameter> extractParameters(List<IDataRecord> rawRecords){
            var parameters = rawRecords.Select(r => new Parameter{
                JobTypeId = r["JobTypeId"].GetNullableint(),
                ParameterId = r["ParameterId"].GetNullableint(),
                Description = r["ParameterDescription"].GetNullableString(),
                Display = r["Display"].GetNullableString(),
                Name = r["ParameterName"].GetNullableString(),
                ParameterGroupId = int.Parse(r["ParameterGroupId"].ToString()),
                DefaultValue = r["Value"].ToString(),
                Type = r["Type"].GetNullableString(),
                Unit = new Unit{
                    Name = r["UnitName"].GetNullableString(),
                    Description = r["UnitDescription"].GetNullableString(),
                    ClrType = r["CLRType"].GetNullableString()
                }
            });
            return parameters;
        }

        private void extractParameterGroups(JobType jobType, List<IDataRecord> rawRecords){
            jobType.ParameterGroups = rawRecords.Select(r => new ParameterGroup{
                ParameterGroupId = int.Parse(r["ParameterGroupId"].ToString()),
                Description = r["ParameterGroupDescription"].ToString(),
                JobType = jobType,
                Name = r["ParameterGroupname"].ToString()
            }).Distinct(new ParamaterGroupIdComparer()).ToList();
        }
    }
}
