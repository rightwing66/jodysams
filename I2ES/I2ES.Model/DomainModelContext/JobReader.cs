using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using I2ES.Model.DomainModel;
using I2ES.Model.Extensions;

namespace I2ES.Model.DomainModelContext {
    public class JobReader : IJobReader {
        public ISqlWrapper SqlWrapper { get; set; }
        private readonly CompileUtilities compileUtilities = new CompileUtilities { Namespace = "I2ES.Model.sql" };

        public List<Job> ReadAllJobs(string sortIndex, string sortOrder) {
            var sql =
                compileUtilities.GetResourceValue("GetAllJobsDynamic.sql")
                    .Replace("~sortIndex", sortIndex)
                    .Replace("~sortOrder", sortOrder);
            var records = SqlWrapper.RunReturnIDataRecords(sql);
            return records.Select(ReadJobFromRecord).ToList();
        }

        public List<Job> LoadJobsForUser(string userId) {
            var sql = compileUtilities.GetResourceValue("GetAllJobs.sql").Replace("~UserId",userId);
            var records = SqlWrapper.RunReturnIDataRecords(sql);
            return records.Select(ReadJobFromRecord).ToList();
        }

        public List<Job> LoadSolverJobQueue(int? slotsOpen) {
            var sql = compileUtilities.GetResourceValue("GetSolverJobQueue.sql").Replace("~Slots", slotsOpen.ToString());
            var records = SqlWrapper.RunReturnIDataRecords(sql);
            return records.Select(ReadJobFromRecord).ToList();
        }

        public virtual Job ReadSingleJob(int? jobId) {
            var sql = compileUtilities.GetResourceValue("GetSingleJob.sql").Replace("~JobId", jobId.ToString());
            var records = SqlWrapper.RunReturnIDataRecords(sql);
            if (records.Count == 0) return null;
            return ReadJobFromRecord(records.FirstOrDefault());
        }

        public void DeleteJob(int? jobId) {
            var sql = compileUtilities.GetResourceValue("DeleteJob.sql").Replace("~JobId", jobId.ToString());
            SqlWrapper.RunReturnReader(sql);
        }

        [SuppressMessage("ReSharper", "FunctionComplexityOverflow")]
        public Job ReadJobFromRecord(IDataRecord record) {
            var job = new Job();
            job.JobId = record.GetInt32(record.GetOrdinal("JobId"));
            job.JobTypeId = record["JobTypeId"].GetNullableint();
            job.UserId = record["UserId"].GetNullableString();

            job.CreateTime = record.GetDateTime(record.GetOrdinal("CreateTime"));

            job.CreateTime = record["CreateTime"].GetNullableDateTime();
            job.EditTime = record["EditTime"].GetNullableDateTime();
            job.JobState = record["JobStateId"].EnumNullableParse<JobState>();
            job.JobStateId = record["JobStateId"].GetNullableint();
            job.CompletionStage = record["CompletionStage"].GetNullableString();
            job.JobTypeName = record["JobTypeName"].GetNullableString();
            job.ProcessBeginTime = record["ProcessBeginTime"].GetNullableDateTime();
            job.ProcessEndTime = record["ProcessEndTime"].GetNullableDateTime();
            job.SubmittingNode = record["SubmittingNode"].GetNullableString();
            job.SolverState = record["SolverStateId"].EnumNullableParse<BackEndProcessState>();
            job.SolverNode = record["SolverNode"].GetNullableString();
            job.SolverBeginTime = record["SolverBeginTime"].GetNullableDateTime();
            job.SolverEndTime = record["SolverEndTime"].GetNullableDateTime();
            job.SolverMessage = record["SolverMessage"].GetNullableString();
            job.LoadDataRetrieverState = record["LoadDataRetreiverStateId"].EnumNullableParse<BackEndProcessState>();
            job.LoadDataRetrieverBeginTime = record["LoadDataRetreiverBeginTime"].GetNullableDateTime();
            job.LoadDataRetrieverEndTime = record["LoadDataRetreiverEndTime"].GetNullableDateTime();
            job.LoadDataRetrieverMessage = record["LoadDataRetrieverMessage"].GetNullableString();
            job.StoreResultsState = record["StoreResultsStateId"].EnumNullableParse<BackEndProcessState>();
            job.StoreResultsBeginTime = record["StoreResultsBeginTime"].GetNullableDateTime();
            job.StoreResultsEndTime = record["StoreResultsEndTime"].GetNullableDateTime();
            job.StoreResultsMessage = record["StoreResultsMessage"].GetNullableString();
            job.TrueNumberState = record["TrueNumberStateId"].EnumNullableParse<BackEndProcessState>();
            job.TrueNumberBeginTime = record["TrueNumberBeginTime"].GetNullableDateTime();
            job.TrueNumberEndTime = record["TrueNumberEndTime"].GetNullableDateTime();
            job.TrueNumberMessage = record["TrueNumberMessage"].GetNullableString();
            job.REINumber = record["REINumber"].GetNullableString();
            job.BUNO = record["BUNO"].GetNullableString();
            job.PartNumber = record["PartNumber"].GetNullableString();
            job.StructureType = record["StructureTypeId"].EnumNullableParse<StructureType>();
            job.AircraftModel = record["AircraftModelId"].EnumNullableParse<AircraftModel>();
            job.Report = record["Report"].GetNullableString();
            return job;
        }


        public JobState GetJobStatus(int jobId) {
            var sql = compileUtilities.GetResourceValue("GetJobStateId.sql").Replace("~JobId", jobId.ToString());
            int? jobStateId = SqlWrapper.RunSqlReturnScalar(sql);
            return jobStateId.EnumNullableParse<JobState>();
        }
    }
}