using System.Collections.Generic;
using System.Data;
using I2ES.Model.DomainModel;

namespace I2ES.Model.DomainModelContext
{
    public interface IJobReader
    {
        List<Job> ReadAllJobs(string sortIndex, string sortOrder);
        List<Job> LoadJobsForUser(string userId);
        Job ReadSingleJob(int? jobId);
        Job ReadJobFromRecord(IDataRecord record);
        JobState GetJobStatus(int jobId);
        ISqlWrapper SqlWrapper { get; set; }
        void DeleteJob(int? jobId);
    }
}