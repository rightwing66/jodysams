﻿using System.Collections.Generic;
using I2ES.Model.DomainModel;
using I2ES.Model.Extensions;

namespace I2ES.Model.DomainModelContext {
    public interface IParameterValueReader {
        List<ParameterValue> ReadAllParameterValues(int? jobId);
    }

    public class ParameterValueReader : IParameterValueReader {
        private readonly ISqlWrapper sqlWrapper;

        public ParameterValueReader(ISqlWrapper sqlWrapper) {
            this.sqlWrapper = sqlWrapper;
        }



        public List<ParameterValue> ReadAllParameterValues(int? jobId) {
            CompileUtilities utilities = new CompileUtilities {Namespace = "I2ES.Model.sql"};
            string sql = utilities.GetResourceValue("GetParameterValues.sql").Replace("~JobId", jobId.ToString());
            var records =  sqlWrapper.RunReturnIDataRecords(sql);
            var parameterValues = new List<ParameterValue>();
            foreach (var dataRecord in records) {
                ParameterValue parameterValue = new ParameterValue();
                parameterValue.JobId = dataRecord["JobId"].GetNullableint();
                parameterValue.ParameterId = dataRecord["ParameterId"].GetNullableint();
                parameterValue.Value = dataRecord["Value"].GetNullablefloat();
                parameterValues.Add(parameterValue);
            }
            return parameterValues;
        }
    }
}