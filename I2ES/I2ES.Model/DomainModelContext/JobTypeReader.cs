﻿using System.Collections.Generic;
using System.Data.SqlClient;
using I2ES.Model.DomainModel;

namespace I2ES.Model.DomainModelContext {
    public class JobTypeReader : IJobTypeReader {
        public ISqlWrapper SqlWrapper { get; set; }
        private readonly CompileUtilities compileUtilities = new CompileUtilities { Namespace = "I2ES.Model.sql." };

        public JobType ReadJobType(int? jobTypeId, Job job) {
            string sql = compileUtilities.GetResourceValue("GetJobTypeById.sql").Replace("~Id", jobTypeId.ToString());
            SqlDataReader reader = SqlWrapper.RunReturnReader(sql);

            JobType jobType = readJobTypeFromReader(reader);
            ParameterGroupReader parameterGroupReader = new ParameterGroupReader(SqlWrapper, new ParameterValueReader(SqlWrapper));
            parameterGroupReader.LoadParameterGroups(jobType, job);
            if (job == null) { return jobType;}
            foreach (var parameter in job.AllParameters) {
                jobType.AllParameters.Add(parameter.Name,parameter);
            }
            return jobType;
        }

        private JobType readJobTypeFromReader(SqlDataReader reader) {
            while (reader.Read()) {
                JobType jobType = new JobType {
                    JobTypeId = reader.GetInt32(reader.GetOrdinal("JobTypeId")),
                    Name = reader["Name"].ToString(),
                    Description = reader["Description"].ToString(),
                    ImagePath = reader["ImagePath"].ToString()

                };
                reader.Dispose();
                return jobType;
            }
            return null;
        }

        public List<JobType> ReadAllJobTypes() {
            string sql = compileUtilities.GetResourceValue("GetAllJobTypes.sql");
            SqlDataReader reader = SqlWrapper.RunReturnReader(sql);
            List<JobType> jobTypes = new List<JobType>();
            loadJobTypeFromDataReader(reader, jobTypes);
            reader.Dispose();
            return jobTypes;
        }

        private void loadJobTypeFromDataReader(SqlDataReader reader, List<JobType> jobTypes){
            while (reader.Read()){
                jobTypes.Add(new JobType{
                    JobTypeId = reader.GetInt32(reader.GetOrdinal("JobTypeId")),
                    Name = reader["Name"].ToString(),
                    Description = reader["Description"].ToString(),
                    ImagePath = reader["ImagePath"].ToString()
                });
            }
        }
    }
}