using System.Collections.Generic;
using System.Data;
using I2ES.Model.DomainModel;

namespace I2ES.Model.DomainModelContext{
    public interface IClassReader{
        List<T> ReadAllElements<T>();
        T ReadSingleElement<T>(int? jobId) where T:new();
        T ReadElementFromRecord<T>(IDataRecord record);
    }
}