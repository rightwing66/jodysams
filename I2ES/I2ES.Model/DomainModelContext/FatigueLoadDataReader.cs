﻿using System.Collections.Generic;
using System.Data;
using I2ES.Model.DomainModel;
using I2ES.Model.Extensions;
// ReSharper disable FunctionComplexityOverflow

namespace I2ES.Model.DomainModelContext {
    public class FatigueLoadDataReader : IFatigueLoadDataReader {
        public ISqlWrapper SqlWrapper { get; set; }

        public List<LoadData> ReadAllElements(int jobId) {
            CompileUtilities compileUtilities = new CompileUtilities { Namespace = "I2ES.Model.sql" };
            string sql = compileUtilities.GetResourceValue("GetFatigueLoadDataForJobId.sql")
                .Replace("~JobId", jobId.ToString());
            var records = SqlWrapper.RunReturnIDataRecords(sql);
            List<LoadData> items = new List<LoadData>();
            foreach (IDataRecord record in records) {
                LoadData item = readFatigueLoadDataFromRecord(record);
                items.Add(item);
            }
            return items;
        }


        private LoadData readFatigueLoadDataFromRecord(IDataRecord record) {
            LoadData loadData = new LoadData();
            loadData.LoadDataId = record["FatigueLoadDataId"].GetNullableint();
            loadData.JobId = record["JobId"].GetNullableint();
            loadData.EID = record["EID"].GetNullableint();
            loadData.LoadCase = record["FatigueLc"].GetNullableint();
            loadData.Node = record["FatigueNode"].GetNullableint();
            loadData.Px = record["FatiguePx"].GetNullablefloat();
            loadData.Py = record["FatiguePy"].GetNullablefloat();
            loadData.Pz = record["FatiguePz"].GetNullablefloat();
            loadData.Mx = record["FatigueMx"].GetNullablefloat();
            loadData.My = record["FatigueMy"].GetNullablefloat();
            loadData.Mz = record["FatigueMz"].GetNullablefloat();
            loadData.Fx = record["FatigueFx"].GetNullablefloat();
            loadData.Fy = record["FatigueFy"].GetNullablefloat();
            loadData.Fxy = record["FatigueFxy"].GetNullablefloat();
            return loadData;
        }

    }
}