﻿using System.Collections.Generic;
using I2ES.Model.DomainModel;

namespace I2ES.Model.DomainModelContext {
    public interface IStaticMaxMinReader {
        ISqlWrapper SqlWrapper { get; set; }
        List<LoadDataMaxMin> ReadAllElements(int jobId);
    }
}