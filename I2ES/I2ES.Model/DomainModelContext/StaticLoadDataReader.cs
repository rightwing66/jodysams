﻿using System.Collections.Generic;
using System.Data;
using I2ES.Model.DomainModel;
using I2ES.Model.Extensions;
// ReSharper disable FunctionComplexityOverflow

namespace I2ES.Model.DomainModelContext {
    public class StaticLoadDataReader : IStaticLoadDataReader {
        public ISqlWrapper SqlWrapper { get; set; }

        public StaticLoadDataReader() {
        }

        public List<LoadData> ReadAllElements(int jobId) {
            CompileUtilities compileUtilities = new CompileUtilities() { Namespace = "I2ES.Model.sql" };
            string sql = compileUtilities.GetResourceValue("GetStaticLoadDataForJobId.sql")
                .Replace("~JobId", jobId.ToString());
            var records = SqlWrapper.RunReturnIDataRecords(sql);
            List<LoadData> items = new List<LoadData>();
            foreach (IDataRecord record in records) {
                LoadData item = readStaticLoadDataFromRecord(record);
                items.Add(item);
            }
            return items;
        }


        private LoadData readStaticLoadDataFromRecord(IDataRecord record) {
            var loadData = new LoadData();
            loadData.LoadDataType = LoadDataType.Static;
            loadData.LoadDataId = record["StaticLoadDataId"].GetNullableint();
            loadData.JobId = record["JobId"].GetNullableint();
            loadData.EID = record["EID"].GetNullableint();
            loadData.LoadCase = record["StaticLc"].GetNullableint();
            loadData.Node = record["StaticNode"].GetNullableint();
            loadData.Px = record["StaticPx"].GetNullablefloat();
            loadData.Py = record["StaticPy"].GetNullablefloat();
            loadData.Pz = record["StaticPz"].GetNullablefloat();
            loadData.Mx = record["StaticMx"].GetNullablefloat();
            loadData.My = record["StaticMy"].GetNullablefloat();
            loadData.Mz = record["StaticMz"].GetNullablefloat();
            loadData.Fx = record["StaticFx"].GetNullablefloat();
            loadData.Fy = record["StaticFy"].GetNullablefloat();
            loadData.Fxy = record["StaticFxy"].GetNullablefloat();
            return loadData;
        }

    }
}