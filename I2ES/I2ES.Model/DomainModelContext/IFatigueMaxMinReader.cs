﻿using System.Collections.Generic;
using I2ES.Model.DomainModel;

namespace I2ES.Model.DomainModelContext {
    public interface IFatigueMaxMinReader {
        List<LoadDataMaxMin> ReadAllElements(int jobId);
        ISqlWrapper SqlWrapper { get; set; }
    }
}