﻿using System.Collections.Generic;
using System.Data;
using I2ES.Model.DomainModel;
using I2ES.Model.Extensions;
// ReSharper disable FunctionComplexityOverflow

namespace I2ES.Model.DomainModelContext {
    public class StaticMaxMinReader : IStaticMaxMinReader {
        public ISqlWrapper SqlWrapper { get; set; }

        public List<LoadDataMaxMin> ReadAllElements(int jobId) {
            CompileUtilities compileUtilities = new CompileUtilities() { Namespace = "I2ES.Model.sql" };
            string sql = compileUtilities.GetResourceValue("GetStaticMaxMinForJobId.sql")
                .Replace("~JobId", jobId.ToString());
            var records = SqlWrapper.RunReturnIDataRecords(sql);
            List<LoadDataMaxMin> items = new List<LoadDataMaxMin>();
            foreach (IDataRecord record in records) {
                LoadDataMaxMin item = readStaticMaxMinFromRecord(record);
                items.Add(item);
            }
            return items;
        }


        private LoadDataMaxMin readStaticMaxMinFromRecord(IDataRecord record) {
            LoadDataMaxMin loadData = new LoadDataMaxMin();
            loadData.Id = record["StaticMaxMinId"].GetNullableint();
            loadData.JobId = record["JobId"].GetNullableint();
            loadData.ElementId = record["ElementId"].GetNullableint();
            loadData.MaxPx = record["MaxPx"].GetNullablefloat();
            loadData.MaxPxLoadCase = record["MaxPxLoadCase"].GetNullablefloat();
            loadData.MinPx = record["MinPx"].GetNullablefloat();
            loadData.MinPxLoadCase = record["MinPxLoadCase"].GetNullablefloat();
            loadData.MaxPy = record["MaxPy"].GetNullablefloat();
            loadData.MaxPyLoadCase = record["MaxPyLoadCase"].GetNullablefloat();
            loadData.MinFy = record["MinFy"].GetNullablefloat();
            loadData.MinFyLoadCase = record["MinFyLoadCase"].GetNullablefloat();
            loadData.MaxFxy = record["MaxFxy"].GetNullablefloat();
            loadData.MaxFxyLoadCase = record["MaxFxyLoadCase"].GetNullablefloat();
            loadData.MinFxy = record["MinFxy"].GetNullablefloat();
            loadData.MinFxyLoadCase = record["MinFxyLoadCase"].GetNullablefloat();
            loadData.MaxFx = record["MaxFx"].GetNullablefloat();
            loadData.MaxFxLoadCase = record["MaxFxLoadCase"].GetNullablefloat();
            loadData.MinFx = record["MinFx"].GetNullablefloat();
            loadData.MinFxLoadCase = record["MinFxLoadCase"].GetNullablefloat();
            loadData.MaxFy = record["MaxFy"].GetNullablefloat();
            loadData.MaxFyLoadCase = record["MaxFyLoadCase"].GetNullablefloat();
            loadData.MinMy = record["MinMy"].GetNullablefloat();
            loadData.MinMyLoadCase = record["MinMyLoadCase"].GetNullablefloat();
            loadData.MaxMz = record["MaxMz"].GetNullablefloat();
            loadData.MaxMzLoadCase = record["MaxMzLoadCase"].GetNullablefloat();
            loadData.MinMz = record["MinMz"].GetNullablefloat();
            loadData.MinMzLoadCase = record["MinMzLoadCase"].GetNullablefloat();
            loadData.MaxMx = record["MaxMx"].GetNullablefloat();
            loadData.MaxMxLoadCase = record["MaxMxLoadCase"].GetNullablefloat();
            loadData.MinMx = record["MinMx"].GetNullablefloat();
            loadData.MinMxLoadCase = record["MinMxLoadCase"].GetNullablefloat();
            loadData.MaxMy = record["MaxMy"].GetNullablefloat();
            loadData.MaxMyLoadCase = record["MaxMyLoadCase"].GetNullablefloat();
            loadData.MinPy = record["MinPy"].GetNullablefloat();
            loadData.MinPyLoadCase = record["MinPyLoadCase"].GetNullablefloat();
            loadData.MaxPz = record["MaxPz"].GetNullablefloat();
            loadData.MaxPzLoadCase = record["MaxPzLoadCase"].GetNullablefloat();
            loadData.MinPz = record["MinPz"].GetNullablefloat();
            loadData.MinPzLoadCase = record["MinPzLoadCase"].GetNullablefloat();
            return loadData;
        }

    }
}