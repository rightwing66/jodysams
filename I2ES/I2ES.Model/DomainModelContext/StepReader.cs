﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using I2ES.Model.DomainModel;
using I2ES.Model.Xml;

namespace I2ES.Model.DomainModelContext{
    public class StepReader{
        private readonly ISqlWrapper sqlWrapper;
        private CompileUtilities compileUtilities = new CompileUtilities{Namespace = "I2ES.Model.sql."};

        public StepReader(ISqlWrapper sqlWrapper){
            this.sqlWrapper = sqlWrapper;
        }

        public void LoadSteps(JobType jobType){
            string sql = compileUtilities.GetResourceValue( "LoadSteps.sql").Replace("~jobTypeId", jobType.JobTypeId.ToString());
            SqlDataReader reader =sqlWrapper.RunReturnReader(sql);
            List<Step> steps = new List<Step>();
            while (reader.Read()){
                Step step = new Step();
                step.StepId = int.Parse(reader["StepId"].ToString());
                step.Name = reader["Name"].ToString();
                step.Caption = reader["Caption"].ToString();
                steps.Add(step);
            }
            jobType.Steps = steps;
            reader.Dispose();
            foreach (var step in steps){
                int stepId = step.StepId;
                foreach (ParameterGroup parameterGroup in jobType.ParameterGroups.Where(pg=>pg.StepId==stepId)){
                    step.ParameterGroups.Add(parameterGroup);
                    parameterGroup.Step = step;
                }
            }

        }
    }
}