﻿using System;
using System.Data.SqlClient;
using I2ES.Model.DomainModel;
using I2ES.Model.Xml;

namespace I2ES.Model.DomainModelContext{
    public class ParameterWriter{
        private readonly ISqlWrapper sqlWrapper;
        private CompileUtilities compileUtilities = new CompileUtilities{Namespace = "I2ES.Model.sql"};

        public ParameterWriter(ISqlWrapper sqlWrapper){
            this.sqlWrapper = sqlWrapper;
        }

        public void SaveChanges(Parameter parameter, Job job){
            if (parameter!=null && parameter.ParameterValue !=null && parameter.ParameterValue.Value != null){
                SaveParameterValue(parameter.ParameterId,parameter.ParameterValue.Value, job.JobId);
            }
        }

        public void SaveParameterValue(int? parameterId, double? parameterValue, int jobId) {
            string sql = compileUtilities.GetResourceValue("UpsertParameter.sql");
            sql =
                sql.Replace("~JobId", jobId.ToString())
                    .Replace("~ParameterId", parameterId.ToString())
                    .Replace("~Value", parameterValue.ToString());
            try {
                sqlWrapper.RunSqlReturnScalar(sql);
            }
            catch (SqlException) {
                Console.WriteLine(sql);
                throw;
            }
        }

        public void SaveParameterValueByParameterName(string parameterName, double? parameterValue, int? jobId) {
            string sql = compileUtilities.GetResourceValue("UpsertParameterByName.sql");
            sql =
                 sql.Replace("~JobId", jobId.ToString())
                     .Replace("~ParameterName", parameterName)
                     .Replace("~Value", parameterValue.ToString());
            try {
                sqlWrapper.RunSqlReturnScalar(sql);
            }
            catch (SqlException) {
                Console.WriteLine(sql);
                throw;
            }
        }
    }
}