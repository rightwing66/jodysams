﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using I2ES.Model.DomainModel;
using I2ES.Model.Extensions;

namespace I2ES.Model.DomainModelContext {
    public class JobWriter : IJobWriter {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ISqlWrapper SqlWrapper { get; set; }
        private CompileUtilities compileUtilities;

        public JobWriter() {
            compileUtilities = new CompileUtilities { Namespace = "I2ES.Model.sql." };
            IsTest = false;
        }
        public bool IsTest { get; set; }

        public Job CreateJob(int jobTypeId, string userId){
            JobType jobType = new JobType(){JobTypeId = jobTypeId};
           return CreateJob(jobType, userId);
        }

        public Job CreateJob(JobType jobType, string userId) {
            string sql = compileUtilities.GetResourceValue("CreateJob.sql")
                .Replace("~UserId", userId).Replace("~JobTypeId", jobType.JobTypeId.ToString())
                .Replace("~StateId", ((int)JobState.New).ToString()).Replace("~CompletionStage", "null")
                .Replace("~IsTest", IsTest ? "1" : "0");
            //            SqlDataReader reader = sqlWrapper.RunReturnReader(sql);
            List<IDataRecord> dataRecords = SqlWrapper.RunReturnIDataRecords(sql);
            var jobId = getJobIdFrom(dataRecords);
            IJobReader jobReader = new JobReader { SqlWrapper = SqlWrapper };
            var job = jobReader.ReadSingleJob(jobId);
            job.JobType = jobType;
            buildJob(job, jobType);
            return job;
        }

        public int? CreateJobId(JobType jobType, string userId) {
            string sql = compileUtilities.GetResourceValue("InsertJobGetID.sql").Replace("~UserId", userId);
            int? jobId = SqlWrapper.RunSqlReturnScalar(sql);
            return jobId;
        }

        private int? getJobIdFrom( List<IDataRecord> dataRecords){
            IDataRecord record = dataRecords.FirstOrDefault();
            if (record == null){
                throw new Exception("Error inserting new Job, no records returned");
            }
            int? jobId = record["JobId"].GetNullableint();
            return jobId;
        }

        public void SaveChanges(Job job)
        {
            log.Debug("SaveChanges " + job);
            string sql = compileUtilities.GetResourceValue("UpdateJob.sql")
                .Replace("~JobTypeId", job.JobType.JobTypeId.ToString())
                .Replace("~UserId", job.UserId)
                .Replace("~JobStateId", job.JobState.ToEnumInt())
                .Replace("~CreateTime", job.CreateTime.ToSqlDate())
                .Replace("~EditTime",job.EditTime.ToSqlDate())
                .Replace("~ProcessBeginTime", job.ProcessBeginTime.ToSqlDate())
                .Replace("~ProcessEndTime", job.ProcessEndTime.ToSqlDate())
                .Replace("~SubmittingNode", job.SubmittingNode)
                .Replace("~SolverStateId", job.SolverState.ToEnumInt())
                .Replace("~SolverNode", job.SolverNode)
                .Replace("~SolverBeginTime", job.SolverBeginTime.ToSqlDate())
                .Replace("~SolverEndTime", job.SolverEndTime.ToSqlDate())
                .Replace("~SolverMessage", job.SolverMessage)
                .Replace("~LoadDataRetreiverStateId", job.LoadDataRetrieverState.ToEnumInt())
                .Replace("~LoadDataRetreiverBeginTime", job.LoadDataRetrieverBeginTime.ToSqlDate())
                .Replace("~LoadDataRetreiverEndTime", job.LoadDataRetrieverEndTime.ToSqlDate())
                .Replace("~LoadDataRetreiverMessage", job.LoadDataRetrieverMessage)
                .Replace("~StoreResultsStateId", job.StoreResultsState.ToEnumInt())
                .Replace("~StoreResultsBeginTime", job.StoreResultsBeginTime.ToSqlDate())
                .Replace("~StoreResultsEndTime", job.StoreResultsEndTime.ToSqlDate())
                .Replace("~StoreResultsMessage", job.StoreResultsMessage)
                .Replace("~TrueNumberStateId", job.TrueNumberState.ToEnumInt())
                .Replace("~TrueNumberBeginTime", job.TrueNumberBeginTime.ToSqlDate())
                .Replace("~TrueNumberEndTime", job.TrueNumberEndTime.ToSqlDate())
                .Replace("~TrueNumberMessage", job.TrueNumberMessage)
                .Replace("~JobId", job.JobId.ToString())
                .Replace("~REINumber", job.REINumber)
                .Replace("~BUNO", job.BUNO)
                .Replace("~PartNumber", job.PartNumber)
                .Replace("~StructureTypeId", job.StructureType.ToEnumInt())
                .Replace("~AircraftModelId", job.AircraftModel.ToEnumInt())
                .Replace("~Report", job.Report.ToSql());
            SqlWrapper.RunSql(sql);
            ParameterWriter writer = new ParameterWriter(SqlWrapper);
            foreach (var parameter in job.AllParameters) {
                writer.SaveChanges(parameter, job);
            }
        }



        private void buildJob(Job job, JobType jobType) {
            jobType.AllParameters.Clear();
            job.ParameterGroups = jobType.ParameterGroups;
            foreach (ParameterGroup parameterGroup in jobType.ParameterGroups) {
                foreach (var parameter in parameterGroup.Parameters) {
                    job.AllParameters.Add(parameter);
                    jobType.AllParameters.Add(parameter.Name,parameter);
                    parameter.ParameterValue = new ParameterValue { Job = job, Parameter = parameter };
                }
            }
        }


    }
}