using System.Collections.Generic;
using I2ES.Model.DomainModel;

namespace I2ES.Model.DomainModelContext {
    public interface IStaticLoadDataReader {
        ISqlWrapper SqlWrapper { get; set; }
        List<LoadData> ReadAllElements(int jobId);
    }
}