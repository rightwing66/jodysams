﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StressCheckExeLauncher {
    class Program {
        static void Main(string[] args) {
            Launcher launcher = new Launcher();
            launcher.Submit();
        }
    }
}
