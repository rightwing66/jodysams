﻿using System;
using I2ES.Model;
using I2ES.Model.SolverLauncher;

namespace StressCheckExeLauncher {
    public class Launcher {
        ConfigurationWrapper wrapper = new ConfigurationWrapper();
        public void Submit() {
            ConnectionProvider provider = new ConnectionProvider();
            SolverLauncher launcher = new SolverLauncher(provider.CreateWrapper(), wrapper);
            launcher.LaunchStressCheckSolver(286, NotifyComplete);
        }


        private void NotifyComplete(int jobId, string outputPath) {
            Console.WriteLine("Complete");
        }
    }
}