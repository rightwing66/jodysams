﻿using System.Collections.Generic;
using System.Xml.Linq;
using I2ES.Services.Validation;
using NUnit.Framework.Constraints;

namespace I2ES.Services.UnitTest {
    public class TokenQueueMatchesConstraint : Constraint {
        private readonly Queue<Token> expectedQueue;
        private string expectedMessage;
        private string actualMessage;
        private Queue<Token> actualQueue;


        public TokenQueueMatchesConstraint(Queue<Token> expected) {
            this.expectedQueue = expected;
        }
        public override void WriteDescriptionTo(MessageWriter writer) {
            throw new System.NotImplementedException();
        }

        public override bool Matches(object actualValue) {
            actualQueue = actualValue as Queue<Token>;
            if (actualQueue == null) {
                setMessage("Valid Queue", "null");
                return false;
            }
            actual = actualValue;
            return compareQueues();
        }

        private bool compareQueues() {
            while (actualQueue.Count > 0) {
                Token actualToken = actualQueue.Dequeue();
                Token expectedToken;
                if (expectedQueue.Count > 0) {
                    expectedToken = expectedQueue.Dequeue();
                }
                else {
                    setMessage("Actual Queue contained more elements than the Expected Queue",null);
                    return false;
                }
                if (actualToken.GetType() != expectedToken.GetType()) {
                    setMessage( expectedToken.GetType().Name, actualToken.GetType().Name);
                    return false;
                }
                if (expectedToken.RawToken != actualToken.RawToken) {
                    setMessage("Token Value: " + expectedToken.RawToken,"Token Value " + actualToken.RawToken);
                    return false;
                }
            }
            if (expectedQueue.Count > 0) {
                setMessage("Expected Queue contained more elements than the Actual Queue", null);
                return false;
            }
            return true;
        }

        private void setMessage(string expectedMessage, string actualMessage) {
            this.expectedMessage = expectedMessage;
            this.actualMessage = actualMessage;
        }

        public override void WriteMessageTo(MessageWriter writer) {
            writer.DisplayDifferences(expectedMessage, actualMessage);
        }
    }
}