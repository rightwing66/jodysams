﻿using System;
using System.Collections.Generic;
using I2ES.Services.Validation;
using NUnit.Framework;

namespace I2ES.Services.UnitTest {
    [TestFixture]
    public class OperationsTest {
        [Test]
        public void exponentiationWorks() {
            OperatorToken op = new OperatorToken("^");
            var result = op.Operate(new List<ValueToken> { new ValueToken(2), new ValueToken(3) });

            Assert.That(result.Value, Is.EqualTo(8));
        }

        [TestCase("-",new object[]{6,2}, Result = 4)]
        public double OperatorTest(string operatorName, object[] paramArray) {
            var operatorToken = new OperatorToken(operatorName);
            return operatorToken.Operate(paramArray.ToValueTokenList()).Value;
        }

        [TestCase("sin", new object[]{Math.PI/2}, Result = 1)]
        [TestCase("cos", new object[] { Math.PI }, Result = -1)]
        public double FunctionReturnsCorrectValue(string functionName, object[] paramArray) {
            
            var function = new FunctionToken(functionName);
            return function.Evaluate(paramArray.ToValueTokenList()).Value;
        }
       

        [TestCase("sin",new object[] { 1, 2 }, ExpectedException = typeof(Exception), ExpectedMessage = "Expected 1 parameter(s) but 2 were passed: 1,2")]
        [TestCase("sin", new object[] { 3, 5 }, ExpectedException = typeof(Exception), ExpectedMessage = "Expected 1 parameter(s) but 2 were passed: 3,5")]
        [TestCase("sin", new object[] { null }, ExpectedException = typeof(Exception), ExpectedMessage = "Parameter 0 was null")]
        public void FunctionReportsIncorrectParameterCount(string functionName, object[] paramarray) {
            var function = new FunctionToken(functionName);
            function.Evaluate(paramarray.ToValueTokenList());
        }
    }
}