﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using I2ES.Services.UnitTest;
using NUnit.Framework;

namespace I2ES.Services.Validation {
 
    [TestFixture]
    public class InfixToPostFixConverterTest {
        private EvaluativeExpression basicExpression;

        [SetUp]
        public void testFixtureSetup() {
            createTestExpressions();
        }

        [Test]
        public void ReportsFailureOnOneParentheses() {
            InfixToPostFixConverter converter = new InfixToPostFixConverter();
            PostfixExpression postfixExpression = converter.Convert(createTestExpression("(33+4*2"));
            Assert.That(postfixExpression.Result, Is.EqualTo(Result.Failure) );
            Assert.That(postfixExpression.Messages.FirstOrDefault(), Is.EqualTo("Mismatched Parenthesis"));
        }

        [Test]
        public void AddsValueToOutputStack() {
            InfixToPostFixConverter converter = new InfixToPostFixConverter();
            PostfixExpression postfixExpression = converter.Convert(basicExpression);
            
            Assert.That(postfixExpression.OutputQue, Is.Not.Null);
            Token firstToken = postfixExpression.OutputQue.Dequeue();
            Assert.That(firstToken.RawToken, Is.EqualTo("8"));
        }


        [Test]
        public void TokenOverrideEqualsWorksWithNull() {
            Token token = null;
            Assert.That(token==null,Is.True);
            Assert.That(token!=null,Is.False);
        }



        [Test]
        public void LeftParenthesisPushedOntoStack() {
            InfixToPostFixConverter converter = new InfixToPostFixConverter();
            PostfixExpression postfixExpression = converter.Convert(basicExpression);
            Assert.That(postfixExpression.OutputQue.WriteQueue(), Is.EqualTo("8 45 6 1 21 - / - * 32 + "));
                                                                            
        }

        [Test]
        public void WhenOperatorIsLeftAndHigherPrecedencePushToStack() {
            InfixToPostFixConverter converter = new InfixToPostFixConverter();
            PostfixExpression postfixExpression = converter.Convert(createTestExpression("33+4*2"));
            Assert.That(postfixExpression.OutputQue.WriteQueue(), Is.EqualTo("33 4 2 * + "));
        }

        [Test]
        public void WhenOperatorIsLeftAndEqualPrecedencePopStackToQueue() {
            InfixToPostFixConverter converter = new InfixToPostFixConverter();
            PostfixExpression postfixExpression = converter.Convert(createTestExpression("33+4*2/3"));

            Assert.That(postfixExpression.OutputQue.WriteQueue(), Is.EqualTo("33 4 2 * 3 / + "));
        }

        [TestCase("33+4*2/3", Result = "33 4 2 * 3 / + ")]
        [TestCase("(33+4)*2/3", Result = "33 4 + 2 * 3 / ")]
        [TestCase("((85-23*2)/(2+56)+81.3/5)+5)", Result = "85 23 2 * - 2 56 + / 81.3 5 / + 5 + ")]
        [TestCase("33^4*2/3", Result = "33 4 ^ 2 * 3 / ")]
        [TestCase("33*4^2/3", Result = "33 4 2 ^ 3 / * ")]
        [TestCase("1.2*Sin(3.14,25,8)/3-25", Result = "1.2 3.14 25 8 Sin * 3 / 25 - ")]
        [TestCase("1.5*6^2*(6-5)", Result = "1.5 6 2 ^ 6 5 - * * ")]
        [TestCase("8*(45-6/(1-21))+32", Result = "8 45 6 1 21 - / - * 32 + ")]
        public string TestConverter(string inputExpression) {
            var converter = new InfixToPostFixConverter();
            var expression = converter.Convert(createTestExpression(inputExpression));
            return expression.OutputQue.WriteQueue();
        }

        [Test]
        public void FunctionOneParameterTokenWorks() {
            var sourceExpression = new EvaluativeExpression(
                new Queue<Token>(
                    new List<Token> {
                        new FunctionToken("SIN"),
                        new ParenthesesToken("("),
                        new ValueToken("3.14"),
                        new ParenthesesToken(")")
                    }
                    )
                );
            var converter = new InfixToPostFixConverter();
            var rpnExpression = converter.Convert(sourceExpression);
            Assert.That(rpnExpression.OutputQue.WriteQueue(),Is.EqualTo("3.14 SIN "));
        }

        private void createTestExpressions() {
            var parser = new TextPrefixParser("8*(45-6/(1-21))+32");
            basicExpression = new EvaluativeExpression(parser.Parse());
        }

        private EvaluativeExpression createTestExpression(string rawText) {
            var parser = new TextPrefixParser(rawText);
            return new EvaluativeExpression(parser.Parse());
        }
    }

}