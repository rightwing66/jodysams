﻿using I2ES.Services.Validation;
using NUnit.Framework;

namespace I2ES.Services.UnitTest {
    [TestFixture]
    public class XmlEncodedParserUnitTest {
        [Test]
        public void CanParseEsrdXmlEncoding() {
            XmlEncodedParser parser = new XmlEncodedParser("Ri_g_Delta_p_Rw_p_Rf");
            Assert.That(parser.Parse(), Is.EqualTo("Ri > Delta + Rw + Rf"));
        }
    }
}