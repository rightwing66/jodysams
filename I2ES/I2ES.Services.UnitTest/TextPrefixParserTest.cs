﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using I2ES.Services.Validation;
using NUnit.Framework;

namespace I2ES.Services.UnitTest {
    [TestFixture]
    public class TextPrefixParserTest {
        [Test]
        public void TwoTokensAreEqualIfTheirTextIsEqual() {
            Token token = new ValueToken("21");
            Token token2 = new ValueToken("23");
            Token token3 = new ValueToken("21");

            Assert.That(token, Is.Not.EqualTo(token2));
            Assert.That(token, Is.EqualTo(token3));
            Assert.That(token == token3, Is.True);
            Assert.That(token != token2, Is.True);
        }

        [Test]
        public void ConvertsBinaryExpressionToTokenQueue() {
            TextPrefixParser parser = new TextPrefixParser("21+42");
            Queue<Token> expectedTokens =
                new Queue<Token>(new List<Token> { new ValueToken("21"), new OperatorToken("+"), new ValueToken("42") });

            Assert.That(parser.Parse(new NameValueCollection()), Is.EqualTo(expectedTokens));
            Assert.That(parser.ToString(), Is.EqualTo("21+42"));
        }

        [Test]
        public void ConvertsExpressionWith2OperatorsToTokenQueue() {
            TextPrefixParser parser = new TextPrefixParser("21+42+81.1");
            Queue<Token> expectedTokens =
                new Queue<Token>(new List<Token> {
                    new ValueToken("21"),
                    new OperatorToken("+"),
                    new ValueToken("42"),
                    new OperatorToken("+"),
                    new ValueToken("81.1")
                });

            Assert.That(parser.Parse(new NameValueCollection()), Is.EqualTo(expectedTokens));
            Assert.That(parser.ToString(), Is.EqualTo("21+42+81.1"));
        }

        [Test]
        public void ConvertsExpressionWith3OperatorsToTokenQueue() {
            TextPrefixParser parser = new TextPrefixParser("21+42+81.1+5");
            Queue<Token> expectedTokens =
                new Queue<Token>(new List<Token> {
                    new ValueToken("21"),
                    new OperatorToken("+"),
                    new ValueToken("42"),
                    new OperatorToken("+"),
                    new ValueToken("81.1"),
                    new OperatorToken("+"),
                    new ValueToken("5")
                });

            Assert.That(parser.Parse(new NameValueCollection()), Is.EqualTo(expectedTokens));
            Assert.That(parser.ToString(), Is.EqualTo("21+42+81.1+5"));
        }

        [Test]
        public void AcceptsAMinusOperator() {
            TextPrefixParser parser = new TextPrefixParser("21-42+81.1+5");
            Queue<Token> expectedTokens =
                new Queue<Token>(new List<Token> {
                    new ValueToken("21"),
                    new OperatorToken("-"),
                    new ValueToken("42"),
                    new OperatorToken("+"),
                    new ValueToken("81.1"),
                    new OperatorToken("+"),
                    new ValueToken("5")
                });

            Assert.That(parser.Parse(new NameValueCollection()), Is.EqualTo(expectedTokens));
            Assert.That(parser.ToString(), Is.EqualTo("21-42+81.1+5"));
        }

        [Test]
        public void AcceptsAllOperators() {
            TextPrefixParser parser = new TextPrefixParser("((21-42)*(81.1+5))/(42+81.1)");
            Queue<Token> expectedTokens =
                new Queue<Token>(new List<Token> {
                    new ParenthesesToken("("),
                    new ParenthesesToken("("),
                    new ValueToken("21"),
                    new OperatorToken("-"),
                    new ValueToken("42"),
                    new ParenthesesToken(")"),
                    new OperatorToken("*"),
                    new ParenthesesToken("("),
                    new ValueToken("81.1"),
                    new OperatorToken("+"),
                    new ValueToken("5"),
                    new ParenthesesToken(")"),
                    new ParenthesesToken(")"),
                    new OperatorToken("/"),
                    new ParenthesesToken("("),
                    new ValueToken("42"),
                    new OperatorToken("+"),
                    new ValueToken("81.1"),
                    new ParenthesesToken(")")
                });

            Assert.That(parser.Parse(new NameValueCollection()), Is.EqualTo(expectedTokens));
            Assert.That(parser.ToString(), Is.EqualTo("((21-42)*(81.1+5))/(42+81.1)"));
        }


        [Test]
        public void IgnoresWhiteSpace() {
            TextPrefixParser parser = new TextPrefixParser(" 21 - 42+\t 81.1 +5 ");
            Queue<Token> expectedTokens =
                new Queue<Token>(new List<Token> {
                    new ValueToken("21"),
                    new OperatorToken("-"),
                    new ValueToken("42"),
                    new OperatorToken("+"),
                    new ValueToken("81.1"),
                    new OperatorToken("+"),
                    new ValueToken("5")
                });

            Assert.That(parser.Parse(new NameValueCollection()), Is.EqualTo(expectedTokens));
            Assert.That(parser.ToString(), Is.EqualTo("21-42+81.1+5"));
        }

        [Test]
        public void CanParseEqualityOperator() {
            TextPrefixParser parser = new TextPrefixParser("21-42>=81.1+5");
            Queue<Token> expectedTokens =
                new Queue<Token>(new List<Token> {
                    new ValueToken("21"),
                    new OperatorToken("-"),
                    new ValueToken("42"),
                    new ComparisonOperatorToken(">="),
                    new ValueToken("81.1"),
                    new OperatorToken("+"),
                    new ValueToken("5")
                });
            var tokenQue = parser.Parse(new NameValueCollection());
            Assert.That(tokenQue, Is.EqualTo(expectedTokens));
            Assert.That(parser.ToString(), Is.EqualTo("21-42>=81.1+5"));
        }

        [Test]
        public void CanHandleParenthesis() {
            TextPrefixParser parser = new TextPrefixParser("21*((42-15)>=81.1+5");
            Queue<Token> expectedTokens =
                new Queue<Token>(new List<Token> {
                    new ValueToken("21"),
                    new OperatorToken("*"),
                    new ParenthesesToken("("),
                    new ParenthesesToken("("),
                    new ValueToken("42"),
                    new OperatorToken("-"),
                    new ValueToken("15"),
                    new ParenthesesToken(")"),
                    new ComparisonOperatorToken(">="),
                    new ValueToken("81.1"),
                    new OperatorToken("+"),
                    new ValueToken("5")
                });
            var tokenQue = parser.Parse(new NameValueCollection());
            Assert.That(tokenQue, Is.EqualTo(expectedTokens), "Checking Tokens: " + parser);
            Assert.That(parser.ToString(), Is.EqualTo("21*((42-15)>=81.1+5"));
        }

        [Test]
        public void CanHandleVariableNames() {
            TextPrefixParser parser = new TextPrefixParser("Ro*((lex-wo)>=iff+5");
            var tokenQue = parser.Parse(new NameValueCollection{{"Ro","1"},{"lex","2"},{"wo","3"},{"iff","4"}});
            Assert.That(parser.ToString(), Is.EqualTo("1*((2-3)>=4+5"));
        }

        [Test]
        public void StripsSpaces() {
            TextPrefixParser parser = new TextPrefixParser("Ro * ((lex-wo)   >=iff + 5");
            var tokenQue = parser.Parse(new NameValueCollection() { { "Ro", "1" }, { "lex", "2" }, { "wo", "3" }, { "iff", "4" } });
            Assert.That(parser.ToString(), Is.EqualTo("1*((2-3)>=4+5"));
        }

        [Test]
        public void HandlesComparisonOperatorsLengthOne() {
            TextPrefixParser parser = new TextPrefixParser("ab<cd+2");
            Queue<Token> expectedTokens =
                new Queue<Token>(new List<Token> {
                    new VariableToken("2"),
                    new ComparisonOperatorToken("<"),
                    new VariableToken("1"),
                    new OperatorToken("+"),
                    new ValueToken("2")
                });
            var tokenQue = parser.Parse(new NameValueCollection{{"ab","2"},{"cd","1"}});
            Assert.That(tokenQue,Is.EqualTo(expectedTokens));
            Assert.That(parser.ToString(), Is.EqualTo("2<1+2"));
        }

        [Test]
        public void ParserFindsFunctionAndOneParameter() {
            TextPrefixParser parser = new TextPrefixParser("SIN(3.14)");
            Queue<Token> expectedTokens =
                new Queue<Token>(new List<Token> {
                    new FunctionToken("SIN"),
                    new ParenthesesToken("("),
                    new ValueToken("3.14"),
                    new ParenthesesToken(")")

                });
            var tokenQue = parser.Parse();
            Debug.WriteLine(tokenQue.WriteQueue());
            Assert.That(tokenQue,new TokenQueueMatchesConstraint(expectedTokens));
        }
    }
}