﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using I2ES.Services.Validation;
using NUnit.Framework;

namespace I2ES.Services.UnitTest
{
    [TestFixture]
    public class ComparisonExpressionTest
    {



        [Test]
        public void FindsLeftHandExpression()
        {
            ComparisonExpression expression = new ComparisonExpression(new TextPrefixParser(new XmlEncodedParser("Ri_g_Delta_p_Rw_p_Rf")), new PostFixEvaluator());
            NameValueCollection variables = new NameValueCollection { { "Ri", "42" }, { "Delta", "13.2" }, { "Rw", "21.4" }, { "Rf", "18.2" } };

            expression.Evaluate(new InfixToPostFixConverter(), variables, .0000001);

            Debug.WriteLine(expression.LeftExpression);
            Assert.That(expression.LeftExpression, Is.Not.Null);
            Queue<Token> tokens = expression.LeftExpression.Tokens;
            Token firstToken = tokens.Dequeue();
            Assert.That(firstToken, Is.Not.Null, "LeftExpression has no tokens");
            Assert.That(firstToken is ValueToken);
            Assert.That(((ValueToken)firstToken).Value, Is.EqualTo(42));
        }

        [Test]
        public void FindsRightHandExpression()
        {
            ComparisonExpression expression = new ComparisonExpression(new TextPrefixParser(new XmlEncodedParser("Ri_g_Delta_p_Rw_p_Rf")), new PostFixEvaluator());
            NameValueCollection variables = new NameValueCollection() { { "Ri", "42" }, { "Delta", "13.2" }, { "Rw", "21.4" }, { "Rf", "18.2" } };

            expression.Evaluate(new InfixToPostFixConverter(), variables, .0000001);

            Assert.That(expression.RightExpression, Is.Not.Null, "RightExpression is null");
            var tokens = expression.RightExpression.Tokens;
            Token firstToken = tokens.Dequeue();
            Assert.That(firstToken, Is.Not.Null, "RightExpression has no tokens");
            Assert.That(firstToken is ValueToken);
            Assert.That(((ValueToken)firstToken).Value, Is.EqualTo(13.2));
            Assert.That(tokens.Dequeue().RawToken, Is.EqualTo("+"));
            Assert.That(tokens.Dequeue().RawToken, Is.EqualTo("21.4"));
            Assert.That(tokens.Dequeue().RawToken, Is.EqualTo("+"));
            Assert.That(tokens.Dequeue().RawToken, Is.EqualTo("18.2"));
        }

        [Test]
        public void FindsEqualityOperator()
        {
            ComparisonExpression expression = new ComparisonExpression(new TextPrefixParser(new XmlEncodedParser("Ri_g_Delta_p_Rw_p_Rf")), new PostFixEvaluator());
            NameValueCollection variables = new NameValueCollection() { { "Ri", "42" }, { "Delta", "13.2" }, { "Rw", "21.4" }, { "Rf", "18.2" } };

            expression.Evaluate(new InfixToPostFixConverter(), variables, .0000001);

            Assert.That(expression.ComparisonOperator.RawToken, Is.EqualTo(">"));
        }

        [Test]
        public void CapturesPostFixExpression()
        {
            var expression = new ComparisonExpression(new TextPrefixParser("Ri+5*(X-2)>51"), new PostFixEvaluator());
            NameValueCollection variables = new NameValueCollection { { "Ri", "42" }, { "X", "8" } };

            expression.Evaluate(new InfixToPostFixConverter(), variables, .0000001);

            Assert.That(expression.LeftExpression.PostFix.OutputQue.WriteQueue(), Is.EqualTo("42 5 8 2 - * + "));
            Assert.That(expression.RightExpression.PostFix.OutputQue.WriteQueue(), Is.EqualTo("51 "));
        }

        [Test]
        public void CanEvaulateSimpleLeftExpression()
        {
            var expression = new ComparisonExpression(new TextPrefixParser("Ri+5*(X-2)>51"), new PostFixEvaluator());
            NameValueCollection variables = new NameValueCollection { { "Ri", "42" }, { "X", "8" } };

            expression.Evaluate(new InfixToPostFixConverter(), variables, .0000001);

            Assert.That(expression.LeftExpression.Result, Is.EqualTo(72));

        }

        [TestCase("33+4*2/3>5", 35.6666)]
        [TestCase("(33+4)*2/3>5", 24.6666)]
        [TestCase("((85-23*2)/(2+56)+81.3/5)+5)>5", 21.9324138)]
        [TestCase("33^4*2/3>5", 790614)]
        [TestCase("1.2*Sin(3.14,2)/3-25>5", -24.048268693255785)]
        [TestCase("1.5*6^2*(6-5)>5", 54)]
        [TestCase("8*(45-6/(1-21))+32>5", 394.4)]
        public void TestConverter(string inputExpression, double result)
        {
            var expression = new ComparisonExpression(new TextPrefixParser(inputExpression), new PostFixEvaluator());
            expression.Evaluate(new InfixToPostFixConverter(), new NameValueCollection(), .0000001);
            Assert.That(expression.LeftExpression.Result, Is.EqualTo(result).Within(.0001));
        }


        [Test]
        public void FullyParsesXMlToEvaluatedExpression()
        {
            ComparisonExpression expression = new ComparisonExpression(new TextPrefixParser(new XmlEncodedParser("Ri_g_Delta_p_Rw_p_Rf")), new PostFixEvaluator());
            NameValueCollection variables = new NameValueCollection() { { "Ri", "42" }, { "Delta", "13.2" }, { "Rw", "21.4" }, { "Rf", "18.2" } };

            expression.Evaluate(new InfixToPostFixConverter(), variables, .0000001);
            Assert.That(expression.ComparisonOperator.RawToken, Is.EqualTo(">"));
            bool isGreater = expression.LeftExpression.Result <= expression.RightExpression.Result;

            Assert.That(isGreater);
        }


        //validte parameters, numeric Security?, test missing right hand expression should throw error

        private EvaluativeExpression createTestExpression(string rawText)
        {
            var parser = new TextPrefixParser(rawText);
            return new EvaluativeExpression(parser.Parse());
        }
    }
}