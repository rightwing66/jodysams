﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using I2ES.Services.Validation;

namespace I2ES.Services.UnitTest {
    public static class UnitTestCollectionHelper {
        public static string WriteStack(this Stack<Token> stack) {
            return stack.Aggregate("", (current, token) => current + token.RawToken).TrimEnd();
        }

        public static string WriteQueue(this Queue<Token> queue) {
            string result = null;
            foreach (Token token in queue) {
                result += token.RawToken + " ";
            }
            return result;
        }

        public static List<ValueToken> ToValueTokenList(this object[] parameters) {
            
            List<ValueToken> valueTokenList = new List<ValueToken>();
            int i = 0;
            foreach (var parameter in parameters) {
                if (parameter == null) {
                    throw new Exception("Parameter "+i+" was null");
                }
                double value;
                if (!double.TryParse(parameter.ToString(), out value)) {
                    throw new Exception("Cannot parse " + parameter + " to a double");
                }
                valueTokenList.Add(new ValueToken(value));
                i++;
            }
            return valueTokenList;
        }
    }
}