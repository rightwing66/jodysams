﻿using I2ES.Services.Validation;
using NUnit.Framework;

namespace I2ES.Services.UnitTest {
    [TestFixture]
    public class PostFixEvaluatorTest {
        [Test]
        public void CanEvaluateSimplePrefix() {
            var converter = new InfixToPostFixConverter();
            var expression = converter.Convert(createTestExpression("33+4*2/8")); ;
            var evaluator = new PostFixEvaluator();

            double result = evaluator.Evaluate(expression);

            Assert.That(result, Is.EqualTo(34));
        }


        private EvaluativeExpression createTestExpression(string rawText) {
            var parser = new TextPrefixParser(rawText);
            return new EvaluativeExpression(parser.Parse());
        }
    }
}