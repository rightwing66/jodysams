EXEC sp_droprolemember 'TrueNumbersDeveloper', 'TrueNumbers';
EXEC sp_droprolemember 'TrueNumbersDeveloper', 'IsamsResultsApp';
drop role [TrueNumbersDeveloper]

USE [ISAMS]
GO
CREATE ROLE [TrueNumbersDeveloper]
GO
use [ISAMS]
GO
GRANT DELETE ON [Results].[Extraction] TO [TrueNumbersDeveloper]
GO
use [ISAMS]
GO
GRANT INSERT ON [Results].[Extraction] TO [TrueNumbersDeveloper]
GO
use [ISAMS]
GO
GRANT SELECT ON [Results].[Extraction] TO [TrueNumbersDeveloper]
GO
use [ISAMS]
GO
GRANT UPDATE ON [Results].[Extraction] TO [TrueNumbersDeveloper]
GO
use [ISAMS]
GO
GRANT VIEW DEFINITION ON [Results].[Extraction] TO [TrueNumbersDeveloper]
GO

GRANT DELETE ON [Results].[ResultField] TO [TrueNumbersDeveloper]
GO
use [ISAMS]
GO
GRANT INSERT ON [Results].[ResultField] TO [TrueNumbersDeveloper]
GO
use [ISAMS]
GO
GRANT SELECT ON [Results].[ResultField] TO [TrueNumbersDeveloper]
GO
use [ISAMS]
GO
GRANT UPDATE ON [Results].[ResultField] TO [TrueNumbersDeveloper]
GO
use [ISAMS]
GO
GRANT VIEW DEFINITION ON [Results].[ResultField] TO [TrueNumbersDeveloper]


GRANT DELETE ON [Results].[ResultFile] TO [TrueNumbersDeveloper]
GO
use [ISAMS]
GO
GRANT INSERT ON [Results].[ResultFile] TO [TrueNumbersDeveloper]
GO
use [ISAMS]
GO
GRANT SELECT ON [Results].[ResultFile] TO [TrueNumbersDeveloper]
GO
use [ISAMS]
GO
GRANT UPDATE ON [Results].[ResultFile] TO [TrueNumbersDeveloper]
GO
use [ISAMS]
GO
GRANT VIEW DEFINITION ON [Results].[ResultFile] TO [TrueNumbersDeveloper]

GRANT DELETE ON [Results].[ResultType] TO [TrueNumbersDeveloper]
GO
use [ISAMS]
GO
GRANT INSERT ON [Results].[ResultType] TO [TrueNumbersDeveloper]
GO
use [ISAMS]
GO
GRANT SELECT ON [Results].[ResultType] TO [TrueNumbersDeveloper]
GO
use [ISAMS]
GO
GRANT UPDATE ON [Results].[ResultType] TO [TrueNumbersDeveloper]
GO
use [ISAMS]
GO
GRANT VIEW DEFINITION ON [Results].[ResultType] TO [TrueNumbersDeveloper]

GRANT DELETE ON [TrueNumbers].[Material] TO [TrueNumbersDeveloper]
GO
use [ISAMS]
GO
GRANT INSERT ON [TrueNumbers].[Material] TO [TrueNumbersDeveloper]
GO
use [ISAMS]
GO
GRANT SELECT ON [TrueNumbers].[Material] TO [TrueNumbersDeveloper]
GO
use [ISAMS]
GO
GRANT UPDATE ON [TrueNumbers].[Material] TO [TrueNumbersDeveloper]
GO
use [ISAMS]
GO
GRANT VIEW DEFINITION ON [TrueNumbers].[Material] TO [TrueNumbersDeveloper]

GRANT DELETE ON [Results].[SolverOutput] TO [TrueNumbersDeveloper]
GO
use [ISAMS]
GO
GRANT INSERT ON [Results].[SolverOutput] TO [TrueNumbersDeveloper]
GO
use [ISAMS]
GO
GRANT SELECT ON [Results].[SolverOutput] TO [TrueNumbersDeveloper]
GO
use [ISAMS]
GO
GRANT UPDATE ON [Results].[SolverOutput] TO [TrueNumbersDeveloper]
GRANT VIEW DEFINITION ON [Results].[SolverOutput] TO [TrueNumbersDeveloper]
go

EXEC sp_addrolemember 'TrueNumbersDeveloper', 'TrueNumbers';
EXEC sp_addrolemember 'TrueNumbersDeveloper', 'IsamsResultsApp';