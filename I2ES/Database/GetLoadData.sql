DECLARE @RodBarOrShell nvarchar(10);
DECLARE @Location nvarchar(20);
DECLARE @JobTypeId int;
DECLARE @JobId int;
DECLARE @StaticLoadCase int;
DECLARE @FatigueLoadCase int;
DECLARE @ElementId int;
SET @RodBarOrShell = 'Shell';
SET @Location = 'Wing';
SET @ElementId = 97166724;
SET @JobTypeId = 1;
SET @JobId = 1;
SET @StaticLoadCase = '';
Set @FatigueLoadCase = '';

IF @RodBarOrShell = 'RodBar'

BEGIN

	IF @Location = 'Aft'

	BEGIN

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobTypeId, @JobId, AR.EID, AR.LC, AR.PX, AR.PY, AR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM AftStaticRods AS AR 
			INNER JOIN StaticMoments AS SM ON AR.EID = SM.EID AND AR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND AR.EID=@ElementId)
			OR (AR.EID=@ElementId AND AR.LC=@StaticLoadCase)
			
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobTypeId, @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM FullFatigueRods AS FFR 
			INNER JOIN FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)

	END

	ELSE IF @Location = 'Center'

	BEGIN

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobTypeId, @JobId, CR.EID, CR.LC, CR.PX, CR.PY, CR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM CenterStaticRods AS CR 
			INNER JOIN StaticMoments AS SM ON CR.EID = SM.EID AND CR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND CR.EID=@ElementId)
			OR (CR.EID=@ElementId AND CR.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobTypeId, @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM FullFatigueRods AS FFR 
			INNER JOIN FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'Forward'

	BEGIN

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobTypeId, @JobId, FR.EID, FR.LC, FR.PX, FR.PY, FR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM ForwardStaticRods AS FR 
			INNER JOIN StaticMoments AS SM ON FR.EID = SM.EID AND FR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND FR.EID=@ElementId)
			OR (FR.EID=@ElementId AND FR.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobTypeId, @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM FullFatigueRods AS FFR 
			INNER JOIN FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)

	END

	ELSE IF @Location = 'LEX'

	BEGIN

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobTypeId, @JobId, LR.EID, LR.LC, LR.PX, LR.PY, LR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM LEXStaticRods AS LR 
			INNER JOIN StaticMoments AS SM ON LR.EID = SM.EID AND LR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND LR.EID=@ElementId)
			OR (LR.EID=@ElementId AND LR.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobTypeId, @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM FullFatigueRods AS FFR 
			INNER JOIN FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)

	END

	ELSE IF @Location = 'NoseBarrel'

	BEGIN

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobTypeId, @JobId, NBR.EID, NBR.LC, NBR.PX, NBR.PY, NBR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM NoseBarrelStaticRods AS NBR 
			INNER JOIN StaticMoments AS SM ON NBR.EID = SM.EID AND NBR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND NBR.EID=@ElementId)
			OR (NBR.EID=@ElementId AND NBR.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobTypeId, @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM FullFatigueRods AS FFR 
			INNER JOIN FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)

	END

	ELSE IF @Location = 'Wing'

	BEGIN

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobTypeId, @JobId, WR.EID, WR.LC, WR.PX, WR.PY, WR.PZ, WSM.Node, WSM.Mx, WSM.My, WSM.Mz
		FROM WingStaticRods AS WR
			INNER JOIN WingStaticMoments AS WSM ON WR.EID = WSM.EID AND WR.LC = WSM.LC
		WHERE 
			(@StaticLoadCase=0 AND WR.EID=@ElementId)
			OR (WR.EID=@ElementId AND WR.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobTypeId, @JobId, WFR.EID, WFR.LC, WFR.PX, WFR.PY, WFR.PZ, WFM.Node, WFM.Mx, WFM.My, WFM.Mz
		FROM WingFatigueRodLoads AS WFR 
			INNER JOIN WingFatigueMoments AS WFM ON WFR.EID = WFM.EID AND WFR.LC = WFM.LC
		WHERE 
			(@FatigueLoadCase=0 AND WFR.EID=@ElementId)
			OR (WFR.EID=@ElementId AND WFR.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'ForwardCenter'

	BEGIN

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobTypeId, @JobId, FR.EID, FR.LC, FR.PX, FR.PY, FR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM ForwardStaticRods AS FR 
			INNER JOIN StaticMoments AS SM ON FR.EID = SM.EID AND FR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND FR.EID=@ElementId)
			OR (FR.EID=@ElementId AND FR.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobTypeId, @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM FullFatigueRods AS FFR 
			INNER JOIN FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)
	
		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobTypeId, @JobId, CR.EID, CR.LC, CR.PX, CR.PY, CR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM CenterStaticRods AS CR
			INNER JOIN StaticMoments AS SM ON CR.EID = SM.EID AND CR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND CR.EID=@ElementId)
			OR (CR.EID=@ElementId AND CR.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobTypeId, @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM FullFatigueRods AS FFR 
			INNER JOIN FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'ForwardNoseBarrel'

	BEGIN

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobTypeId, @JobId, FR.EID, FR.LC, FR.PX, FR.PY, FR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM ForwardStaticRods AS FR 
			INNER JOIN StaticMoments AS SM ON FR.EID = SM.EID AND FR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND FR.EID=@ElementId)
			OR (FR.EID=@ElementId AND FR.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobTypeId, @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM FullFatigueRods AS FFR 
			INNER JOIN FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobTypeId, @JobId, NBR.EID, NBR.LC, NBR.PX, NBR.PY, NBR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM NoseBarrelStaticRods AS NBR
			INNER JOIN StaticMoments AS SM ON NBR.EID = SM.EID AND NBR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND NBR.EID=@ElementId)
			OR (NBR.EID=@ElementId AND NBR.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobTypeId, @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM FullFatigueRods AS FFR 
			INNER JOIN FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'ForwardLEX'

	BEGIN

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobTypeId, @JobId, FR.EID, FR.LC, FR.PX, FR.PY, FR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM ForwardStaticRods AS FR 
			INNER JOIN StaticMoments AS SM ON FR.EID = SM.EID AND FR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND FR.EID=@ElementId)
			OR (FR.EID=@ElementId AND FR.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobTypeId, @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM FullFatigueRods AS FFR 
			INNER JOIN FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobTypeId, @JobId, LR.EID, LR.LC, LR.PX, LR.PY, LR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM LEXStaticRods AS LR
			INNER JOIN StaticMoments AS SM ON LR.EID = SM.EID AND LR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND LR.EID=@ElementId)
			OR (LR.EID=@ElementId AND LR.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobTypeId, @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM FullFatigueRods AS FFR 
			INNER JOIN FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'CenterLEX'

	BEGIN

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobTypeId, @JobId, CR.EID, CR.LC, CR.PX, CR.PY, CR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM CenterStaticRods AS CR
			INNER JOIN StaticMoments AS SM ON CR.EID = SM.EID AND CR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND CR.EID=@ElementId)
			OR (CR.EID=@ElementId AND CR.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobTypeId, @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM FullFatigueRods AS FFR 
			INNER JOIN FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobTypeId, @JobId, LR.EID, LR.LC, LR.PX, LR.PY, LR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM LEXStaticRods AS LR
			INNER JOIN StaticMoments AS SM ON LR.EID = SM.EID AND LR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND LR.EID=@ElementId)
			OR (LR.EID=@ElementId AND LR.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobTypeId, @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM FullFatigueRods AS FFR 
			INNER JOIN FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'CenterForwardNoseBarrel'

	BEGIN

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobTypeId, @JobId, CR.EID, CR.LC, CR.PX, CR.PY, CR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM CenterStaticRods AS CR
			INNER JOIN StaticMoments AS SM ON CR.EID = SM.EID AND CR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND CR.EID=@ElementId)
			OR (CR.EID=@ElementId AND CR.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobTypeId, @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM FullFatigueRods AS FFR 
			INNER JOIN FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)
	

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobTypeId, @JobId, FR.EID, FR.LC, FR.PX, FR.PY, FR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM ForwardStaticRods AS FR 
			INNER JOIN StaticMoments AS SM ON FR.EID = SM.EID AND FR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND FR.EID=@ElementId)
			OR (FR.EID=@ElementId AND FR.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobTypeId, @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM FullFatigueRods AS FFR 
			INNER JOIN FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobTypeId, @JobId, NBR.EID, NBR.LC, NBR.PX, NBR.PY, NBR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM NoseBarrelStaticRods AS NBR
			INNER JOIN StaticMoments AS SM ON NBR.EID = SM.EID AND NBR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND NBR.EID=@ElementId)
			OR (NBR.EID=@ElementId AND NBR.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobTypeId, @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM FullFatigueRods AS FFR 
			INNER JOIN FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'ForwardNoseBarrelLEX'

	BEGIN

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobTypeId, @JobId, FR.EID, FR.LC, FR.PX, FR.PY, FR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM ForwardStaticRods AS FR 
			INNER JOIN StaticMoments AS SM ON FR.EID = SM.EID AND FR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND FR.EID=@ElementId)
			OR (FR.EID=@ElementId AND FR.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobTypeId, @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM FullFatigueRods AS FFR 
			INNER JOIN FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobTypeId, @JobId, NBR.EID, NBR.LC, NBR.PX, NBR.PY, NBR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM NoseBarrelStaticRods AS NBR
			INNER JOIN StaticMoments AS SM ON NBR.EID = SM.EID AND NBR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND NBR.EID=@ElementId)
			OR (NBR.EID=@ElementId AND NBR.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobTypeId, @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM FullFatigueRods AS FFR 
			INNER JOIN FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobTypeId, @JobId, LR.EID, LR.LC, LR.PX, LR.PY, LR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM LEXStaticRods AS LR
			INNER JOIN StaticMoments AS SM ON LR.EID = SM.EID AND LR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND LR.EID=@ElementId)
			OR (LR.EID=@ElementId AND LR.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobTypeId, @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM FullFatigueRods AS FFR 
			INNER JOIN FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)

	END

	ELSE IF @Location = 'ForwardCenterLEX'

	BEGIN

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobTypeId, @JobId, FR.EID, FR.LC, FR.PX, FR.PY, FR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM ForwardStaticRods AS FR 
			INNER JOIN StaticMoments AS SM ON FR.EID = SM.EID AND FR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND FR.EID=@ElementId)
			OR (FR.EID=@ElementId AND FR.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobTypeId, @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM FullFatigueRods AS FFR 
			INNER JOIN FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobTypeId, @JobId, CR.EID, CR.LC, CR.PX, CR.PY, CR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM CenterStaticRods AS CR
			INNER JOIN StaticMoments AS SM ON CR.EID = SM.EID AND CR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND CR.EID=@ElementId)
			OR (CR.EID=@ElementId AND CR.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobTypeId, @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM FullFatigueRods AS FFR 
			INNER JOIN FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)
		
		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticPx, StaticPy, StaticPz, StaticNode, StaticMx, StaticMy, staticMz)
		SELECT @JobTypeId, @JobId, LR.EID, LR.LC, LR.PX, LR.PY, LR.PZ, SM.Node, SM.Mx, SM.My, SM.Mz
		FROM LEXStaticRods AS LR
			INNER JOIN StaticMoments AS SM ON LR.EID = SM.EID AND LR.LC = SM.LC
		WHERE 
			(@StaticLoadCase=0 AND LR.EID=@ElementId)
			OR (LR.EID=@ElementId AND LR.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatiguePx, FatiguePy, FatiguePz, FatigueNode, FatigueMx, FatigueMy, FatigueMz)
		SELECT @JobTypeId, @JobId, FFR.EID, FFR.LC, FFR.PX, FFR.PY, FFR.PZ, FM.Node, FM.Mx, FM.My, FM.Mz
		FROM FullFatigueRods AS FFR 
			INNER JOIN FatigueMoments AS FM ON FFR.EID = FM.EID AND FFR.LC = FM.LC
		WHERE 
			(@FatigueLoadCase=0 AND FFR.EID=@ElementId)
			OR (FFR.EID=@ElementId AND FFR.LC=@FatigueLoadCase)

	END

END

ELSE IF @RodBarOrShell = 'Shell'

BEGIN

	IF @Location = 'Aft'

	BEGIN

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobTypeId, @JobId, AFS.EID, AFS.LC, AFS.Fx, AFS.Fy, AFS.Fxy
		FROM AftStaticShells AS AFS 
		WHERE 
			(@StaticLoadCase=0 AND AFS.EID=@ElementId)
			OR (AFS.EID=@ElementId AND AFS.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobTypeId, @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

	END

	ELSE IF @Location = 'Center'

	BEGIN

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobTypeId, @JobId, CS.EID, CS.LC, CS.Fx, CS.Fy, CS.Fxy
		FROM CenterStaticShells AS CS 
		WHERE 
			(@StaticLoadCase=0 AND CS.EID=@ElementId)
			OR (CS.EID=@ElementId AND CS.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobTypeId, @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'Forward'

	BEGIN

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobTypeId, @JobId, FS.EID, FS.LC, FS.Fx, FS.Fy, FS.Fxy
		FROM ForwardStaticShells AS FS 
		WHERE 
			(@StaticLoadCase=0 AND FS.EID=@ElementId)
			OR (FS.EID=@ElementId AND FS.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobTypeId, @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

	END

	ELSE IF @Location = 'LEX'

	BEGIN

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobTypeId, @JobId, LS.EID, LS.LC, LS.Fx, LS.Fy, LS.Fxy
		FROM LEXStaticShells AS LS 
		WHERE 
			(@StaticLoadCase=0 AND LS.EID=@ElementId)
			OR (LS.EID=@ElementId AND LS.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobTypeId, @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

	END

	ELSE IF @Location = 'NoseBarrel'

	BEGIN

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobTypeId, @JobId, NBS.EID, NBS.LC, NBS.Fx, NBS.Fy, NBS.Fxy
		FROM NoseBarrelStaticShells AS NBS 
		WHERE 
			(@StaticLoadCase=0 AND NBS.EID=@ElementId)
			OR (NBS.EID=@ElementId AND NBS.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobTypeId, @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

	END

	ELSE IF @Location = 'Wing'

	BEGIN

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobTypeId, @JobId, WS.EID, WS.LC, WS.Fx, WS.Fy, WS.Fxy
		FROM WingStaticShellLoads AS WS 
		WHERE 
			(@StaticLoadCase=0 AND WS.EID=@ElementId)
			OR (WS.EID=@ElementId AND WS.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobTypeId, @JobId, WFS.EID, WFS.LC, WFS.Fx, WFS.Fy, WFS.Fxy
		FROM WingFatigueShellLoads AS WFS 
		WHERE 
			(@FatigueLoadCase=0 AND WFS.EID=@ElementId)
			OR (WFS.EID=@ElementId AND WFS.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'ForwardCenter'

	BEGIN

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobTypeId, @JobId, FS.EID, FS.LC, FS.Fx, FS.Fy, FS.Fxy
		FROM ForwardStaticShells AS FS 
		WHERE 
			(@StaticLoadCase=0 AND FS.EID=@ElementId)
			OR (FS.EID=@ElementId AND FS.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobTypeId, @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)
	
		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobTypeId, @JobId, CS.EID, CS.LC, CS.Fx, CS.Fy, CS.Fxy
		FROM CenterStaticShells AS CS 
		WHERE 
			(@StaticLoadCase=0 AND CS.EID=@ElementId)
			OR (CS.EID=@ElementId AND CS.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobTypeId, @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'ForwardNoseBarrel'

	BEGIN

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobTypeId, @JobId, FS.EID, FS.LC, FS.Fx, FS.Fy, FS.Fxy
		FROM ForwardStaticShells AS FS 
		WHERE 
			(@StaticLoadCase=0 AND FS.EID=@ElementId)
			OR (FS.EID=@ElementId AND FS.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobTypeId, @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobTypeId, @JobId, NBS.EID, NBS.LC, NBS.Fx, NBS.Fy, NBS.Fxy
		FROM NoseBarrelStaticShells AS NBS 
		WHERE 
			(@StaticLoadCase=0 AND NBS.EID=@ElementId)
			OR (NBS.EID=@ElementId AND NBS.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobTypeId, @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'ForwardLEX'

	BEGIN

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobTypeId, @JobId, FS.EID, FS.LC, FS.Fx, FS.Fy, FS.Fxy
		FROM ForwardStaticShells AS FS 
		WHERE 
			(@StaticLoadCase=0 AND FS.EID=@ElementId)
			OR (FS.EID=@ElementId AND FS.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobTypeId, @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobTypeId, @JobId, LS.EID, LS.LC, LS.Fx, LS.Fy, LS.Fxy
		FROM LEXStaticShells AS LS 
		WHERE 
			(@StaticLoadCase=0 AND LS.EID=@ElementId)
			OR (LS.EID=@ElementId AND LS.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobTypeId, @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'CenterLEX'

	BEGIN

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobTypeId, @JobId, CS.EID, CS.LC, CS.Fx, CS.Fy, CS.Fxy
		FROM CenterStaticShells AS CS 
		WHERE 
			(@StaticLoadCase=0 AND CS.EID=@ElementId)
			OR (CS.EID=@ElementId AND CS.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobTypeId, @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobTypeId, @JobId, LS.EID, LS.LC, LS.Fx, LS.Fy, LS.Fxy
		FROM LEXStaticShells AS LS 
		WHERE 
			(@StaticLoadCase=0 AND LS.EID=@ElementId)
			OR (LS.EID=@ElementId AND LS.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobTypeId, @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'CenterForwardNoseBarrel'

	BEGIN

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobTypeId, @JobId, CS.EID, CS.LC, CS.Fx, CS.Fy, CS.Fxy
		FROM CenterStaticShells AS CS 
		WHERE 
			(@StaticLoadCase=0 AND CS.EID=@ElementId)
			OR (CS.EID=@ElementId AND CS.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobTypeId, @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)
	

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobTypeId, @JobId, FS.EID, FS.LC, FS.Fx, FS.Fy, FS.Fxy
		FROM ForwardStaticShells AS FS 
		WHERE 
			(@StaticLoadCase=0 AND FS.EID=@ElementId)
			OR (FS.EID=@ElementId AND FS.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobTypeId, @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobTypeId, @JobId, NBS.EID, NBS.LC, NBS.Fx, NBS.Fy, NBS.Fxy
		FROM NoseBarrelStaticShells AS NBS 
		WHERE 
			(@StaticLoadCase=0 AND NBS.EID=@ElementId)
			OR (NBS.EID=@ElementId AND NBS.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobTypeId, @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)
	
	END

	ELSE IF @Location = 'ForwardNoseBarrelLEX'

	BEGIN

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobTypeId, @JobId, FS.EID, FS.LC, FS.Fx, FS.Fy, FS.Fxy
		FROM ForwardStaticShells AS FS 
		WHERE 
			(@StaticLoadCase=0 AND FS.EID=@ElementId)
			OR (FS.EID=@ElementId AND FS.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobTypeId, @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobTypeId, @JobId, NBS.EID, NBS.LC, NBS.Fx, NBS.Fy, NBS.Fxy
		FROM NoseBarrelStaticShells AS NBS 
		WHERE 
			(@StaticLoadCase=0 AND NBS.EID=@ElementId)
			OR (NBS.EID=@ElementId AND NBS.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobTypeId, @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobTypeId, @JobId, LS.EID, LS.LC, LS.Fx, LS.Fy, LS.Fxy
		FROM LEXStaticShells AS LS 
		WHERE 
			(@StaticLoadCase=0 AND LS.EID=@ElementId)
			OR (LS.EID=@ElementId AND LS.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobTypeId, @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

	END

	ELSE IF @Location = 'ForwardCenterLEX'

	BEGIN

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobTypeId, @JobId, FS.EID, FS.LC, FS.Fx, FS.Fy, FS.Fxy
		FROM ForwardStaticShells AS FS 
		WHERE 
			(@StaticLoadCase=0 AND FS.EID=@ElementId)
			OR (FS.EID=@ElementId AND FS.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobTypeId, @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobTypeId, @JobId, CS.EID, CS.LC, CS.Fx, CS.Fy, CS.Fxy
		FROM CenterStaticShells AS CS 
		WHERE 
			(@StaticLoadCase=0 AND CS.EID=@ElementId)
			OR (CS.EID=@ElementId AND CS.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobTypeId, @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

		INSERT INTO StaticOutputLoadData(JobTypeId, JobId, EID, StaticLc, StaticFx, StaticFy, StaticFxy)
		SELECT @JobTypeId, @JobId, LS.EID, LS.LC, LS.Fx, LS.Fy, LS.Fxy
		FROM LEXStaticShells AS LS 
		WHERE 
			(@StaticLoadCase=0 AND LS.EID=@ElementId)
			OR (LS.EID=@ElementId AND LS.LC=@StaticLoadCase)
		INSERT INTO FatigueOutputLoadData(JobTypeId, JobId, EID, FatigueLc, FatigueFx, FatigueFy, FatigueFxy)
		SELECT @JobTypeId, @JobId, FFS.EID, FFS.LC, FFS.Fx, FFS.Fy, FFS.Fxy
		FROM FullFatigueShells AS FFS 
		WHERE 
			(@FatigueLoadCase=0 AND FFS.EID=@ElementId)
			OR (FFS.EID=@ElementId AND FFS.LC=@FatigueLoadCase)

	END

END

SELECT * FROM StaticOutputLoadData
SELECT * FROM FatigueOutputLoadData

