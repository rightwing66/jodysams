USE [ISAMS]
GO
ALTER TABLE [dbo].[FatigueLoadData] DROP CONSTRAINT [FK_FatigueLoadData_JobType]
GO
ALTER TABLE [dbo].[FatigueLoadData] DROP CONSTRAINT [FK_FatigueLoadData_Job]
GO
DROP TABLE [dbo].[FatigueLoadData]
GO



CREATE TABLE [dbo].[FatigueLoadData](
	[FatigueLoadDataId] [int] NOT NULL,
	[JobTypeId] [int] NOT NULL,
	[JobId] [int] NOT NULL,
 CONSTRAINT [PK_FatigueLoadData] PRIMARY KEY CLUSTERED 
(
	[FatigueLoadDataId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]





GO
ALTER TABLE [dbo].[FatigueLoadData]  WITH CHECK ADD  CONSTRAINT [FK_FatigueLoadData_Job] FOREIGN KEY([JobId])
REFERENCES [dbo].[Job] ([JobId])
GO
ALTER TABLE [dbo].[FatigueLoadData] CHECK CONSTRAINT [FK_FatigueLoadData_Job]
GO
ALTER TABLE [dbo].[FatigueLoadData]  WITH CHECK ADD  CONSTRAINT [FK_FatigueLoadData_JobType] FOREIGN KEY([JobTypeId])
REFERENCES [dbo].[JobType] ([JobTypeId])
GO
ALTER TABLE [dbo].[FatigueLoadData] CHECK CONSTRAINT [FK_FatigueLoadData_JobType]
GO
