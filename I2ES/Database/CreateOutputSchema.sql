USE ISAMS

ALTER TABLE Results.ResultFile DROP CONSTRAINT FK_ResultFile_SolverOutput
go
alter table Results.extraction drop constraint FK_Extraction_SolverOutput
go
Alter Table Results.extraction drop constraint FK_Extraction_ResultType
go
Alter Table Results.ResultField drop constraint FK_ResultField_Extraction
go
Alter Table Results.SolverOutput drop constraint FK_SolverOutput_Job

go
Drop Table Results.ResultType
go
DROP TABLE Results.SolverOutput
go
Drop Table Results.ResultFile
go
Drop Table Results.Extraction
go
Drop Table Results.ResultField
go
DROP SCHEMA Results
go

CREATE SCHEMA Results AUTHORIZATION IsamsApp



CREATE TABLE Results.SolverOutput(
	SolverOutputId int NOT NULL,
	SovlerId varchar(50) NULL,
	CompletionTime datetime NULL,
	JobId int not null,
	Status int not null,
 CONSTRAINT PK_SolverOutput PRIMARY KEY CLUSTERED (	SolverOutputId ASC)ON [PRIMARY]
) ON [PRIMARY]

Create Table Results.ResultFile(
	ResultFileId int not null,
	SolverOutputId int not null,
	FilePath varchar(max),
	Constraint PK_ResultFile primary key clustered (ResultFileId ASC) on [primary]
)on [primary]

Create Table Results.Extraction(
	ExtractionId int not null,
	SolverOutputId int not null,
	ResultTypeId int not null,
	Name		 Varchar(50) null,
	Constraint PK_Extraction primary key clustered (ExtractionId ASC) on [primary]
) on [primary]

Create Table Results.ResultType(
	ResultTypeId int not null,
	Name varchar(50) not null,
	Constraint PK_ResultType primary key clustered (ResultTypeId ASC) on [primary]
) on [primary]

Create Table Results.ResultField(
	ResultFieledId int not null,
	ExtractionId int not null,
	Name varchar(50) not null,
	[Value] decimal,
	Constraint PK_ResultField primary key clustered (ResultFieledId ASC) on [primary]
)on [primary]

go
ALTER TABLE Results.SolverOutput  WITH CHECK ADD  CONSTRAINT FK_SolverOutput_Job FOREIGN KEY(JobId)
REFERENCES dbo.Job (JobId)
ALTER TABLE Results.SolverOutput CHECK CONSTRAINT FK_SolverOutput_Job

go
ALTER TABLE Results.ResultFile  WITH CHECK ADD  CONSTRAINT FK_ResultFile_SolverOutput FOREIGN KEY(SolverOutputId)
REFERENCES Results.SolverOutput (SolverOutputId)
ALTER TABLE Results.ResultFile CHECK CONSTRAINT FK_ResultFile_SolverOutput

ALTER TABLE [Results].[Extraction]  WITH CHECK ADD  CONSTRAINT [FK_Extraction_SolverOutput] FOREIGN KEY([SolverOutputId])
REFERENCES [Results].[SolverOutput] ([SolverOutputId])
ALTER TABLE [Results].[Extraction] CHECK CONSTRAINT [FK_Extraction_SolverOutput]
GO

ALTER TABLE [Results].[Extraction]  WITH CHECK ADD  CONSTRAINT [FK_Extraction_ResultType] FOREIGN KEY(ResultTypeId)
REFERENCES Results.ResultType (ResultTypeId)
ALTER TABLE Results.Extraction CHECK CONSTRAINT FK_Extraction_ResultType
GO

ALTER TABLE Results.ResultField  WITH CHECK ADD  CONSTRAINT FK_ResultField_Extraction FOREIGN KEY(ExtractionId)
REFERENCES Results.Extraction (ExtractionId)
ALTER TABLE Results.ResultField CHECK CONSTRAINT FK_ResultField_Extraction
GO


