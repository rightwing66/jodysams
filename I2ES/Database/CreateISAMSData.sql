USE [ISAMS]
GO
INSERT [dbo].[JobState] ([JobStateId], [Name]) VALUES (0, N'Uncreated')
INSERT [dbo].[JobState] ([JobStateId], [Name]) VALUES (1, N'Created')
INSERT [dbo].[JobState] ([JobStateId], [Name]) VALUES (2, N'WaitingOnSolver')
INSERT [dbo].[JobState] ([JobStateId], [Name]) VALUES (3, N'WaitingOnStoreData')
INSERT [dbo].[JobState] ([JobStateId], [Name]) VALUES (4, N'WaitingOnTrueNumbers')
INSERT [dbo].[JobState] ([JobStateId], [Name]) VALUES (5, N'Complete')
INSERT [dbo].[JobState] ([JobStateId], [Name]) VALUES (6, N'Failed')
SET IDENTITY_INSERT [dbo].[Unit] ON 

INSERT [dbo].[Unit] ([UnitId], [Name], [Description], [CLRType]) VALUES (1, N'in', NULL, NULL)
INSERT [dbo].[Unit] ([UnitId], [Name], [Description], [CLRType]) VALUES (2, N'lbf', NULL, NULL)
INSERT [dbo].[Unit] ([UnitId], [Name], [Description], [CLRType]) VALUES (3, N'psi', NULL, NULL)
INSERT [dbo].[Unit] ([UnitId], [Name], [Description], [CLRType]) VALUES (4, N'null', NULL, NULL)
INSERT [dbo].[Unit] ([UnitId], [Name], [Description], [CLRType]) VALUES (5, N'lb', NULL, NULL)
INSERT [dbo].[Unit] ([UnitId], [Name], [Description], [CLRType]) VALUES (6, N'lb/in^{2}', NULL, NULL)
SET IDENTITY_INSERT [dbo].[Unit] OFF
