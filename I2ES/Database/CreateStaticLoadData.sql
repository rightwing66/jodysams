USE [ISAMS]
GO
ALTER TABLE [dbo].[StaticLoadData] DROP CONSTRAINT [FK_StaticLoadData_JobType]
ALTER TABLE [dbo].[StaticLoadData] DROP CONSTRAINT [FK_StaticLoadData_Job]
DROP TABLE [dbo].[StaticLoadData]
GO


CREATE TABLE [dbo].[StaticLoadData](
	[StaticLoadDataId] [int] NOT NULL,
	[JobTypeId] [int] NOT NULL,
	[JobId] [int] NOT NULL,
 CONSTRAINT [PK_StaticLoadData] PRIMARY KEY CLUSTERED 
(
	[StaticLoadDataId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



GO
ALTER TABLE [dbo].[StaticLoadData]  WITH CHECK ADD  CONSTRAINT [FK_StaticLoadData_Job] FOREIGN KEY([JobId])
REFERENCES [dbo].[Job] ([JobId])
GO
ALTER TABLE [dbo].[StaticLoadData] CHECK CONSTRAINT [FK_StaticLoadData_Job]
GO
ALTER TABLE [dbo].[StaticLoadData]  WITH CHECK ADD  CONSTRAINT [FK_StaticLoadData_JobType] FOREIGN KEY([JobTypeId])
REFERENCES [dbo].[JobType] ([JobTypeId])
GO
ALTER TABLE [dbo].[StaticLoadData] CHECK CONSTRAINT [FK_StaticLoadData_JobType]
GO
