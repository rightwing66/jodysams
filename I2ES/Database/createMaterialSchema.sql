if object_id('material.Form', 'U') IS NOT NULL
  drop table material.Form; 

if object_id('material.Alloy', 'U') IS NOT NULL
  drop table material.Alloy; 

if object_id('material.MaterialName', 'U') IS NOT NULL
  drop table material.MaterialName; 


 
create table Material.MaterialName(
	MaterialNameID int not null primary key identity(1,1),
	Name varchar(50)
 )
 
insert Material.MaterialName (Name)
select distinct Material 
from Materialxls

create table Material.Alloy(
	AlloyID int not null primary key identity(1,1),
	MaterialNameId int foreign key references Material.MaterialName(MaterialNameID),
	Name varchar(50)
 )

 insert Material.Alloy(MaterialNameId, Name)
 select distinct MaterialNameId, Alloy 
 from Materialxls, Material.MaterialName
 where Materialxls.Material = Material.MaterialName.Name
 
 select * from Material.Alloy 


