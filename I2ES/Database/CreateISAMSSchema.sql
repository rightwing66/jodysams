USE [ISAMS]
GO
/****** Object:  Schema [Results]    Script Date: 12/2/2015 12:17:08 PM ******/
CREATE SCHEMA [Results]
GO
/****** Object:  Table [dbo].[Access]    Script Date: 12/2/2015 12:17:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Access](
	[AccessId] [int] IDENTITY(1,1) NOT NULL,
	[AccessDate] [datetime] NOT NULL,
	[UserId] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Access] PRIMARY KEY CLUSTERED 
(
	[AccessId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AircraftModel]    Script Date: 12/2/2015 12:17:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AircraftModel](
	[AircraftModelId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_AircraftModel] PRIMARY KEY CLUSTERED 
(
	[AircraftModelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BackEndProcessState]    Script Date: 12/2/2015 12:17:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BackEndProcessState](
	[BackEndProcessStateId] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GoodJobTypeIds]    Script Date: 12/2/2015 12:17:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GoodJobTypeIds](
	[goodId] [int] IDENTITY(1,1) NOT NULL,
	[id] [int] NOT NULL,
 CONSTRAINT [PK_GoodJobTypeIds] PRIMARY KEY CLUSTERED 
(
	[goodId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Job]    Script Date: 12/2/2015 12:17:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Job](
	[JobId] [int] IDENTITY(1,1) NOT NULL,
	[JobTypeId] [int] NULL,
	[UserId] [varchar](50) NOT NULL,
	[JobStateId] [int] NULL,
	[CompletionStage] [varchar](50) NULL,
	[CreateTime] [datetime] NULL,
	[ProcessBeginTime] [datetime] NULL,
	[ProcessEndTime] [datetime] NULL,
	[IsTest] [bit] NULL,
	[SubmittingNode] [nvarchar](50) NULL,
	[SolverStateId] [int] NULL,
	[SolverNode] [nvarchar](50) NULL,
	[SolverBeginTime] [datetime] NULL,
	[SolverEndTime] [datetime] NULL,
	[SolverMessage] [varchar](max) NULL,
	[LoadDataRetreiverStateId] [int] NULL,
	[LoadDataRetreiverBeginTime] [datetime] NULL,
	[LoadDataRetreiverEndTime] [datetime] NULL,
	[LoadDataRetrieverMessage] [nvarchar](max) NULL,
	[StoreResultsStateId] [int] NULL,
	[StoreResultsBeginTime] [datetime] NULL,
	[StoreResultsEndTime] [datetime] NULL,
	[StoreResultsMessage] [nvarchar](max) NULL,
	[TrueNumberStateId] [int] NULL,
	[TrueNumberBeginTime] [datetime] NULL,
	[TrueNumberEndTime] [datetime] NULL,
	[TrueNumberMessage] [nvarchar](max) NULL,
	[SolverOutcome] [int] NULL,
	[REINumber] [nvarchar](50) NULL,
	[BUNO] [nvarchar](50) NULL,
	[PartNumber] [nvarchar](50) NULL,
	[StructureTypeId] [int] NULL,
	[AircraftModelId] [int] NULL,
	[Report] [nvarchar](300) NULL,
	[EditTime] [datetime] NULL,
 CONSTRAINT [PK_Job] PRIMARY KEY CLUSTERED 
(
	[JobId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[JobResults]    Script Date: 12/2/2015 12:17:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JobResults](
	[ResultId] [int] NOT NULL,
	[StoragePath] [varchar](max) NULL,
 CONSTRAINT [PK_JobResults] PRIMARY KEY CLUSTERED 
(
	[ResultId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[JobState]    Script Date: 12/2/2015 12:17:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JobState](
	[JobStateId] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_JobState] PRIMARY KEY CLUSTERED 
(
	[JobStateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[JobType]    Script Date: 12/2/2015 12:17:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JobType](
	[JobTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](max) NOT NULL,
	[Description] [varchar](max) NULL,
	[InputSchema] [varchar](max) NULL,
	[OutputSchema] [varchar](max) NULL,
	[ImagePath] [varchar](max) NULL,
 CONSTRAINT [PK_JobType] PRIMARY KEY CLUSTERED 
(
	[JobTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Materialxls]    Script Date: 12/2/2015 12:17:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Materialxls](
	[Material] [nvarchar](255) NULL,
	[Alloy] [nvarchar](255) NULL,
	[Form] [nvarchar](255) NULL,
	[Temper] [nvarchar](255) NULL,
	[ThicknessMin] [float] NULL,
	[ThicknessMax] [float] NULL,
	[Spec] [nvarchar](255) NULL,
	[MMPDS pp] [nvarchar](255) NULL,
	[Area1] [float] NULL,
	[Condition] [nvarchar](255) NULL,
	[Width-Min] [float] NULL,
	[Width-Max] [float] NULL,
	[Ftu_L] [float] NULL,
	[Ftu_LT] [float] NULL,
	[Ftu_ST] [float] NULL,
	[Fty_L] [float] NULL,
	[Fty_LT] [float] NULL,
	[Fty_ST] [float] NULL,
	[Fcy_L] [float] NULL,
	[Fcy_LT] [float] NULL,
	[Fcy_ST] [float] NULL,
	[Fsu] [float] NULL,
	[Fbru15] [float] NULL,
	[Fbru20] [float] NULL,
	[Fbry15] [float] NULL,
	[Fbry20] [float] NULL,
	[Ee] [float] NULL,
	[E] [float] NULL,
	[Ec] [float] NULL,
	[G] [float] NULL,
	[μ] [float] NULL,
	[ρ] [float] NULL,
	[Ftu Min] [float] NULL,
	[Fty Min] [float] NULL,
	[Fcy Min] [float] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Parameter]    Script Date: 12/2/2015 12:17:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Parameter](
	[ParameterId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Label] [varchar](max) NULL,
	[Type] [varchar](max) NULL,
	[JobTypeId] [int] NULL,
	[UnitId] [int] NULL,
	[Value] [varchar](max) NULL,
	[Display] [varchar](50) NULL,
 CONSTRAINT [PK_Parameter] PRIMARY KEY CLUSTERED 
(
	[ParameterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ParameterGroup]    Script Date: 12/2/2015 12:17:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ParameterGroup](
	[ParameterGroupId] [int] IDENTITY(1,1) NOT NULL,
	[JobTypeId] [int] NOT NULL,
	[Name] [varchar](max) NOT NULL,
	[Description] [varchar](max) NULL,
	[StepId] [int] NULL,
 CONSTRAINT [PK_ParameterGroup] PRIMARY KEY CLUSTERED 
(
	[ParameterGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ParameterGroup_x_Parameter]    Script Date: 12/2/2015 12:17:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ParameterGroup_x_Parameter](
	[ParameterId] [int] NOT NULL,
	[ParameterGroupId] [int] NOT NULL,
 CONSTRAINT [PK_ParameterGroup_x_Parameter] PRIMARY KEY CLUSTERED 
(
	[ParameterId] ASC,
	[ParameterGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ParameterValue]    Script Date: 12/2/2015 12:17:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ParameterValue](
	[JobId] [int] NOT NULL,
	[ParameterId] [int] NOT NULL,
	[Value] [float] NULL,
 CONSTRAINT [PK_ParameterValue_1] PRIMARY KEY CLUSTERED 
(
	[JobId] ASC,
	[ParameterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SolverOutcomeState]    Script Date: 12/2/2015 12:17:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SolverOutcomeState](
	[SolverOutcomeStateId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_SolverOutcomeState] PRIMARY KEY CLUSTERED 
(
	[SolverOutcomeStateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Structure]    Script Date: 12/2/2015 12:17:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Structure](
	[StructureId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Structure] PRIMARY KEY CLUSTERED 
(
	[StructureId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Unit]    Script Date: 12/2/2015 12:17:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Unit](
	[UnitId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](max) NOT NULL,
	[Description] [varchar](max) NULL,
	[CLRType] [varchar](max) NULL,
 CONSTRAINT [PK_Unit] PRIMARY KEY CLUSTERED 
(
	[UnitId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Results].[Extraction]    Script Date: 12/2/2015 12:17:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Results].[Extraction](
	[ExtractionId] [int] IDENTITY(1,1) NOT NULL,
	[SolverOutputId] [int] NOT NULL,
	[ResultTypeId] [int] NOT NULL,
	[Name] [varchar](50) NULL,
 CONSTRAINT [PK_Extraction] PRIMARY KEY CLUSTERED 
(
	[ExtractionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Results].[ResultField]    Script Date: 12/2/2015 12:17:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Results].[ResultField](
	[ResultFieledId] [int] IDENTITY(1,1) NOT NULL,
	[ExtractionId] [int] NOT NULL,
	[Name] [varchar](max) NOT NULL,
	[IsNumber] [bit] NOT NULL,
	[NumericValue] [decimal](18, 0) NULL,
	[StringValue] [varchar](max) NULL,
	[Units] [varchar](50) NULL,
	[DataType] [varchar](50) NULL,
	[TrueNumber] [uniqueidentifier] NULL,
 CONSTRAINT [PK_ResultField] PRIMARY KEY CLUSTERED 
(
	[ResultFieledId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Results].[ResultFile]    Script Date: 12/2/2015 12:17:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Results].[ResultFile](
	[ResultFileId] [int] IDENTITY(1,1) NOT NULL,
	[SolverOutputId] [int] NOT NULL,
	[FileType] [varchar](50) NULL,
	[FilePath] [varchar](max) NULL,
	[TrueNumber] [uniqueidentifier] NULL,
	[FileBinary] [varbinary](max) NULL,
 CONSTRAINT [PK_ResultFile] PRIMARY KEY CLUSTERED 
(
	[ResultFileId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Results].[ResultType]    Script Date: 12/2/2015 12:17:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Results].[ResultType](
	[ResultTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ResultType] PRIMARY KEY CLUSTERED 
(
	[ResultTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Results].[SolverOutput]    Script Date: 12/2/2015 12:17:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Results].[SolverOutput](
	[SolverOutputId] [int] IDENTITY(1,1) NOT NULL,
	[SolverId] [varchar](50) NULL,
	[CompletionTime] [datetime] NULL,
	[JobId] [int] NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_SolverOutput] PRIMARY KEY CLUSTERED 
(
	[SolverOutputId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Job]  WITH CHECK ADD  CONSTRAINT [FK_Job_JobState] FOREIGN KEY([JobStateId])
REFERENCES [dbo].[JobState] ([JobStateId])
GO
ALTER TABLE [dbo].[Job] CHECK CONSTRAINT [FK_Job_JobState]
GO
ALTER TABLE [dbo].[Job]  WITH CHECK ADD  CONSTRAINT [FK_Job_JobType] FOREIGN KEY([JobTypeId])
REFERENCES [dbo].[JobType] ([JobTypeId])
GO
ALTER TABLE [dbo].[Job] CHECK CONSTRAINT [FK_Job_JobType]
GO
ALTER TABLE [dbo].[Job]  WITH CHECK ADD  CONSTRAINT [FK_Job_SolverOutcomeState] FOREIGN KEY([SolverOutcome])
REFERENCES [dbo].[SolverOutcomeState] ([SolverOutcomeStateId])
GO
ALTER TABLE [dbo].[Job] CHECK CONSTRAINT [FK_Job_SolverOutcomeState]
GO
ALTER TABLE [dbo].[Parameter]  WITH CHECK ADD  CONSTRAINT [FK_Parameter_JobType] FOREIGN KEY([JobTypeId])
REFERENCES [dbo].[JobType] ([JobTypeId])
GO
ALTER TABLE [dbo].[Parameter] CHECK CONSTRAINT [FK_Parameter_JobType]
GO
ALTER TABLE [dbo].[Parameter]  WITH CHECK ADD  CONSTRAINT [FK_Parameter_Unit] FOREIGN KEY([UnitId])
REFERENCES [dbo].[Unit] ([UnitId])
GO
ALTER TABLE [dbo].[Parameter] CHECK CONSTRAINT [FK_Parameter_Unit]
GO
ALTER TABLE [dbo].[ParameterGroup]  WITH CHECK ADD  CONSTRAINT [FK_ParameterGroup_JobType] FOREIGN KEY([JobTypeId])
REFERENCES [dbo].[JobType] ([JobTypeId])
GO
ALTER TABLE [dbo].[ParameterGroup] CHECK CONSTRAINT [FK_ParameterGroup_JobType]
GO
ALTER TABLE [dbo].[ParameterGroup_x_Parameter]  WITH CHECK ADD  CONSTRAINT [FK_ParameterGroup_x_Parameter_Parameter] FOREIGN KEY([ParameterId])
REFERENCES [dbo].[Parameter] ([ParameterId])
GO
ALTER TABLE [dbo].[ParameterGroup_x_Parameter] CHECK CONSTRAINT [FK_ParameterGroup_x_Parameter_Parameter]
GO
ALTER TABLE [dbo].[ParameterGroup_x_Parameter]  WITH CHECK ADD  CONSTRAINT [FK_ParameterGroup_x_Parameter_ParameterGroup] FOREIGN KEY([ParameterGroupId])
REFERENCES [dbo].[ParameterGroup] ([ParameterGroupId])
GO
ALTER TABLE [dbo].[ParameterGroup_x_Parameter] CHECK CONSTRAINT [FK_ParameterGroup_x_Parameter_ParameterGroup]
GO
ALTER TABLE [dbo].[ParameterValue]  WITH CHECK ADD  CONSTRAINT [FK_ParameterValue_Job] FOREIGN KEY([JobId])
REFERENCES [dbo].[Job] ([JobId])
GO
ALTER TABLE [dbo].[ParameterValue] CHECK CONSTRAINT [FK_ParameterValue_Job]
GO
ALTER TABLE [dbo].[ParameterValue]  WITH CHECK ADD  CONSTRAINT [FK_ParameterValue_Parameter] FOREIGN KEY([ParameterId])
REFERENCES [dbo].[Parameter] ([ParameterId])
GO
ALTER TABLE [dbo].[ParameterValue] CHECK CONSTRAINT [FK_ParameterValue_Parameter]
GO
ALTER TABLE [Results].[Extraction]  WITH CHECK ADD  CONSTRAINT [FK_Extraction_ResultType] FOREIGN KEY([ResultTypeId])
REFERENCES [Results].[ResultType] ([ResultTypeId])
GO
ALTER TABLE [Results].[Extraction] CHECK CONSTRAINT [FK_Extraction_ResultType]
GO
ALTER TABLE [Results].[Extraction]  WITH CHECK ADD  CONSTRAINT [FK_Extraction_SolverOutput] FOREIGN KEY([SolverOutputId])
REFERENCES [Results].[SolverOutput] ([SolverOutputId])
GO
ALTER TABLE [Results].[Extraction] CHECK CONSTRAINT [FK_Extraction_SolverOutput]
GO
ALTER TABLE [Results].[ResultField]  WITH CHECK ADD  CONSTRAINT [FK_ResultField_Extraction] FOREIGN KEY([ExtractionId])
REFERENCES [Results].[Extraction] ([ExtractionId])
GO
ALTER TABLE [Results].[ResultField] CHECK CONSTRAINT [FK_ResultField_Extraction]
GO
ALTER TABLE [Results].[ResultFile]  WITH CHECK ADD  CONSTRAINT [FK_ResultFile_SolverOutput] FOREIGN KEY([SolverOutputId])
REFERENCES [Results].[SolverOutput] ([SolverOutputId])
GO
ALTER TABLE [Results].[ResultFile] CHECK CONSTRAINT [FK_ResultFile_SolverOutput]
GO
ALTER TABLE [Results].[SolverOutput]  WITH CHECK ADD  CONSTRAINT [FK_SolverOutput_Job] FOREIGN KEY([JobId])
REFERENCES [dbo].[Job] ([JobId])
GO
ALTER TABLE [Results].[SolverOutput] CHECK CONSTRAINT [FK_SolverOutput_Job]
GO
