﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using SolverNode;

namespace I2ES.SolverNode {
    public class MvcApplication : System.Web.HttpApplication {
        private static readonly log4net.ILog log =
log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Application_Start() {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            Logging logging = new Logging();
            logging.WireUpLogging();
        }
        void Application_Error(object sender, EventArgs e) {
            Exception exc = Server.GetLastError();
            log.Error(exc);

        }
    }
}
