﻿using System;
using System.IO;
using System.Xml.Linq;
using I2ES.Model;
using I2ES.Model.DomainModel;
using I2ES.Model.Xml;

namespace I2ES.SolverNode {
    public class StoreResults {
        private readonly ISqlWrapper sqlWrapper;
        private static readonly log4net.ILog Log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public enum CompletionStateEnum {
            Failure,
            Success
        }

        public StoreResults(ISqlWrapper sqlWrapper) {
            this.sqlWrapper = sqlWrapper;
        }

        public void Start(int jobId, string SolverOutputFolderPath, Action<int> completeCallback ) {
            string solverOutputFolderPath = SolverOutputFolderPath;
            string outputfilePath = Path.Combine(solverOutputFolderPath, "output.xml");
            StoreData(solverOutputFolderPath, outputfilePath,  jobId);
            completeCallback.Invoke(jobId);
        }
        public int StoreData(string solverOutputFolderPath, string outputFilePath,  int jobId) {
            try {
                //todo:Remove the image put
//                string i2esImagePath = getImagePath(jobId);
                Log.DebugFormat("Begin jobId:{0} outputPath{1} ", jobId, solverOutputFolderPath);
                var outputDocument = XDocument.Load(outputFilePath);
                var outputXml = outputDocument.ToString();
                var outputIndex = outputFilePath.LastIndexOf("\\", StringComparison.Ordinal);
                var outputFileNameLength = outputFilePath.LastIndexOf(".", StringComparison.Ordinal) - outputIndex;

//                var inputXmlDatabaseWriter = new ModelLoadXmlDatabaseWriter(sqlWrapper);
                var stressCheckOutputXmlDatabaseWriter = new StressCheckOutputXmlDatabaseWriter(sqlWrapper);
                Log.Debug("Calling ParseOutputXmlFile");
                stressCheckOutputXmlDatabaseWriter.ParseOutputXmlFile(solverOutputFolderPath,  outputXml, jobId);
                Log.Debug("ParseOutputXmlFile complete");
                return jobId;
            }
            catch (Exception e) {
                Log.Debug(e);
                throw;
            }
        }

//        private string getImagePath(int jobId) {
//            IConfigurationWrapper wrapper = new ConfigurationWrapper();
//            string sharePath = wrapper.I2ESImagePath;
//            string finalPath = Path.Combine(sharePath, "jobImages");
//            string finalpath = Path.Combine(finalPath, "job" + jobId);
//            var imageDirectoryInfo = new DirectoryInfo(finalpath);
//            Log.DebugFormat("Createing image put path {0}", finalpath);
//            imageDirectoryInfo.Create();
//            return finalpath;
//        }

        private void throwException() {
            throw new Exception("StoreResults Failed");
        }


        public struct StoreDataThreadParameter {
            public ISAMSModel Model { get; set; }
            public string SolverOutputFolderPath { get; set; }
            public ISqlWrapper SqlWrapper { get; set; }
            public Action<int> NotifyCallback { get; set; }
            public int JobId { get; set; }
        }

        public CompletionStateEnum CompletionState { get; set; }
    }
}
