﻿using System;
using System.Configuration;
using System.Net;
using System.Threading;
using System.Web.Mvc;
using I2ES.Model;
using I2ES.Model.DomainModel;
using I2ES.Model.DomainModelContext;

namespace I2ES.SolverNode.Controllers {
    public class SolverController : Controller {
        private static readonly log4net.ILog Log =
                        log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        readonly ConfigurationWrapper wrapper = new ConfigurationWrapper();

        public ActionResult Submit(int id) {
            try {
                StartSolverThread(id);
                return Json("Job Submitted to Solver", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e) {
                return new HttpStatusCodeResult(500,e.Message);
            }
        }

        public Thread StartSolverThread(int jobId) {
            var t = new Thread(() => startSolver(jobId));
            t.Start();
            return t;
        }

        private void startSolver(int jobId) {
            try {
                Log.Info("Submitting to solver jobId:" + jobId);
                ConnectionProvider provider = new ConnectionProvider();
                var launcher = new SolverLauncher.SolverLauncher(provider.CreateWrapper(), wrapper);
                var solverOutputFolderPath = launcher.LaunchStressCheckSolver(jobId);
                Log.InfoFormat("JobId:{0} Notify Solver Complete. ", jobId);

                ISAMSModel model = new ISAMSModel(provider, new JobReader {SqlWrapper = provider.CreateWrapper()},
                    new JobTypeReader {SqlWrapper = provider.CreateWrapper()}, new JobWriter {SqlWrapper = provider.CreateWrapper()});
                var job = model.LoadJob(jobId);
                job.SolverState = BackEndProcessState.Complete;
                job.JobState = JobState.SolverProcessing;
                job.SolverEndTime = DateTime.Now;
                model.SaveChanges();

                StoreResults storeResults = new StoreResults(provider.CreateWrapper());
                storeResults.Start(jobId, solverOutputFolderPath, notifyComplete);
            }
            catch (Exception e) {
                NotifyFail(e, jobId);
            }
        }

        private void NotifyFail(Exception exception, int jobId) {
            Log.Error(exception);
            var client = new TrafficManager.TrafficManagerServiceClient();
            client.NotifySolverStoreDataFailed(jobId, System.Environment.MachineName, exception.Message + "\r\n" + exception.StackTrace + "\r\n" + exception.InnerException);
        }


        private void notifyComplete(int jobId) {
            try {
                var client = new TrafficManager.TrafficManagerServiceClient();
                string trafficManagerUrl = wrapper.TrafficManagerUrl;
                client.NotifyStoreResultsComplete(jobId, Environment.MachineName);
                Log.InfoFormat("JobId:{0} Notify store results Complete.  Calling TrafficManager at{1} ", jobId, trafficManagerUrl);
            }
            catch (Exception ex) {
                Log.Debug(ex);
            }
        }
    }
}
