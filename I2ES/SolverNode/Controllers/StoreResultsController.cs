﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using I2ES.Model;
using I2ES.Model.Xml;

namespace I2ES.SolverNode.Controllers {
    public class StoreResultsController : Controller {
        private static readonly log4net.ILog Log =
                        log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        readonly ConfigurationWrapper wrapper = new ConfigurationWrapper();

        public JsonResult StartStoreResults(int jobId, string solverOutputFolderPath) {
            Log.InfoFormat("JobId:{0} Starting StoreResults solverFolderOutputPath:{1}", jobId, solverOutputFolderPath );
            ConnectionProvider provider = new ConnectionProvider();
            
            
            StoreResults storeResults = new StoreResults(provider.CreateWrapper());
            storeResults.Start(jobId,solverOutputFolderPath, NotifyComplete);



            return Json("StoreResults Started", JsonRequestBehavior.AllowGet);
        }

        private void NotifyComplete(int jobId) {
            try {
                var client = new TrafficManager.TrafficManagerServiceClient();
                ConfigurationWrapper wrapper = new ConfigurationWrapper();
                string trafficManagerUrl = wrapper.TrafficManagerUrl;
                client.NotifyStoreResultsComplete(jobId);
                Log.InfoFormat("JobId:{0} Notify store results Complete.  Calling TrafficManager at{1} ", jobId, trafficManagerUrl);
            }
            catch (Exception ex) {
                Log.Debug(ex);
            }
        }

    }
}
