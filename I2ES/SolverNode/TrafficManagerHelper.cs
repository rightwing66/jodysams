﻿using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Web.Services.Description;
using I2ES.Model;
using I2ES.SolverNode.TrafficManager;

namespace I2ES.SolverNode {
    public static class TrafficManagerHelper {
        public static TrafficManagerServiceClient GetReference(string requestUrl=null) {
            ConfigurationWrapper wrapper = new ConfigurationWrapper();
            string url = requestUrl?? wrapper.TrafficManagerUrl;
            EndpointAddress address = new EndpointAddress(url);
            Binding binding = new BasicHttpBinding();
            TrafficManagerServiceClient trafficManager = new TrafficManagerServiceClient(binding, address);
            return trafficManager;
        }
    }
}