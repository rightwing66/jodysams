﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SolverNode.Startup))]
namespace SolverNode
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
