﻿using System.Collections.Generic;
using System.Linq;
using I2ES.Model.DomainModel;
using I2ES.Model.DomainModelContext;
using Moq;
using NUnit.Framework;

namespace I2ES.Model.UnitTest {
    [TestFixture]
    public class ParameterValueReaderTest {
        [Test]
        public void AssignsOneParameterValueToAParameter() {
            Mock<ISqlWrapper> sqlWrapperMock = new Mock<ISqlWrapper>();
            Mock<IParameterValueReader> parameterValueReaderMock = new Mock<IParameterValueReader>();
            Parameter parameter = new Parameter {DefaultValue = "16", Name = "Em", ParameterId = 13};
            JobType jobType = new JobType {JobTypeId = 13};
            Job job = new Job{JobId = 42};
            job.AllParameters.Add(parameter);
            jobType.ParameterGroups.Add(new ParameterGroup{JobType = jobType,Name = "Fnerkle", ParameterGroupId = 1, Parameters = new List<Parameter>{parameter}});
            List<ParameterValue> parameterValues = new List<ParameterValue> {
                new ParameterValue {JobId = 42, ParameterId = 13, Value = 69}
            };


            ParameterGroupReader parameterGroupReader = new ParameterGroupReader(sqlWrapperMock.Object,parameterValueReaderMock.Object);
            parameterGroupReader.LoadParameterValuesIntoParameters(jobType,job,parameterValues);


            Parameter jobParameter = job.AllParameters.FirstOrDefault(p => p.ParameterId == 13);
            Assert.That(jobParameter, Is.Not.Null);
            Assert.That(jobParameter.ParameterValue, Is.Not.Null, "ParameterValue was null");
            Assert.That(jobParameter.ParameterValue.Value,Is.EqualTo(69));
            Assert.That(jobType.ParameterGroups.FirstOrDefault().Parameters.FirstOrDefault().ParameterValue.Value,Is.EqualTo(69));

        }

        [Test]
        public void Assigns2ParameterValuesTo2parameters() {
            Mock<ISqlWrapper> sqlWrapperMock = new Mock<ISqlWrapper>();
            Mock<IParameterValueReader> parameterValueReaderMock = new Mock<IParameterValueReader>();
            Parameter parameter1 = new Parameter { DefaultValue = "16", Name = "Em", ParameterId = 13 };
            Parameter parameter2 = new Parameter { DefaultValue = "32", Name = "nu", ParameterId = 14 };
            JobType jobType = new JobType { JobTypeId = 13 };
            Job job = new Job { JobId = 42 };
            job.AllParameters.Add(parameter1);
            job.AllParameters.Add(parameter2);
            jobType.ParameterGroups.Add(new ParameterGroup { JobType = jobType, Name = "Fnerkle", ParameterGroupId = 1, Parameters = new List<Parameter> { parameter1,parameter2 } });
            List<ParameterValue> parameterValues = new List<ParameterValue> {
                new ParameterValue {JobId = 42, ParameterId = 13, Value = 69},
                new ParameterValue {JobId = 42, ParameterId = 14, Value = 52}
            };

            ParameterGroupReader parameterGroupReader = new ParameterGroupReader(sqlWrapperMock.Object, parameterValueReaderMock.Object);
            parameterGroupReader.LoadParameterValuesIntoParameters(jobType, job, parameterValues);


            Parameter jobParameter1 = job.AllParameters.FirstOrDefault(p => p.ParameterId == 13);
            Parameter jobParameter2 = job.AllParameters.FirstOrDefault(p => p.ParameterId == 14);
            Assert.That(jobParameter1.ParameterValue, Is.Not.Null, "ParameterValue was null");
            Assert.That(jobParameter1.ParameterValue.Value, Is.EqualTo(69));
            Assert.That(jobParameter2.ParameterValue, Is.Not.Null, "ParameterValue was null");
            Assert.That(jobParameter2.ParameterValue.Value, Is.EqualTo(52));
            Assert.That(jobType.ParameterGroups.FirstOrDefault().Parameters.FirstOrDefault().ParameterValue.Value, Is.EqualTo(69));
            Assert.That(jobType.ParameterGroups.FirstOrDefault().Parameters.FirstOrDefault(p=>p.ParameterId==14).ParameterValue.Value, Is.EqualTo(52));
        }
    }
}