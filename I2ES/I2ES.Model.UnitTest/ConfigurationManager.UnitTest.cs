﻿using NUnit.Framework;

namespace I2ES.Model.UnitTest {
    [TestFixture]
    public class ConfigurationManagerUnitTest {
        [Test]
        public void CanReadConfiguration() {
            ConfigurationWrapper wrapper = new ConfigurationWrapper();
            Assert.That(wrapper.NodeName, Is.EqualTo("I2ESNodeJodyDev"));
            Assert.That(wrapper.SolverOutputRootDirectory, Is.EqualTo("C:\\Solver"));
            Assert.That(wrapper.SolverRootDirectory, Is.EqualTo("C:\\Workspaces\\iSAMS\\StressCheck Solver\\bin\\x64\\Debug"));
            Assert.That(wrapper.TrafficManagerUrl, Is.EqualTo("http://isamsdev01/I2ES.TrafficManager/TrafficManagerService.svc"));

        }

        [Test]
        [ExpectedException]
        public void ThrowsExceptionforMissingConfigValue() {
            ConfigurationWrapper wrapper = new ConfigurationWrapper();
            wrapper.GetConfigValue("Bah Humbug");
        }
         
    }
}