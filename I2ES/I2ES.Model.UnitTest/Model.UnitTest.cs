﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using I2ES.Model.DomainModel;
using I2ES.Model.DomainModelContext;
using Moq;
using NUnit.Framework;

namespace I2ES.Model.UnitTest {
    //    [Category("DataDependent")]
    [TestFixture]
    public class ModelUnitTest {
        private ISAMSModel model;
        private Mock<IConnectionProvider> connectionProviderMock;

        [SetUp]
        public void TestSetup() {
            connectionProviderMock = new Mock<IConnectionProvider>();
            

            model = new ISAMSModel(new ConnectionProvider(), new JobReader {SqlWrapper = connectionProviderMock.Object.CreateWrapper()},
                new JobTypeReader() { SqlWrapper = connectionProviderMock.Object.CreateWrapper() }, new JobWriter { SqlWrapper = connectionProviderMock.Object.CreateWrapper() }) { IsTest = true };
            //transaction = model.BeginTransaction();
        }

        [TearDown]
        public void TearDown() {
            //transaction.Rollback();
            model.Dispose();
            
        }

        [Test]
        [ExpectedException(typeof (InvalidOperationException))]
        public void CallingCreateJobBeforeLoadThrowsException() {
            var sqlWrapperMock = new Mock<ISqlWrapper>();
            var dataRecordMock = createMockIDataRecordForJob(sqlWrapperMock);
            connectionProviderMock.Setup(x => x.CreateWrapper()).Returns(sqlWrapperMock.Object);
            model.CreateJob("jbreshears");
        }

        [Test]
        public void NewModelLoadsJobWhenCreateJobCalled() {
            ConnectionProvider provider = new ConnectionProvider();

            
            model.Load(6, "jbreshears");
            model.CreateJob("jbreshears");
            Assert.That(model.JobType.JobTypeId, Is.EqualTo(6));
            Assert.That(model.Job, Is.Not.Null, "model.Job was null");
            Assert.That(model.Job.JobType.Name, Is.EqualTo("BondedDoubler"));
        }


        [Test]
        public void ModelLoadsParameterGroups() {
            model.Load(6, "jbreshears");
            Assert.That(model.JobType.ParameterGroups, Is.Not.Null, "model.ParameterGroups was null");
            Assert.That(model.JobType.ParameterGroups.Count, Is.EqualTo(4));
            ParameterGroup parameterGroup = model.JobType.ParameterGroups[3];
            Assert.That(parameterGroup.Name, Is.EqualTo("loads"));
            Assert.That(parameterGroup.Description, Is.EqualTo("Loads"));
            Assert.That(parameterGroup.JobType.JobTypeId, Is.EqualTo(6));
        }

        [Test]
        public void LoadJobLoadsAllJobs() {
            model.LoadJobs("JobId", "asc");
            Assert.That(model.AllJobs.Count(), Is.GreaterThan(0));
        }

        [Test]
        public void ModelLoadsParameters() {
            model.Load(6, "jbreshears");
            Assert.That(model.JobType.ParameterGroups, Is.Not.Null);
            Assert.That(model.JobType.ParameterGroups.Count, Is.GreaterThan(0));
            Assert.That(model.JobType.ParameterGroups.Count(pg => pg.Name == "originalStructure"), Is.GreaterThan(0));
            ParameterGroup parameterGroup =
                model.JobType.ParameterGroups.FirstOrDefault(pg => pg.Name == "originalStructure");
            Assert.That(parameterGroup.Parameters, Is.Not.Null, "Parameters collection was null");
            Assert.That(parameterGroup.Parameters.Count(), Is.EqualTo(6));
            Assert.That(parameterGroup.Parameters.Count(p => p.Name == "Scalefactor"), Is.EqualTo(1));
            Parameter parameter = parameterGroup.Parameters.FirstOrDefault(p => p.Name == "Scalefactor");
            Assert.That(parameter.Description, Is.EqualTo("ratio panel to doubler size (>1.0)"));
            Assert.That(parameter.Type, Is.EqualTo("Scalefactor"));
        }

        private static Mock<IDataRecord> createMockIDataRecordForJob(Mock<ISqlWrapper> sqlWrapperMock) {
            var dataRecordMock = new Mock<IDataRecord>();
            sqlWrapperMock.Setup(m => m.RunReturnIDataRecords(It.IsAny<string>()))
                .Returns(new List<IDataRecord>() {dataRecordMock.Object});
            dataRecordMock.Setup(m => m.GetOrdinal("JobId")).Returns(69);
            dataRecordMock.Setup(m => m.GetInt32(69)).Returns(189);
            dataRecordMock.Setup(m => m.GetOrdinal("CreateTime")).Returns(68);
            dataRecordMock.Setup(m => m.GetDateTime(68)).Returns(DateTime.Now);
            return dataRecordMock;
        }

        [Test]
        public void ModelWillLoadItselfWithaJobId() {
            
            model.Load(13, "jbreshears", 1641);
            Assert.That(model.Job, Is.Not.Null);
            Assert.That(model.Job.JobId, Is.EqualTo(107));
            Assert.That(model.Job.UserId, Is.EqualTo("jbreshears"));
        }


        [Test]
        public void ModelCanGetJobStatusWithoutLoading() {
            var provider = new ConnectionProvider();
            IISAMSModel model = new ISAMSModel(provider, new JobReader {SqlWrapper = provider.CreateWrapper()},
                new JobTypeReader() { SqlWrapper = provider.CreateWrapper() }, new JobWriter());
            JobState jobState = model.GetJobStatus(107);

            Assert.That(jobState, Is.EqualTo(JobState.WaitingOnSolver));
        }

        [Test]
        [ExpectedException(ExpectedException = typeof (Exception))]
        public void GetSingleJobThrowsExceptionWhenJobIsNull() {
            Mock<ConnectionProvider> ConnectionProviderMock = new Mock<ConnectionProvider>();
            Mock<JobReader> jobReaderMock = new Mock<JobReader>();
            ISAMSModel isamsModel = new ISAMSModel(ConnectionProviderMock.Object, jobReaderMock.Object,
               new JobTypeReader() { SqlWrapper = ConnectionProviderMock.Object.CreateWrapper() }, new JobWriter());
            isamsModel.LoadJob(69);
        }

        [Test]
        [ExpectedException(ExpectedException = typeof (Exception))]
        public void GetSingleJobThrowsExceptionWhenJobTypeIsNull() {
            Mock<ConnectionProvider> ConnectionProviderMock = new Mock<ConnectionProvider>();
            Mock<JobReader> jobReaderMock = new Mock<JobReader>();
            Mock<JobTypeReader> jobTypeReaderMock = new Mock<JobTypeReader>();
            jobReaderMock.Setup(j => j.ReadSingleJob(69)).Returns( new Job(){JobId = 69});
            ISAMSModel isamsModel = new ISAMSModel(ConnectionProviderMock.Object, jobReaderMock.Object,
                jobTypeReaderMock.Object, new JobWriter());

            isamsModel.LoadJob(69);
        }



        [Test]
        public void CreateJobSetStatusToNew() {
            model.Load(13, "jbreshears");
            Job job =  model.CreateJob("froboz");
            Assert.That(job.JobState,Is.EqualTo(JobState.New));
        }


        [Test]
        public void CanLoadAllNewJobsForAUser() {
            model.LoadJobsForUser("froboz");
            Assert.That(model.AllJobs.Count(j => (j.JobState==JobState.New) && j.UserId=="froboz"), Is.EqualTo(3));
            Assert.That(model.AllJobs.Count(j => (j.JobState == JobState.New) && j.UserId != "froboz"), Is.EqualTo(0));
        }

    }
}