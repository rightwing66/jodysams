﻿using System;
using I2ES.Model.DomainModel;
using Moq;
using NUnit.Framework;

namespace I2ES.Model.UnitTest {
    [TestFixture]
    class TrafficManagerTest {
//        [Test]
//        public void CallsLoadDataRetrieverLoadData() {
//            Mock<ILoadDataRetriever> mockLoadDataRetriever = new Mock<ILoadDataRetriever>();
//            var mockConnectionProvider = new Mock<IConnectionProvider>();
//            TrafficManager trafficManager = new TrafficManager(mockConnectionProvider.Object, new ISAMSModel(), mockLoadDataRetriever.Object);
//
//
//            trafficManager.StartLoadDataRetriever(1001, 1111, "NoseBarrel");
//            mockLoadDataRetriever.Verify(l => l.LoadData(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(),
//                It.IsAny<ISqlWrapper>(), It.IsAny<Action<int>>(), It.IsAny<int>(), It.IsAny<int>()));
//        }
//
//        [Test]
//        public void WhenLoadDataRetrieverCompleteCalledCallsModelSetJobStatus() {
//            var mockLoadDataRetriever = new Mock<ILoadDataRetriever>();
//            var mockJobTypeModel = new Mock<IISAMSModel>();
//            var mockConnectionProvider = new Mock<IConnectionProvider>();
//            TrafficManager trafficManager = new TrafficManager(mockConnectionProvider.Object, mockJobTypeModel.Object, mockLoadDataRetriever.Object);
//            mockLoadDataRetriever.Setup(
//                i =>
//                    i.LoadData(1001, 1111, "NoseBarrel", It.IsAny<ISqlWrapper>(),
//                        trafficManager.LoadDataRetreverComplete, 0, 0))
//                .Callback(
//                    (int elementId, int jobId, string location, ISqlWrapper sqlWrapper, Action<int> completeCallback,
//                        int staticLoadCase, int fatigueLoadCase)
//                        => completeCallback(1111));
//
//            trafficManager.StartLoadDataRetriever(1001, 1111, "NoseBarrel");
//            mockJobTypeModel.Verify(j => j.SetJobStatus(1111, JobState.Complete));
//        }
//
//        [Test]
//        public void JobCreatedCallsSetJobStatus() {
//            var mockLoadDataRetriever = new Mock<ILoadDataRetriever>();
//            var mockJobTypeModel = new Mock<IISAMSModel>();
//            var mockConnectionProvider = new Mock<IConnectionProvider>();
//            TrafficManager trafficManager = new TrafficManager(mockConnectionProvider.Object, mockJobTypeModel.Object, mockLoadDataRetriever.Object);
//
//            trafficManager.JobCreated(42);
//
//            mockJobTypeModel.Verify(j => j.SetJobStatus(42, JobState.Created));
//        }
//
//        [Test]
//        public void TrafficManagerSetsJobStatus() {
//            Mock<ConnectionProvider> mockSqlWrapper = new Mock<ConnectionProvider>();
//            Mock<IISAMSModel> mockJobTypeModel = new Mock<IISAMSModel>();
//            mockJobTypeModel.Setup(j => j.GetJobStatus(42)).Returns(JobState.Complete);
//            var mockLoadDataRetriever = new Mock<ILoadDataRetriever>();
//            ITrafficManager manager = new TrafficManager(mockSqlWrapper.Object, mockJobTypeModel.Object, mockLoadDataRetriever.Object);
//            manager.LoadDataRetreverComplete(42);
//            var status = manager.GetJobStatus(42);
//            mockJobTypeModel.Verify(j => j.SetJobStatus(42, JobState.Complete));
//            mockJobTypeModel.Verify(j => j.GetJobStatus(42));
//            Assert.That(status, Is.EqualTo(JobState.Complete));
//        }
    }
}
