﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace I2ES.Model {
    static class CompileExtensions {
        public static string GetConsoleList(this string[] names){
            return names.Aggregate("\r\n", (current, name) => current + (name + "\r\n"));
        }
    }
}
