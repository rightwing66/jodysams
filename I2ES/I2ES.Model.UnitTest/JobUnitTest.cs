﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Remoting;
using I2ES.Model.DomainModel;
using I2ES.Model.DomainModelContext;
using I2ES.Model.Xml;
using Moq;
using NUnit.Framework;

namespace I2ES.Model.UnitTest{
    [TestFixture]
    public class JobUnitTest{
        [Test]
        public void JobReaderLoadsOneFieldFromRecord(){
            var mockDataRecord = new Mock<IDataRecord>();
            mockDataRecord.Setup(i => i.GetOrdinal("JobId")).Returns(0);
            mockDataRecord.Setup(i => i.GetInt32(0)).Returns(66);
            var dataList = new List<IDataRecord>{mockDataRecord.Object};
            var mockSqlWrapper = new Mock<ISqlWrapper>();
            mockSqlWrapper.Setup(i => i.RunReturnIDataRecords(It.IsAny<string>())).Returns(dataList);

            IJobReader reader = new JobReader { SqlWrapper = mockSqlWrapper.Object };
            List<Job> jobs = reader.ReadAllJobs("JobId", "Asc");

            Assert.That(jobs,Is.Not.Null);
            Assert.That(jobs.Count, Is.GreaterThanOrEqualTo(1));
            Job job = jobs.FirstOrDefault();
            Assert.That(job.JobId,Is.EqualTo(66));
        }

        [Test]
        public void JobReaderLoadsAllFieldsFromOneRecord(){
            var mockDataRecord = new Mock<IDataRecord>();
            mockDataRecord.Setup(i => i.GetOrdinal("JobId")).Returns(0);  
            mockDataRecord.Setup(i => i.GetInt32(0)).Returns(41);
            mockDataRecord.Setup(i => i["UserId"]).Returns("jbreshears");
            mockDataRecord.Setup(i => i.GetOrdinal("StateId")).Returns(3);
            mockDataRecord.Setup(i => i.GetInt32(3)).Returns(null);
            mockDataRecord.Setup(i => i.GetOrdinal("CompletionStage")).Returns(4);
            mockDataRecord.Setup(i => i.GetInt32(4)).Returns(null);
            
            mockDataRecord.Setup(i => i["CreateTime"]).Returns(DateTime.Parse("2015-04-08 13:14:43"));
            mockDataRecord.Setup(i => i["JobTypeName"]).Returns("BondedDoubler");
            mockDataRecord.Setup(i => i["ProcessBeginTime"]).Returns(DateTime.Parse("2015-04-08 15:15:43"));
            mockDataRecord.Setup(i => i["ProcessEndTime"]).Returns(DateTime.Parse("2015-04-08 16:16:43"));
            mockDataRecord.Setup(i => i["EditTime"]).Returns(DateTime.Parse("2015-04-08 16:16:43"));
            var dataList = new List<IDataRecord>{mockDataRecord.Object};
            var mockSqlWrapper = new Mock<ISqlWrapper>();
            mockSqlWrapper.Setup(i => i.RunReturnIDataRecords(It.IsAny<string>())).Returns(dataList);

            IJobReader reader = new JobReader { SqlWrapper = mockSqlWrapper.Object };
            List<Job> jobs = reader.ReadAllJobs("JobId", "Asc");

            Assert.That(jobs,Is.Not.Null);
            Assert.That(jobs.Count, Is.GreaterThanOrEqualTo(1));
            Job job = jobs.FirstOrDefault();
            Assert.That(job.JobId,Is.EqualTo(41));
            Assert.That(job.UserId,Is.EqualTo("jbreshears"));
            Assert.That(job.JobStateId,Is.EqualTo(null),"StateId");
            Assert.That(job.CompletionStage,Is.EqualTo(null),"CompletionStage");
            Assert.That(job.CreateTime,Is.EqualTo(DateTime.Parse("2015-04-08 13:14:43")),"CreateTime");
            Assert.That(job.JobTypeName,Is.EqualTo("BondedDoubler"),"JobTypeName");
            Assert.That(job.ProcessBeginTime, Is.EqualTo(DateTime.Parse("2015-04-08 15:15:43")),"ProcessBeginTime");
            Assert.That(job.ProcessEndTime, Is.EqualTo(DateTime.Parse("2015-04-08 16:16:43")), "ProcessEndTime");
            Assert.That(job.EditTime, Is.EqualTo(DateTime.Parse("2015-04-08 16:16:43")), "ProcessEndTime");
            
        }


        

    }
}

/*
 0        1           2         3         4               5          6                   7               8
JobId	JobTypeId	UserId	    StateId	CompletionStage	CreateTime	ProcessBeginTime	ProcessEndTime	JobTypeName
41	    6	        jbreshears	NULL	NULL	2015-03-23 17:11:20.990	NULL	NULL	BondedDoubler
*/
