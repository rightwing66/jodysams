﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using I2ES.Model.DomainModel;
using NUnit.Framework;

namespace I2ES.Model.UnitTest {
    [TestFixture]
    class ParameterComparerUnitTest {
        [Test]
        public void EqualIsBaseOnParameterId() {
            Parameter firstParameter = new Parameter { ParameterId = 1, Name = "First", Description = "FirstParameter" };
            Parameter secondParameter = new Parameter { ParameterId = 2, Name = "Second", Description = "SEcond parameter" };
            ParamaterIdComparer comparer = new ParamaterIdComparer();
            Assert.That(comparer.Equals(firstParameter, secondParameter), Is.False);
            Assert.That(comparer.Equals(firstParameter, firstParameter), Is.True);

        }

        [Test]
        public void GetsCorrectHasCode() {
            Parameter firstParameter = new Parameter { ParameterId = 1, Name = "First", Description = "FirstParameter" };
            ParamaterIdComparer comparer = new ParamaterIdComparer();
            Assert.That(comparer.GetHashCode(firstParameter), Is.EqualTo(comparer.GetHashCode(firstParameter)));
            Assert.That(comparer.GetHashCode(null), Is.EqualTo(0));
        }

        [Test]
        public void ToStringGetsCorrectString() {
            Parameter firstParameter = new Parameter { ParameterId = 1, Name = "First", Description = "FirstParameter" };
            Assert.That(firstParameter.ToString(), Is.EqualTo("1 First | FirstParameter"));
        }
    }
}
