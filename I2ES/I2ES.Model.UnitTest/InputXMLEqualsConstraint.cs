﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.XPath;
using I2ES.Model.DomainModel;
using I2ES.Model.Extensions;
using NUnit.Framework.Constraints;

namespace I2ES.Model.UnitTest {
    class InputXmlMatchesConstraint : Constraint {
        private readonly XDocument expectedDocument;
        private string expectedMessage;
        private string actualMessage;
        private XDocument actualDocument;

        public InputXmlMatchesConstraint(XDocument expected) {
            this.expectedDocument = expected;
        }

        public override bool Matches(object actualValue) {
            actualDocument = actualValue as XDocument;
            if (actualDocument == null) {
                setMessage("Valid XMLDocument", "null");
                return false;
            }
            actual = actualValue;
            return compareDocuments();
        }

        private void setMessage(string expectedMessage, string actualMessage) {
            this.expectedMessage = expectedMessage;
            this.actualMessage = actualMessage;
        }
        private void setMessage(string path, string expected, string actual) {
            setMessage(path + "/" + expected, path + "/" + actual);
        }

        private void setNullMessage(string path, string expectedValue) {
            setMessage(path + "/" + expectedValue,"missing");
        }

        private bool compareDocuments() {
            var matches = compareValues("rarat_input/information/jobid");
            matches = matches && compareValues("rarat_input/information/job_type");
            matches = matches && compareValues("rarat_input/information/userId");
            matches = matches && compareValues("rarat_input/information/jobtype_name");
            matches = matches && compareParameters();
            
            return matches;
        }

        private bool compareParameters() {
            foreach (XElement paramterGroup in expectedDocument.Descendants("parametergroup")) {
                string parameterGroupName = paramterGroup.Attribute("name").NullableValue();
                foreach (XElement parameter in paramterGroup.Descendants("parameter")) {
                    string parameterName = parameter.Attribute("name").NullableValue();
                    string xpath = "rarat_input/parameters/parametergroup[@name='{0}']/parameter[@name='{1}']";
                    if (!compareValues(string.Format(xpath, parameterGroupName, parameterName))) {
                        return false;
                    }
                }

            }
            return true;
        }

        private bool compareValues(string path) {
            var actualValue = actualDocument.XPathSelectElement(path).NullableValue();
            var expectedValue = expectedDocument.XPathSelectElement(path).NullableValue();
            if (actualValue == null) {
                setNullMessage(path, expectedValue);
                return false;
            }
            if (actualValue != expectedValue) {
                setMessage(path, expectedValue, actualValue);
                return false;
            }
            return true;
        }




        public override void WriteDescriptionTo(MessageWriter writer) {
            writer.WriteExpectedValue("Expected Shit");
        }

        public override void WriteMessageTo(MessageWriter writer) {
            writer.DisplayDifferences(expectedMessage, actualMessage);
        }
    }
}
