﻿using System;
using NUnit.Framework;
using I2ES.Model.Extensions;

namespace I2ES.Model.UnitTest {
    [TestFixture]
    public class ExtensionUnitTest {
        [Test]
        public void ToSqlDateWritesAMFormat() {
            DateTime? daytime = new DateTime(1985, 12,26,9,30,0);

            Assert.That(daytime.ToSqlDate(), Is.EqualTo("19851226 09:30:00 AM"));
        }

        [Test]
        public void ToSqlDateWritesPMFormat() {
            DateTime evening = new DateTime(1985,12,26,17,0,0);
            Assert.That(evening.ToSqlDate(), Is.EqualTo("19851226 05:00:00 PM"));
        }
    }
}