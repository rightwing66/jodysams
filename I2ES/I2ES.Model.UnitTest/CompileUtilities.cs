﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
 using I2ES.Model.Extensions;
namespace I2ES.Model {
    public class CompileUtilities {
        private string callingNameSpace;
        public  string Namespace{
            get { return callingNameSpace; }
            set{
                if (!value.EndsWith(".")){
                   callingNameSpace = value + ".";
                }
                else{
                    callingNameSpace = value;
                }
            }
        }

        public Stream GetResourceStream(string resourceName){
            var names = Assembly.GetExecutingAssembly().GetManifestResourceNames();
            var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(Namespace +  resourceName);
            if (stream == null){
                throw new Exception("Couldn't find manifest assembly named " +Namespace +  resourceName + "\r\nAvailable names are:" + names.GetConsoleList());
            }
            return stream;
        }

        public  string GetResourceValue(string resourceName) {
            string xmlValue = new StreamReader(GetResourceStream(resourceName)).ReadToEnd();
            return xmlValue;
        }

    }
}