﻿using System.Configuration;
using System.Data.SqlClient;
using NUnit.Framework;

namespace I2ES.Model.UnitTest {
    [TestFixture]
    public class SqlWrapperTest {
        private SqlConnection sqlConnection;

        [TestFixtureSetUp]
        public void TestFixtureSetup() {
            var connectionString = ConfigurationManager.ConnectionStrings["ESRDConnectionString"].ConnectionString;
            sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();

        }

        [TestFixtureTearDown]
        public void TestFixtureTearDown() {
            sqlConnection.Close();
            sqlConnection.Dispose();
        }

        [Test]
        [ExpectedException(typeof(System.Exception), ExpectedMessage = "No return from \r\nselect * from job where jobid is null")]
        public void RunSqlReturnScalarReportsNoResults() {
            SqlWrapper sqlWrapper = new SqlWrapper(sqlConnection);
            sqlWrapper.RunSqlReturnScalar("select * from job where jobid is null");
        }

        [Test]
        [ExpectedException(typeof(System.Exception))]
        public void RunSqlReturnScalarReportsException() {
            SqlWrapper sqlWrapper = new SqlWrapper(sqlConnection);
            sqlWrapper.RunSqlReturnScalar("select * from jobs234 where jobid is null");
        }

        [Test]
        [ExpectedException(typeof(System.Exception))]
        public void RunSqlReturnIDataREcordReportsException() {
            SqlWrapper sqlWrapper = new SqlWrapper(sqlConnection);
            sqlWrapper.RunReturnIDataRecords("select * from jobs234 where jobid is null");
        }

        [Test]
        [ExpectedException(typeof(System.Exception))]
        public void RunReturnReaderReportsException() {
            SqlWrapper sqlWrapper = new SqlWrapper(sqlConnection);
            sqlWrapper.RunReturnReader("select * from jobs234 where jobid is null");
        }

        [Test]
        [ExpectedException(typeof(System.Exception))]
        public void RunSqlReportsException() {
            SqlWrapper sqlWrapper = new SqlWrapper(sqlConnection);
            sqlWrapper.RunSql("select * from jobs234 where jobid is null");
        }
    }
}