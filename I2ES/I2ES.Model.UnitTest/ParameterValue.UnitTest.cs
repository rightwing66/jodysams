﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Text;
using I2ES.Model.DomainModel;
using I2ES.Model.DomainModelContext;
using Moq;
using NUnit.Framework;

namespace I2ES.Model.UnitTest {
    [TestFixture]
    public class ParameterValueUnitTest {
        private SqlConnection connection;
        [Test]
        [Category("DataDependent")]
        public void WritesOneValueToTheDatabase() {
            var ConnectionProviderMock = new Mock<IConnectionProvider>();
            var sqlWrapperMock = new Mock<ISqlWrapper>();
            var dataRecordMock = new Mock<IDataRecord>();

            sqlWrapperMock.Setup(m => m.RunReturnIDataRecords(It.IsAny<string>()))
                .Returns(new List<IDataRecord>(){dataRecordMock.Object});
            dataRecordMock.Setup(m => m.GetOrdinal("JobId")).Returns(69);
            dataRecordMock.Setup(m => m.GetInt32(69)).Returns(125);
            dataRecordMock.Setup(m => m.GetOrdinal("CreateTime")).Returns(68);
            dataRecordMock.Setup(m =>m.GetDateTime( 68)).Returns(DateTime.Now);
            ConnectionProviderMock.Setup(x => x.CreateWrapper()).Returns(sqlWrapperMock.Object);

            //Bit of hack here.  It seems this test was all along reading the database, via the JobTypeReader. This 
            //didn't show until I refactored JobTypeReader to take a mock.
            //In the interest of time, I'm going to let this continue
            ConnectionProvider realConnectionProvider = new ConnectionProvider();
            ;
            IISAMSModel model = new ISAMSModel(ConnectionProviderMock.Object, new JobReader { SqlWrapper = sqlWrapperMock.Object }, new JobTypeReader { SqlWrapper = realConnectionProvider.CreateWrapper()}, new JobWriter()) { IsTest = true };

            model.Load(6, "jbreshears");
            model.CreateJob("jbreshears");
            Assert.That(model.Job.ParameterGroups, Is.Not.Null);
            Assert.That(model.Job.AllParameters.Count(), Is.GreaterThan(0));
            var parameter = model.Job.AllParameters.FirstOrDefault(p => p.ParameterId == 56);
            parameter.ParameterValue.Value = 25;
            model.SaveChanges();


            var connectionString = ConfigurationManager.ConnectionStrings["ESRDConnectionString"].ConnectionString;
            Debug.Assert(connectionString != null, "ConnectionString named 'ESRDConnectionString' was null");
            connection = new SqlConnection { ConnectionString = connectionString };
            connection.Open();
            CompileUtilities compileUtilities = new CompileUtilities { Namespace = "I2ES.Model.UnitTest.sql" };
            string sql = compileUtilities.GetResourceValue("VerifyValue.sql")
                .Replace("~JobId", model.Job.JobId.ToString())
                .Replace("~ParameterId", parameter.ParameterId.ToString());
            Assert.That(int.Parse(SqlUtilities.RunReturnScalar(sql, connection).ToString()), Is.EqualTo(25));
        }
    }
}
