﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using I2ES.Model.Xml;
using Moq;
using NUnit.Framework;

namespace I2ES.Model.UnitTest.sql {
    [TestFixture]
    internal class StoreResultsTest {
        [TestFixtureSetUp]
        public void TestFixtureSetup() {

        }

        [TestFixtureTearDown]
        public void TestFixtureTearDown() {

        }



        [Test]
        public void CallsOutputXmlDatabaseWriter() {
            var mockISqlWrapper = new Mock<ISqlWrapper>();
            var mockOutputXmlDatabaseWriter = new Mock<StressCheckOutputXmlDatabaseWriter>();
            //var storeResults = new StoreResults();
            var inputFileLocation = Path.Combine(Path.GetDirectoryName(path: Assembly.GetExecutingAssembly().CodeBase), "XmlFiles\\StoreResultsInput.xml");
            var outputFileLocation = Path.Combine(Path.GetDirectoryName(path: Assembly.GetExecutingAssembly().CodeBase), "XmlFiles\\StoreResultsOutput.xml");

            //var result = storeResults.StoreData(Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase), outputFileLocation, mockISqlWrapper.Object, 123);
            mockOutputXmlDatabaseWriter.VerifyAll();
        }
    }
}
