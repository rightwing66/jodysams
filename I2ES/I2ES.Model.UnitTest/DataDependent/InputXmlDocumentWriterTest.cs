﻿using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using I2ES.Model.Xml;
using NUnit.Framework;

namespace I2ES.Model.UnitTest.DataDependent{
    [TestFixture]
    public class InputXmlDocumentWriterTest{
        CompileUtilities compileUtilities = new CompileUtilities{Namespace ="I2ES.Model.UnitTest.XmlFiles" };
        [Test]
        public void CreatedInputXmlFileMatchesTestFile(){
            XDocument expected = 
                XDocument.Load(XmlReader.Create(compileUtilities.GetResourceStream("InputJob408.xml")));
            var sqlWrapper = new ConnectionProvider().CreateWrapper();
            InputXmlDocumentWriter writer = new InputXmlDocumentWriter(sqlWrapper);
            XDocument actual = writer.CreateDocument(408);
            XmlWriter xmlWriter = new XmlTextWriter("InputXmlText.xml",new ASCIIEncoding());
            actual.WriteTo(xmlWriter);
            xmlWriter.Close();
            Assert.That(actual, new InputXmlMatchesConstraint(expected));
        }

        [Test]
        [ExpectedException]
        public void GetResourceStreamThrowsException() {
            CompileUtilities compileUtilities = new CompileUtilities { Namespace = "I2ES.Model.UnitTest.XmlFiles" };
            compileUtilities.GetResourceStream("Bugger");
        }
    }
}