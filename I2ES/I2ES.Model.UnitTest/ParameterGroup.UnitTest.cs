﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using I2ES.Model.DomainModel;
using NUnit.Framework;

namespace I2ES.Model.UnitTest {
    [TestFixture]
    class ParameterGroupUnitTest {
        [Test]
        public void ToStringWorks() {
            ParameterGroup group = new ParameterGroup { ParameterGroupId = 1, Name = "Fred" };
            Assert.That(group.ToString(), Is.EqualTo("1 Fred"));
        }

    }
}
