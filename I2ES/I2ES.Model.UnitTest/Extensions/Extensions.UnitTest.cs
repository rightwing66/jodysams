﻿using System;
using I2ES.Model.DomainModel;
using I2ES.Model.Extensions;
using NUnit.Framework;

namespace I2ES.Model.UnitTest.Extensions{
    [TestFixture]
    public class ExtensionsUnitTest{
        [Test]
        public void GetEnumNameWorks(){
            string name = 1.EnumGetName<JobState>();
            Assert.That(name,Is.EqualTo("New"));
        }
        [Test]
        public void GetEnumNameWorksOnNull(){
            string name = "".EnumGetName<JobState>();
            Assert.That(name,Is.EqualTo(""));
        }

        [Test]
        public void NullableParseHandlesEmpty(){
            Assert.That("".NullableParse(), Is.EqualTo(null));
            Assert.That("  ".NullableParse(),Is.EqualTo(null));
            Assert.That("1".NullableParse(),Is.EqualTo(1));
            Assert.That("x".NullableParse(),Is.EqualTo(null));
        }

        [Test]
        public void NullableParseIntHandlesEmpty(){
            Assert.That("".NullableParseInt(), Is.EqualTo(null));
            Assert.That("  ".NullableParseInt(),Is.EqualTo(null));
            Assert.That("1".NullableParseInt(),Is.EqualTo(1));
            Assert.That("x".NullableParseInt(),Is.EqualTo(null));
        }

        [Test]
        public void ToSqlDatePovidesSqlDate(){
            DateTime day = new DateTime(1999,1,9);
            Assert.That(day.ToSqlDate(), Is.EqualTo("19990109 12:00:00 AM"));
        }

        [Test]
        public void ToShortDateTimeWorks(){
            DateTime day = new DateTime(1999,1,9);
            Assert.That(day.ToShortDateTimeString(), Is.EqualTo("09-00-99 12:00:00"));
        }


    }
}