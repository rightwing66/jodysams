﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using I2ES.Model.Extensions;
using NUnit.Framework;

namespace I2ES.Model.UnitTest.Extensions {
    [TestFixture]
    public class XElementExtensionsUnitTest {
        [Test]
        public void XAttributeParse() {
            var onhead = new XAttribute("Head", "66");
            Assert.That(onhead.NullableValue(), Is.EqualTo("66"));
            Assert.That(onhead.NullableIntValue(), Is.EqualTo(66));
            XAttribute nullAtrriAttribute = null;
            Assert.That(nullAtrriAttribute.NullableValue(), Is.EqualTo(null));
            Assert.That(nullAtrriAttribute.NullableIntValue(), Is.EqualTo(null));
            XElement me = new XElement("Me", "8");
            Assert.That(me.NullableIntValue(), Is.EqualTo(8));
            Assert.That(me.NullableValue(), Is.EqualTo("8"));
            XElement nullme = null;
            Assert.That(nullme.NullableValue(), Is.EqualTo(null));
            Assert.That(nullme.NullableIntValue(), Is.EqualTo(null));
        }

        [Test]
        public void SqlTextExtensions() {
            double? val = 2;
            Assert.That(val.ToSql(), Is.EqualTo("'2'"));
            val = null;
            Assert.That(val.ToSql(), Is.EqualTo("null"));
            Assert.That("help".ToSql(), Is.EqualTo("'help'"));
            string help = null;
            Assert.That(help.ToSql(), Is.EqualTo("null"));
        }

        [Test]
        public void FindsFirstElementInAList() {
            var first = new XElement("Me", "8");
            List<XElement> elements = new List<XElement> { first, new XElement("You", "9") };
            Assert.That(elements.FirstNullableValue(), Is.EqualTo("8"));
        }

    }
}