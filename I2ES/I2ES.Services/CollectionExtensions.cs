﻿using System.Collections.Generic;
using System.Linq;
using I2ES.Services.Validation;

namespace I2ES.Services {
    public static class CollectionExtensions {
        public static string WriteStack(this Stack<Token> stack) {
            return stack.Aggregate("", (current, token) => current + token.RawToken).TrimEnd();
        }

        public static string WriteQueue(this Queue<Token> queue) {
            string result = null;
            foreach (Token token in queue) {
                result += token.RawToken + " ";
            }
            return result;
        }

        public static string ToCommaList(this IEnumerable<Token> itemsEnumerable) {
            Token[] items = itemsEnumerable.ToArray();
            int count= items.Count();
            string result = "";
            for (int i = 0; i < count; i++) {
                result += items[i];
                if (i < count - 1) {
                    result += ",";
                }
            }
            return result;
        }
    }
}