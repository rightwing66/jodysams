﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace I2ES.Services.Validation {
    public class OperatorToken : Token {
        private readonly string operatorToken;
        public override string RawToken {
            get { return operatorToken; }
        }

        public int Precedence { get { return operatorPrecedence[operatorToken]; } }
        public int OperandCount { get; private set; }

        private readonly Dictionary<string, int> operatorPrecedence = new Dictionary<string, int> {
            {"+",10},
            {"-", 10},
            {"*", 11},
            {"/", 11},
            {"^",13}
        };

        public OperatorToken(string operatorToken) {
            //since we only have binary operators
            OperandCount = 2;
            if (!Operators.Contains(operatorToken)) { throw new Exception(operatorToken + " is not an operator"); }
            this.operatorToken = operatorToken;
        }

        public ValueToken Operate(List<ValueToken> operands) {
            switch (RawToken) {
                case "+" :
                    return new ValueToken( operands[0].Value + operands[1].Value);
                case "-":
                    return new ValueToken(operands[0].Value - operands[1].Value);
                case "*":
                    return new ValueToken(operands[0].Value * operands[1].Value);
                case "/":
                    return new ValueToken(operands[0].Value / operands[1].Value);
                case "^":
                    return new ValueToken(Math.Pow(operands[0].Value, operands[1].Value) );
            }
            throw new Exception("Unuported operator: " + RawToken);
        }
    }
}