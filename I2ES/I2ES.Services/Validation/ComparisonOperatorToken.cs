﻿namespace I2ES.Services.Validation {
    public class ComparisonOperatorToken : Token {
        private readonly string token;

        public ComparisonOperatorToken(string token) {
            this.token = token;
        }

        public override string RawToken {
            get { return token; }
        }
    }
}