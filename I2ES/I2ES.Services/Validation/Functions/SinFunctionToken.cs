﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace I2ES.Services.Validation.Functions {
    public class SinFunctionToken : FunctionToken {
        public SinFunctionToken(string rawToken) : base(rawToken) {}
        
        public override double Evaluate(List<ValueToken> operands) {
            ParameterCheck.CheckCount(1, operands);
            return Math.Sin(operands[0].Value);
        }
    }
}