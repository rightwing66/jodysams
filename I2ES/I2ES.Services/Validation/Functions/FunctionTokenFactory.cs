﻿using System;
using I2ES.Services.Validation.Functions;

namespace I2ES.Services.Validation {
    public class FunctionTokenFactory {
        public FunctionToken Create(string functionName) {
            switch (functionName.ToLower()) {
                case "sin":
                    return new SinFunctionToken(functionName);
                case "cos":
                    return new CosFunctionToken(functionName);
            }
            throw new Exception("Unrecognized function name: " + functionName);
        }
    }
}