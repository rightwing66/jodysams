﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace I2ES.Services.Validation {
    public class ComparisonExpression : ValidationExpression {
        private readonly TextPrefixParser textPrefixParser;
//        private NameValueCollection variables;
        private readonly PostFixEvaluator evaluator;
        public EvaluativeExpression LeftExpression { get; private set; }
        public EvaluativeExpression RightExpression { get; private set; }
        public ComparisonOperatorToken ComparisonOperator { get; private set; }

        public ComparisonExpression(TextPrefixParser textPrefixParser, PostFixEvaluator evaluator) {
            this.textPrefixParser = textPrefixParser;
            this.evaluator = evaluator;
        }

        public void Evaluate(InfixToPostFixConverter infixToPostFixConverter, NameValueCollection variableValues, double comparisonTolerance) {
            Queue<Token> leftExpressionQueue = new Queue<Token>(); 
            
            var rightExpressionQueue = new Queue<Token>();
            var x = rightExpressionQueue.WriteQueue();
            var tokenQue = textPrefixParser.Parse(variableValues);
            Token token = tokenQue.Dequeue();
            while (token != null && !(token is ComparisonOperatorToken)) {
                leftExpressionQueue.Enqueue(token);
                token = tokenQue.Dequeue();
            }
            LeftExpression = new EvaluativeExpression(leftExpressionQueue);
            ComparisonOperator = (ComparisonOperatorToken)token;
            foreach (var token1 in tokenQue) {
                rightExpressionQueue.Enqueue(token1);
            }
            RightExpression = new EvaluativeExpression(rightExpressionQueue);
            LeftExpression.PostFix = infixToPostFixConverter.Convert(LeftExpression);
            RightExpression.PostFix = infixToPostFixConverter.Convert(RightExpression);

            LeftExpression.Result = evaluator.Evaluate(LeftExpression.PostFix);
            RightExpression.Result = evaluator.Evaluate(RightExpression.PostFix);

        }

    }
}