﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace I2ES.Services.Validation {
    public class FunctionToken : Token {
        protected readonly string rawToken;
        public override string RawToken { get { return rawToken; } }
        public int ParameterCount { get; private set; }

        public FunctionToken(string rawToken) {
            this.rawToken = rawToken;
            ParameterCount = getParameterCount(rawToken);
        }

        public ValueToken Evaluate(List<ValueToken> operands) {
            return new ValueToken(EvaluateValue(operands));
        }

        private int getParameterCount(string token) {
            switch (rawToken.ToLower()) {
                case "sin":
                    return 1;
                case "cos":
                    return 1;
                case "froboz":
                    return 3;
            }
            throw new Exception("Unrecognized function name: " + rawToken);
        }


        private double EvaluateValue(List<ValueToken> operands) {
            switch (rawToken.ToLower()) {
                case "sin":
                    ParameterCheck.CheckCount(1, operands);
                    return Math.Sin(operands[0].Value);
                case "cos":
                    ParameterCheck.CheckCount(1, operands);
                    return Math.Cos(operands[0].Value);
                case "froboz":
                    ParameterCheck.CheckCount(3,operands);
                    return operands[0].Value + operands[1].Value + operands[2].Value;
            }
            throw new Exception("Unrecognized function name: " + rawToken);
        }

    }
}