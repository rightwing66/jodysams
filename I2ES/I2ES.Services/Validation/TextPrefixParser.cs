﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace I2ES.Services.Validation {
    public class TextPrefixParser {
        private readonly string rawExpression;
        private Queue<Token> outputTokens;

        public TextPrefixParser(XmlEncodedParser encodedParser) {
            rawExpression = encodedParser.Parse();
        }

        public TextPrefixParser(string rawExpression) {
            this.rawExpression = rawExpression;
        }

        public Queue<Token> Parse(NameValueCollection variableValues) {
            string substitutedExpression = substitueVaribleNamesWithValues(variableValues, rawExpression);
            var tokens = splitTokensFromInput(substitutedExpression);
            buildQueueFromTokens(tokens);
            return outputTokens;
        }

        public Queue<Token> Parse() {
            return Parse(new NameValueCollection());
        }

        private string substitueVaribleNamesWithValues(NameValueCollection variableValues, string expression) {
            foreach (string key in variableValues.AllKeys) {
                expression = expression.Replace(key, variableValues[key]);
            }
            return expression;
        }


        private List<string> splitTokensFromInput(string input) {
            List<string> elementStrings = new List<string>();
            int lastIndex = 0;
            for (int i = 0; i < input.Length; i++) {
                lastIndex = lookForTwoCharacterEqualityOperator(input, ref i, elementStrings, lastIndex);
                lastIndex = lookForOneCharacterOperator(input, i, lastIndex, elementStrings);
                
            }
            if (lastIndex < input.Length) {
                elementStrings.Add(input.Substring(lastIndex, input.Length - lastIndex).Trim());
            }
            return elementStrings;
        }

        private int lookForTwoCharacterEqualityOperator(string input, ref int i, List<string> elementStrings, int lastIndex) {
            if (i < input.Length - 2) {
                string peek = input[i].ToString() + input[i + 1];
                if (Token.ComparisonOperators.Contains(peek)) {
                    gatherPreceedingNumericTokens(input, i, lastIndex, elementStrings);
                    elementStrings.Add(peek);
                    lastIndex = i + 2;
                    i++;
                }
            }
            return lastIndex;
        }

        private int lookForOneCharacterOperator(string input, int i, int lastIndex, List<string> elementStrings) {
            if (Token.Operators.Contains(input[i].ToString()) || 
                Token.Paranthesis.Contains(input[i].ToString()) ||
                Token.ComparisonOperators.Contains(input[i].ToString())) {
                gatherPreceedingNumericTokens(input, i, lastIndex, elementStrings);
                if (input[i] != ',') {
                    elementStrings.Add(input[i].ToString().Trim());
                }
                lastIndex = i + 1;
            }
            return lastIndex;
        }

        private void gatherPreceedingNumericTokens(string input, int i, int lastIndex, List<string> elementStrings) {
            if (i > 0 && i != lastIndex) {
                elementStrings.Add(input.Substring(lastIndex, i - lastIndex).Trim());
            }
        }

        private void buildQueueFromTokens(List<string> tokens) {
            outputTokens = new Queue<Token>();
            foreach (string textToken in tokens) {
                if (string.IsNullOrEmpty(textToken.Trim())) {
                    continue;
                }
                if (Token.Operators.Contains(textToken)) {
                    outputTokens.Enqueue(new OperatorToken(textToken));
                    continue;
                }

                if (Token.ComparisonOperators.Contains(textToken)) {
                    outputTokens.Enqueue(new ComparisonOperatorToken(textToken));
                    continue;
                }
                if (Token.Paranthesis.Contains(textToken)) {
                    outputTokens.Enqueue(new ParenthesesToken(textToken));
                    continue;
                }
                try {
                    outputTokens.Enqueue(new ValueToken(textToken));
                }
                catch (Exception ) {
                    //cavallierly assume that the failure was that token was non-numeric
                    
                    outputTokens.Enqueue(new FunctionToken(textToken));
                }
            }
        }

        public override string ToString() {
            return outputTokens.Aggregate("", (current, outputToken) => current + outputToken.ToString());
        }
    }
}