﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace I2ES.Services.Validation {
    public enum Result { Success, Failure }
    public class PostfixExpression {
        private readonly Queue<Token> outputQue;

        /// <summary>
        /// Returns a clone of the OutputQue, which is immutable.
        /// </summary>
        public Queue<Token> OutputQue {
            get { return new Queue<Token>(outputQue); }
        }

        public Stack<Token> OperatorStack { get; private set; }
        public Result Result { get; set; }
        public List<string> Messages { get; set; }

        public PostfixExpression() {
            outputQue = new Queue<Token>();
            OperatorStack = new Stack<Token>();
            Messages = new List<string>();
        }

        public PostfixExpression(Queue<Token> queue, Stack<Token> stack) {
            outputQue = queue;
            OperatorStack = stack;
        }

        public override string ToString() {
            return OutputQue.Aggregate("", (current, token) => current + (token.RawToken + " "));
        }
    }
}