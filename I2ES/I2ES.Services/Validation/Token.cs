﻿using System;
using System.Collections.Generic;

namespace I2ES.Services.Validation {
    public abstract class Token  {
        public static readonly List<string> Operators = new List<string> { "+", "-","*","/", "^","," };
        public static readonly List<string> ComparisonOperators = new List<string>{">","<",">=","<=","==", "!="};
        public static readonly List<string> Paranthesis = new List<string>{"(",")"};

        public abstract string RawToken { get;  }
        


        public override bool Equals(object obj) {
            Token compare = obj as Token;
            if ((object)compare == null) return false;
            return this.RawToken == compare.RawToken;
        }

        public override int GetHashCode() {
            return RawToken.GetHashCode();
        }

        public override string ToString() {
            return RawToken;
        }

        public static bool operator ==(Token token1, Token token2) {

            if ((object) token1 == null) {
                if ((object)token2 == null) {
                    return true;
                }
                return false;
            }
            return token1.Equals(token2);
        }

        public static bool operator !=(Token token1, Token token2) {
            return !(token1 == token2);
        }
        
    }
}