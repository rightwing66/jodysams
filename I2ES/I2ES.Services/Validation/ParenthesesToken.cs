﻿namespace I2ES.Services.Validation {
    public class ParenthesesToken : Token {
        private readonly string textToken;
        public override string RawToken { get { return textToken; } }

        public bool Left { get; private set; }
        public bool Right { get; private set; }

        public ParenthesesToken(string textToken) {
            this.textToken = textToken;
            if (textToken == "(") {
                Left = true;
                Right = false;
            }
            else {
                Right = true;
                Left = false;
            }
        }
    }
}