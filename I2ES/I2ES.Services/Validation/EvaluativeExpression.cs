﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace I2ES.Services.Validation {
    public class EvaluativeExpression {

        private readonly Queue<Token> tokens;

        /// <summary>
        /// Returns a clone of the tokens.  This list is immutable.
        /// </summary>
        public Queue<Token> Tokens
        {
            get { return new Queue<Token>(tokens); }
        }

        public double Result { get;  set; }
        public PostfixExpression PostFix { get; set; }

        public EvaluativeExpression() {
            tokens = new Queue<Token>();
        }

        public EvaluativeExpression(Queue<Token> tokens) {
            this.tokens = tokens;
        }


        public override string ToString() {
            return Tokens.Aggregate<Token, string>(null, (current, token) => current + (token.RawToken + ""));
        }

 
    }
}