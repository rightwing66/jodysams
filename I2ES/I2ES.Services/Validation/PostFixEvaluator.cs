﻿using System;
using System.Collections.Generic;

namespace I2ES.Services.Validation {
    public class PostFixEvaluator {
        public double Evaluate(PostfixExpression expression) {
            Stack<Token> stack = new Stack<Token>();
            Queue<Token> queue = new Queue<Token>(expression.OutputQue);

            var x = stack.WriteStack();
            var y = queue.WriteQueue();

            while (queue.Count > 0) {
                Token token = queue.Dequeue();
                if (token is ValueToken) {
                    stack.Push(token);
                    continue;
                }

                var operatorToken = token as OperatorToken;
                if (operatorToken != null) {
                    List<ValueToken> operands = new List<ValueToken>();
                    for (int i = 0; i < operatorToken.OperandCount; i++) {
                        operands.Add((ValueToken)stack.Pop());
                    }
                    //operands come off the stack in reverse order, which matters for non commuttative operations like -
                    operands.Reverse();
                    stack.Push( operatorToken.Operate(operands));
                    continue;
                }

                var functionToken = token as FunctionToken;
                if (functionToken != null) {
                    List<ValueToken> parameters = new List<ValueToken>();
                    for (int i = 0; i < functionToken.ParameterCount; i++) {
                        parameters.Add((ValueToken)stack.Pop());
                    }
                    //operands come off the stack in reverse order, which matters for non commuttative operations like -
                    parameters.Reverse();
                    stack.Push(functionToken.Evaluate(parameters));
                }
            }
            Token lastToken = stack.Pop();
            ValueToken lastValueToken = lastToken as ValueToken;
            if (lastToken == null) {
                throw new Exception("Expected last value on the stack to be a ValueToken but it was a " +
                                    lastToken.GetType().Name);
            }
            return lastValueToken.Value;
        }
    }
}