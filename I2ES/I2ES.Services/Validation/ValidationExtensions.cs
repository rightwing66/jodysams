﻿using System.Collections.Generic;

namespace I2ES.Services.Validation {
    public static class ValidationExtensions {
        public static Token SafePeek(this Stack<Token> stack) {
            if (stack.Count == 0) return null;
            return stack.Peek();
        }
    }
}