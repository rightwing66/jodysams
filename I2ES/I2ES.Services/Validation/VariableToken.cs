﻿using System;

namespace I2ES.Services.Validation {
    public class VariableToken : Token {
        private readonly string textToken;

        public override string RawToken {
            get { return textToken; }
        }

        public VariableToken(string textToken) {
            this.textToken = textToken;
        }
    }
}