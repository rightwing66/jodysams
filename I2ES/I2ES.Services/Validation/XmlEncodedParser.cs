﻿namespace I2ES.Services.Validation {
    public class XmlEncodedParser {
        private  string expression;

        public XmlEncodedParser(string expression) {
            this.expression = expression;
        }

        public string Parse() {
            expression = expression.Replace("_m_", "-");
            expression = expression.Replace("_pt_", ".");
            expression = expression.Replace("_o_", " ( ");
            expression = expression.Replace("_c_", " ) ");
            expression = expression.Replace("_p_", " + ");
            expression = expression.Replace("_s_", " - ");
            expression = expression.Replace("_t_", " * ");
            expression = expression.Replace("_d_", " / ");
            expression = expression.Replace("_e_", " ^ ");
            expression = expression.Replace("_g_", " > ");
            expression = expression.Replace("_l_", " < ");
            expression = expression.Replace("_ge_", " >= ");
            expression = expression.Replace("_le_", " <= ");
            expression = expression.Replace("_eq_", " == ");
            expression = expression.Replace("_ne_", " != ");
            return expression;
        }
    }
}