using System;

namespace I2ES.Services.Validation {
    public class ValueToken : Token {
        public Double Value { get; private set; }
        private readonly string textToken;

        public override string RawToken {
            get { return textToken; }
        }

        public ValueToken(string textToken) {
            double result;
            if (!double.TryParse(textToken, out result)) { throw new Exception(textToken + " is not a number");}
            Value = result;
            this.textToken = textToken;
        }

        public ValueToken(double numericValue) {
            Value = numericValue;
            textToken = numericValue.ToString();
        }
    }
}