﻿using System.Collections.Generic;

namespace I2ES.Services.Validation {
    public class InfixToPostFixConverter {
        Result result;
        List<string> messages;

        public PostfixExpression Convert(EvaluativeExpression sourceExpression) {
            var resultOutputQue = new Queue<Token>();
            var resultOperatorStack = new Stack<Token>();
            result = Result.Success;
            messages = new List<string>();    

            Queue<Token> sourceTokens = sourceExpression.Tokens;
            while (sourceTokens.Count>0) {
                Token token = sourceTokens.Dequeue();
                if (token is ValueToken) {
                    resultOutputQue.Enqueue(token);
                    continue;
                }
                if (token is FunctionToken) {
                    resultOperatorStack.Push(token);
                    continue;
                }
                if (token is OperatorToken) {
                    processOperator(resultOperatorStack, (OperatorToken)token, resultOutputQue, token);
                    continue;
                }
                if (token is ParenthesesToken) {
                    processParenthesis((ParenthesesToken)token, resultOperatorStack, resultOutputQue);
                }
            }
            runTheStackAddRemainingOperators(resultOperatorStack,  resultOutputQue);
            return new PostfixExpression(resultOutputQue,resultOperatorStack){Result = result, Messages = messages};
        }

        private  void runTheStackAddRemainingOperators(Stack<Token> operatorStack,
           Queue<Token> outputQueue) {
            while (operatorStack.Count > 0) {
                Token token = operatorStack.Pop();
                if (token is ParenthesesToken) {
                    addFailureMessage( Result.Failure, "Mismatched Parenthesis");
                }
                outputQueue.Enqueue(token);
            }
        }

        private  void processParenthesis(ParenthesesToken parenthesesToken,
            Stack<Token> operatorStack,
            Queue<Token> outputQueue) {
            if (parenthesesToken.Left) {
                operatorStack.Push(parenthesesToken);
            }
            else {
                handleRightParenthesis(operatorStack, outputQueue);
            }
        }

        private void handleRightParenthesis(Stack<Token> operatorStack, Queue<Token> outputQueue) {
            bool foundLeftParenthesis = false;
            while (operatorStack.Count > 0) {
                Token stackToken = operatorStack.Pop();
                if (stackToken is ParenthesesToken && ((ParenthesesToken) stackToken).Left) {
                    foundLeftParenthesis = true;
                    break;
                }
                outputQueue.Enqueue(stackToken);
            }
            if (operatorStack.SafePeek() is FunctionToken) {
                outputQueue.Enqueue(operatorStack.Pop());
            }
            if (!foundLeftParenthesis) {
                addFailureMessage(Result.Failure, "Mismatched Parenthesis");
            }
        }

        private  void addFailureMessage( Result newResult, string message) {
            result = newResult;
            messages.Add(message);
        }

        private static void processOperator(Stack<Token> operatorStack, OperatorToken queueToken, Queue<Token> outputQueue, Token token) {
            if (operatorStack.Count > 0) {
                OperatorToken stackToken = operatorStack.Peek() as OperatorToken;
                if (stackToken != null) {
                    if (queueToken.Precedence <= stackToken.Precedence) {
                        outputQueue.Enqueue(operatorStack.Pop());
                    }
                }
            }
            operatorStack.Push(token);
        }
    }
}