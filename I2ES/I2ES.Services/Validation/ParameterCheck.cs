﻿using System;
using System.Collections.Generic;

namespace I2ES.Services.Validation {
    public class ParameterCheck {
        public static void CheckCount(int parameterCount, List<ValueToken> parameters) {
            if (parameters.Count != parameterCount) throw new Exception("Expected "+ parameterCount +" parameter(s) but " + parameters.Count + " were passed: " + parameters.ToCommaList()); 
        }
    }
}