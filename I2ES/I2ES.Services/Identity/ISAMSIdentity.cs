﻿using System;
using System.DirectoryServices.AccountManagement;
using System.Security.Principal;


namespace I2ES.Services.Identity {
    public static class ISAMSIdentity {
        public static string GetFullName(IPrincipal user) {
            string accountName = user.Identity.Name.AfterTheSlash();
            try {
                using (var context = new PrincipalContext(ContextType.Domain)) {
                    var principal = UserPrincipal.FindByIdentity(context, accountName);
                    if (principal != null) {
                        return $"{principal.GivenName} {principal.Surname}";
                    }
                    return accountName;
                }
            }
            catch (Exception ) {
                return accountName;
            }

        }

        public static string GetUserId(IPrincipal user) {
            return user.Identity.Name.AfterTheSlash();
        }
    }
}