﻿using System.Collections.Generic;
using System.Linq;
using I2ES.Model;
using I2ES.Model.DomainModel;
using I2ES.Model.DomainModel.JobProcessModel;
using Moq;
using NUnit.Framework;

namespace TrafficManagerWindowsService.UnitTest {
    [TestFixture]
    public class TrafficManagerWatcherTest {
        [Test]
        public void LoadsACollectionOfJobs() {
            ConnectionProvider provider = new ConnectionProvider();
            TrafficManagerWatcher watcher = new TrafficManagerWatcher(new JobProcessModel(provider.CreateWrapper()),provider.CreateWrapper() );

            watcher.ProcessJobs();
        }
    }
}