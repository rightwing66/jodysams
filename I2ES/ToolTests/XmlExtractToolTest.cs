﻿using I2ES.Model;
using I2ES.Model.Xml;
using I2ES.UnitTest.toolTests;
using NUnit.Framework;

namespace ToolTests {
    [TestFixture]
    [Category("ToolTests")]
    public class XmlExtractToolTest {
        private const string sqlNameSpace = "ToolTests.sqlTemplates.";
        private const string xmlNameSpace = "ToolTests.xml.";
        private ISqlWrapper sqlWrapper;

        [TestFixtureSetUp]
        public void Setup() {
            ConnectionProvider connectionProvider = new ConnectionProvider();
            sqlWrapper = connectionProvider.CreateWrapper();
        }

        [Test, Explicit]
        public void PullInputXmlIntoDatabase() {
            deleteExistingParameterGroups("SingleHole", sqlWrapper);
            var xmlstring = TestUtilities.GetResourceValue(xmlNameSpace + "modelDefinition.xml");
            ModelLoadXmlDatabaseWriter writer = new ModelLoadXmlDatabaseWriter(sqlWrapper);
            writer.ParseInputXmlFile(xmlstring);
        }

        [Test, Explicit]
        public void WriteOutputDataToDatabase(){
            var xmlString = TestUtilities.GetResourceValue(xmlNameSpace + "Output16840.xml");
            StressCheckOutputXmlDatabaseWriter writer = new StressCheckOutputXmlDatabaseWriter(sqlWrapper);
            writer.ParseOutputXmlFile(@"C:\Solver\job16840\output", xmlString, 16840);
        }

        private void deleteExistingParameterGroups(string jobTypeName, ISqlWrapper sqlWrapper) {
            string sql = TestUtilities.GetResourceValue(sqlNameSpace+"DeleteExistingParameterGroups.sql");
            sql = sql.Replace("~JobTypeName", jobTypeName);
            sqlWrapper.RunSql(sql);
        }
        [TestFixtureTearDown]
        public void Teardown() {
            sqlWrapper.Dispose();
        }
    }
}