﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace ToolTests{
    [TestFixture]
    [Explicit]
    [Category("ToolTests")]
    public class LoggingTests{
        private static readonly log4net.ILog log = 
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [TestFixtureSetUp]
        public void TestFixtureSetup(){
            DirectoryInfo di = new DirectoryInfo(".");
            string list = di.EnumerateFiles().Select(f => f.DirectoryName + "\\" + f.Name + "\r\n").
                Aggregate((current, next)=>current + "\r\n" + next );
            Console.WriteLine(list);

            FileInfo configFileInfo = new FileInfo("ISAMSLog4Net.xml");
            Assert.That(configFileInfo.Exists);
            log4net.Config.XmlConfigurator.ConfigureAndWatch(configFileInfo);
        }

        [Test]
        public void TryLog(){
            log.Info("Hello World");    
        }
    }
}