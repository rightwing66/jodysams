﻿using System.Diagnostics;
using System.IO;
using System.Reflection;
using NUnit.Framework;

namespace I2ES.UnitTest.toolTests {
    public class TestUtilities {
        public static string GetResourceValue(string resourceName) {
            var names = Assembly.GetExecutingAssembly().GetManifestResourceNames();
            var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName);
            if (stream == null) {
                Debug.WriteLine("Could not find Manifest Resource named:" + resourceName);
                Assert.Fail();
            }
            string xmlValue = new StreamReader(stream).ReadToEnd();
            return xmlValue;
        }

    }
}