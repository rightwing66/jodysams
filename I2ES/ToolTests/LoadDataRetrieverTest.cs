﻿using AircraftLoadData.LoadDataRetriever;
using I2ES.Model;
using I2ES.Model.LoadDataRetriever;
using NUnit.Framework;

namespace ToolTests {
    [TestFixture]
    [Explicit]
    [Category("ToolTests")]
    public class LoadDataRetrieverTest {

        [Test]
        public void StartTheRetriever() {
            ILoadDataRetriever retriever = new LoadDataRetriever();
            ConnectionProvider connectionProvider = new ConnectionProvider();
            retriever.LoadData(19520, 351, "ForwardNoseBarrel", connectionProvider.CreateWrapper(), 0, 1);
        }

        public void CompleteCallback(int jobId) {

        }
    }
}