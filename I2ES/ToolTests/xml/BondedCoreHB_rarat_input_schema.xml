<?xml version="1.0" encoding="utf-8" ?>
<rarat_input_schema>
  <job name="BondedDoubler">
    <description>A Bonded Doubler Repair</description>
    <files>
      <file type="handbook" name="handbook">BondedCoreHB.sci</file>
    </files>
  </job>
  <interface>
    <button name="next">Next</button>
    <button name="prev">Prev</button>
    <button name="save">Save</button>
    <caption>Bonded Core Repair</caption>
    <files>
      <file type="image" name="mainimage" alt="Bonded Core Repair">images/BondedCoreHB/DoubleRepair_0.png</file>
    </files>
    <step name="BondedCoreHB_step1">
      <caption>Enter Parameters for Bonded Core Repair [1/4]</caption>
      <button name="next" target="BondedCoreHB_confirm1"></button>
      <files>
        <file type="image" name="mainimage" alt="Bonded Core Repair">images/BondedCoreHB/DoubleRepair_1.jpg</file>
      </files>
    </step>
    <step name="BondedCoreHB_confirm1">
      <caption>Confirm Parameters for Bonded Core Repair [2/4]</caption>
      <button name="next" target="BondedCoreHB_step2" />
      <files>
        <file type="image" name="mainimage" alt="Bonded Core Repair">images/BondedCoreHB/DoubleRepair_1.jpg</file>
      </files>
    </step>
    <step name="BondedCoreHB_step2">
      <caption>Enter More Parameters for Bonded Core Repair [3/4]</caption>
      <button name="next" target="BondedCoreHB_submit" />
      <files>
        <file type="image" name="mainimage" alt="Bonded Core Repair">images/BondedCoreHB/DoubleRepair_2.jpg</file>
      </files>
    </step>
    <step name="BondedCoreHB_submit">
      <caption>Confirm Parameters for Bonded Core Repair [4/4]</caption>
      <button name="next">Submit</button>
      <files>
        <file type="image" name="mainimage" alt="Bonded Core Repair">images/BondedCoreHB/DoubleRepair_2.jpg</file>
      </files>
    </step>
  </interface>
  <parameters>
    <step name="BondedCoreHB_step1,display:BondedCoreHB_confirm1">
      <parametergroup name="hiddenParameters">
        <material name="Isotropic"  type="hidden" description="material surrounding original structure">Iso</material>
        <parameter name="TensileAllow" type="hidden" units="psi" />
        <parameter name="ShearAllow" type="hidden" units="psi" />
      </parametergroup>
      <parametergroup name="originalStructure" description="Original Structure">
        <material name="Skin" type="dropdownlist" description="upper and lower skin material">
          <option name="IM7/977" label="IM7/977" default="default">
          </option>
        </material>
        <parameter name="nplies" label="n_{plies}" constraint="nplies_g_0" type="int" description="number skin plies">7</parameter>
        <parameter name="Tplylower" label="T_{plylower}" constraint="Tplylower_g_0" description="ply thickness of lower panel" units="in">0.01</parameter>
        <parameter name="Thc" label="T_{hc}" constraint="Thc_g_0" description="thickness of honeycomb core" units="in">0.2</parameter>
        <parameter name="Tplyupper" label="T_{plyupper}" constraint="Tplyupper_g_0" description="ply thickness of upper panel" units="in">0.01</parameter>
        <material name="Honeycomb" type="dropdownlist" description="Honeycomb Core Material">
          <option name="Honeycomb" label="Honeycomb" default="default">
          </option>
        </material>
        <parameter name="Rmold" label="R_{mold}" constraint="Rmold_g_60" description="moldline radius (>60)" units="in">75.0</parameter>
        <parameter name="Scalefactor" label="Scalefactor" constraint="Scalefactor_g_1,Scalefactor_l_10" description="ratio panel to doubler size (>1.0)">2.0</parameter>
      </parametergroup>
      <parametergroup name="repairStructure" description="Repair Structure">
        <material name="Plug" type="dropdownlist" description="core plug material">
          <option name="Plug" label="Plug" default="default">
          </option>
        </material>
        <parameter name="Lplug" label="L_{plug}" constraint="Lplug_g_0" description="length of core plug" units="in">5.0</parameter>
        <parameter name="Wplug" label="W_{plug}" constraint="Wplug_g_0" description="width of core plug" units="in">3.0</parameter>
        <parameter name="Rplug" label="R_{plug}" constraint="Rplug_g_0" description="radius of core plug" units="in">0.5</parameter>
        <material name="Gap" type="dropdownlist" description="Gap Material">
          <option name="Gap" label="Gap" default="default">
          </option>
        </material>
        <parameter name="Gap" label="Gap" constraint="Gap_g_0" description="gap between core plug and original structure" units="in">0.05</parameter>
        <material name="Doubler" type="dropdownlist" description="doubler material">
          <option name="IM7/8551-7A" label="IM7/8551-7A" default="default">
          </option>
        </material>
        <parameter name="Rdoubler" label="R_{doubler}" constraint="Rdoubler_g_0" description="doubler radius" units="in">1.5</parameter>
        <parameter name="Ltaper" label="L_{taper}" constraint="Ltaper_g_0" description="taper length" units="in">2.0</parameter>
        <parameter name="mplies" label="m_{plies}" constraint="mplies_g_0" type="int" description="number doubler plies">6</parameter>
        <parameter name="bplies" label="b_{plies}" constraint="bplies_g_0,bplies_l_3" type="int" description="number doubler plies (1 or 2)">1</parameter>
        <parameter name="Tplydoubler" label="T_{plydoubler}" constraint="Tplydoubler_g_0" description="ply thickness of doubler" units="in">0.01</parameter>
        <parameter name="OL" label="OL" constraint="OL_g_0" description="overlap length" units="in">3.0</parameter>
        <material name="Adhesive" type="dropdownlist" description="adhesive material">
          <option name="FM300-K" label="FM300-K" default="default">
            <change name="TensileAllow" value="10000" />
            <change name="ShearAllow" value="6000" />
          </option>
        </material>
        <parameter name="Tadh" label="T_{adh}" constraint="Tadh_g_0" description="adhesive thickness" units="in">0.005</parameter>
      </parametergroup>
    </step>
    <step name="BondedCoreHB_step2,display:BondedCoreHB_submit">
      <parametergroup name="loads" description="Loads">
        <parameter name="Ft" label="&amp;sigma;_{t}" type="real" description="&amp;sigma;t" units="lbf">8000</parameter>
        <parameter name="Fz" label="&amp;sigma;_{z}" type="real" description="&amp;sigma;z" units="lbf">-35999</parameter>
        <parameter name="Fzt" label="&amp;tau;_{zt}" type="real" description="&amp;tau;zt" units="lbf">-18000</parameter>
      </parametergroup>
    </step>
  </parameters>
  <rules>

    <step name="BondedCoreHB_step1">
      <rule name="Ltaper_l_09OL" description="doubler taper less than 0.9*Overlap">Ltaper_l_0_pt_9_t_OL</rule>
    </step>
  </rules>
  <additionalParameters />
</rarat_input_schema>
