﻿declare @stepId int

select @stepId = (select StepId from Step where Name='~StepName')

insert ParameterGroup (JobTypeId, Name, Description, StepId)
values ('~JobTypeId', '~ParameterGroupName', '~ParameterGroupDescription', @stepId)
select @@identity