﻿declare @unitId int
declare @parameterId int

if not exists(select UnitId from Unit where Name='~UnitName')
Begin
	Insert Unit(Name) Values('~UnitName')
	set @unitId=@@Identity
End
else
	set @unitId = (select UnitId from Unit where Name='~UnitName')


if not exists(select parameterId from Parameter where Name = '~ParameterName' and JobTypeId=~JobTypeId)
begin
	Insert Parameter(Name, Description, JobTypeId, UnitId,[Constraint], Label)
	Values ('~ParameterName','~Description',~JobTypeId, @unitId, '~Constraint', '~Label')
end





set @parameterId = (select parameterId from Parameter where Name = '~ParameterName' and JobTypeId=~JobTypeId)
Insert ParameterGroup_x_Parameter (ParameterId, ParameterGroupId)
values (@parameterId, ~ParameterGroupId)

select @parameterId

