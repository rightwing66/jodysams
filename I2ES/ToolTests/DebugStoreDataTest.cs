﻿using I2ES.Model;
using I2ES.SolverNode;
using NUnit.Framework;

namespace ToolTests {
    [TestFixture]
    [Explicit]
    public class DebugStoreDataTest {


        [Test]

        public void LaunchStoreData() {
            ConnectionProvider connectionProvider = new ConnectionProvider();
            StoreResults storeResults = new StoreResults(connectionProvider.CreateWrapper());
            storeResults.StoreData(@"C:\Solver\job501\output", @"C:\Solver\job501\output\output.xml", 501);
        }
    }
}