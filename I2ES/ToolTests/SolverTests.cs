﻿using I2ES.Model;
using I2ES.Model.SolverLauncher;
using NUnit.Framework;

namespace ToolTests {
    [TestFixture]
    [Category("ToolTests")]
    public class SolverTests {
        [Test]
        [Explicit]
        public void LaunchSolver() {
            ConnectionProvider provider = new ConnectionProvider();
            
            SolverLauncher launcher = new SolverLauncher(provider.CreateWrapper(), new ConfigurationWrapper());
            launcher.LaunchStressCheckSolver(281,Callback);
        }

        public void Callback(int jobId,string somethingOrOther)
        {
            
        }
    }
}