﻿using System.Data.SqlClient;
using I2ES.UnitTest.toolTests;
using NUnit.Framework;

namespace ToolTests {
    [TestFixture]
    [Category("ToolTests")]
    public class RaRATWebServiceTest {


        [Test, Explicit]
        public void stuffResourceIntoTable(){
            var xmlValue = TestUtilities.GetResourceValue("I2ES.UnitTest.BathtubJobConfiguration.xml");
            using (
                SqlConnection connection =
                    new SqlConnection(
                        "Password=raratpw;Persist Security Info=True;User ID=RaRAT;Initial Catalog=RaRAT;Data Source=rrd1")
                ) {
                connection.Open();
                SqlCommand command = new SqlCommand("update tbl_jobtype_data set jobtype_input_schema = '"+xmlValue+"' WHERE jobtype_name = 'Bathtub'", connection);
                command.ExecuteNonQuery();
            }
        }

    }
}
