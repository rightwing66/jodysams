﻿using System;
using System.Linq;
using System.Web.Mvc;
using I2ES.Model.DomainModel;
using I2ES.Web.Controllers;
using Moq;
using NUnit.Framework;

namespace I2ES.Web.Prototype.UnitTest {
    [TestFixture]
    public class ParameterControllerTest {
        [Test]
        public void  ReturnsModelWithCorrectJobTypeId() {
            Mock<ISAMSModel> modelMock = new Mock<ISAMSModel>();
            Mock<Job> jobMock = new Mock<Job>();
            jobMock.SetupAllProperties();
            jobMock.Object.JobId = 13;
            modelMock.SetupGet(m => m.Job).Returns(jobMock.Object);
            ParameterController controller = new ParameterController();
            controller.JobTypeModel = modelMock.Object;

            ViewResult result= (ViewResult) controller.Index(12, 13);
            ISAMSModel model = (ISAMSModel) result.Model;

            Assert.That(model.Job, Is.Not.Null);
            Assert.That(model.Job.JobId,Is.EqualTo(13));
            Assert.That( model.Job.EditTime.Value.DayOfYear,Is.EqualTo(DateTime.Now.DayOfYear) );
            modelMock.Verify(m=>m.SaveChanges(),Times.AtLeastOnce);

        }

        [Test]
        public void WhenCalledToAddANewJobAddsParametersToJob() {
            Mock<ISAMSModel> modelMock = new Mock<ISAMSModel>();
            Mock<Job> jobMock = new Mock<Job>();
            jobMock.SetupAllProperties();
            modelMock.SetupGet(m => m.Job).Returns(jobMock.Object);
            ParameterController controller = new ParameterController();
            controller.JobTypeModel = modelMock.Object;
            jobMock.Object.AllParameters.Add(new Parameter {Name = "Em", ParameterValue = new ParameterValue()});
            jobMock.Object.AllParameters.Add(new Parameter { Name = "Nu", ParameterValue = new ParameterValue() });

            ViewResult result = (ViewResult) controller.AddParametersToNewJob(13, 1, 2, "rei", "buno", "part");
            ISAMSModel model = (ISAMSModel)result.Model;

            Assert.That(model.Job.REINumber,Is.EqualTo("rei"));
            Assert.That(model.Job.BUNO, Is.EqualTo("buno"));
            Assert.That(model.Job.PartNumber, Is.EqualTo("part"));
            Assert.That(model.Job.AllParameters.Count, Is.EqualTo(2));
            Assert.That(model.Job.AllParameters.FirstOrDefault(p=>p.Name=="Em").ParameterValue.Value,Is.EqualTo(1));
            Assert.That(model.Job.AllParameters.FirstOrDefault(p => p.Name == "Nu").ParameterValue.Value, Is.EqualTo(2));
            modelMock.Verify(m=>m.SaveChanges());
        }

        [Test]
        public void WhenJobIdIsNullInsertCallsCreateJob() {
            Mock<ISAMSModel> modelMock = new Mock<ISAMSModel>();
            modelMock.SetupAllProperties();
            Mock<Job> jobMock = new Mock<Job>();
            jobMock.SetupAllProperties();
            ParameterController controller = new ParameterController();
            controller.JobTypeModel = modelMock.Object;
            modelMock.Object.Job = jobMock.Object;

            controller.Insert(new FormCollection(), 13, null);

            modelMock.Verify(m=>m.Load(It.IsAny<int>(),It.IsAny<string>()));
            modelMock.Verify(m=>m.CreateJob(It.IsAny<string>()));
        }


        [Test]
        public void WhenJobIdIsNotNullInsertCallsLoadExisting() {
            Mock<ISAMSModel> modelMock = new Mock<ISAMSModel>();
            modelMock.SetupAllProperties();
            Mock<Job> jobMock = new Mock<Job>();
            jobMock.SetupAllProperties();
            ParameterController controller = new ParameterController();
            controller.JobTypeModel = modelMock.Object;
            modelMock.Object.Job = jobMock.Object;

            controller.Insert(new FormCollection(), 13, 1);

            modelMock.Verify(m => m.LoadJob(It.IsAny<int>()));
            jobMock.VerifySet(j=>j.EditTime);
        }

        [Test]
        public void InsertPullsParametersFromPostAndWritesThem() {
            Mock<ISAMSModel> modelMock = new Mock<ISAMSModel>();
            modelMock.SetupAllProperties();
            Mock<Job> jobMock = new Mock<Job>();
            jobMock.SetupAllProperties();
            ParameterController controller = new ParameterController();
            controller.JobTypeModel = modelMock.Object;
            modelMock.Object.Job = jobMock.Object;
            jobMock.Object.AllParameters.Add(new Parameter{ParameterId = 34});
            jobMock.Object.AllParameters.Add(new Parameter { ParameterId = 12 });

            var formPost = new FormCollection();
            formPost.Add("34","1.2");
            formPost.Add("12","3.3");

            controller.Insert(formPost, 13, 1);

            var parameter = jobMock.Object.AllParameters.FirstOrDefault(p => p.ParameterId == 34);
            Assert.That(parameter.ParameterValue.Value,Is.EqualTo(1.2));

        }
        
         
    }
}
