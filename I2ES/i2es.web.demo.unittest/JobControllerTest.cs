﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using I2ES.Model;
using I2ES.Model.DomainModel;
using I2ES.Model.DomainModelContext;
using I2ES.Model.Extensions;
using I2ES.Web.Controllers;
using Moq;
using NUnit.Framework;

namespace I2ES.Web.Prototype.UnitTest {
    [TestFixture]
    public class JobControllerTest {

        [Test]
        public void JobControllerEmitsJson() {
            var controller = createJobsController();
            ActionResult result = controller.AllJobs();
        }

        [Test]
        public void JobControllerReturnsListofJobs() {
            var controller = createJobsController();
            ActionResult result = controller.AllJobsJson();
            Assert.That(result, Is.InstanceOf<JsonResult>());
            JsonResult jresult = result as JsonResult;
            Assert.That(jresult.Data, Is.Not.Null);
            Console.WriteLine(jresult.Data);
        }

        [Test]
        public void AllJobTypes() {
            var controller = createJobsController();
            ActionResult result = controller.AllJobTypes();
            Assert.That(result, Is.InstanceOf<JsonResult>());
            var jresult = (JsonResult)result;
            Assert.That(jresult.Data, Is.Not.Null);
            List<ModelType> jobTypes = jresult.Data as List<ModelType>;
            Assert.That(jobTypes.FirstOrDefault().JobTypeId, Is.EqualTo(5));
        }

        [Test]
        public void AllJobsJson() {
            Mock<ConnectionProvider> connectionProviderMock = new Mock<ConnectionProvider>();
            Mock<ISAMSModel> modelMock = new Mock<ISAMSModel>();
            Mock<SqlWrapper> sqlWrapperMock = new Mock<SqlWrapper>();

            var controller = new JobsController() { Model = modelMock.Object, ConnectionProvider = connectionProviderMock.Object };
            var result = controller.AllJobsJson();
            Assert.That(result, Is.Null);
        }

        [Test]
        public void IndexReturnsAnActionResult() {
            Mock<ConnectionProvider> connectionProviderMock = new Mock<ConnectionProvider>();
            Mock<ISAMSModel> modelMock = new Mock<ISAMSModel>();
            Mock<SqlWrapper> sqlWrapperMock = new Mock<SqlWrapper>();

            var controller = new JobsController() { Model = modelMock.Object, ConnectionProvider = connectionProviderMock.Object };
            var result = controller.Index(2);
            Assert.That(result,Is.InstanceOf<ActionResult>());
        }

        [Test]
        public void AllJobsJsonFiltersToOneDaysResults() {
            Mock<ConnectionProvider> connectionProviderMock = new Mock<ConnectionProvider>();
            Mock<ISAMSModel> modelMock = new Mock<ISAMSModel>();
            Mock<SqlWrapper> sqlWrapperMock = new Mock<SqlWrapper>();
            modelMock.Setup(x => x.AllJobs)
                .Returns(new List<Job> {
                    new Job {JobId = 1, CreateTime = DateTime.Now},
                    new Job {JobId = 2, CreateTime = DateTime.Now - TimeSpan.FromDays(2)}
                });
            var controller = new JobsController() { Model = modelMock.Object, ConnectionProvider = connectionProviderMock.Object };


            var result = controller.AllJobsJson() as JsonResult;

            var rowData = result.Data.GetPropertyValue("rows");
            var count = rowData.GetPropertyValue("Count");

            Assert.That(count, Is.EqualTo(1));

        }

        [Test]
        [ExpectedException]
        public void ControllerThrowsAnExceptionWhenParametersAreNotSet() {
            var controller = new JobsController();
            controller.AllJobsJson();
        }

        [Test]
        [ExpectedException]
        public void ControllerThrowsAnExceptionWhenSecondParameterIsNotSet() {
            Mock<ConnectionProvider> connectionProviderMock = new Mock<ConnectionProvider>();
            var controller = new JobsController() {ConnectionProvider = connectionProviderMock.Object};
            controller.AllJobsJson();
        }

        private JobsController createJobsController() {
            var connectionProvider = new ConnectionProvider();
            JobsController controller = new JobsController {
                ConnectionProvider = connectionProvider,
                Model = new ISAMSModel(connectionProvider, new JobReader(), new JobTypeReader(), new JobWriter())
            };
            return controller;
        }
    }
}