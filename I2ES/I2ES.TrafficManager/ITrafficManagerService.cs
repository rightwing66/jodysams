﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace I2ES.TrafficManager {
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITrafficManagerService" in both code and config file together.
    [ServiceContract]
    public interface ITrafficManagerService {


        [OperationContract]
        void SubmitJob(int jobId, string url);

        [OperationContract]
        void NotifySolverComplete(int jobId, string machineName, string solverOutputFolderPath);

        [OperationContract]
        void NotifyStoreResultsComplete(int jobId, string machineName);

        [OperationContract]
        void StressReportReady(int jobId, string machineName, string reportUrl);


        [OperationContract]
        string Ping();

        [OperationContract]
        void NotifySolverStoreDataFailed(int jobId, string machineName,  string exception);
    }



}
