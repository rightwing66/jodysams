﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using I2ES.Model;
using I2ES.Model.DomainModel;
using I2ES.Model.DomainModelContext;
using static System.String;

namespace I2ES.TrafficManager {
    public class TrafficManager {
        private HttpClient httpClient;
        private readonly ISqlWrapper sqlWrapper;

        private static readonly log4net.ILog Log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly ConnectionProvider connectionProvider;

        public TrafficManager() {
            connectionProvider = new ConnectionProvider();
            sqlWrapper = connectionProvider.CreateWrapper();
        }

        public  void SubmitJobToSolver(int jobId, string submittingNode) {
            Log.InfoFormat("SubmitJobToSolver id:{0}   submittingNode:{1}", jobId, submittingNode);
            ISAMSModel model = new ISAMSModel(connectionProvider, new JobReader { SqlWrapper = sqlWrapper }, new JobTypeReader { SqlWrapper = sqlWrapper }, new JobWriter { SqlWrapper = sqlWrapper });
            var job = model.LoadJob(jobId);
            if (job.JobState != JobState.New) {
                string message = "Attempted to submit Job {0} when its state was {1}";
                throw new Exception(Format(message, job.JobId, job.JobState.ToString()));
            }
            job.JobState = JobState.WaitingOnSolver;
            job.ProcessBeginTime = DateTime.Now;
            job.SubmittingNode = submittingNode;
            model.SaveChanges();
        
        }

        public void NotifySolverComplete(int jobId, string solverOutputFolderPath) {
            Log.InfoFormat("NotifySolverComplete jobId:{0}", jobId);
            ISAMSModel model = new ISAMSModel(new ConnectionProvider(), new JobReader { SqlWrapper = sqlWrapper }, new JobTypeReader { SqlWrapper = sqlWrapper }, new JobWriter { SqlWrapper = sqlWrapper });
            var job = model.LoadJob(jobId);
            job.SolverState = BackEndProcessState.Complete;
            job.JobState = JobState.WaitingOnStoreData;
            job.SolverEndTime = DateTime.Now;
            model.SaveChanges();
            LaunchStoreResults(model, solverOutputFolderPath);
        }

        public void NotifyStoreResultsComplete(int jobId, string url) {
            Log.InfoFormat("NotifyStoreResultsComplete jobId:{0}", jobId);
            ISAMSModel model = new ISAMSModel(new ConnectionProvider(), new JobReader { SqlWrapper = sqlWrapper }, new JobTypeReader { SqlWrapper = sqlWrapper }, new JobWriter { SqlWrapper = sqlWrapper });
            var job = model.LoadJob(jobId);
            job.StoreResultsState = BackEndProcessState.Complete;
            job.JobState = JobState.WaitingOnTrueNumbers;
            job.StoreResultsEndTime = DateTime.Now;
            launchTrueNumbers(model);
        }

        private async void launchTrueNumbers(ISAMSModel model) {
            //hack: we are shortcircuiting true numbers for now
            StressReportReady(model.Job.JobId, "http://www.urbandictionary.com/define.php?term=dumbass");
            return;

            Job job = model.Job;
            model.Job.TrueNumberState = BackEndProcessState.InProgress;
            model.Job.TrueNumberBeginTime = DateTime.Now;
            model.SaveChanges();
            string url = $"http://localhost/TrueNumbersProxy/stressreport/JobReadyForTrueNumbers?jobId={job.JobId}";
            Log.Info("Launch True Numbers:" + url);
            await sendHTTPMessage(url);

        }

        public async void LaunchStoreResults(ISAMSModel model, string solverOutputFolderPath) {
            model.Job.StoreResultsBeginTime = DateTime.Now;
            model.Job.StoreResultsState = BackEndProcessState.InProgress;
            model.SaveChanges();
            await signalStoreData(model.Job, solverOutputFolderPath);
        }


        public void StressReportReady(int jobId, string reportUrl) {
            ISAMSModel model = new ISAMSModel(new ConnectionProvider(), new JobReader { SqlWrapper = sqlWrapper }, new JobTypeReader { SqlWrapper = sqlWrapper }, new JobWriter { SqlWrapper = sqlWrapper });
            var job = model.LoadJob(jobId);
            Log.InfoFormat("StressReportReady jobId:{0}", job.JobId);
            model.Job.TrueNumberEndTime = DateTime.Now;
            model.Job.TrueNumberState = BackEndProcessState.Complete;
            model.Job.JobState = JobState.Complete;
            model.Job.ProcessEndTime = DateTime.Now;
            model.Job.Report = reportUrl;
           // model.Job.Report = "<a href=\"http://isamsi1/I2ES/Reports/report.docx\">Job Report</a>";
            model.SaveChanges();
            Log.InfoFormat("Job {0} is complete ", model.Job.JobId);
        }

        public void NotifySolverStoreDataFailed(int jobId, string exception) {
            Log.InfoFormat("jobId:{0} TrafficManager notified solver failed: refcount -1", jobId);
            ISAMSModel model = new ISAMSModel(new ConnectionProvider(), new JobReader { SqlWrapper = sqlWrapper }, new JobTypeReader { SqlWrapper = sqlWrapper }, new JobWriter { SqlWrapper = sqlWrapper });
            var job = model.LoadJob(jobId);
            model.Job.StoreResultsEndTime = DateTime.Now;
            model.Job.StoreResultsMessage = exception;
            model.Job.JobState = JobState.Failed;
            model.Job.ProcessEndTime = DateTime.Now;
            // model.Job.Report = "<a href=\"http://isamsi1/I2ES/Reports/report.docx\">Job Report</a>";
            model.SaveChanges();
        }

        private async Task signalStoreData(Job job, string solverOutputFolderPath) {
            try {
                string url = Format(
                    job.SolverNode + "/StoreResults/StartStoreResults?jobId={0}&solverOutputFolderPath={1}",
                    job.JobId, HttpUtility.UrlEncode(solverOutputFolderPath));
                Log.Info("signalSolver StoreResults Http:" + url);
//                await sendHTTPMessage(url);

                httpClient = new HttpClient();
                httpClient.MaxResponseContentBufferSize = 256000;
                httpClient.DefaultRequestHeaders.Add("user-agent",
                    "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)");

                HttpResponseMessage response = await httpClient.GetAsync(url);
            }
            catch (Exception ex) {
                Log.Info(ex);
            }
        }



        private async Task sendHTTPMessage(string url) {
            try {
                httpClient = new HttpClient();
                httpClient.MaxResponseContentBufferSize = 256000;
                httpClient.DefaultRequestHeaders.Add("user-agent",
                    "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)");
                HttpResponseMessage response = await httpClient.GetAsync(url);
            }
            catch (Exception e) {
                Log.Error(e);
            }
//            response.EnsureSuccessStatusCode();
        }
    }

}