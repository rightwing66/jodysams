﻿namespace I2ES.TrafficManager {
    public class InitializeService {
        private static readonly log4net.ILog log =
        log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static void AppInitialize() {
            Logging logging = new Logging();
            logging.WireUpLogging();
            log.Info("Initialize TrafficManager service");
        }
    }
}