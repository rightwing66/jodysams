﻿namespace I2ES.TrafficManager {
    [GlobalErrorBehaviorAttribute(typeof(GlobalErrorHandler))]
    public class TrafficManagerService : ITrafficManagerService {
        private static readonly log4net.ILog Log =
        log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void SubmitJob(int jobId, string url) {
            Log.InfoFormat("jobId:{0}  Server:{1}",   jobId, url);
            TrafficManager trafficManager = new TrafficManager();
            trafficManager.SubmitJobToSolver(jobId, url);
        }


        public void NotifySolverComplete(int jobId, string url, string solverOutputFolderPath) {
            Log.InfoFormat("jobId:{0} Server:{1} solverOutputFolderpath:{2}", jobId, url,solverOutputFolderPath);
            TrafficManager trafficManager = new TrafficManager();
            trafficManager.NotifySolverComplete(jobId, solverOutputFolderPath);
        }

        public void NotifySolverStoreDataFailed(int jobId, string url,  string exception) {
            Log.InfoFormat("jobId:{0}, Server:{1} Solver/StoreData failed\r\n{2}", jobId,url, exception);
            TrafficManager trafficManager = new TrafficManager();
            trafficManager.NotifySolverStoreDataFailed(jobId, exception);
        }

        public void NotifyStoreResultsComplete(int jobId, string url) {
            Log.InfoFormat("jobId:{0}, server:{1}",jobId, url);
            TrafficManager trafficManager = new TrafficManager();
            trafficManager.NotifyStoreResultsComplete(jobId, url);
        }

        public void StressReportReady(int jobId, string url, string reportUrl) {
            Log.InfoFormat("jobId: {0}  server:{1}", jobId);
            TrafficManager trafficManager = new TrafficManager();
            trafficManager.StressReportReady(jobId, reportUrl);
        }

        public string Ping() {

            Log.Info("Ping called .");
            return "Up";
        }


    }
}
