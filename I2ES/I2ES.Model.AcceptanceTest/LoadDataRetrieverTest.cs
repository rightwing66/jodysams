﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using NUnit.Framework;

namespace I2ES.Model.AcceptanceTest {
    [TestFixture]
    [Category("DataDependent")]
    public class LoadDataRetrieverTest {
        [TestFixtureSetUp]
        public void TestFixtureSetup() {

        }

        [TestFixtureTearDown]
        public void TestFixtureTearDown() {

        }

        [Test]
        public void Exists() {
            var retriever = new LoadDataRetriever.LoadDataRetriever();
            Assert.That(retriever, Is.Not.Null);
        }
        [Test]
        public void AcceptsParameters() {
            var retriever = new LoadDataRetriever.LoadDataRetriever();
            const int elementId = 1001;
            const int jobId = 193;
            const int staticLoadCase = 82;
            const int fatigueLoadCase = 23;
            const string location = "NoseBarrel";
            var connectionString = ConfigurationManager.ConnectionStrings["ESRDConnectionString"].ConnectionString;
            var connection = new SqlConnection(connectionString);

            connection.Open();
            retriever.LoadData(elementId, jobId, location, new SqlWrapper(connection), staticLoadCase, fatigueLoadCase);
            connection.Dispose();
            Assert.That(retriever.CompletionState, Is.EqualTo(LoadDataRetriever.LoadDataRetriever.CompletionStateEnum.Success));

            connection = new SqlConnection(connectionString);
            connection.Open();
            retriever.LoadData(elementId, jobId, location, new SqlWrapper(connection), staticLoadCase, 0);
            connection.Dispose();
            Assert.That(retriever.CompletionState, Is.EqualTo(LoadDataRetriever.LoadDataRetriever.CompletionStateEnum.Success));

            connection = new SqlConnection(connectionString);
            connection.Open();
            retriever.LoadData(elementId, jobId, location, new SqlWrapper(connection), 0, fatigueLoadCase);
            connection.Dispose();
            Assert.That(retriever.CompletionState, Is.EqualTo(LoadDataRetriever.LoadDataRetriever.CompletionStateEnum.Success));

            connection = new SqlConnection(connectionString);
            connection.Open();
            retriever.LoadData(elementId, jobId, location, new SqlWrapper(connection), 0, 0);
            connection.Dispose();
            Assert.That(retriever.CompletionState, Is.EqualTo(LoadDataRetriever.LoadDataRetriever.CompletionStateEnum.Success));
        }

        [Test]
        [Category("DataDependent")]
        public void DeterminesRodOrShell() {
            var retriever = new LoadDataRetriever.LoadDataRetriever();
            const int shellElementId = 1001;
            const int rodElementId = 1019;
            const int jobId = 193;
            const int staticLoadCase = 82;
            const int fatigueLoadCase = 23;
            const string location = "NoseBarrel";
            var connectionString = ConfigurationManager.ConnectionStrings["ESRDConnectionString"].ConnectionString;
            var connection = new SqlConnection(connectionString);

            connection.Open();
            retriever.LoadData(shellElementId, jobId, location, new SqlWrapper(connection), staticLoadCase, fatigueLoadCase);
            connection.Dispose();
            Assert.That(retriever.CompletionState, Is.EqualTo(LoadDataRetriever.LoadDataRetriever.CompletionStateEnum.Success));

            connection = new SqlConnection(connectionString);
            connection.Open();
            retriever.LoadData(rodElementId, jobId, location, new SqlWrapper(connection), staticLoadCase, fatigueLoadCase);
            connection.Dispose();
            Assert.That(retriever.CompletionState, Is.EqualTo(LoadDataRetriever.LoadDataRetriever.CompletionStateEnum.Success));
        }

        [Test]
        [Category("DataDependent")]
        [Ignore]
        public void PullsLoadData() {
            const int elementId = 1019;
            const int jobId = 193;
            const int staticLoadCase = 82;
            const string location = "NoseBarrel";
            var retriever = new LoadDataRetriever.LoadDataRetriever();
            var connectionString = ConfigurationManager.ConnectionStrings["ESRDConnectionString"].ConnectionString;
            var connection = new SqlConnection(connectionString);
            var filled = false;
            connection.Open();
            retriever.LoadData(elementId, jobId, location, new SqlWrapper(connection), staticLoadCase, 0);
            connection.Dispose();
            var command = new SqlCommand();
            var command2 = new SqlCommand();
            connection = new SqlConnection(connectionString);
            command.Connection = connection;
            command2.Connection = connection;
            command.CommandText = "SELECT * FROM F18GlobalLoads.dbo.StaticOutputLoadData";
            command2.CommandText = "SELECT * FROM F18GlobalLoads.dbo.FatigueOutputLoadData";
            connection.Open();
            var reader = command.ExecuteReader();
            while (reader.Read()) {
                if (reader["JobId"].ToString() == jobId.ToString()) {
                    reader.Close();
                    var reader2 = command.ExecuteReader();
                    while (reader2.Read()) {
                        if (reader2["JobId"].ToString() == jobId.ToString()) {
                            filled = true;
                            break;
                        }
                    }
                    break;
                }
            }

            connection.Dispose();
            Assert.That(retriever.CompletionState, Is.EqualTo(LoadDataRetriever.LoadDataRetriever.CompletionStateEnum.Success));
            Assert.That(filled);
        }

        [Test]
        [Category("DataDependent")]
        [Ignore]
        public void PullsMaxMinLoadData() {
            const int elementId = 1019;
            const int jobId = 193;
            const string location = "NoseBarrel";
            var retriever = new LoadDataRetriever.LoadDataRetriever();
            var connectionString = ConfigurationManager.ConnectionStrings["ESRDConnectionString"].ConnectionString;
            var connection = new SqlConnection(connectionString);
            var filled = false;
            connection.Open();
            retriever.LoadData(elementId, jobId, location, new SqlWrapper(connection), 0, 0);
            connection.Dispose();
            var command = new SqlCommand();
            var command2 = new SqlCommand();
            connection = new SqlConnection(connectionString);
            command.Connection = connection;
            command2.Connection = connection;
            command.CommandText = "SELECT * FROM F18GlobalLoads.dbo.StaticMaxMin";
            command2.CommandText = "SELECT * FROM F18GlobalLoads.dbo.FatigueMaxMin";
            connection.Open();
            var reader = command.ExecuteReader();
            while (reader.Read()) {
                if (reader["JobId"].ToString() == jobId.ToString()) {
                    reader.Close();
                    var reader2 = command.ExecuteReader();
                    while (reader2.Read()) {
                        if (reader2["JobId"].ToString() == jobId.ToString()) {
                            filled = true;
                            break;
                        }
                    }
                    break;
                }
            }

            connection.Dispose();
            Assert.That(retriever.CompletionState, Is.EqualTo(LoadDataRetriever.LoadDataRetriever.CompletionStateEnum.Success));
            Assert.That(filled);
        }

        [Test]
        [Category("DataDependent")]
        [Ignore]
        public void ReplacesDataByJobId() {
            const int elementId = 19520;
            const int jobId = 193;
            var initElemIdList = new List<string>();
            var finalElemIdList = new List<string>();
            const string location = "ForwardNoseBarrelLEX";
            var connectionString = ConfigurationManager.ConnectionStrings["ESRDConnectionString"].ConnectionString;
            var connection = new SqlConnection(connectionString);
            var retriever = new LoadDataRetriever.LoadDataRetriever();
            connection.Open();
            var command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "IF EXISTS (SELECT * FROM StaticLoadData WHERE '193' = JobId)"
                                 + " SELECT TOP 1 EID FROM StaticLoadData WHERE '193' = JobId";
            var reader = command.ExecuteReader();
            while (reader.Read()) {
                initElemIdList.Add(reader.GetInt32(0).ToString());
            }
            connection.Close();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "IF EXISTS (SELECT * FROM StaticMaxMin WHERE '193' = JobId)"
                                 + " SELECT TOP 1 ElementId FROM StaticMaxMin WHERE '193' = JobId";
            reader = command.ExecuteReader();
            while (reader.Read()) {
                initElemIdList.Add(reader.GetInt32(0).ToString());
            }
            connection.Close();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "IF EXISTS (SELECT * FROM FatigueLoadData WHERE '193' = JobId)"
                                 + " SELECT TOP 1 EID FROM FatigueLoadData WHERE '193' = JobId";
            reader = command.ExecuteReader();
            while (reader.Read()) {
                initElemIdList.Add(reader.GetInt32(0).ToString());
            }
            connection.Close();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "IF EXISTS (SELECT * FROM FatigueMaxMin WHERE '193' = JobId)"
                                + " SELECT TOP 1 ElementId FROM FatigueMaxMin WHERE '193' = JobId";
            reader = command.ExecuteReader();
            while (reader.Read()) {
                initElemIdList.Add(reader.GetInt32(0).ToString());
            }
            connection.Close();

            connection.Open();
            retriever.LoadData(elementId, jobId, location, new SqlWrapper(connection), 0, 0);
            connection.Dispose();
            connection = new SqlConnection(connectionString);
            connection.Open();
            command.Connection = connection;
            command.CommandText = "IF EXISTS (SELECT * FROM StaticLoadData WHERE '193' = JobId)"
                                 + " SELECT TOP 1 EID FROM StaticLoadData WHERE '193' = JobId";
            reader = command.ExecuteReader();
            while (reader.Read()) {
                finalElemIdList.Add(reader.GetInt32(0).ToString());
            }
            connection.Close();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "IF EXISTS (SELECT * FROM StaticMaxMin WHERE '193' = JobId)"
                                 + " SELECT TOP 1 ElementId FROM StaticMaxMin WHERE '193' = JobId";
            reader = command.ExecuteReader();
            while (reader.Read()) {
                finalElemIdList.Add(reader.GetInt32(0).ToString());
            }
            connection.Close();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "IF EXISTS (SELECT * FROM FatigueLoadData WHERE '193' = JobId)"
                                 + " SELECT TOP 1 EID FROM FatigueLoadData WHERE '193' = JobId";
            reader = command.ExecuteReader();
            while (reader.Read()) {
                finalElemIdList.Add(reader.GetInt32(0).ToString());
            }
            connection.Close();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "IF EXISTS (SELECT * FROM FatigueMaxMin WHERE '193' = JobId)"
                                + " SELECT TOP 1 ElementId FROM FatigueMaxMin WHERE '193' = JobId";
            reader = command.ExecuteReader();
            while (reader.Read()) {
                finalElemIdList.Add(reader.GetInt32(0).ToString());
            }
            connection.Close();
            connection.Dispose();
            int i = 0;
            foreach (var elemId in initElemIdList) {
                Assert.AreNotEqual(elemId, finalElemIdList[i]);
                i++;
            }
        }

        [Test]
        [Category("DataDependent")]
        public void DeletesRowsByJobId() {
            const int jobId = 193;
            var connectionString = ConfigurationManager.ConnectionStrings["ESRDConnectionString"].ConnectionString;
            var connection = new SqlConnection(connectionString);
            var utilties = new CompileUtilities { Namespace = "I2ES.Model.sql" };
            var retriever = new LoadDataRetriever.LoadDataRetriever();
            connection.Open();
            var sqlWrapper = new SqlWrapper(connection);
            retriever.DeleteRowsForJobId(jobId, sqlWrapper, utilties);
            connection.Close();
            connection.Open();
            var command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "SELECT * FROM ISAMS.dbo.StaticLoadData WHERE JobId = 193" +
                                  "\nSELECT * FROM ISAMS.dbo.FatigueLoadData WHERE JobId = 193" +
                                  "\nSELECT * FROM ISAMS.dbo.StaticMaxMin WHERE JobId = 193" +
                                  "\nSELECT * FROM ISAMS.dbo.FatigueMaxMin WHERE JobId = 193";
            var reader = command.ExecuteReader();
            Assert.That(retriever.CompletionState, Is.EqualTo(LoadDataRetriever.LoadDataRetriever.CompletionStateEnum.Success));
            Assert.That(reader, Is.Empty);
            connection.Dispose();
        }
    }
}