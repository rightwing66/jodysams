﻿using System.Diagnostics;
using System.Security.Policy;
using I2ES.Model.DomainModel;
using I2ES.Model.DomainModelContext;
using NUnit.Framework;
using I2ES.UnitTest.Extensions;

namespace I2ES.Model.AcceptanceTest{
    [TestFixture]
    public class LoadDataModelTest{
        [Test]
        public void LoadLoadData(){
            ConnectionProvider connectionProvider = new ConnectionProvider();
            ISqlWrapper sqlWrapper = connectionProvider.CreateWrapper();
            ILoadDataRetriever einstein = new LoadDataRetriever.LoadDataRetriever();
            //eid 19520 - only element in the data!

            /* Locations:
               'Aft', 'Center', 'Forward', 'LEX', 'NoseBarrel', 'Wing', 'ForwardCenter', 
               'ForwardNoseBarrel', 'ForwardLEX', 'CenterLEX', 'CenterForwardNoseBarrel', 'ForwardCenterLEX'
             */
            einstein.LoadData(19520, 351, "ForwardNoseBarrel", connectionProvider.CreateWrapper(), 0, 1);

            sqlWrapper = connectionProvider.CreateWrapper();

            StaticLoadDataReader reader = new StaticLoadDataReader(sqlWrapper);
            var staticLoad = reader.ReadAllElements(351);
            staticLoad.WriteToTrace("Static LoadData");
            
            FatigueLoadDataReader fatigueReader = new FatigueLoadDataReader(sqlWrapper);
            var fatigueResults = fatigueReader.ReadAllElements(351);

            var staticMaxMinReader = new StaticMaxMinReader(sqlWrapper);
            var staticMaxMinResults = staticMaxMinReader.ReadAllElements(351);

            var fatigueMaxMinReader = new FatigueMaxMinReader(sqlWrapper);
            var fatigueMaxMinResults = fatigueMaxMinReader.ReadAllElements(351);
            
            Trace.Flush();
        }

        public void CompleteCallBack(int jobId){
            
        }
    }
}