﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;

namespace I2ES.UnitTest.Extensions{
    public static  class CollectionTraceWriterExtensions{
        public static void WriteToTrace<T>(this List<T> items, string collectionName){
            Trace.WriteLine(collectionName);
            foreach (var item in items){
                foreach (PropertyInfo info in item.GetType().GetProperties()){
                    string line = string.Format("{0}:{1} ", info.Name, info.GetValue(item, null));
                    Trace.Write(line);
                }
                Trace.WriteLine("");
            }
            Trace.WriteLine("");
        }
    }
}