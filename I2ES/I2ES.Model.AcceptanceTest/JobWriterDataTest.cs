﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using I2ES.Model.DomainModel;
using I2ES.Model.DomainModelContext;
using NUnit.Framework;

namespace I2ES.Model.AcceptanceTest {
    [Category("DataDependent")]
    [TestFixture]
    public class JobWriterDataTest {
        private SqlConnection sqlConnection;
        private SqlWrapper sqlWrapper;
        private SqlTransaction sqlTransaction;

        [TestFixtureSetUp]
        public void TestFixtureSetup() {
            var connectionString = ConfigurationManager.ConnectionStrings["ESRDConnectionString"].ConnectionString;
            sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            sqlTransaction = sqlConnection.BeginTransaction();
            sqlWrapper = new SqlWrapper(sqlConnection){Transaction = sqlTransaction};
//            sqlWrapper = new SqlWrapper(sqlConnection);
        }

        [TestFixtureTearDown]
        public void TearDown() {
            sqlTransaction.Rollback();
            sqlWrapper.Dispose();
        }

        [Test]
        [Ignore]
        public void doit(){
            JobWriter jobWriter = new JobWriter{SqlWrapper = sqlWrapper};
            Job job = jobWriter.CreateJob(6, "jbreshears");
            Console.WriteLine(job.JobId);
            job.JobState = JobState.New;
            job.SubmittingNode = "UnitTestSubmitNode";
            job.CreateTime = new DateTime(2015, 4, 29, 12, 42, 53);
            job.ProcessBeginTime = new DateTime(2015, 4, 29, 12, 43, 15);
            job.ProcessEndTime = new DateTime(2015, 4, 29, 12, 43, 15);
            job.SolverState = BackEndProcessState.InProgress;
            job.SolverNode = "UnitTestSolverNode";
            job.SolverBeginTime = new DateTime(2015, 4, 29, 12, 44, 00);
            job.SolverEndTime = new DateTime(2015, 4, 29, 12, 45, 00);
            job.SolverMessage = null;
            job.LoadDataRetrieverState = BackEndProcessState.InProgress;
            job.LoadDataRetrieverBeginTime = new DateTime(2015, 4, 29, 12, 46, 00);
            job.LoadDataRetrieverEndTime = new DateTime(2015, 4, 29, 12, 47, 00);
            job.LoadDataRetrieverMessage = null;
            job.StoreResultsState = BackEndProcessState.InProgress;
            job.StoreResultsBeginTime = new DateTime(2015, 4, 29, 12, 48, 00);
            job.StoreResultsEndTime = new DateTime(2015, 4, 29, 12, 49, 00);
            job.StoreResultsMessage = null;
            job.TrueNumberState = BackEndProcessState.InProgress;
            job.TrueNumberBeginTime = new DateTime(2015, 4, 29, 12, 50, 00);
            job.TrueNumberEndTime = new DateTime(2015, 4, 29, 12, 51, 00);
            job.TrueNumberMessage = null;
            job.REINumber = "REI-2349077234821734908";
            job.BUNO = "2342134-1234-1234234-1";
            job.PartNumber = "B-23412-3453452345234-3";
            job.StructureType = StructureType.Metal;
            job.AircraftModel = AircraftModel.FA18C;

            jobWriter.SaveChanges(job);
        }

        [Test]
        public void LoadsAllFieldsFromDatabase() {
            JobWriter jobWriter = new JobWriter { SqlWrapper = sqlWrapper };
            Job job = jobWriter.CreateJob(6, "jbreshears");
            Console.WriteLine(job.JobId);
            job.JobState = JobState.New;
            job.SubmittingNode = "UnitTestSubmitNode";
            job.CreateTime = new DateTime(2015, 4, 29, 12, 42, 53);
            job.EditTime = new DateTime(2015, 5, 29, 12, 42, 53);
            job.ProcessBeginTime = new DateTime(2015, 4, 29, 12, 43, 15);
            job.ProcessEndTime = new DateTime(2015, 4, 29, 12, 43, 15);
            job.SolverState = BackEndProcessState.InProgress;
            job.SolverNode = "UnitTestSolverNode";
            job.SolverBeginTime = new DateTime(2015, 4, 29, 12, 44, 00);
            job.SolverEndTime = new DateTime(2015, 4, 29, 12, 45, 00);
            job.SolverMessage = null;
            job.LoadDataRetrieverState = BackEndProcessState.InProgress;
            job.LoadDataRetrieverBeginTime = new DateTime(2015, 4, 29, 12, 46, 00);
            job.LoadDataRetrieverEndTime = new DateTime(2015, 4, 29, 12, 47, 00);
            job.LoadDataRetrieverMessage = null;
            job.StoreResultsState = BackEndProcessState.InProgress;
            job.StoreResultsBeginTime = new DateTime(2015, 4, 29, 12, 48, 00);
            job.StoreResultsEndTime = new DateTime(2015, 4, 29, 12, 49, 00);
            job.StoreResultsMessage = null;
            job.TrueNumberState = BackEndProcessState.InProgress;
            job.TrueNumberBeginTime = new DateTime(2015, 4, 29, 12, 50, 00);
            job.TrueNumberEndTime = new DateTime(2015, 4, 29, 12, 51, 00);
            job.TrueNumberMessage = null;
            job.REINumber = "REI-2349077234821734908";
            job.BUNO = "2342134-1234-1234234-1";
            job.PartNumber = "B-23412-3453452345234-3";
            job.StructureType = StructureType.Metal;
            job.AircraftModel = AircraftModel.FA18C;

            jobWriter.SaveChanges(job);
            IJobReader jobReader = new JobReader{SqlWrapper = sqlWrapper};
            Job newJob = jobReader.ReadSingleJob(job.JobId);


            Assert.That(newJob.CreateTime, Is.EqualTo(new DateTime(2015, 4, 29, 12, 42, 53)));
            Assert.That(newJob.EditTime, Is.EqualTo(new DateTime(2015, 5, 29, 12, 42, 53)));
            Assert.That(newJob.SolverState, Is.EqualTo(BackEndProcessState.InProgress));
            Assert.That(newJob.SolverNode, Is.EqualTo("UnitTestSolverNode"));
            Assert.That(newJob.SolverBeginTime, Is.EqualTo(new DateTime(2015, 4, 29, 12, 44, 00)));
            Assert.That(newJob.SolverEndTime, Is.EqualTo(new DateTime(2015, 4, 29, 12, 45, 00)));
            Assert.That(newJob.SolverMessage, Is.Empty);
            Assert.That(newJob.LoadDataRetrieverState ,Is.EqualTo(BackEndProcessState.InProgress));
            Assert.That(newJob.LoadDataRetrieverBeginTime, Is.EqualTo(new DateTime(2015, 4, 29, 12, 46, 00)));
            Assert.That(newJob.LoadDataRetrieverEndTime, Is.EqualTo(new DateTime(2015, 4, 29, 12, 47, 00)));
            Assert.That(newJob.LoadDataRetrieverMessage, Is.Empty);
            Assert.That(newJob.StoreResultsState, Is.EqualTo(BackEndProcessState.InProgress));
            Assert.That(newJob.StoreResultsBeginTime, Is.EqualTo(new DateTime(2015, 4, 29, 12, 48, 00)));
            Assert.That(newJob.StoreResultsEndTime, Is.EqualTo(new DateTime(2015, 4, 29, 12, 49, 00)));
            Assert.That(newJob.StoreResultsMessage, Is.Empty);
            Assert.That(newJob.TrueNumberState, Is.EqualTo(BackEndProcessState.InProgress));
            Assert.That(newJob.TrueNumberBeginTime, Is.EqualTo(new DateTime(2015, 4, 29, 12, 50, 00)));
            Assert.That(newJob.TrueNumberEndTime, Is.EqualTo(new DateTime(2015, 4, 29, 12, 51, 00)));
            Assert.That(newJob.TrueNumberMessage, Is.Empty);
            Assert.That(newJob.REINumber,Is.EqualTo("REI-2349077234821734908"));
            Assert.That(newJob.BUNO,Is.EqualTo("2342134-1234-1234234-1"));
            Assert.That(newJob.PartNumber,Is.EqualTo("B-23412-3453452345234-3"));
            Assert.That(newJob.StructureType,Is.EqualTo(StructureType.Metal));
            Assert.That(newJob.AircraftModel ,Is.EqualTo(AircraftModel.FA18C));
        }

        [Test]
        public void LoadsASingleJob(){
            IJobReader jobReader = new JobReader { SqlWrapper = sqlWrapper };
            Job j = jobReader.ReadSingleJob(353);
        }
    }
}