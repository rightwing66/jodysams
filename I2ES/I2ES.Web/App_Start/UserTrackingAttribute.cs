﻿using System.Web.Mvc;
using System.Web.Mvc.Filters;
using I2ES.Model;

namespace I2ES.Web {
    public class UserTrackingAttribute : ActionFilterAttribute, IAuthenticationFilter {
        public void OnAuthentication(AuthenticationContext filterContext) {
        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext) {
            var user = filterContext?.HttpContext?.User;
            if (user == null || user.Identity.IsAuthenticated == false) {
                return;
            }
            bool? isAlreadyTracked = filterContext.HttpContext.Session?["HasBeenTracked"] as bool?;
            if (isAlreadyTracked != null && isAlreadyTracked.Value) {
                return;
            }
            // ReSharper disable once PossibleNullReferenceException
            filterContext.HttpContext.Session["HasBeenTracked"] = true;
            ConnectionProvider connectionProvider = new ConnectionProvider();
            var wrapper = connectionProvider.CreateWrapper();
            string sql = "insert[Access](AccessDate, UserId) values(GetDate(), '~userID')".Replace("~userID", user.Identity.Name);
            wrapper.RunSql(sql);
        }
    }
}