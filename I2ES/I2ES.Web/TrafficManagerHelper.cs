﻿using System.ServiceModel;
using System.ServiceModel.Channels;
using I2ES.Model;
using I2ES.Web.TrafficManagerService;
//using I2ES.Web.Service_References.TrafficManager;
using Ninject.Planning.Bindings;
using Binding = System.Web.Services.Description.Binding;

namespace I2ES.Web {
    public static class TrafficManagerHelper {
        public static TrafficManagerServiceClient GetReference(string requestUrl=null) {
            ConfigurationWrapper wrapper = new ConfigurationWrapper();
            string url = requestUrl?? wrapper.TrafficManagerUrl;
            EndpointAddress address = new EndpointAddress(url);
            System.Web.Services.Description.Binding binding = new System.Web.Services.Description.Binding();
            TrafficManagerServiceClient trafficManager = new TrafficManagerServiceClient(new Binding(),new EndpointAddress(url));
            return trafficManager;
        }
    }
}