﻿/// <reference path="VectorDisplay.js" />
var $subViewData;
var $firstDiv = "tempDivId1";
var $secondDiv = "tempDivId2";
var $currentDiv;
var $newDiv;
var $lastButton;
var $buttons = new Object();
var $lastJobEntrySubNavSelected;
var $lastAppliedLoadsSubNavSelected;


$(document).ready(function () {
    $.ajax({
        dataType: "json",
        method: "post",
        url: "/i2es/JobEntry/SubViews",
        data: { "jobId": $("#JobIDDisplay").html() },
        success: recieveSubviews
    });
});

function ActivateTab(tab) {
    $('.nav-tabs a[href="#GeometrySubView' + tab + '"]').tab('show');
};

function recieveSubviews(data) {
    $subViewData = data;

    $.each($(".subViewButton"), function (index, button) {
        $buttons[button.id] = new Object();
        $buttons[button.id].button = button;
        $buttons[button.id].subViewName = button.id.substring(6, button.id.length);
        $("#" + button.id).click(function () {
            $("#AppliedLoadsSubNav").removeClass("AppliedLoadsButtonRowHidden").addClass("AppliedLoadsButtonRowVisible");
            switchSubViews("#body", $buttons[button.id].subViewName);
        });
    });
    $("#buttonAppliedLoads").click(function () {
        $("#AppliedLoadsSubNav").removeClass("ButtonRowHidden").addClass("AppliedLoadsButtonRowVisible");
        $("#buttonFatigueBypassSubview").click();
    });
    //buttons that hide the second row
    $(".hideSecondRow").click(function () {
        $("#AppliedLoadsSubNav").removeClass("AppliedLoadsButtonRowVisible").addClass("AppliedLoadsButtonRowHidden");
        switchSubViews("#body", $buttons[$(this).attr("id")].subViewName);
    });
    wireUpSaveByField("JobEntryBasicInformation");
    wireUpJobSaveByField();

    $("#JobeEntrySubmitButton").click(function () {
        //        $.post("/i2es/JobEntry/Submit", { "jobId": $("#JobIDDisplay").html() });
        $.ajax({
            type: "Post",
            url: "/i2es/JobEntry/Submit",
            data: {
                "jobId": $("#JobIDDisplay").html()
            },
            success: function () { window.location.replace("/i2es"); }
        });
    });

    //wire up tab selected behaivor
    $($("a", ".JobEntrySubNav")).click(function (event) { handleJobEntrySubNavSelected(event.target, $lastJobEntrySubNavSelected, "JobEntrySubNavSelected", 'subNav') });
    $($("a", ".AppliedLoadsSubNav")).click(function (event) { handleJobEntrySubNavSelected(event.target, $lastAppliedLoadsSubNavSelected, "AppliedLoadsSubNavSelected", 'appliedLoads') });
    $lastJobEntrySubNavSelected = $(".JobEntrySubNavSelected").first();
    $lastAppliedLoadsSubNavSelected = $(".AppliedLoadsSubNavSelected").first();

    //set initial view to Geometry
    switchSubViews("#body", Object.keys($subViewData.SubViews)[0]);
    //hide second row of buttons
    $("#AppliedLoadsSubNav").addClass("AppliedLoadsButtonRowHidden");

    setupReactionVectors();
}



function handleJobEntrySubNavSelected(newButton, lastButton, selectedClass, level) {
    $(newButton).addClass(selectedClass);
    $(lastButton).removeClass(selectedClass);
    switch (level) {
        case "subNav":
            $lastJobEntrySubNavSelected = newButton;
            break;
        case "appliedLoads":
            $lastAppliedLoadsSubNavSelected = newButton;
            break;

    }
}

function wireUpJobSaveByField() {

    $(".JobInput").change(function () {
        var value = $(this).val();
        if ($(this).attr("id") === "AircraftModel") {
            value = this[this.selectedIndex].id;
        }
        $.ajax({
            type: "Post",
            url: "/i2es/JobEntry/SaveJobValue",
            data: {
                "jobId": $("#JobIDDisplay").html(),
                "parameterName": $(this).attr("id"),
                "parameterValue": value
            }
        });
    });
}

function wireUpSaveByField(contextSelector) {

    $(".JobEntryInput").change(function () {
        $.ajax({
            type: "POST",
            url: "/i2es/JobEntry/SaveValue",
            data: {
                "jobId": $("#JobIDDisplay").html(),
                "parameterName": $(this).attr("id"),
                "parameterValue": $(this).val()
            },
            dataType: 'html'
        });
    });
}

function switchSubViews(target, subview) {

    if ($currentDiv === $secondDiv) {
        $newDiv = $firstDiv;
    } else {
        $currentDiv = $firstDiv;
        $newDiv = $secondDiv;
    }
    $("#" + $currentDiv).hide();
    $(target).append($("<div>", { id: $newDiv, class: "subViewDivs" }));
    $("#" + $newDiv).append($subViewData.SubViews[subview]);
    //inject saved values into the view we are loading
    $(".JobEntryInput", "#" + $newDiv).each(function (index, element) {
        var i = $subViewData.Parameters[element.id];
        $(element).val(i);
    });
    //and then wire up the change event to save individual changes
    wireUpSaveByField("SubViewDiv");
    //save the previous div
    if ($lastButton) {
        $(".JobEntryInput", "#" + $currentDiv).each(function (index, value) {
            var x = value.id;
            $subViewData.Parameters[value.id] = value.value;
        });
    }
    $("#" + $currentDiv).remove();

    if ($currentDiv === $secondDiv) {
        $currentDiv = $firstDiv;
    } else {
        $currentDiv = $secondDiv;
    }
    $lastButton = subview;
    setupReactionVectors();
}

function setupReactionVectors() {
    //collect up parameters here
    var plateWidth = $subViewData.Parameters["#printParameters_W"];
    var byp3 = $subViewData.Parameters["#fatigueLoad_BYP3"];
    var plateHeight = $subViewData.Parameters["#printParameters_H"];


    //apply them here
    $("#fatigueLoad_BYP3").change(function() {
        $("fatigueLoad_BYP3_1").val(byp3 * plateHeight / plateWidth);
        $("fatigueLoad_BYP3_2").val(byp3 * plateHeight / plateWidth);
        $("fatigueLoad_BYP3_3").val(byp3);
    });
}