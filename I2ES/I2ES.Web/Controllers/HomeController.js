﻿$(document).ready(function () {
    window.jobHistoryButtonState = "new";
    $("#FA18PlatformNewJobsButton").button().click(function () {
        window.jobHistoryButtonState = "new";
        loadJobList(jobHistoryButtonState);
    });
    $("#FA18PlatformSubmittedJobsButton").button().click(function () {
        window.jobHistoryButtonState = "submitted";
        loadJobList(jobHistoryButtonState);
    });
    $(".JobIdLink").button();

    loadJobList(jobHistoryButtonState);

    loadJobNavList(jobHistoryButtonState);

    $("#AlertiSAMSPlatform").colorbox({
        html: "<div style=padding:30px;><h3>iSAMS Main Platform Page Coming Soon</h3><p>This is a link to the main Platform page where the user<br/>would choose their Airframe and Program.</p></div>"
    });
    $("#AlertSearchButton").colorbox({
        html: "<div style=padding:30px;><h3>Search Functionality Coming Soon</h3><p>After selecting the criteria to search by, the user<br/>will fill in their appropriate REI, BUNO or Part number.</p></div>"
    });
    $("#AlertAdvancedSearch").colorbox({
        html: "<div style=padding:30px;><h3>Advanced Search Coming Soon</h3><p>For users with numerous submitted jobs, they will be<br/>able to make more granular searches to find exactly what they need.</p></div>"
    });
    $("#AlertJobCentral").colorbox({
        html: "<div style=padding:30px;><h3>Job Central Coming Soon</h3><p>Job Central will be a place to see all of the jobs you<br/>have submitted, with tools to filter and sort the data.</p></div>"
    });
    $("#AlertFAQ").colorbox({
        html: "<div style=padding:30px;><h3>FAQ and Instructions Coming Soon</h3><p>The FAQ page will feature detailed instructions for creating<br/>and editing jobs using iSAMS. As we get questions about how to use<br/>the system, we will answer them here as well.</p></div>"
    });
});


function recieveJobList(data, textStatus, jqXHR) {
    $('#FA18PlatformJobList').html(data);
    wireUpEditButtons();
};

function loadJobList(buttonState) {
//    $('#FA18PlatformJobList').load('/I2ES/home/joblist', { "buttonState": buttonState });
    $.ajax({
        dataType: "html",
        method: "post",
        url: "/I2ES/home/joblist",
        data: { "buttonState": buttonState },
        success: recieveJobList
    });
};



function loadJobNavList() {
    $('#FA18PlatformNewJobMenu').load('/I2Es/home/ModelTypeSubmitList');
}

function wireUpEditButtons() {
    $(".JobIdLinksubmitted").click(function () {
        window.location.replace("/i2es/Home/Copy?jobId=" + $(this).html());
    });
    $(".JobIdLinknew").click(function () {
        window.location.replace("/i2es/JobEntry?jobId=" + $(this).html());
    });
};

