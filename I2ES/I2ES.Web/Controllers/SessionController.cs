﻿using System.Web.Mvc;

namespace I2ES.Web.Controllers {
    public class SessionController : Controller {
        // GET: Session
        public ActionResult clear() {
            Session.Abandon();
            return new EmptyResult();
        }
    }
}