﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using I2ES.Model;
using I2ES.Model.DomainModel;
using I2ES.Model.DomainModelContext;
using I2ES.Services.Identity;
using I2ES.Web.Models;
using Ninject;

namespace I2ES.Web.Controllers {
    //    [Authorize(Roles = "esrd\\Domain Users")]

    public class HomeController : Controller {
        [Inject]
        public IConnectionProvider ConnectionProvider { get; set; }

        [Inject]
        public IISAMSModel Model { get; set; }

        [UserTracking]
        public ActionResult Index() {
            return View();
        }

        public ActionResult JobList(string buttonState) {
            IISAMSModel model = new ISAMSModel(new ConnectionProvider(), new JobReader(), new JobTypeReader(),
                new JobWriter());

            model.LoadJobsForUser(ISAMSIdentity.GetUserId(User));
            List<Job> jobs;
            if (buttonState == "new") {
                jobs = model.AllJobs.Where(j => j.JobState == JobState.New).OrderByDescending(j => j.CreateTime).Take(10).ToList();
            }
            else if (buttonState == "submitted") {
                jobs = model.AllJobs.Where(j => j.JobState != JobState.New).OrderByDescending(j => j.CreateTime).Take(10).ToList();
            }
            else {
                return new HttpStatusCodeResult(400, "buttonState must be 'new' or 'submitted'");
            }

            var viewModel = materializeViewModel(jobs, buttonState);
            addEvenOddRowClasses(viewModel);

            return PartialView(viewModel);
        }

        public ActionResult ModelTypeSubmitList() {
            List<ModelTypeViewModel> items = new List<ModelTypeViewModel>();
            return PartialView(items);
        }

        public ActionResult NewJob(int jobTypeId) {
            IISAMSModel model = new ISAMSModel(new ConnectionProvider(), new JobReader(), new JobTypeReader(),
                new JobWriter());
            string userID = ISAMSIdentity.GetUserId(User);
            model.Load(jobTypeId, userID);
            var job = model.CreateJob(userID);
            return RedirectToAction("Index", "JobEntry", new {jobId=job.JobId});
        }

        public ActionResult Copy(int jobId) {
            IISAMSModel model = new ISAMSModel(new ConnectionProvider(), new JobReader(), new JobTypeReader(), new JobWriter());
            Job newJob = model.CopyJobIntoNewJob(jobId, ISAMSIdentity.GetUserId(User));

            return RedirectToAction("Index", "JobEntry", new {jobId = newJob.JobId});
        }

        private static void addEvenOddRowClasses(List<ShortJobListViewModel> viewModel) {
            int index = 0;
            foreach (var item in viewModel) {
                item.Index = index;
                item.EvenOddStyle = index % 2 == 0 ? "class='JobTableEvenRow' " : "class='JobTableOddRow' ";
                index++;
            }
         }

        private List<ShortJobListViewModel> materializeViewModel(List<Job> jobs, string buttonState) {
            var viewModel = jobs.Select(j => {
                var jobTypeId = j.JobTypeId;
                return new ShortJobListViewModel {
                    ButtonClass = "JobIdLink" + buttonState,
                    JobId = j.JobId,
                    Date = j.CreateTime?.ToShortDateString() ?? "",
                    Status = j.JobState,
                    ModelName = j.JobTypeName,
                    ModelGraphicPath = getModelGraphicPath(jobTypeId),
                    StressReportIconPath = @"/I2ES/Images/icon_doc_stress_report.svg",
                    StressReportLinkPath = @"/I2ES/Reports/NotInThisUniverse/StressReport.pdf",
                    CLIReportIconPath = @"/I2ES/Images/icon_doc_cli_form.svg",
                    CLIReportLinkPath = @"/I2ES/Reports/NotInThisUniverse/CLIReport.pdf",
                    SCWReportIconPath = @"/I2ES/Images/icon_doc_scw_file.svg",
                    SCWReportLinkPath = @"/I2ES/Reports/NotInThisUniverse/SCWReport.pdf"
                };
            }).OrderByDescending(v=>v.JobId).ToList();
            return viewModel;
        }

        private string getModelGraphicPath(int? value) {
            switch (value) {
                case 13:
                    return "/I2ES/Images/graphic_single_hole.svg";
                case 10:
                    return "/I2ES/Images/graphic_2d_lug.svg";
                case 14:
                    return "/I2ES/Images/graphic_3d_lug.svg";
                case 12:
                    return "/I2ES/Images/graphic_multiple_hole.svg";
                default:
                    return null;
            }
        }


    }
}