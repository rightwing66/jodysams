using System;
using System.IO;
using System.Web.Mvc;

namespace I2ES.Web.Controllers {
    public static class MVCHelpers {
        public static String RenderViewToString<T>(ControllerContext context, String viewPath, T model) {
            
            context.Controller.ViewData.Model = model;
            using (var sw = new StringWriter()) {
                
                var viewResult = ViewEngines.Engines.FindPartialView(context, viewPath);
                var viewContext = new ViewContext(context, viewResult.View, context.Controller.ViewData, context.Controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(context, viewResult.View);
                var result = sw.GetStringBuilder().ToString();
                return result;
            }
        }
    }
}