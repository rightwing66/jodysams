﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using I2ES.Model;
using I2ES.Model.DomainModel;
using I2ES.Model.DomainModelContext;
using I2ES.Services.Identity;
using I2ES.Web.Models;

namespace I2ES.Web.Controllers {
    public class JobEntryController : Controller {
        private static readonly log4net.ILog Log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public ActionResult Index(int? jobId) {
            IISAMSModel model = new ISAMSModel(new ConnectionProvider(), new JobReader(), new JobTypeReader(), new JobWriter());
            model.LoadJob(jobId ?? 1641);
            return View(model);
        }

        public ActionResult ViewSubview() {
            IISAMSModel model = new ISAMSModel(new ConnectionProvider(), new JobReader(), new JobTypeReader(), new JobWriter());
            model.LoadJob(1641);
            GeometryViewModel geoModel = new GeometryViewModel(model);
            return View("GeometrySubView", geoModel);
        }

        public JsonResult GetSubViews() {
            IISAMSModel model = new ISAMSModel(new ConnectionProvider(), new JobReader(), new JobTypeReader(), new JobWriter());

            string subView1 = MVCHelpers.RenderViewToString<IISAMSModel>(ControllerContext, "GeometrySubView", model);
            //            string subView2 = MVCHelpers.RenderViewToString<IISAMSModel>(ControllerContext, "SubView2", model);
            //            string subView3 = MVCHelpers.RenderViewToString<IISAMSModel>(ControllerContext, "SubView3", model);
            var json = new { subView1 };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public JsonResult SubViews(int jobId) {
            IISAMSModel model = new ISAMSModel(new ConnectionProvider(), new JobReader(), new JobTypeReader(), new JobWriter());
            model.LoadJob(jobId);

            var geometryViewModel = new GeometryViewModel(model);
            string GeometrySubview = MVCHelpers.RenderViewToString(ControllerContext, "GeometrySubView", geometryViewModel);
            string FatigueBypassSubview = MVCHelpers.RenderViewToString(ControllerContext, "FatigueBypassSubview", geometryViewModel);
            string FatigueAxialSubview = MVCHelpers.RenderViewToString(ControllerContext, "FatigueAxialSubView", geometryViewModel);
            string FatigueBearingSubview = MVCHelpers.RenderViewToString(ControllerContext, "FatigueBearingSubView", geometryViewModel);
            string StaticAxialSubview = MVCHelpers.RenderViewToString(ControllerContext, "StaticAxialSubView", geometryViewModel);
            string StaticBearingSubview = MVCHelpers.RenderViewToString(ControllerContext, "StaticBearingSubView", geometryViewModel);
            string StaticBypassSubview = MVCHelpers.RenderViewToString(ControllerContext, "StaticBypassSubview", geometryViewModel);
            string LoadBalanceSubview = MVCHelpers.RenderViewToString(ControllerContext, "LoadBalanceSubView", geometryViewModel);
            var parameters = model.JobType.AllParameters.ToDictionary(s => s.Key, s => s.Value.ParameterValue?.Value);

            var json = new {
                SubViews = new {
                    GeometrySubview, FatigueBypassSubview, FatigueAxialSubview, FatigueBearingSubview, StaticAxialSubview, StaticBearingSubview, StaticBypassSubview, LoadBalanceSubview
                },
                Parameters = parameters
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveJobValue(int jobId, string parameterName, string parameterValue) {
            try {
                IISAMSModel model = new ISAMSModel(new ConnectionProvider(), new JobReader(), new JobTypeReader(),
                    new JobWriter());
                model.LoadJob(jobId);
                switch (parameterName) {
                    case "REI":
                        model.Job.REINumber = parameterValue;
                        break;
                    case "BUNO":
                        model.Job.BUNO = parameterValue;
                        break;
                    case "PART":
                        model.Job.PartNumber = parameterValue;
                        break;
                    case "AircraftModel":
                        model.Job.AircraftModel = (AircraftModel) int.Parse(parameterValue);
                        break;
                }
                model.SaveChanges();
                return new HttpStatusCodeResult(HttpStatusCode.Accepted);
            }
            catch (Exception ) {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "A error has occured.  Check the logs.");
            }
        }

        public ActionResult SaveValue(int jobId, string parameterName, double parameterValue) {
            try {
                IISAMSModel model = new ISAMSModel(new ConnectionProvider(), new JobReader(), new JobTypeReader(), new JobWriter());
                model.WriteParameterValue(jobId, parameterName, parameterValue);
                return new HttpStatusCodeResult(HttpStatusCode.Accepted);
            }
            catch (SqlException ) {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "A sql error has occured.  Check the logs.");
            }
        }

        public ActionResult Submit(int jobId) {
            try {
                var client = new TrafficManagerService.TrafficManagerServiceClient();
                //                var trafficManager = TrafficManagerHelper.GetReference();
                ConfigurationWrapper wrapper = new ConfigurationWrapper();
                client.SubmitJob(jobId, System.Environment.MachineName);
            }
            catch (Exception ex) {
                Log.Info("Exception calling webservice TrafficManager");
                Log.Info(ex);
            }
            return RedirectToAction("Index","Home");
        }

        public ActionResult Edit(int jobId) {
            return RedirectToAction("Index", new { jobId });
        }


    }
}