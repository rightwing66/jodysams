﻿using System.Text.RegularExpressions;

namespace I2ES.Web {

    public static class Extensions {
        public static string AfterTheSlash(this string source) {
            return Regex.Match(source, @"([^\\]+$)").Value;
        }
    }
}