﻿using I2ES.Model.DomainModel;

namespace I2ES.Web.Models {
    public class GeometryViewModel {
        private readonly IISAMSModel model;

        public GeometryViewModel(IISAMSModel model) {
            this.model = model;
        }

        public JobType JobType => model.JobType;
        public Job Job => model.Job;
    }
}