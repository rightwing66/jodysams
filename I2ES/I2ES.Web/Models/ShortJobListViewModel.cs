﻿using System;
using I2ES.Model.DomainModel;

namespace I2ES.Web.Models {
    public class ShortJobListViewModel {
        public int Index { get; set; }
        public int? JobId { get; set; }
        public string ModelName { get; set; }
        public string ModelGraphicPath { get; set; }
        public string Date { get; set; }
        public JobState? Status { get; set; }

        public string StressReportIconPath { get; set; }
        public string StressReportLinkPath { get; set; }
        public string CLIReportIconPath { get; set; }
        public string CLIReportLinkPath { get; set; }
        public string SCWReportIconPath { get; set; }
        public string SCWReportLinkPath { get; set; }
        public string EvenOddStyle { get; set; }
        public string ButtonClass { get; set; }
    }
}