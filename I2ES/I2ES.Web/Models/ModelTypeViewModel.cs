﻿namespace I2ES.Web.Models {
    public class ModelTypeViewModel {
        public string ModelTypeName { get; set; }
        public string ModelTypeImagePath { get; set; }
    }
}