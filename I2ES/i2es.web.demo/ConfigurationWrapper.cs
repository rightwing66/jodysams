﻿using System;
using System.Configuration;

namespace I2ES.Web{
    public class ConfigurationWrapper : IConfigurationWrapper{
        public string NodeName { get { return ConfigurationManager.AppSettings["NodeName"]; } }

        public string TrafficManagerUrl{
            get{
                if (ConfigurationManager.AppSettings["TrafficManagerURL"] == null){
                    throw new Exception("Could not find configuration entry for 'TrafficManagerURL");
                }
                return ConfigurationManager.AppSettings["TrafficManagerURL"];
            }
        }
    }
}