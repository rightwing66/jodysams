﻿$(function() {
    setup();
});

var jobId;
var staticGridCreated = false;
var fatigueGridCreated = false;
var staticMaxMinGridCreated = false;
var fatigueMaxMinGridCreated = false;

function setup() {
    $("#buttonSubmit").button().click(function () { submitLoadDataJob(); });
    $("#selectLocation").selectmenu({width:300});
    jobId = $("#inputJobId").val();
    $("#divDataGrids").css("visibility", "hidden");
}

function submitLoadDataJob() {
    $("#divDataGrids").css("visibility","visible");
    var formData = {
        jobId: jobId,
        elementId: $("#selectElementId").val(),
        location: $("#selectLocation").val(),
        staticLoadCase: $("#inputStaticLoadCase").val(),
        fatigueLoadCase: $("#inputFatigueLoadCase").val()
    };

    $.ajax({
        url: baseURL + "/LoadData/Submit",
        type: "POST",
        data: formData,
        success: function(data, textStatus, jqXHR) {
            buildAllGrids();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Error!!!' + errorThrown + textStatus + jqXHR);
        }
    });
}


function buildAllGrids() {
    if (staticGridCreated) {
        $("#jqGridStatic").trigger("reloadGrid");
    } else {
        buildGrid($("#jqGridStatic"), "#jqGridPagerStatic", 'GetStaticLoad');
        staticGridCreated = true;
    }

    if (fatigueGridCreated) {
        $("#jqGridFatigue").trigger("reloadGrid");
    } else {
        buildGrid($("#jqGridFatigue"), "#jqGridPagerFatigue", 'GetFatigueLoad');
        fatigueGridCreated = true;
    }

    if (staticMaxMinGridCreated) {
        $("#jqGridStaticMinMax").trigger("reloadGrid");
    } else {
        buildMaxMinGrid($("#jqGridStaticMinMax"), "#jqGridPagerStaticMinMax", 'GetStaticMaxMin');
    }
    if (fatigueMaxMinGridCreated) {
        $("#jqGridFatigueMinMax").trigger("reloadGrid");
    } else {
        buildMaxMinGrid($("#jqGridFatigueMinMax"), "#jqGridPagerFatigueMinMax", 'GetFatigueMaxMin');
    }
}

function buildGrid(gridElement, pagerElement, operation) {
    gridElement.jqGrid({
        url: baseURL+"/LoadData/" + operation + "?jobId=" + jobId,
        mtype: "GET",
        datatype: "json",
        colModel: [
            { label: 'Job Id', name: 'JobId', key: true, width: 75 },
            { label: 'Load Case', name: 'LoadCase', width: 75 },
            { label: 'Node', name: 'Node', width: 75 },
            { label: 'Px', name: 'Px', width: 75 },
            { label: 'Py', name: 'Py', width: 75 },
            { label: 'Pz', name: 'Pz', width: 75 },
            { label: 'Mx', name: 'Mx', width: 75 },
            { label: 'My', name: 'My', width: 75 },
            { label: 'Mz', name: 'Mz', width: 75 },
            { label: 'Fx', name: 'Fx', width: 75 },
            { label: 'Fy', name: 'Fy', width: 75 },
            { label: 'Fxy', name: 'Fxy', width: 75 }
        ],
        page: 1,
        width: "100%",
        height: "100%",
        rowNum: 0,
        scrollPopUp: true,
        scrollLeftOffset: "83%",
        viewrecords: true,
        scroll: 1, // set the scroll property to 1 to enable paging with scrollbar - virtual loading of records
        emptyrecords: 'Scroll to bottom to retrieve new page', // the message will be displayed at the bottom 
        pager: pagerElement
    });
}

function buildMaxMinGrid(gridElement, pagerElement, operation) {
    gridElement.jqGrid({
        url: baseURL+"/LoadData/" + operation + '?jobId=' + jobId,
        mtype: "GET",
        datatype: "json",
        colModel: [
            { label: 'Id', name: 'Id', width: 75 },
            { label: 'JobId', name: 'JobId', width: 75 },
            { label: 'ElementId', name: 'ElementId', width: 75 },
            { label: 'MaxPx', name: 'MaxPx', width: 75 },
            { label: 'MaxPxLoadCase', name: 'MaxPxLoadCase', width: 75 },
            { label: 'MinPx', name: 'MinPx', width: 75 },
            { label: 'MinPxLoadCase', name: 'MinPxLoadCase', width: 75 },
            { label: 'MaxPy', name: 'MaxPy', width: 75 },
            { label: 'MaxPyLoadCase', name: 'MaxPyLoadCase', width: 75 },
            { label: 'MinFy', name: 'MinFy', width: 75 },
            { label: 'MinFyLoadCase', name: 'MinFyLoadCase', width: 75 },
            { label: 'MaxFxy', name: 'MaxFxy', width: 75 },
            { label: 'MaxFxyLoadCase', name: 'MaxFxyLoadCase', width: 75 },
            { label: 'MinFxy', name: 'MinFxy', width: 75 },
            { label: 'MinFxyLoadCase', name: 'MinFxyLoadCase', width: 75 },
            { label: 'MaxFx', name: 'MaxFx', width: 75 },
            { label: 'MaxFxLoadCase', name: 'MaxFxLoadCase', width: 75 },
            { label: 'MinFx', name: 'MinFx', width: 75 },
            { label: 'MinFxLoadCase', name: 'MinFxLoadCase', width: 75 },
            { label: 'MaxFy', name: 'MaxFy', width: 75 },
            { label: 'MaxFyLoadCase', name: 'MaxFyLoadCase', width: 75 },
            { label: 'MinMy', name: 'MinMy', width: 75 },
            { label: 'MinMyLoadCase', name: 'MinMyLoadCase', width: 75 },
            { label: 'MaxMz', name: 'MaxMz', width: 75 },
            { label: 'MaxMzLoadCase', name: 'MaxMzLoadCase', width: 75 },
            { label: 'MinMz', name: 'MinMz', width: 75 },
            { label: 'MinMzLoadCase', name: 'MinMzLoadCase', width: 75 },
            { label: 'MaxMx', name: 'MaxMx', width: 75 },
            { label: 'MaxMxLoadCase', name: 'MaxMxLoadCase', width: 75 },
            { label: 'MinMx', name: 'MinMx', width: 75 },
            { label: 'MinMxLoadCase', name: 'MinMxLoadCase', width: 75 },
            { label: 'MaxMy', name: 'MaxMy', width: 75 },
            { label: 'MaxMyLoadCase', name: 'MaxMyLoadCase', width: 75 },
            { label: 'MinPy', name: 'MinPy', width: 75 },
            { label: 'MinPyLoadCase', name: 'MinPyLoadCase', width: 75 },
            { label: 'MaxPz', name: 'MaxPz', width: 75 },
            { label: 'MaxPzLoadCase', name: 'MaxPzLoadCase', width: 75 },
            { label: 'MinPz', name: 'MinPz', width: 75 },
            { label: 'MinPzLoadCase', name: 'MinPzLoadCase', width: 75 }
        ],

        page: 1,
        width: "100%",
        height: "100%",
        rowNum: 0,
        scrollPopUp: true,
        scrollLeftOffset: "83%",
        viewrecords: true,
        scroll: 1, // set the scroll property to 1 to enable paging with scrollbar - virtual loading of records
        emptyrecords: 'Scroll to bottom to retrieve new page', // the message will be displayed at the bottom 
        pager: pagerElement
    });

}