﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AircraftLoadData.LoadDataRetriever;
using I2ES.Model;
using I2ES.Model.DomainModel;
using I2ES.Model.DomainModelContext;
using I2ES.Model.LoadDataRetriever;
using Ninject;

namespace I2ES.Web.Controllers {
    public class LoadDataController : Controller {
        [Inject]
        public virtual IStaticLoadDataReader StaticLoadDataReader { get; set; }

        [Inject]
        public virtual IFatigueLoadDataReader FatigueLoadDataReader { get; set; }

        [Inject]
        public virtual IStaticMaxMinReader StaticMaxMinReader { get; set; }

        [Inject]
        public virtual IFatigueMaxMinReader FatigueMaxMinReader { get; set; }
        

        public ActionResult Index(int jobId) {
            return View();
        }

        public ActionResult Submit(int jobId, int elementId, string location, int staticLoadCase, int fatigueLoadCase) {
            ILoadDataRetriever einstein = new LoadDataRetriever();
            einstein.LoadData(elementId, jobId, location, getSqlWrapper(), staticLoadCase, fatigueLoadCase);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        //
        // GET: /LoadData/Create

        public JsonResult GetStaticLoad(int jobId) {
            StaticLoadDataReader.SqlWrapper = getSqlWrapper();
            var staticLoad = StaticLoadDataReader.ReadAllElements(jobId);
            var result = getJasonFromLoadList(staticLoad);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetFatigueLoad(int jobId) {
            FatigueLoadDataReader.SqlWrapper = getSqlWrapper();
            var fatigueLoad = FatigueLoadDataReader.ReadAllElements(jobId);
            var result = getJasonFromLoadList(fatigueLoad);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStaticMaxMin(int jobId) {
            StaticMaxMinReader.SqlWrapper = getSqlWrapper();
            var maxMin = StaticMaxMinReader.ReadAllElements(jobId);
            var result = getJasonFromMaxMinList(maxMin);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetFatigueMaxMin(int jobId) {
            FatigueMaxMinReader.SqlWrapper =  getSqlWrapper();
            var maxmin = FatigueMaxMinReader.ReadAllElements(jobId);
            var result = getJasonFromMaxMinList(maxmin);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private object getJasonFromLoadList(List<LoadData> staticLoad) {
            var result =
                new {
                    records = staticLoad.Count,
                    page = 1,
                    total = 1,
                    rows =
                        staticLoad.Select(
                            t =>
                                new {t.JobId, t.LoadCase, t.Node, t.Px, t.Py, t.Pz, t.Mx, t.My, t.Mz, t.Fx, t.Fy, t.Fxy})
                            .OrderBy(t => t.LoadCase)
                };
            return result;
        }

        private object getJasonFromMaxMinList(List<LoadDataMaxMin> list) {
            var result =
                new {
                    records = list.Count,
                    page = 1,
                    total = 1,
                    rows =
                        list.Select(
                            t =>
                                new {
                                    t.Id,
                                    t.JobId,
                                    t.ElementId,
                                    t.MaxPx,
                                    t.MaxPxLoadCase,
                                    t.MinPx,
                                    t.MinPxLoadCase,
                                    t.MaxPy,
                                    t.MaxPyLoadCase,
                                    t.MinFy,
                                    t.MinFyLoadCase,
                                    t.MaxFxy,
                                    t.MaxFxyLoadCase,
                                    t.MinFxy,
                                    t.MinFxyLoadCase,
                                    t.MaxFx,
                                    t.MaxFxLoadCase,
                                    t.MinFx,
                                    t.MinFxLoadCase,
                                    t.MaxFy,
                                    t.MaxFyLoadCase,
                                    t.MinMy,
                                    t.MinMyLoadCase,
                                    t.MaxMz,
                                    t.MaxMzLoadCase,
                                    t.MinMz,
                                    t.MinMzLoadCase,
                                    t.MaxMx,
                                    t.MaxMxLoadCase,
                                    t.MinMx,
                                    t.MinMxLoadCase,
                                    t.MaxMy,
                                    t.MaxMyLoadCase,
                                    t.MinPy,
                                    t.MinPyLoadCase,
                                    t.MaxPz,
                                    t.MaxPzLoadCase,
                                    t.MinPz,
                                    t.MinPzLoadCase,
                                })
                };
            return result;
        }

        private ISqlWrapper getSqlWrapper() {
            ConnectionProvider connectionProvider = new ConnectionProvider();
            ISqlWrapper sqlWrapper = connectionProvider.CreateWrapper();
            return sqlWrapper;
        }
    }
}