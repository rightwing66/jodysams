﻿using System;
using System.Web.Mvc;
using I2ES.Web.Models;

namespace I2ES.Web.Controllers {
    public class HealthController : Controller {
        private static readonly log4net.ILog log =
        log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public ActionResult Index(string requestUrl=null){
            if (string.IsNullOrEmpty(requestUrl)){
                requestUrl = null;}
            var client = new TrafficManagerService.TrafficManagerServiceClient();
            //                var trafficManager = TrafficManagerHelper.GetReference();
            Model.ConfigurationWrapper wrapper = new Model.ConfigurationWrapper();
            
            HealthViewModel healthViewModel = new HealthViewModel();
            try {
                log.InfoFormat("Calling TrafficManager.Ping requestUrl={0} ",requestUrl);
                string result = client.Ping();
                healthViewModel.TrafficManagerServiceStatus = "Up";
//                result = client.WriteFile();
//                healthViewModel.WriteFileMessage = result;
                log.Info("Ping returned 'UP'");
            }
            catch (Exception ex) {
                healthViewModel.TrafficManagerServiceStatus = "Down";
                healthViewModel.TrafficManagerServiceMessage = "Error: " + ex.Message + "\r\n" + ex.StackTrace;
                
            }
            return View(healthViewModel);
        }

    }
}
