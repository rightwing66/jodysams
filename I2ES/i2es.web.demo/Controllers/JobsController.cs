﻿using System;
using System.Linq;
using System.Web.Mvc;
using I2ES.Model;
using I2ES.Model.DomainModel;
using I2ES.Model.Extensions;
using Ninject;

namespace I2ES.Web.Controllers {
    public class JobsController : Controller {
        private string sortIndex;

        [Inject]
        public IConnectionProvider ConnectionProvider { get; set; }

        [Inject]
        public IISAMSModel Model { get; set; }


        public ActionResult Index(int? id) {
            return View();
        }

        public ActionResult AllJobs() {
            return View();
        }

        //search=false&nd=1436288412340&rows=20&page=1&sidx=CreateTime&sord=asc

        public JsonResult AllJobTypes() {
            verifyInjectedParameters();
            Model.LoadJobTypes();
            return Json(Model.AllJobTypes, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult AllJobsJson(string sidx = "JobId", string sord = "Asc") {
            if (String.IsNullOrEmpty(sidx)) {sidx = "JobId";}
            if (String.IsNullOrEmpty(sord)) { sord = "Asc"; }
            verifyInjectedParameters();
            sortIndex = transformFieldName(sidx);
            Model.LoadJobs(sortIndex, sord);
            if (Model.AllJobs == null) {
                return null;
            }
            Func<Job, bool> limitJobsByDatePredicate =
                t => t.CreateTime != null && DateTime.Now.Date ==t.CreateTime.Value.Date ;
                

            var jobs = Model.AllJobs.Where(limitJobsByDatePredicate).ToList();
            var result = new {
                records = jobs.Count(),
                page = 1,
                total = jobs.Count()/40+1,
                rows = jobs
                    .Select(
                        t =>
                            new {
                                t.JobId,
                                JobState = t.JobState.EnumGetName<JobState>(),
                                JobType = t.JobTypeName,
                                CreateTime = t.CreateTime == null ? "" : t.CreateTime.Value.ToString("MM/dd/yy H:mm:ss"),
                                EditTime = t.EditTime == null ? "" : t.EditTime.Value.ToString("MM/dd/yy H:mm:ss"),
                                ProcessBeginTime =
                                    t.ProcessBeginTime == null
                                        ? ""
                                        : t.ProcessBeginTime.Value.ToString("MM/dd/yy H:mm:ss"),
                                ProcessEndtime =
                                    t.ProcessEndTime == null || t.ProcessEndTime.Equals(ISAMSModel.SqlZeroDateTime) ? "" : t.ProcessEndTime.Value.ToString("MM/dd/yy H:mm:ss"),
                                t.UserId,
                                t.Report
                            })
                    .ToList()
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private string transformFieldName(string sortIndexFromJqGrid) {
            if (sortIndexFromJqGrid == "JobState") return "JobStateId";
            if (sortIndexFromJqGrid == "JobType") return "JobTypeId";
            return sortIndexFromJqGrid;
        }


        private void verifyInjectedParameters() {
            if (ConnectionProvider == null) {
                throw new Exception("Must provide an IConnectionProvider");
            }
            if (Model == null) {
                throw new Exception("Must provide an IISAMSModel");
            }
        }


    }
}