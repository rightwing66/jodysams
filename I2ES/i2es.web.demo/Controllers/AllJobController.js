﻿var doneOnce = false;
$(document).ready(function () {
    LoadGrid();
//    $("#buttonRefresh").button().click(function () { reloadGrid(); });
    setupStatusTimer();
});
function setupStatusTimer() {
    if (!doneOnce) {
        setInterval(reloadGrid, 15000);
        doneOnce = true;
    }
}

function reloadGrid() {
    $("#jqGrid").setGridParam({ datatype: 'json', page: 1 }).trigger('reloadGrid');
}


function LoadGrid() {
    $("#jqGrid").jqGrid({
        url: getControllerURL('jobs','AllJobsJSON'),
        mtype: "GET",
        datatype: "json",
        colModel: [
            { label: 'Job Id', name: 'JobId', key: true, width: 75 },
            { label: 'User Id', name: 'UserId', width: 75 },
            { label: 'Create Time', name: 'CreateTime', width: 150 },
            { label: 'Process Begin', name: 'ProcessBeginTime', width: 150 },
            { label: 'Process End', name: 'ProcessEndtime', width: 150 },
            { label: 'State', name: 'JobState', width: 150 },
            { label: 'Type', name: 'JobType', width: 150 },
            { label: 'Report', name: 'Report', width: 150, formatter: 'showlink', formatoptions: { baseLinkUrl: 'http://isamsi1/I2ES/Reports/report.docx'} }
            
        ],
        page: 1,
        width: 820,
        height: "100%",
        rowNum: 20,
        scrollPopUp: true,
        scrollLeftOffset: "83%",
        viewrecords: true,
        //scroll: 1, // set the scroll property to 1 to enable paging with scrollbar - virtual loading of records
        //emptyrecords: 'Scroll to bottom to retrieve new page', // the message will be displayed at the bottom 
        pager: "#jqGridPager"



    });
    $("#jqGrid").navGrid('#jqGridPager', { edit: false, add: false, del: false, search: false })
        .navButtonAdd('#jqGridPager', {
            caption: "Edit",
            buttonicon: "ui-icon-pencil",
            onClickButton: function() {
                var grid = $("#jqGrid");
                var rowid = grid.jqGrid('getGridParam', 'selrow');
                var jobId = grid.jqGrid('getCell', rowid, 'JobId');
                if (jobId == undefined) {
                    $("#dialog").dialog({modal:true});
                }
                else { window.location = baseURL+'/parameter?jobId='+jobId; }
               
            },
            position: "last"
        });
 
};


function ajaxLoad(callback, url, data) {
    $.ajax({
        url: url,
        data: data,
        type: "GET",
        dataType: "json",
        success: callback,
        error: function (xhr, status, errorThrown) {
            alert("Ajax failed");
            console.log("Error: " + errorThrown);
            console.log("Status: " + status);
            console.dir(xhr);
        },
        complete: function (xhr, status) {
            console.log("ajax to " + url + " is complete");
        }
    });
}