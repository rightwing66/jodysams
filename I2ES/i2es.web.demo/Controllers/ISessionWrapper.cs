﻿namespace I2ES.Web.Controllers{
    public interface ISessionWrapper{
        int CurrentJobId { get; set; }
    }
}