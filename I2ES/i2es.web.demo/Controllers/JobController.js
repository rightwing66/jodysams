﻿$(function () {
    beginInitialLoad();
});

var jobTypeData = [];
var menu;
var currentMenuId = 0;

function beginInitialLoad() {
    ajaxLoad(completeInitialLoadSelectData, getControllerURL("Jobs", "AllJobTypes"));
}

function completeInitialLoadSelectData(data, textStatus, jqXHR) {
    var isFirst = true;
    var firstElement;
    var firstValue;
    var materialMenu = $("#selectMaterial").selectmenu({
        width: 300,
        change: function (event, ui) {
            var jsonitem = $.parseJSON(ui.item.value);
            $("#tdevalue").val(jsonitem["E-value"] + "E6");
            $("#tdnuvalue").val(jsonitem["Nu-value"]);
//            getTrueNumberMaterial(ui.item.label);

        }
    });
    jobTypeMenu = $("#JobType").selectmenu({
        width: 300,
        change: function (event, ui) { }
    });
    jobTypeMenu.on("selectmenuchange", function (event, ui) {
        jobTypeSelectorChanged(event,ui);
    });
    for (i = 0; i <= data.length-1; i++) {
        jobTypeData[data[i].JobTypeId] = data[i].ImagePath;
        var value = data[i].JobTypeId;
        var element = $("<option></option>").attr("value",value ).text(data[i].Description);
        jobTypeMenu.append(element);
        if (isFirst) {
            isFirst = false;
            firstElement = element;
            firstValue = value;
            $("#jobTypeImage").attr("src", baseURL + data[i].ImagePath);
        }
    }
    jobTypeMenu.val("5");
    jobTypeMenu.selectmenu("refresh");

    var structureTypeMenu = $("#selectStructureType").selectmenu({
        width:300
    });
    var modelMenu = $("#selectModel").selectmenu({
        width:300
    })
//    firstElement.attr('selected', 'selected');
    currentMenuId = firstValue;
    $("#buttonNext").button(); //.click(function () { $("#dataForm").post });
};

//{ location.href = "/i2es/Parameter/Index/" + currentMenuId }

function getTrueNumberMaterial(materialName) {
    var url = " http://dev.truenumbers.com/Numberflow/API"; //"?auth=jody.breshears@esrd.com:isams&cmd=ISAMS-matprop&ns=ESRD-ISAMS01&mat=" + materialName;
    var data= {
        auth: "jody.breshears@esrd.com:isams",
        cmd: "ISAMS-matprop",
        ns: "ESRD-ISAMS01",
        mat: materialName

    }
    ajaxLoad(completeGetTrueNumberMaterial, url, null);
}

function completeGetTrueNumberMaterial(data, textStatus, jqXHR) {
    var jsonitem = $.parseJSON(data);
    $("#tdevalue").html(jsonitem["E-value"]);
    $("#tdnuvalue").html(jsonitem["Nu-value"]);
}


function jobTypeSelectorChanged(event, ui) {
    
    currentMenuId = ui.item.value;
    $("#jobTypeImage").attr("src", baseURL+ jobTypeData[ui.item.value]);
}

function ajaxLoad(callback, url, data) {
    $.ajax({
        url: url,
        data: data,
        type: "GET",
        dataType: "json",
        success: callback,
        error: function (xhr, status, errorThrown) {
            alert("Ajax failed");
            console.log("Error: " + errorThrown);
            console.log("Status: " + status);
            console.dir(xhr);
        },
        complete: function (xhr, status) {
            console.log("ajax to " + url + " is complete");
        }
    });
}

function LoadGrid() {
    $("#jqGrid").jqGrid({
        url: '/i2esdemo/Jobs/AllJobs',
        mtype: "GET",
        datatype: "json",
        colModel: [
            { label: 'Job Id', name: 'JobId', key: true, width: 75 },
            { label: 'User Id', name: 'UserId', width: 75 },
            { label: 'Create Time', name: 'CreateTime', width: 150 },
            { label: 'Process Begin', name: 'ProcessBeginTime', width: 150 },
            { label: 'Process End', name: 'ProcessEndTime', width: 150 },
            { label: 'State', name: 'State', width: 150 },
            { label: 'Type', name: 'JobTypeName', width: 150 },
            { label: 'Stage', name: 'CompletionStage', width: 150 }
        ],
        page: 1,
        width: 780,
        height: "100%",
        rowNum: 150,
        scrollPopUp: true,
        scrollLeftOffset: "83%",
        viewrecords: true,
        scroll: 1, // set the scroll property to 1 to enable paging with scrollbar - virtual loading of records
        //emptyrecords: 'Scroll to bottom to retrieve new page', // the message will be displayed at the bottom 
        pager: "#jqGridPager"
    });

};