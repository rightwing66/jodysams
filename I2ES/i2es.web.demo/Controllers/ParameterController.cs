﻿using System;
using System.Linq;
using System.Web.Mvc;
using I2ES.Model.DomainModel;
using I2ES.Model.Extensions;
using Ninject;

namespace I2ES.Web.Controllers {
    public class ParameterController : Controller {
        private static readonly log4net.ILog Log =
        log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Inject]
        public ISessionWrapper SessionWrapper { get; set; }

        [Inject]
        public IISAMSModel JobTypeModel { get; set; }


        [Inject]
        public IConfigurationWrapper ConfigurationWrapper { get; set; }


        public ActionResult Index(int? jobTypeId, int jobId) {
            JobTypeModel.LoadJob(jobId);
            JobTypeModel.Job.EditTime = DateTime.Now;

            JobTypeModel.SaveChanges();
            return View(JobTypeModel);
        }

        public ActionResult AddParametersToNewJob(int jobTypeId, double? e, double? nu, string reiNumber, string buno, string partNumber) {
            JobTypeModel.Load(jobTypeId, "untracked");
            JobTypeModel.CreateJob("untracked");
            JobTypeModel.Job.REINumber = reiNumber;
            JobTypeModel.Job.BUNO = buno;
            JobTypeModel.Job.PartNumber = partNumber;
            insertParameterValueByName("Em", e.ToString());
            insertParameterValueByName("Nu", nu.ToString());
            JobTypeModel.SaveChanges();
            return View("Index", JobTypeModel);
        }


        [HttpPost]
        public ActionResult Insert(FormCollection collection, int jobTypeId, int? jobId = null) {
            Log.InfoFormat("Insert jobTypeId:{0}", jobTypeId);
            
            if (jobId == null) {
                JobTypeModel.Load(jobTypeId, "untracked");
                JobTypeModel.CreateJob("untracked");
            }
            else {
                JobTypeModel.LoadJob(jobId.Value);
                JobTypeModel.Job.EditTime = DateTime.Now;
            }

            //SessionWrapper.CurrentJobId = ISAMSModel.Job.JobId;
            foreach (var name in collection) {
                if (!string.IsNullOrEmpty(collection[name.ToString()])) {
                    insertParameterValue(name, collection[name.ToString()]);
                }
            }
            JobTypeModel.SaveChanges();
            try {
//                var trafficManager = TrafficManagerHelper.GetReference();
//                trafficManager.SubmitJob(JobTypeModel.Job.JobId, ConfigurationWrapper.NodeName);
            }
            catch (Exception ex) {
                Log.Info("Exception calling webservice TrafficManager");
                Log.Info(ex);
            }
            return RedirectToRoute(new { controller = "Jobs", action = "AllJobs" });

        }

        private void insertParameterValue(object parameterId, string value) {
            Parameter parameter = JobTypeModel.Job.AllParameters.FirstOrDefault(p => p.ParameterId.ToString() == parameterId.ToString());
            if (parameter != null) {
                parameter.ParameterValue.Value = value.NullableParse();
            }
        }
        private void insertParameterValueByName(object name, string value) {
            Parameter parameter = JobTypeModel.Job.AllParameters.FirstOrDefault(p => p.Name.ToString() == name.ToString());
            if (parameter != null) {
                parameter.ParameterValue.Value = value.NullableParse();
            }
        }
    }
}
