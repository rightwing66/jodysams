﻿using System.ServiceModel;
using System.ServiceModel.Channels;
using I2ES.Web.Service_References.TrafficManager;

namespace I2ES.Web {
    public static class TrafficManagerHelper {
        public static TrafficManagerServiceClient GetReference(string requestUrl=null) {
            ConfigurationWrapper wrapper = new ConfigurationWrapper();
            string url = requestUrl?? wrapper.TrafficManagerUrl;
            EndpointAddress address = new EndpointAddress(url);
            Binding binding = new BasicHttpBinding();
            TrafficManagerServiceClient trafficManager = new TrafficManagerServiceClient(binding, address);
            return trafficManager;
        }
    }
}