﻿using I2ES.Model;
using I2ES.Model.DomainModel;
using I2ES.Model.DomainModelContext;
using I2ES.Web.Controllers;
using Ninject;

namespace I2ES.Web.Ninject{
    public class InjectionBinder{
        public void Bind(IKernel kernal){
            kernal.Bind<IJobTypeReader>().To<JobTypeReader>();
            kernal.Bind<ISessionWrapper>().To<SessionWrapper>();
            kernal.Bind<IConfigurationWrapper>().To<ConfigurationWrapper>();
            kernal.Bind<IConnectionProvider>().To<ConnectionProvider>();
            kernal.Bind<ISqlWrapper>().To<SqlWrapper>();
            kernal.Bind<IJobReader>().To<JobReader>();
            kernal.Bind<IISAMSModel>().ToConstructor(x => 
                new ISAMSModel(x.Inject<IConnectionProvider>(), x.Inject<IJobReader>(), x.Inject<JobTypeReader>(), new JobWriter()));
            kernal.Bind<IJobWriter>().To<JobWriter>();
            kernal.Bind<IStaticLoadDataReader>().To<StaticLoadDataReader>();
            kernal.Bind<IFatigueLoadDataReader>().To<FatigueLoadDataReader>();
            kernal.Bind<IStaticMaxMinReader>().To<StaticMaxMinReader>();
            kernal.Bind<IFatigueMaxMinReader>().To<FatigueMaxMinReader>();
        }
    }
}