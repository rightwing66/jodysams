﻿namespace I2ES.Web.Models{
    public class HealthViewModel{
        public string TrafficManagerServiceStatus { get; set; }
        public string TrafficManagerServiceMessage { get; set; }
        public string WriteFileMessage { get; set; }
    }
}