﻿using System.Web.Mvc;
using System.Web.Routing;

namespace I2ES.Web {
    public class RouteConfig {
        public static void RegisterRoutes(RouteCollection routes) {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                name: "Parameter",
                url: "Parameter/{action}/{jobTypeId}",
                defaults: new { controller = "Parameter", action = "Index", jobTypeId = UrlParameter.Optional }
            );
 
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );



            routes.MapRoute(
                name: "Submit",
                url: "LoadData/{action}",
                defaults: new { controller = "Home", action = "Index"}
            );
        }
    }
}