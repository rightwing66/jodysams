﻿using System.Web;
using System.Web.Mvc;

namespace I2ES.SolverNode {
    public class FilterConfig {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters) {
            filters.Add(new HandleErrorAttribute());
        }
    }
}