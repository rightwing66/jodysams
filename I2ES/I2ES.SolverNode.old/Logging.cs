﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security;
using System.Web.Hosting;
using System.Xml.Linq;
using I2ES.Model;
using I2ES.Model.Extensions;

namespace I2ES.SolverNode{
    public class Logging {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void WireUpLogging() {
            var log4NetConfigPath = getConfigPathFromWebConfig();
            var logFileName = GetLogFileNameFromLog4NetConfig(log4NetConfigPath);
            testForLogDirectoryExistence(logFileName);
            configureLogForNet(log4NetConfigPath);
            
        }

        private  void configureLogForNet(string log4NetConfigPath){
            FileInfo configFileInfo = new FileInfo(log4NetConfigPath);
            log4net.Config.XmlConfigurator.ConfigureAndWatch(configFileInfo);
        }

        private string getConfigPathFromWebConfig(){
            var log4NetConfigFile = System.Configuration.ConfigurationManager.AppSettings["Log4NetConfigFileName"];
            string appPath = HostingEnvironment.MapPath("~");
            Debug.Assert(appPath != null, "appPath != null");
            var log4NetConfigPath = Path.Combine(appPath, log4NetConfigFile);
            return log4NetConfigPath;
        }

        private  void testForLogDirectoryExistence(string logFileName){
            string logDirectory = Path.GetDirectoryName(logFileName);
            DirectoryInfo logDirectoryInfo = new DirectoryInfo(logDirectory);
            if (!logDirectoryInfo.Exists){
                throw new Exception("Log Directory is Missing");
            }
            FileInfo logFileInfo = new FileInfo(logFileName+".test");
//            try{
//                File.Delete(logFileName+".query");
//                logFileInfo.Create();
//            }
//            catch (SecurityException sEx){
//                throw new Exception("Worker Process does not have correct permissions on Log Directory",sEx);
//            }
        }

        private string GetLogFileNameFromLog4NetConfig(string log4NetConfigPath){
            StreamReader reader = new StreamReader(log4NetConfigPath);
            XDocument document = XDocument.Load(reader);
            var elementList = document.Descendants("appender");
            var logFileName = elementList.Where(e => e != null && e.Attribute("name").NullableValue() == "RollingFileAppender")
                .Elements("file").FirstOrDefault()
                .Attribute("value").NullableValue();
            return logFileName;
        }
    }
}