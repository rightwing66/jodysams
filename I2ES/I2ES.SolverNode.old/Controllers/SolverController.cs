﻿using System;
using System.Web.Mvc;
using I2ES.Model;

namespace I2ES.SolverNode.Controllers {
    public class SolverController : Controller {
        private static readonly log4net.ILog Log =
                        log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        readonly ConfigurationWrapper wrapper = new ConfigurationWrapper();

        public JsonResult Submit(int id) {
            Log.Info("Submitting " + id);
            ConnectionProvider provider = new ConnectionProvider();
            SolverLauncher.SolverLauncher launcher = new SolverLauncher.SolverLauncher(provider.CreateWrapper(), wrapper);
            launcher.LaunchStressCheckSolver(id, notifyComplete);
            return Json("Job Submitted", JsonRequestBehavior.AllowGet);
        }


        private void notifyComplete(int jobId, string outputPath) {
            try {
                string trafficManagerUrl = wrapper.TrafficManagerUrl;
                var trafficClient = TrafficManagerHelper.GetReference();
                Log.Info("Notify Complete.  Calling TrafficManager at " + trafficManagerUrl);
                trafficClient.NotifySolverComplete(jobId, outputPath);
            }
            catch (Exception ex) {
                Log.Info(ex);
            }
        }
    }
}
