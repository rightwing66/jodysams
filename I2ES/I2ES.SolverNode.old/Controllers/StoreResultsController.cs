﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using I2ES.Model;
using I2ES.Model.Xml;

namespace I2ES.SolverNode.Controllers {
    public class StoreResultsController : Controller {
        private static readonly log4net.ILog log =
                        log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        readonly ConfigurationWrapper wrapper = new ConfigurationWrapper();

        public JsonResult StartStoreResults(int jobId, string solverOutputFolderPath) {
            log.InfoFormat("Starting StoreResults JobId:{0}, solverFolderOutputPath:{1}",jobId, solverOutputFolderPath );
            ConnectionProvider provider = new ConnectionProvider();
            
            
            StoreResults storeResults = new StoreResults(provider.CreateWrapper());
            storeResults.Start(jobId,solverOutputFolderPath, NotifyComplete);



            return Json("StoreResults Started", JsonRequestBehavior.AllowGet);
        }

        private void NotifyComplete(int jobId) {
            try {
                string trafficManagerUrl = wrapper.TrafficManagerUrl;
                var trafficClient = TrafficManagerHelper.GetReference();
                log.Info("Notify StoreResults Complete.  Calling TrafficManager at " + trafficManagerUrl);
                trafficClient.NotifyStoreResultsComplete(jobId);
            }
            catch (Exception ex) {
                log.Info(ex);
            }
        }

    }
}
